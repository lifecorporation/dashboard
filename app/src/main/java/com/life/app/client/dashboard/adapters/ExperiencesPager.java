package com.life.app.client.dashboard.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.life.app.client.dashboard.fragments.menu.ExperiencesPage1;
import com.life.app.client.dashboard.fragments.menu.ExperiencesPage2;

/*
 * Created by chkalog on 30/9/2016.
 */
public class ExperiencesPager  extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    public ExperiencesPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        switch (arg0) {
            case 0:
                return new ExperiencesPage1();
            case 1:
                return new ExperiencesPage2();
            default:
                return new ExperiencesPage1();
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
