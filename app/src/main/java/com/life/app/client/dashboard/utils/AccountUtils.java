package com.life.app.client.dashboard.utils;

/*
 * Created by chara on 08-Nov-16.
 */

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.life.app.client.dashboard.account.ParseComServerAuthenticate;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.interfaces.ServerAuthenticate;


public class AccountUtils {

    static final ServerAuthenticate sServerAuthenticate = new ParseComServerAuthenticate();

    /**
     * The tag used for log
     */
    private static final String LOG_TAG = AccountUtils.class.getSimpleName();

    /**
     * Check if an account with the specific user name exists or not
     * @param context  The context used
     * @param username The user name of the account
     * @return If the account exists, return the account
     *         Otherwise return null
     */
    public static Account getUserAccount(Context context, String username) {
        AccountManager accountManager = AccountManager.get(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        Account[] accounts = accountManager.getAccountsByType(Consts.ACCOUNT_TYPE);
        for (Account account : accounts) {
            if (account.name.equalsIgnoreCase(username)) {
                return account;
            }
        }

        return null;
    }

    /**
     * Obtains the password stored in the account manager.
     * Password is encrypted, so it must be decrypted to get its real value
     * @param context  The context used
     * @param username The user name that the password is associated
     * @return The password stored in the account manager
     *         "" if the account is not set
     */
    public static String getPasswordyByUserName(Context context, String username) {
        AccountManager accountManager = AccountManager.get(context);
        String encryptedPassword;
        String decryptedPassword = "";

        Account account = getUserAccount(context, username);
        if (account != null) {
            encryptedPassword = accountManager.getPassword(account);
            try {
                decryptedPassword = SecurityUtils.decrypt(encryptedPassword);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getLocalizedMessage(), e);
            }
        }

        return decryptedPassword;
    }

    /**
     * Set the encrypted password in the user account. If the account doesn't exist
     * before, it creates a new one.
     * @param password The password to be stored in the account manager
     * @param username The user name associated with the password
     * @return true if the password has been set
     *         false otherwise
     */
    public static boolean setPasswordByUserName(Context context, String username, String password) {
        AccountManager accountManager = AccountManager.get(context);

        // Encrypt the password
        try {
            String encryptedPassword = SecurityUtils.encryptToHex(password);
            Account account = getUserAccount(context, username);
            if (account != null) {
                // Check if the old password is the same as the new one
                String oldPassword = accountManager.getPassword(account);
                if (!oldPassword.equalsIgnoreCase(encryptedPassword)) {
                    accountManager.setPassword(account, encryptedPassword);
                }
            }
            // If the account doesn't exist before, create it.
            else {
                Account newUserAccount = new Account(username, Consts.ACCOUNT_TYPE);
                accountManager.addAccountExplicitly(newUserAccount, encryptedPassword, null);
            }
            return true;
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getLocalizedMessage(), e);
            return false;
        }
    }

    static Account[] getAccountByType(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        return accountManager.getAccountsByType(Consts.ACCOUNT_TYPE);
    }

    static boolean isEmpty(String str){
        return (str != null && !str.trim().isEmpty());
    }
}
