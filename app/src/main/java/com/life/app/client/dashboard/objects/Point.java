package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 13-Mar-17.
 */

public class Point {

    private int x;
    private int y;

    public Point (int x, int y){
        this.setX(x);
        this.setY(y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }
}
