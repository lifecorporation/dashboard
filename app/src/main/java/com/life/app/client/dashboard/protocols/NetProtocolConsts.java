package com.life.app.client.dashboard.protocols;

/**
 * Created by Vittorio on 24/03/15.
 */
public  class NetProtocolConsts {
    public static final byte FLAG_ALIGN = -2; //0x7e;

    // Control byte values
    public static final byte CTRL_COMMAND=0x01;
    public static final byte CTRL_POS_RESPONSE=0x02;
    public static final byte CTRL_ASYNC=0x03;
    public static final byte CTRL_NEG_RESPONSE=0x04;
    public static final byte CTRL_STREAMING=0x05;

    // Command codes
    public static final byte CC_HELLO=0x01;
    public static final byte CC_KEEP_ALIVE=0x02;
    public static final byte CC_GET_ASSETS=0x03;
    public static final byte CC_START_STREAMING=0x04;
    public static final byte CC_STOP_STREAMING=0x05;
    public static final byte CC_TOUCHPAD_EVENT=0x06;

    // Frame array index
    public static final byte FLAG_INDEX=0;
    public static final byte ADDR_INDEX=1;
    public static final byte CTRL_INDEX=2;
    public static final byte LEN_INDEX=3;
    public static final byte CC_INDEX=4;

    // Address
    public static final byte SMS_ADDRESS  = 0;


}
