package com.life.app.client.dashboard.protocols;

/**
 * Created by Vittorio on 29/09/15.
 */
public class BTProtocolNak extends BTProtocolFrame {

    public BTProtocolNak(byte CommandCode, byte Reason) {
        frame.put(BTProtocolFrame.STX);
        frame.put(BTProtocolFrame.CTRL_NACK);
        frame.put(CommandCode);
        frame.putShort((short) 1); // LEN
        frame.put(Reason);
        frame.putShort((short) calculateCRC16CCITT());
        frame.put(BTProtocolFrame.ETX);
    }
}
