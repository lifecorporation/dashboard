package com.life.app.client.dashboard.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.services.bind.messages.Request;

/*
 * Created by user on 22/7/2016.
 */
public class SettingsArrayAdapter extends ArrayAdapter<String> {

    private Context context;
    private int layoutResourceId;
    private String data[] = null;

    public SettingsArrayAdapter(Context context, int layoutResourceId, String[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.length;
    }

    @Override
    public String getItem(int position) {
        return this.data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if(row == null){
            LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.tvTitle  = (TextView)row.findViewById(R.id.text1);

            row.setTag(holder);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(((MainActivity)context).getStatus().isDefined()) {
                        if(position==2) //LANGUAGE
                            showLanguageDialog();
                    }else{
                        showAsToast(context.getResources().getString(R.string.error_disconnected));
                    }
                }
            });
        }
        else{
            holder = (ViewHolder)row.getTag();
        }

        holder.tvTitle.setText(this.data[position]);

        return row;
    }

    private Toast mToast = null;

    private void showAsToast (String message){
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this.context, message, Toast.LENGTH_SHORT);
        mToast.show();
    }

    private class ViewHolder{
        TextView tvTitle;
    }

    private String lang_code = null;
    private String[] lang_codes;

    private void showLanguageDialog(){
        lang_code = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.context);
        final View dialogView = View.inflate(this.context, R.layout.dialog_spinner_language, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(context.getResources().getString(R.string.title_language));
        dialogBuilder.setCancelable(false);

        lang_codes = context.getResources().getStringArray(R.array.locale_codes);

        Spinner sLang = (Spinner) dialogView.findViewById(R.id.sLang);

        final TextView tvError = (TextView) dialogView.findViewById(R.id.tvError);
        sLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvError.setVisibility(View.GONE);
                lang_code = lang_codes[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                lang_code = null;
            }
        });

        dialogBuilder.setPositiveButton(this.context.getResources().getString(R.string.dialog_button_select), null);
        dialogBuilder.setNegativeButton(this.context.getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                lang_code = null;
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lang_code == null){
                    tvError.setVisibility(View.VISIBLE);
                }else{
                    //Log.i("Language", lang_code);
                    Request.request(context, 0x06, lang_code.getBytes());
                    lang_code = null;
                    b.dismiss();
                }
            }
        });
    }
}
