package com.life.app.client.dashboard.fragments.radarChart.AerobicPower;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import java.util.ArrayList;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chara on 16-Dec-16.
 */

public class LeftFragment extends Fragment {

    static BarChart mChart1, mChart2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_aerobic_power_results_left, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChart1 = (BarChart) view.findViewById(R.id.barChart);
        mChart2 = (BarChart) view.findViewById(R.id.stackedbarchart);

        BarChartSetup.setBarChart(mChart1, false, null, 0f, 100f);
        BarChartSetup.setBarChartWithLimitLine( mChart2, false, "", 0f, 45f, 24f);
        addDataToChart(mChart1);
        addDataToChart3(mChart2);
    }

    private void addDataToChart(BarChart chart) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(6);

        ArrayList<BarEntry> entries = new ArrayList<>();

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_" +
                AerobicPowerResults.position);
        if(synthesis!=null) {
            int totalDeviations = Math.round(synthesis.get(2));
            for (int i = 0; i < totalDeviations; i++) {
                ArrayList<Float> arrayList = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_" + AerobicPowerResults.position
                        + "_TRY_" + (i + 1));
                if(arrayList!=null)
                    entries.add(new BarEntry(i + 1, arrayList.get(1)));
            }
        }

        BarDataSet set1 = new BarDataSet(entries, "");
        set1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        set1.setHighlightEnabled(true);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.6f);
        data.setValueTextColor(Color.WHITE);
        data.setDrawValues(true);

        chart.setData(data);
        chart.invalidate();
    }

    private void addDataToChart3(BarChart chart) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(2);

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_" +
                AerobicPowerResults.position);

        if(synthesis!=null) {
            int totalDeviations = Math.round(synthesis.get(2));
            if(totalDeviations > 0) {
                int totalPercentage = 0;
                for (int i = 0; i < totalDeviations; i++) {
                    ArrayList<Float> totalentries = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_1_TRY_" + (i + 1));
                    if(totalentries!=null)
                        totalPercentage += (totalentries.get(1) + 24);
                }

                ArrayList<BarEntry> entries = new ArrayList<>();
                entries.add(new BarEntry(1, totalPercentage / totalDeviations));

                BarDataSet set1 = new BarDataSet(entries, "");
                set1.setColor(ColorTemplate.rgb("#001A23"));
                set1.setHighlightEnabled(true);

                ArrayList<IBarDataSet> dataSets = new ArrayList<>();
                dataSets.add(set1);

                BarData data = new BarData(dataSets);
                data.setValueTextSize(10f);
                data.setValueTextColor(GRAPHS_COLOR_WHITE);
                data.setBarWidth(1f);

                chart.setData(data);
                chart.invalidate();
            }
        }
    }
}
