package com.life.app.client.dashboard.fragments.radarChart.Strength;

/*
 * Created by chara on 28-Nov-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.StackedBarModel;

import java.util.ArrayList;

import static com.life.app.client.dashboard.constants.Consts.*;

public class RightFragment extends Fragment {

    StackedBarChart mChart1 = null;
    StackedBarChart mChart2 = null;
    BarChart        mChart3 = null;
    BarChart        mChart4 = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_strength_results_right, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mChart1 = (StackedBarChart) view.findViewById(R.id.stackedbarchart1);
        mChart2 = (StackedBarChart) view.findViewById(R.id.stackedbarchart2);
        mChart3 = (BarChart) view.findViewById(R.id.stackedbarchart3);
        mChart4 = (BarChart) view.findViewById(R.id.stackedbarchart4);

        showHistoryChart();

        setClickListenermChart(mChart4);
    }

    private void showHistoryChart() {
        if(mChart1!=null && mChart2!=null)
            displayHistoryValues();
    }

    private void displayHistoryValues() {
        int totalpushups = Math.round(SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.STRENGTH_KEY + "_" + StrengthResults.position).get(0));

        int armPercentage;
        int backPercentage;
        int totalarmPercentage = 0;
        int totalbackPercentage = 0;

        int totalArm = 0;
        int totalBack = 0;


        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i=0;i<totalpushups;i++){

            ArrayList<Float> pushups = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.STRENGTH_KEY + "_" + StrengthResults.position + "_TRY_" + (i + 1));

            if(pushups!=null){

                armPercentage = (Math.round(pushups.get(1))+25) * 2;
                backPercentage = (Math.round(pushups.get(2)));//*100)) / 30;

                StackedBarModel s2 = new StackedBarModel("");
                s2.addBar(new BarModel(armPercentage, 0xFF2196F3));
                s2.addBar(new BarModel(100-armPercentage, 0xFF3F51B5));
                mChart2.addBar(s2);

                entries.add(new BarEntry((i+1), backPercentage));

                /*StackedBarModel s4 = new StackedBarModel("");
                s4.addBar(new BarModel(backPercentage, 0xFF009688));
                s4.addBar(new BarModel(100-backPercentage, 0xFF001A23));
                mChart4.addBar(s4);*/

                totalArm += Math.round(pushups.get(1));
                totalBack += Math.round(pushups.get(2));
            }
        }

        if(totalpushups != 0){
            totalarmPercentage = ((totalArm/totalpushups) + 25) * 2;
            totalbackPercentage = totalBack/totalpushups;
        }

        StackedBarModel s1 = new StackedBarModel("");
        BarModel bm1 = new BarModel(totalarmPercentage, 0xFF2196F3);
        BarModel bm2 = new BarModel(100-totalarmPercentage, 0xFF3F51B5);
        bm1.setShowValue(false);
        bm2.setShowValue(false);
        s1.addBar(bm1);  // vertical
        s1.addBar(bm2);  // medio
        mChart1.addBar(s1);

        BarChartSetup.setBarChartWithLimitLine(mChart3, false, "", 0f, 30f, 15f);
        BarChartSetup.setBarChartWithLimitLine(mChart4, false, "", 0f, 30f, 15f);
        addDataToChart(mChart3, totalbackPercentage);
        addDataToChart(mChart4, entries);

        mChart1.startAnimation();
        mChart2.startAnimation();
    }

    private static void addDataToChart(BarChart chart, int totalbackPercentage) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(2);

        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(1, totalbackPercentage));

        BarDataSet set1 = new BarDataSet(entries, "");
        set1.setColor(ColorTemplate.rgb("#001A23"));
        set1.setHighlightEnabled(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setDrawValues(true);
        data.setValueTextSize(VALUE_TEXT_SIZE);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(1f);

        chart.setData(data);
        chart.invalidate();
    }

    private static void addDataToChart(BarChart chart, ArrayList<BarEntry> entries) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(entries.size() + 1);

        BarDataSet set1 = new BarDataSet(entries, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setDrawValues(true);
        data.setValueTextSize(VALUE_TEXT_SIZE);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.8f);

        chart.setData(data);
        chart.invalidate();
    }

    private void setClickListenermChart(final BarChart mChart){
        mChart.setHighlightPerTapEnabled(true);
        // Make sure it's clickable.
        mChart.setClickable(true);
        mChart.setTouchEnabled(true);
        mChart.setEnabled(true);
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                showAsToast(e);
            }

            @Override
            public void onNothingSelected() {
                Log.i("Nothing selected", "Nothing selected.");
            }
        });
    }

    Toast mToast = null;

    public void showAsToast (Entry e){
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(getActivity(),
                e.toString() + "",
                Toast.LENGTH_SHORT);
        mToast.show();
    }
}
