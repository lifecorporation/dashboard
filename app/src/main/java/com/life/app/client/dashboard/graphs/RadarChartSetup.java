package com.life.app.client.dashboard.graphs;

import android.app.Activity;
import android.graphics.Color;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.custom.RadarMarkerView;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;


import java.util.ArrayList;

/*
 * Created by peppe on 20/07/2016.
 */
public class RadarChartSetup {

    private static ArrayList<String> mActivities = null;
    private static RadarDataSet set1 = null;

    public static void setup(Activity activity, RadarChart radarchart){

        mActivities = new ArrayList<>();
        mActivities.add(activity.getResources().getString(R.string.anaerobic_capacity));
        mActivities.add(activity.getResources().getString(R.string.aerobic_power));
        //mActivities.add(MainActivity.mainActivityContext.getResources().getString(R.string.joint_mobility));
        mActivities.add(activity.getResources().getString(R.string.reaction_time));
        mActivities.add(activity.getResources().getString(R.string.agility));
        mActivities.add(activity.getResources().getString(R.string.balance));
        mActivities.add(activity.getString(R.string.explosiveness));
        mActivities.add(activity.getResources().getString(R.string.rhythm));
        mActivities.add(activity.getResources().getString(R.string.speed));
        mActivities.add(activity.getResources().getString(R.string.strength));

        radarchart.setMinimumHeight(250);
        radarchart.setMinimumWidth(350);
        //radarchart.setBackgroundColor(Color.rgb(55, 71, 79));
        //radarchart.setBackgroundColor(Color.rgb(0, 26, 35));
        radarchart.setWebLineWidth(1f);
        radarchart.setWebColor(Color.LTGRAY);
        radarchart.setWebLineWidthInner(1f);
        radarchart.setWebColorInner(Color.LTGRAY);
        radarchart.setRotationEnabled(false);
        radarchart.setWebAlpha(100);
        radarchart.setNoDataText("");
        //radarchart.setScaleX(1.15f);
        //radarchart.setScaleY(1.15f);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MarkerView mv = new RadarMarkerView(activity, R.layout.radar_markerview);
        // set the marker to the chart
        radarchart.setMarkerView(mv);
        radarchart.animateXY(
                1400, 1400,
                Easing.EasingOption.EaseInOutQuad,
                Easing.EasingOption.EaseInOutQuad);

        XAxis xAxis = radarchart.getXAxis();
        //xAxis.setTypeface(mTfLight);
        xAxis.setTextSize(13f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new AxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                //Log.i("RadarChart","value " + (int) value % mActivities.length);
                //return mActivities[(int) value % mActivities.length];
                return mActivities.get((int) value % mActivities.size());
            }
        });

        xAxis.setTextColor(Color.WHITE);
        xAxis.setAxisMaxValue(100);

        YAxis yAxis = radarchart.getYAxis();
        //yAxis.setTypeface(mTfLight);
        yAxis.setLabelCount(3, false);
        yAxis.setTextSize(10f);
        yAxis.setAxisMinValue(0f);
        yAxis.setAxisMaxValue(100f);
        yAxis.setDrawLabels(false);
        /*
        Legend l = radarchart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
        //l.setTypeface(mTfLight);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);
        */
    }

    public static void addEntries(Activity activity, RadarChart radarchart){

        ArrayList<RadarEntry> entries1 = new ArrayList<>();

        //SharedPreferences prefs = MainActivity.getInstance().getSharedPreferences(RadarChartConst.MY_PREFERENCES, MainActivity.getInstance().MODE_PRIVATE);

        int anaerobicCapacityData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY).get(0));
        int aerobicPowerData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.AEROBIC_POWER_KEY).get(0));
        int reactionTimeData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.REACTION_TIME_KEY).get(0));
        int agilityData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.AGILITY_KEY).get(0));
        int balanceData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.BALANCE_KEY).get(0));
        int explosivnessData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.EXPLOSIVENESS_KEY).get(0));
        int RhythmData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.RHYTHM_KEY).get(0));
        int speedData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.SPEED_KEY).get(0));
        int strenghtData = Math.round(SharedPrefDataHandler.getBest(activity, RadarChartConst.STRENGTH_KEY).get(1));

        entries1.add(new RadarEntry(anaerobicCapacityData));
        entries1.add(new RadarEntry(aerobicPowerData));
        entries1.add(new RadarEntry(reactionTimeData));
        entries1.add(new RadarEntry(agilityData));
        entries1.add(new RadarEntry(balanceData));
        entries1.add(new RadarEntry(explosivnessData));
        entries1.add(new RadarEntry(RhythmData));
        entries1.add(new RadarEntry(speedData));
        entries1.add(new RadarEntry(strenghtData));

        set1 = new RadarDataSet(entries1, "Text");
        set1.setColor(Color.parseColor("#BDBDBD"));
        set1.setFillColor(Color.parseColor("#E0E0E0"));
        //set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);
        set1.calcMinMax();
        set1.setDrawValues(false);
        set1.setDrawFilled(true);
        set1.setDrawHighlightCircleEnabled(false);
        set1.setDrawHighlightIndicators(false);

        //remove legend
        Legend l = radarchart.getLegend();
        l.setEnabled(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);

        RadarData data = new RadarData(sets);
        //data.setValueTypeface(mTfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        radarchart.setData(data);
        radarchart.invalidate();
    }

    // return the array position for the hashRequester hash code
    public static int getSelectedValuePosition(int hashRequester){
        int position = -1;
        // search for the index position of the hashRequester element
        for (int i=0; i< set1.getEntryCount(); i++){
            if (set1.getEntryForIndex(i).hashCode() == hashRequester){
                position = i;
            }
        }
        return position;
    }

    public static int getSelectedValuePosition(String activity){
        // search for the index position of the hashRequester element
        if (mActivities!=null && mActivities.contains(activity))
            return mActivities.indexOf(activity);
        return -1;
    }

}
