package com.life.app.client.dashboard.fragments.radarChart.Rhythm;

/*
 * Created by chara on 06-Dec-16.
 */

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.life.app.client.dashboard.constants.Consts.*;

public class RightFragment extends Fragment {

    BarChart mChart1 = null, mChart2 = null;
    ArrayList<Float> try1 = null, try2 = null, try3 = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rhythm_results_right, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChart1 = (BarChart) view.findViewById(R.id.barChart1);
        mChart2 = (BarChart) view.findViewById(R.id.barChart2);

        try1 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_1");
        try2 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_2");
        try3 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_3");

        ArrayList<Float> sNumbers = new ArrayList<>();
        sNumbers.add(try1.get(3));
        sNumbers.add(try2.get(3));
        sNumbers.add(try3.get(3));

        ArrayList<Float> errors = new ArrayList<>();
        errors.add(try1.get(1));
        errors.add(try2.get(1));
        errors.add(try3.get(1));

        initiateChart(mChart1);
        initiateChart(mChart2);

        addDataToChart(mChart1, sNumbers);
        addDataToChart(mChart2, errors);
    }

    private void initiateChart(BarChart chart){
        chart.clear();
        chart.animateY(2000);

        Paint paint = chart.getRenderer().getPaintRender();

        LinearGradient linGrad;
        if(chart == mChart1) {
            linGrad = new LinearGradient(0, 0, 0, 450,
                    GRAPHS_COLOR_BLUE_LIGHT,
                    Color.parseColor("#BDBDBD"),
                    Shader.TileMode.REPEAT);
        }else{
            linGrad = new LinearGradient(0, 0, 0, 450,
                    GRAPHS_COLOR_RED_DARK,
                    Color.parseColor("#BDBDBD"),
                    Shader.TileMode.REPEAT);
        }
        paint.setShader(linGrad);

        Description descr = new Description();
        descr.setText("");
        chart.setDescription(descr);

        chart.getRenderer().getPaintRender().setShader(linGrad);
        chart.setHighlightFullBarEnabled(false);
        chart.setFocusable(false);
        chart.setDrawBarShadow(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setDrawValueAboveBar(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(5);
        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(true);
        chart.setDrawGridBackground(false);
        chart.setScaleEnabled(true);
        chart.getLegend().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
        xAxis.setDrawLabels(false);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);
        xAxis.setCenterAxisLabels(true);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(0f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        if(chart == mChart2)
            leftAxis.setAxisMaximum(0.22f);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawZeroLine(true); // draw a zero line
        leftAxis.setZeroLineWidth(1.2f);
        leftAxis.setSpaceBottom(4f);
        leftAxis.setSpaceTop(4f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, false);
        rightAxis.setSpaceTop(0f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        if(chart == mChart2)
            rightAxis.setAxisMaximum(0.22f);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);
        rightAxis.setDrawZeroLine(true); // draw a zero line
        rightAxis.setZeroLineWidth(1.2f);
        rightAxis.setSpaceBottom(4f);
        rightAxis.setSpaceTop(4f);

        if(chart == mChart1) {
            leftAxis.setAxisMaximum(100f);
            rightAxis.setAxisMaximum(100f);
        }

        Legend l = chart.getLegend();
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART_INSIDE);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(0f);
        l.setTextSize(11f);
        l.setXEntrySpace(1f);
        l.setEnabled(false);
        l.setTextColor(GRAPHS_COLOR_WHITE);
    }

    private void addDataToChart(BarChart chart, ArrayList<Float> results) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(results.size() + 1);

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = results.size()-1; i >=0 ; i--) {

            if(chart == mChart1){
                yVals1.add(new BarEntry(i+1, results.get(i)));
            }else{
                yVals1.add(new BarEntry(i+1, results.get(i)/1000));
            }
        }

        Collections.sort(yVals1, new EntryComparator());

        BarDataSet set1;
        //set1.setBarShadowColor();

        set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(false);
        set1.setValueTextColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(12f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.6f);

        chart.setData(data);
    }

    public class EntryComparator implements Comparator<Entry> {
        @Override
        public int compare(Entry entry1, Entry entry2) {
            return (int)entry1.getX() - (int)entry2.getX();
        }
    }
}
