package com.life.app.client.dashboard.protocols;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/*
 * Created by Vittorio on 24/03/15.
 */
public class BTProtocolFrame {

    public static class BTProtocolException extends Exception {

    }

    public static class BTProtocolInvalidLengthException extends BTProtocolException {

    }

    public static class BTProtocolInvalidCRCException extends BTProtocolException {

    }

    public static byte STX                             = 0x02;
    public static byte ETX                             = 0x03;

    // Control byte
    public static final byte CTRL_REQUEST              = 0x00;
    public static final byte CTRL_ACK                  = 0x01;
    public static final byte CTRL_NACK                 = 0x02;
    public static final byte CTRL_EVENT                = 0x03;


    // Command Code
    public static final byte CC_DATA                   = 0x00;
    public static final byte CC_STRING_DATA            = (byte)0xA0;

    public static final byte CC_SET_DATETIME           = 0x03;
    public static final byte CC_GET_DATETIME           = 0x04;

    public static final byte CC_REGISTER               = 0x01;
    public static final byte CC_GETSTATUS              = 0x02;

    public static final byte CC_GETALGORITHMS          = 0x10;
    public static final byte CC_STARTALGORITHM         = 0x11;
    public static final byte CC_STOPALGORITHM          = 0x12;


    public static final byte CC_UPLOAD_EXPERIENCES     = 0x20;
    public static final byte CC_START_EXPERIENCE       = 0x21;
    public static final byte CC_STOP_EXPERIENCE        = 0x22;
    public static final byte CC_SET_DEFAULT_EXPERIENCE = 0x23;
    public static final byte CC_GET_DEFAULT_EXPERIENCE = 0x24;
    public static final byte CC_GET_EXPERIENCE_STATUS  = 0x25;

    public static final byte CC_SCAN_WIFI              = 0x30;
    public static final byte CC_CONNECT_WIFI           = 0x31;
    public static final byte CC_DISCONNECT_WIFI        = 0x32;
    public static final byte CC_GET_WIFISTATUS         = 0x33;
    public static final byte CC_SWITCH_WIFI            = 0x37;

    public static final byte CC_ACTIVATE_AP            = 0x34;

    public static final byte CC_GET_ASSET              = 0x43;

    public static final byte CC_LIST_SYNC              = 0x60;
    public static final byte CC_START_SYNC             = 0x61;
    public static final byte CC_STOP_SYNC              = 0x62;
    public static final byte CC_SYNC_STATUS            = 0x63;
    public static final byte CC_GET_SYNC_CONFIGURE     = 0x64;

    public static final byte CC_START_CALIBRATION      = 0x40;
    public static final byte CC_STOP_CALIBRATION       = 0x41;
    public static final byte CC_CALIBRATION_PROGRESS   = 0x42;

    public static final byte CC_SN_HARD_RESET          = 0x46;

    public static final byte CC_GET_LOCALE             = 0x50;
    public static final byte CC_SET_LOCALE             = 0x51;

    public static final byte CC_PLACE_CALL             = 0x70;
    public static final byte CC_HANG_UP                = 0x71;
    public static final byte CC_NEW_CALL               = 0x73;
    public static final byte CC_GET_CALL_STATUS        = 0x74;

    public static final byte CC_SMS_ERROR              = (byte)0x90;
    public static final byte CC_SN_ERROR               = (byte)0x91;

    // Error reason
    public static final byte REASON_INVALID_LENGHT     = 0x01;
    public static final byte REASON_INVALID_CRC        = 0x02;
    public static final byte REASON_INVALID_PARAMETER  = 0x03;
    public static final byte REASON_INVALID_IMEI       = 0x04;
    public static final byte REASON_UNKNOWN_COMMAND    = 0x05;
    public static final byte REASON_UNAUTHORIZED       = 0x06;
    public static final byte REASON_NO_SN              = 0x07;
    public static final byte REASON_STREAMING_ALREADY_RUNNING = 0x08;
    public static final byte REASON_NOTFOUND          = 0x09;
    public static final byte REASON_GENERIC = -1;

    public static final int  STD_PACKET_LEN = 8;

    private static final int STX_INDEX = 0;
    private static final int CTRL_INDEX = 1;
    private static final int CMD_INDEX = 2;
    private static final int LEN_INDEX = 3;
    public static final int DATA_INDEX = 5;

    protected ByteBuffer frame;
    private short payloadLen;
    private int frameLenght;

    //Contructor for mini temporary buffer
    public BTProtocolFrame() {
        frame = ByteBuffer.allocate(STD_PACKET_LEN);
        frame.order(ByteOrder.BIG_ENDIAN);
        payloadLen = -1;
    }

    public BTProtocolFrame(int size){
        frame = ByteBuffer.allocate(size + 8);
        frame.order(ByteOrder.BIG_ENDIAN);
        payloadLen = -1;
    }

    public int getControl() {
        return frame.get(CTRL_INDEX);
    }

    public void setControl(byte ctrl) {
        frame.put(CTRL_INDEX, ctrl);
    }

    public byte getCommandCode() {
        return frame.get(CMD_INDEX);
    }

    // N.B. position give the next buffer free index. All length calculation
    //      must consider this (+1)
    public boolean addData(byte value) throws BTProtocolException {

        // Wait for STX
        if (frame.position() == 0 && value != STX) return false;

        // Queue byte
        //Log.d("ByteBuffer","size " + frame.array().length);

        //Log.d("ByteBuffer","position " + frame.position());
        frame.put(value);

        // Retrieve payload len
        if (frame.position() == NetProtocolConsts.LEN_INDEX + 2) {
            payloadLen = frame.getShort(NetProtocolConsts.LEN_INDEX);
        }

        // Check CRC
        if (payloadLen != -1 && frame.position() == payloadLen + 3) {
//            if(!checkLRC())
//                throw  new BTProtocolInvalidCRCException();
        }

        // Check for frame termination
        if (payloadLen != -1 && frame.position() == payloadLen + 8) {
            //Log.d("BytePayload","frame.position() " + frame.position());

            //Log.d("BytePayload","payloadLen + 8 " + (payloadLen + 8));
            //Log.d("BytePayload","last Byte " + value);
            if (value == ETX) {
                frameLenght = frame.position();
                frame.rewind();
                return true;
            }
            throw new BTProtocolInvalidLengthException();
        }
        //Log.i("BTProtocolLenException", "payloadLen " + "     frame.position() " + frame.position());

        return false;
    }


    /******************************************************************************
     * Compilation:  javac CRC16CCITT.java
     * Execution:    java CRC16CCITT s
     * Dependencies:
     * <p/>
     * Reads in a sequence of bytes and prints out its 16 bit
     * Cylcic Redundancy Check (CRC-CCIIT 0xFFFF).
     * <p/>
     * 1 + x + x^5 + x^12 + x^16 is irreducible polynomial.
     * <p/>
     * % java CRC16-CCITT 123456789
     * CRC16-CCITT = 29b1
     ******************************************************************************/
    protected int calculateCRC16CCITT() {
        int crc = 0xFFFF;          // initial value
        int polynomial = 0x1021;   // 0001 0000 0010 0001  (0, 5, 12)

        for (int i = 1; i < frame.position(); i++) {
            for (int j = 0; j < 8; j++) {
                boolean bit = ((frame.get(i) >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
            }
        }
        crc &= 0xffff;
        return crc;
    }

    private short getLRC() {
        return frame.getShort(payloadLen + 5);
    }

    private boolean checkLRC() {
        return getLRC() == calculateCRC16CCITT();
    }

    public int getFrameSize() {
        return frameLenght;
//                frame.position();
    }

    public byte[] bytes() {
        return frame.array();
    }

    public ByteBuffer getFrame() {
        return frame;
    }

    public void moveToData() {
        frame.position(DATA_INDEX);
    }

    public short getDataSize() {
        return frame.getShort(LEN_INDEX) ;
    }

    public int getDataSizeInt() {
        return frame.getShort(LEN_INDEX) & 0xFFFF;
    }

    public byte[] getData() {
        if (getCommandCode() == CC_DATA) {
            byte[] data = new byte[4 * getDataCount()];
            frame.position(DATA_INDEX + 16 + 1);
            frame.get(data);

            return data;
        } else return null;
    }

    public byte[] getUUID() {
        if (getCommandCode() == CC_DATA) {
            byte[] data = new byte[16];
            moveToData();
            frame.get(data);
            return data;
        } else
            return null;
    }

    public Integer getDataCount() {
        if (getCommandCode() == CC_DATA) {
//            byte[] data=new byte[1];
            frame.position(DATA_INDEX + 16);
//            frame.get(data);
            return (int) frame.get();
        } else
            return null;
    }

}


