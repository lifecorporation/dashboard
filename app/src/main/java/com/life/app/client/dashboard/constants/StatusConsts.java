package com.life.app.client.dashboard.constants;

/**
 * Created by Giuseppe Piccione on 09/01/2017.
 */

public class StatusConsts {

    public static final int NOTIFICATION_BATTERY_LEVEL            = 16845;
    public static final int NOTIFICATION_WEARABLE_STATUS          = 89634;
    public static final int NOTIFICATION_SIM_STATUS               = 54718;
    public static final int NOTIFICATION_NETWORK_TYPE             = 10593;
    public static final int NOTIFICATION_CLOUD_STATUS             = 41258;
    public static final int NOTIFICATION_GSM_LEVEL_STATUS         = 59382;
    public static final int NOTIFICATION_SMARTSCREEN_DISCONNECTED = 34025;
    public static final int NOTIFICATION_WIFI_STATUS              = 74309;
    public static final int NOTIFICATION_STORAGE_STATUS           = 64951;


}
