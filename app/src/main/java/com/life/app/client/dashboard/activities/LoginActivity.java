package com.life.app.client.dashboard.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.Life;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.managers.PreferencesManager;
import com.life.app.client.dashboard.models.RegistrationInfo;
import com.life.app.client.dashboard.objects.User;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.app.client.dashboard.utils.AccountUtils;
import com.life.app.client.dashboard.utils.NetworkUtilities;
import com.life.app.client.dashboard.utils.SecurityUtils;


import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chara on 08-Nov-16.
 */

public class LoginActivity extends AccountAuthenticatorActivity {

    public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";

    /**
     * The tag utilized for the log.
     */
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    /**
     * The context of the program.
     */
    private Context context;

    /**
     * The user name input by the user.
     */
    private TextInputLayout usernameET;

    /**
     * The password input by the user.
     */
    private TextInputLayout passwordET;

    /**
     * The button to add a new account.
     */
    private Button addNewAccountButton;

    /**
     * The button to login in the account.
     */
    private Button loginAccountButton;

    /**
     * Inner LIFE TextView to get the credentials of each user
     */
    private AutoCompleteTextView tvUser;

    /**
     * The response passed by the service.
     * It is used to give the user name and the password to the account manager
     */
    private AccountAuthenticatorResponse response;

    /**
     * The account manager used to request and add account.
     */
    private AccountManager mAccountManager;

    /** Was the original caller asking for an entirely new account? */
    protected boolean mRequestNewAccount = false;

    boolean success = false;

    boolean accountCreationSuccess = false;

    protected String password = null;

    public static Context mContext;
    boolean firstBluetoothSearch = true;
    boolean bluetoothInitialStatus;

    String username, imei, mac;
    int uid;

    private int uid_array[] = {
            381316,
            46001,
            380416,
            378504,
            378078,
            380014,
            380148,
            380282,
            380550,
            380685,
            380819,
            380953,
            108055,
            378234,
            381087,
            426030,
            96027,
            378234,
            426030,
            380685,
            480022,
            380148,
            394984,
            480205,
            480389,
            520176,
            378234,
            380148,
            380819,
            480570,
            512011,
            512192,
            512011,
            000000
    };

    private String imei_array[] = {
            "100914405968661",
            "100765477502329",
            "018276115040913",
            "507064781204098",
            "105365494828685",
            "105447373111936",
            "307799052041883",
            "919023697620807",
            "530690931632399",
            "860627739102027",
            "867971395421323",
            "861779490347892",
            "332373471659755",
            "518225597332210",
            "445534457030055",
            "350211679459445",
            "337695169579159",
            "103360801257696",
            "861266820568546",
            "516771610271135",
            "522373038124756",
            "541471280390732",
            "301258582183604",
            "330339797733972",
            "549329265146614",
            "521147678118976",
            "103360801257696",
            "497635568580529",
            "012743195022083",
            "358102217791377",
            "356322523373239",
            "541603067245461",
            "998887197935756",
            "994257254167046"
    };

    private String mac_array[] = {
            "FA:98:46:03:21:60",
            "E6:8E:46:03:21:BF",
            "DE:B2:46:03:21:B7",
            "7E:C7:46:03:21:A4",
            "E6:FF:46:03:21:0F",
            "7E:0F:46:03:21:07",
            "76:85:46:03:21:16",
            "E6:19:46:03:21:C1",
            "6E:05:46:03:21:10",
            "F6:F2:46:03:21:E8",
            "DE:28:46:03:21:9E",
            "FA:E4:46:03:21:2F",
            "66:16:46:03:21:AE",
            "3E:28:46:03:21:CF",
            "7E:27:46:03:21:3F",
            "7E:C4:46:03:21:65",
            "3A:C1:46:03:21:EC",
            "00:00:46:03:21:01",
            "EA:43:46:03:21:0F",
            "01:00:46:03:21:01",
            "02:00:46:03:21:01",
            "08:00:46:03:21:01",
            "03:00:46:03:21:01",
            "04:00:46:03:21:01",
            "05:00:46:03:21:01",
            "12:AE:45:FF:00:01",
            "00:00:46:03:21:01",
            "3E:45:46:0F:21:CF",
            "11:00:46:03:21:01",
            "03:00:46:03:21:01",
            "12:00:46:03:21:01",
            "13:00:46:03:21:01",
            "AE:00:46:03:21:01",
            "10:00:46:03:21:01"
    };

    RelativeLayout rlRegister = null, rlLogin = null;


    int persona = -1;


    ArrayList<String> users = new ArrayList<String>(){{
        add("Giuseppe Piccione (1)");
        add("Pietro Edantippe (2)");
        add("Charalampos Kalogiannakis (3)");
        add("Domenico Colella (4)");
        add("Carlo Franceschini (5)");
        add("Camilla Minella (6)");
        add("Alessandra Leonardi (7)");
        add("LIFEDEMO (8)");
        add("Marco Mauri (9)");
        add("Marco Giovanelli (10)");
        add("Rudy Rigoni(11)");
        add("Fabrizio Pallai (12)");
        add("Gianluigi Longinotti Buitoni (13)");
        add("Dario Ossola (14)");
        add("Yingji Wu (15)");
        add("Charalampos Kalogiannakis(16)");
        add("Valentina De Pascalis (17)");
        add("Dario Ossola (?)");
        add("- (20)");
        add("Marco Giovanelli (22)");
        add("Francesca Zuppante (23)");
        add("Alessandra Leonardi (26)");
        add("Lorenzo Marcheschi (29)");
        add("Shyam Gopal (34)");
        add("Walter Bonfiglio (38)");
        add("Jenny Mancini (35)");
        add("Dario Ossola (41)");
        add("Alessandra Leonardi (43)");
        add("Rudy Rigoni(46)");
        add("Celeste Longinotti Buitoni (49)");
        add("Gianluigi Frare (24)");
        add("Mattia Frigerio");
        add("Gianluigi Frare (47)");
        add("Ricardo Stefanoni (36)");
    }};

    @Override
    public void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);
        Fabric.with(this, new Crashlytics());

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // inizializzazione variabili
        // connessione con le view
        final Intent intent = getIntent();
        mAuthtoken = SecurityUtils.generateToken();
        mAuthtokenType = intent.getStringExtra(PARAM_AUTHTOKEN_TYPE);
        mUsername = intent.getStringExtra(PARAM_USERNAME);
        mRequestNewAccount = mUsername == null;
        mConfirmCredentials =
                intent.getBooleanExtra(PARAM_CONFIRMCREDENTIALS, false);

        setContentView(R.layout.activity_login);

        context = this;

        mAccountManager = AccountManager.get(this);

        usernameET  = (TextInputLayout) findViewById(R.id.username);
        passwordET  = (TextInputLayout) findViewById(R.id.password);

        if(savedInstancestate != null)
        {
            usernameET.getEditText().setText(savedInstancestate.getString("email"));
            usernameET.getEditText().setSelection(savedInstancestate.getString("email").length());
            passwordET.getEditText().setText(savedInstancestate.getString("password"));
            passwordET.getEditText().setSelection(savedInstancestate.getString("password").length());

            isToast = true;
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_dropdown_item_1line,
                users
        );

        tvUser      = (AutoCompleteTextView) findViewById(R.id.tvUser);
        tvUser.setAdapter(adapter);
        tvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int pos,long id) {
                persona = users.indexOf(tvUser.getText().toString());

                if(Log.isLoggable("PERSONA", Log.INFO))
                    Log.i("PERSONA", persona + "");

                //al momento li cablo a mano per i test
                /*
                 * 0  : Giuseppe
                 * 1  : Pietro
                 * 2  : Babis
                 * 3  : Domenico
                 * 4  : Carlo
                 * 5  : Camilla
                 * 6  : Alessandra
                 * 7  : LIFEDEMO
                 * 8  : Marco M.
                 * 9  : Marco G.
                 * 10 : Rudy
                 * 11 : Fax
                 * 12 : Gianluigi
                 * 13 : Dario O.
                 * 14 : ingji
                 * 15 : Dario C. / Babis
                 * 16 : Valentina
                 * 17 : Dario O. 41
                 * 18 : Dario C. 20
                 * 19 : Marco G. 22
                 * 20 : Francesca 23
                 * 21 : Alessandra 26
                 * 22 : Lorenzo 29
                 * 23 : Shyam 34
                 * 24 : Walter 38
                 * 25 : Jenny 35
                 * 26 : Dario O. 41
                 * 27 : Alessandra 43
                 * 28 : Rudy 46
                 * 29 : Celeste 49
                 * 30 : Gian 24
                 * 31 : Mattia 30
                 * 32 : Gian 47
                 * 33 : Ricardo 36
                 */

                uid = uid_array[persona];
                imei = imei_array[persona];
                mac = mac_array[persona];

            }
        });
        tvUser.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s == null || s.toString().trim().isEmpty()) {
                    persona = -1;
                    uid = -1;
                    imei = null;
                    mac = null;
                }
            }
        });

        rlRegister  = (RelativeLayout) findViewById(R.id.rlRegister);
        rlLogin = (RelativeLayout) findViewById(R.id.rlLogin);

        if (mUsername != null &&
                usernameET != null && usernameET.getEditText() != null) {
            usernameET.getEditText().setText(mUsername);
            usernameET.getEditText().setSelection(mUsername.length());
        }

        addNewAccountButton = (Button) findViewById(R.id.createNewAccountButton);
        loginAccountButton = (Button) findViewById(R.id.loginAccountButton);
        addNewAccountButton.setOnClickListener(onClickListener);
        loginAccountButton.setOnClickListener(onClickListener);

        if (mConfirmCredentials)
            rlRegister.setVisibility(View.GONE);
        else
            rlLogin.setVisibility(View.GONE);


        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            /*
             * Pass the new account back to the account manager
             */
            response = extras.getParcelable(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
        }

        mContext = this;

        //controlli preliminari
        // se non sono connesso a internet
        if (!checkInternet()) {
            // TODO decidere come gestire il caso incui io non sia connesso a internet
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("email", usernameET.getEditText().getText().toString());
        outState.putString("password", passwordET.getEditText().getText().toString());
    }

    private boolean checkInternet() {
        /*
        Runtime runtime = Runtime.getRuntime();
        try {
            // provo a contattare i dns di google ma forse sarebbe meglio fare anche un controllo
            // per pingare i nostri server e veificare che anche li ci sia la connessione
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
        */
        return true;
    }

    private boolean getDatiDaDB() {
        //TODO funzione di scarico dei dati di connessione dal DB
        // anche qui dentro devo controllare se la connessione è presente perché potrebbe pure essersi staccata
        if (checkInternet()) {
            //faccio la mia get
        } else {
            // devo vedere come gestire questo caso di errore
        }
        return true;
    }

    protected String mUsername = null;
    protected String mPassword = null;

    /**
     * The listener for the button pressed.
     */
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mUsername = usernameET.getEditText().getText().toString();
            mPassword = passwordET.getEditText().getText().toString();

            // Check the contents
            // Check the user name
            if (mUsername == null || mUsername.equalsIgnoreCase("")) {
                Toast.makeText(context, getResources().getString(R.string.warning_username_empty), Toast.LENGTH_LONG).show();
                return;
            }

            // Check the password
            if (mPassword == null || mPassword.equalsIgnoreCase("")) {
                Toast.makeText(context, getResources().getString(R.string.warning_password_empty), Toast.LENGTH_LONG).show();
                return;
            }

            if(persona == -1){
                Toast.makeText(context, getResources().getString(R.string.warning_user_empty), Toast.LENGTH_LONG).show();
                return;
            }

            /*
             * Check if the account already exists in case of new account.
             */

            if (!mConfirmCredentials && AccountUtils.getUserAccount(context, mUsername) != null) {
                Toast.makeText(context, R.string.warning_account_already_exists, Toast.LENGTH_SHORT).show();
                return;
            }

            /*
             * Check the user name and the password against the server.
             */
            if (!mConfirmCredentials && checkType() == 0) {
                Account newUserAccount = new Account(mUsername, getResources().getString(R.string.account_type));

                try {
                    String encryptedPassword = SecurityUtils.encryptToHex(mPassword);
                    boolean accountCreated = mAccountManager.addAccountExplicitly(newUserAccount, encryptedPassword, null);
                    if (accountCreated) {
                        //if (response != null) {
                        Bundle result = new Bundle();
                        result.putString(AccountManager.KEY_ACCOUNT_NAME, mUsername);
                        result.putString(AccountManager.KEY_ACCOUNT_TYPE, getString(R.string.account_type));
                        result.putString(AccountManager.KEY_AUTHTOKEN, SecurityUtils.generateToken());
                        //response.onResult(result);
                        Toast.makeText(context, R.string.add_new_account_done, Toast.LENGTH_LONG).show();

                        updateToken(newUserAccount);

                        RegistrationInfo info = new RegistrationInfo();
                        info.username = mUsername;
                        info.imei = imei;
                        info.userId = uid;
                        info.mac = mac;
                        info.pass = mPassword;
                        PreferencesManager.saveRegistrationInfo(info, LoginActivity.this);

                        accountCreationSuccess = true;


                        ((Life) getApplicationContext()).setUser(new User(persona, info.username, imei, mac));

                        // a questo punto posso anche fare partire il servizio di connessione BT con il telefono
                        Intent i = new Intent(LoginActivity.this, BTConnectionManagerService.class);
                        startService(i);

                        finish();
                        //} else {
                        //    Toast.makeText(context, R.string.error_creating_account, Toast.LENGTH_LONG).show();
                        //}
                    } else {
                        Toast.makeText(context, R.string.error_creating_account, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Log.e(LOG_TAG, e.getLocalizedMessage(), e);
                    Toast.makeText(context, R.string.error_creating_account, Toast.LENGTH_LONG).show();
                }
            } else if (!mConfirmCredentials && checkType() == 1) {
                Toast.makeText(context, R.string.error_creating_same_type_account, Toast.LENGTH_LONG).show();
            } else if (checkType() == -1) {
                Toast.makeText(context, "Security issues", Toast.LENGTH_LONG).show();
            } else {

                accountCreationSuccess = true;

                RegistrationInfo info = new RegistrationInfo();
                info.username = mUsername;
                info.imei = imei;
                info.userId = uid;
                info.mac = mac;
                info.pass = mPassword;
                PreferencesManager.saveRegistrationInfo(info, LoginActivity.this);
                //Log.d(TAG, "Account created");

                ((Life) getApplicationContext()).setUser(new User(persona, info.username, imei, mac));

                // a questo punto posso anche fare partire il servizio di connessione BT con il telefono
                Intent i = new Intent(LoginActivity.this, BTConnectionManagerService.class);
                startService(i);

                if (NetworkUtilities.authenticate(mUsername, mPassword,
                        null/* Handler */, LoginActivity.this)) {
                    finishLogin();
                }
            }
        }
    };

    private int checkType() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    1);


            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return TODO;
        }
        Account[] accounts = mAccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if(account.type.equals(Consts.ACCOUNT_TYPE))
                return 1;
        }

        return 0;
    }

    private void updateToken(Account mAccount){
        mAuthtoken = SecurityUtils.generateToken();
        mAccountManager.setAuthToken(mAccount, Consts.AUTHTOKEN_TYPE, mAuthtoken);
    }

    private String mAuthtoken;
    private String mAuthtokenType;

    /**
     *
     * Called when response is received from the server for authentication
     * request. See onAuthenticationResult(). Sets the
     * AccountAuthenticatorResult which is sent back to the caller. Also sets
     * the authToken in AccountManager for this account.
     *
     */
    protected void finishLogin() {

        final Account account = new Account(mUsername, Consts.ACCOUNT_TYPE);
        if (mRequestNewAccount) {
            mAccountManager.addAccountExplicitly(account, mPassword, null);
            // Set contacts sync for this account.
            ContentResolver.setSyncAutomatically(account,
                    ContactsContract.AUTHORITY, true);
        } else {

            if(mAccountManager.getPassword(account).equals(mPassword)) {
                mAccountManager.setPassword(account, mPassword);
            }else {
                if(passwordET!=null && passwordET.getEditText()!=null)
                    passwordET.getEditText().setError(getResources().getString(R.string.error_incorrect_password));
                return;
            }
        }
        final Intent intent = new Intent();

        updateToken(account);

        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Consts.ACCOUNT_TYPE);
        if (mAuthtokenType != null
                && mAuthtokenType.equals(Consts.AUTHTOKEN_TYPE)) {
            intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        if(intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)!=null)
            finish(); // call this to finish the current activity
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    /**
     * Called when the authentication process completes (see attemptLogin()).
     */
    private static final String TAG = "AuthenticatorActivity";


    /**
     * If set we are just checking that the user knows their credentials; this
     * doesn't cause the user's password to be changed on the device.
     */
    private Boolean mConfirmCredentials = false;

    public void onAuthenticationResult(boolean result) {
        if (result) {
            if (!mConfirmCredentials) {
                finishLogin();
            } else {
                finishConfirmCredentials(true);
            }
        } else {
            if (mRequestNewAccount) {
                usernameET.setError(getResources().getString(R.string.error_invalid_password_or_password));
                // "Please enter a valid username/password.
            } else {
                // "Please enter a valid password." (Used when the
                // account is already in the database but the password
                // doesn't work.)
                passwordET.setError(getResources().getString(R.string.error_incorrect_password));
            }
        }
    }

    /**
     * Called when response is received from the server for confirm credentials
     * request. See onAuthenticationResult(). Sets the
     * AccountAuthenticatorResult which is sent back to the caller.
     *
     */
    protected void finishConfirmCredentials(boolean result) {
        final Account account = new Account(mUsername, Consts.ACCOUNT_TYPE);
        mAccountManager.setPassword(account, mPassword);

        updateToken(account);

        final Intent intent = new Intent();

        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Consts.ACCOUNT_TYPE);
        if (mAuthtokenType != null
                && mAuthtokenType.equals(Consts.AUTHTOKEN_TYPE)) {
            intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    boolean isToast = false;
    @Override
    public void onBackPressed(){
        if(!accountCreationSuccess) {
            isToast = true;
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.warning_login_credentials_title))
                    .setMessage(getResources().getString(R.string.warning_login_credentials_message))
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            LoginActivity.super.onBackPressed();
                            System.exit(0);
                        }
                    }).create().show();
        }else{
            LoginActivity.super.onBackPressed();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if(!accountCreationSuccess) {
            if (!isToast) {
                Toast.makeText(this, getResources().getString(R.string.warning_login_credentials_message1), Toast.LENGTH_LONG).show();
            }
            else {
                isToast = false;
            }
        }
    }

    @Override
    public void onStop(){
        super.onStop();
    }
}
