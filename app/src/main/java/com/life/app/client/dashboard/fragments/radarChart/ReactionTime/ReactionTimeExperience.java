package com.life.app.client.dashboard.fragments.radarChart.ReactionTime;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.ReactionTimeActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.ArrayList;
import java.util.UUID;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 02/08/2016.
 */
public class ReactionTimeExperience extends Fragment implements CountDownAnimation.CountDownListener{

    private static TextView tvAttempt, tvTime;

    private static BarChart mChart;

    private static ArrayList<Float> [] tries = null; //number of tries
    private static ArrayList<Float> synthesis = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reaction_time_experience, container, false);
    }

    private static Activity mInstance = null;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        counter = 0;
        mInstance = getActivity();

        ReactionTimeActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.hint_reaction_time_title));

        tvAttempt = (TextView) view.findViewById(R.id.tvAttempt);
        tvTime    = (TextView) view.findViewById(R.id.tvTime);
        mChart    = (BarChart) view.findViewById(R.id.barChart);

        RelativeLayout back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        yVals1 = new ArrayList<>();
        historyValues = new ArrayList<>();

        synthesis = new ArrayList<>();
        tries    = (ArrayList<Float>[])new ArrayList[3];
        tries[0] = new ArrayList<>();
        tries[1] = new ArrayList<>();
        tries[2] = new ArrayList<>();

        BarChartSetup.setBarChart(mChart, false, null, 0f, 2200f);
        addDataToChart();

        startCountDown(view);
    }

    private void goBack(){
        ReactionTimeActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), ReactionTimeActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private static void gotoResults(final Activity activity){
        new Handler().postDelayed(new Runnable() {
            public void run() {

                saveToSharedPref(activity);

                if (!mInstance.isFinishing()) {

                    Bundle b = new Bundle();
                    b.putInt("position", 1);
                    android.app.Fragment f = new ReactionTimeResults();
                    f.setArguments(b);
                    mInstance.getFragmentManager().beginTransaction().
                            replace(R.id.reaction_time_placeholder, f, "ReactionTimeResults").
                            commit();
                }
            }
        }, 1000);
    }

    private static void saveToSharedPref(Activity activity){
        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.REACTION_TIME_KEY, synthesis);
        SharedPrefDataHandler.addBest(activity, RadarChartConst.REACTION_TIME_KEY, synthesis);
        for(int i=0;i<3;i++){
            SharedPrefDataHandler.addNewTry(activity, RadarChartConst.REACTION_TIME_KEY + "_1_TRY_" + (i+1), tries[i]);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private static void updateDataToChart(int input) {
        historyValues.add(input);
        yVals1.add(new BarEntry(yVals1.size(), input));
        data.notifyDataChanged();        // NOTIFIES THE DATA OBJECT
        ReactionTimeActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {
                mChart.notifyDataSetChanged();  // let the chart know it's data changed
                mChart.invalidate();            // refresh
            }
        });
    }

    private static int counter = 0;

    public static void updateViews(final String [] data){
        /**
         *  data[0] :   UUID
         *  data[1] :   timestamp
         *  data[2] :   counter
         *  data[3] :   time
         */
        ReactionTimeActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(counter<3) {
                    tries[counter] = new ArrayList<>();
                    tries[counter].add((Float.valueOf(data[3])));

                    String placeholder1 = String.valueOf(counter +1);
                    String placeholder2 = String.valueOf((Double.valueOf(data[3]).intValue()) / 1000);

                    tvAttempt.setText(placeholder1);
                    tvTime.setText(placeholder2);
                    updateDataToChart((Double.valueOf(data[3]).intValue()));
                    counter++;
                }
            }
        });
    }

    private static void showDataDialog(final Activity mActivity){
        View dialogView = View.inflate(mActivity, R.layout.reaction_time_output, null);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mActivity);
        dialogBuilder.setTitle("Reaction Time output");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setView(dialogView);

        TextView tvFirst  = (TextView) dialogView.findViewById(R.id.tvFirst);
        TextView tvSecond = (TextView) dialogView.findViewById(R.id.tvSecond);
        TextView tvThird  = (TextView) dialogView.findViewById(R.id.tvThird);
        TextView tvFinal  = (TextView) dialogView.findViewById(R.id.tvFinal);

        String placeholder1 = tries[0].get(0)+" ms";
        String placeholder2 = tries[1].get(0)+" ms";
        String placeholder3 = tries[2].get(0)+" ms";
        String placeholder4 = synthesis.get(0)+" in " +
                              synthesis.get(1) + " ms";

        tvFirst.setText(placeholder1);
        tvSecond.setText(placeholder2);
        tvThird.setText(placeholder3);
        tvFinal.setText(placeholder4);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, mActivity.getResources().getString(R.string.ok) , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (alertDialog!=null)  alertDialog.dismiss();
                ReactionTimeActivity.showTutorial();
                //ExperiencesUtils.stopExperience(mActivity);
            }
        });
        alertDialog.show();
    }

    public static void finalize(final Activity activity, String [] data){
        synthesis.add(((Float.valueOf(data[2]))));
        synthesis.add(((Float.valueOf(data[3]))));
        ReactionTimeActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //showDataDialog(activity);
                gotoResults(activity);
            }
        });
    }

    private void addDataToChart() {
        float start = 0f;

        mChart.getXAxis().setAxisMinimum(start);
        mChart.getXAxis().setAxisMaximum(4);

        yVals1.add(new BarEntry(0, 0));

        BarDataSet set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(false);
        set1.setValueTextColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        data = new BarData(dataSets);
        data.setValueTextSize(12f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.7f);

        mChart.setData(data);
        ReactionTimeActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                mChart.invalidate();
            }
        });
    }

    private void startExperience(){
        //Request.startExperience(getActivity(), UUID.fromString(Uuid.EX_REACTION_TIME));
        Utils.startExperience(UUID.fromString(Uuid.EX_REACTION_TIME));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_REACTION_TIME;
        tvAttempt.setText("1");
        tvTime.setText("-");
    }

    private static void resetValues(){

    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        startExperience();
    }

    private static BarData data;
    private static ArrayList<BarEntry> yVals1 = null;
    private static ArrayList<Integer> historyValues = null;
}