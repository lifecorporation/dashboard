package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 10-Mar-17.
 */

import java.util.ArrayList;

public class User {

    private String firstname;
    private String lastname;
    private String email;
    private String deviceid;
    private String imei;
    private String mac;

    public User(){

    }

    public User(int pos, String email, String imei, String mac){

        //Log.i("PERSONA : FirstName", this.firstNamesLife.get(pos));
        //Log.i("PERSONA : LastName", this.lastNamesLife.get(pos));
        //Log.i("PERSONA : DeviceID", this.deviceidLife.get(pos));

        this.setFirstname(this.firstNamesLife.get(pos));
        this.setLastname(this.lastNamesLife.get(pos));
        this.setEmail(email);
        this.setDeviceid(this.deviceidLife.get(pos));
        this.setImei(imei);
        this.setMac(mac);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }


    protected ArrayList<String> firstNamesLife = new ArrayList<String>(){{
        add("Giuseppe");
        add("Pietro");
        add("Charalampos");
        add("Domenico");
        add("Carlo");
        add("Camilla");
        add("Alessandra");
        add("LIFEDEMO");
        add("Marco Mauri");
        add("Marco Giovanelli");
        add("Rudy");
        add("Fabrizio");
        add("Gianluigi");
        add("Dario");
        add("Yingji");
        add("Charalampos");
        add("Valentina");
        add("Dario");
        add("Dario");
        add("Marco");
        add("Francesca");
        add("Alessandra");
        add("Lorenzo");
        add("Shyam");
        add("Walter");
        add("Jenny");
        add("Dario");
        add("Alessandra");
        add("Rudy");
        add("Celeste");
        add("Gianluigi");
        add("Mattia");
        add("Gianluigi");
        add("Ricardo");
    }};

    protected ArrayList<String> lastNamesLife = new ArrayList<String>(){{
        add("Piccione");
        add("Edantippe");
        add("Kalogiannakis");
        add("Colella");
        add("Franceschini");
        add("Minella");
        add("Leonardi");
        add("LIFE");
        add("Mauri");
        add("Giovanelli");
        add("Rigoni");
        add("Pallai");
        add("Longinotti Buitoni");
        add("Ossola");
        add("Wu");
        add("Kalogiannakis");
        add("De Pascalis");
        add("Ossola");
        add("Chiappetta");
        add("Giovanelli");
        add("Zuppante");
        add("Leonardi");
        add("Marcheschi");
        add("Gopal");
        add("Bonfiglio");
        add("Mancini");
        add("Ossola");
        add("Leonardi");
        add("Rigoni");
        add("Longinotti Buitoni");
        add("Frare");
        add("Frigerio");
        add("Frare");
        add("Stefanoni");
    }};

    protected ArrayList<String> deviceidLife = new ArrayList<String>(){{
        add("1");
        add("2");
        add("3");
        add("4");
        add("5");
        add("6");
        add("7");
        add("8");
        add("9");
        add("10");
        add("11");
        add("12");
        add("13");
        add("14");
        add("15");
        add("16");
        add("17");
        add("?");
        add("20");
        add("22");
        add("23");
        add("26");
        add("29");
        add("34");
        add("38");
        add("35");
        add("41");
        add("43");
        add("46");
        add("49");
        add("24");
        add("");
        add("47");
        add("36");
    }};
}
