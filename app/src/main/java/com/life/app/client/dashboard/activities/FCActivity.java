package com.life.app.client.dashboard.activities;

/*
 * Created by chara on 14-Nov-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.R;

import io.fabric.sdk.android.Fabric;

public class FCActivity extends MyFragmentActivity {

    RelativeLayout back;

    PagerAdapter pagerAdapter;
    ViewPager mViewPager;

    private static String[] hr_target_zone = null, hr_duration = null, hr_bpm_range = null,
            hr_benefits = null, hr_feels = null, hr_recommended = null;

    private static int[] buttons = {R.id.bMaximum, R.id.bHard, R.id.bModerate, R.id.bLight, R.id.bVeryLight};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_fc);

        ((TextView) findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.hint_physical_shape));

        hr_target_zone  = getResources().getStringArray(R.array.hr_target_zone);
        hr_duration     = getResources().getStringArray(R.array.hr_duration);
        hr_bpm_range    = getResources().getStringArray(R.array.hr_bpm_range);
        hr_benefits     = getResources().getStringArray(R.array.hr_benefits);
        hr_feels        = getResources().getStringArray(R.array.hr_feels);
        hr_recommended  = getResources().getStringArray(R.array.hr_recommended);

        back   = (RelativeLayout) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        pagerAdapter =
                new PagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setCurrentItem(0);
        updateSelectedButton(buttons[0]);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateSelectedButton(buttons[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    // Since this is an object collection, use a FragmentStatePagerAdapter,
    // and NOT a FragmentPagerAdapter.
    public class PagerAdapter extends FragmentStatePagerAdapter {
        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(0);
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return FirstFragment.newInstance(1);
                case 2: // Fragment # 1 - This will show SecondFragment
                    return FirstFragment.newInstance(2);
                case 3: // Fragment # 1 - This will show SecondFragment
                    return FirstFragment.newInstance(3);
                case 4: // Fragment # 1 - This will show SecondFragment
                    return FirstFragment.newInstance(4);
                default:
                    return FirstFragment.newInstance(0);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + position;
        }
    }

    public static class FirstFragment extends Fragment {

        private int page;

        // newInstance constructor for creating fragment with arguments
        public static FirstFragment newInstance(int page) {
            FirstFragment fragmentFirst = new FirstFragment();
            Bundle args = new Bundle();
            args.putInt("someInt", page);
            fragmentFirst.setArguments(args);
            return fragmentFirst;
        }

        // Store instance variables based on arguments passed
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            page = getArguments().getInt("someInt", 0);
        }

        // Inflate the view for the fragment based on layout XML
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_fc_details, container, false);
            ((TextView) rootView.findViewById(R.id.tvTarget)).setText(
                    hr_target_zone[page]);
            ((TextView) rootView.findViewById(R.id.tvDuration)).setText(
                    hr_duration[page]);
            ((TextView) rootView.findViewById(R.id.tvBpmRange)).setText(
                    hr_bpm_range[page]);
            ((TextView) rootView.findViewById(R.id.tvBenefits)).setText(
                    hr_benefits[page]);
            ((TextView) rootView.findViewById(R.id.tvFeels)).setText(
                    hr_feels[page]);
            ((TextView) rootView.findViewById(R.id.tvRecommended)).setText(
                    hr_recommended[page]);
            return rootView;
        }
    }

    public void onLevelSelected(View view){

        switch (view.getId()){
            case R.id.bMaximum:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.bHard:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.bModerate:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.bLight:
                mViewPager.setCurrentItem(3);
                break;
            case R.id.bVeryLight:
                mViewPager.setCurrentItem(4);
                break;
            default:
                mViewPager.setCurrentItem(0);
                break;
        }

        updateSelectedButton(view.getId());
    }

    private void updateSelectedButton(int which){
        (findViewById(R.id.bMaximum)).setBackgroundResource(R.drawable.fc_button_very_hard);
        (findViewById(R.id.bHard)).setBackgroundResource(R.drawable.fc_button_hard);
        (findViewById(R.id.bModerate)).setBackgroundResource(R.drawable.fc_button_moderate);
        (findViewById(R.id.bLight)).setBackgroundResource(R.drawable.fc_button_light);
        (findViewById(R.id.bVeryLight)).setBackgroundResource(R.drawable.fc_button_very_light);

        switch (which){
            case R.id.bMaximum:
                (findViewById(which)).setBackgroundResource(R.drawable.fc_button_very_hard_selected);
                break;
            case R.id.bHard:
                (findViewById(which)).setBackgroundResource(R.drawable.fc_button_hard_selected);
                break;
            case R.id.bModerate:
                (findViewById(which)).setBackgroundResource(R.drawable.fc_button_moderate_selected);
                break;
            case R.id.bLight:
                (findViewById(which)).setBackgroundResource(R.drawable.fc_button_light_selected);
                break;
            case R.id.bVeryLight:
                (findViewById(which)).setBackgroundResource(R.drawable.fc_button_very_light_selected);
                break;
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
