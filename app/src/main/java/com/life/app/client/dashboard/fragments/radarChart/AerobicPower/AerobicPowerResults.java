package com.life.app.client.dashboard.fragments.radarChart.AerobicPower;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AerobicPowerActivity;
import com.life.app.client.dashboard.adapters.AerobicPowerResultsPager;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/*
 * Created by peppe on 17/10/2016.
 */
public class AerobicPowerResults extends Fragment {

    View mLeak = null;
    RelativeLayout back;

    TextView tvScore, tvVO2;

    ViewPager pager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        AerobicPowerActivity.hideTutorial();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_aerobic_power_results, container, false);
    }

    public static int position = 1;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_aerobic_power));

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        tvScore = (TextView) view.findViewById(R.id.tvScore);
        tvVO2   = (TextView) view.findViewById(R.id.tvVO2);
        back    = (RelativeLayout) view.findViewById(R.id.back);

        tvScore.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvVO2.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_" + position);

        String placeholderScore = "0";
        String placeholderVO2 = "0";

        if(synthesis!=null){
            if(synthesis.size()>=1) placeholderScore = Float.toString(synthesis.get(0));
            if(synthesis.size()>=2) placeholderVO2 = Float.toString(synthesis.get(1));
        }

        tvScore.setText(placeholderScore);
        tvVO2.setText(placeholderVO2);

        pager = (ViewPager) view.findViewById(R.id.pager);
        FragmentManager fm = getFragmentManager();
        AerobicPowerResultsPager pagerAdapter = new AerobicPowerResultsPager(fm);
        // Here you would declare which page to visit on creation
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);

        CircleIndicator circleIndicator = (CircleIndicator) view.findViewById(R.id.indicator);
        circleIndicator.setViewPager(pager);
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), AerobicPowerActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }
}
