package com.life.app.client.dashboard.graphs;

/*
 * Created by chara on 24-Jan-17.
 */

import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.view.ViewTreeObserver;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.utils.ColorTemplate;

import static com.life.app.client.dashboard.constants.Consts.DESCRIPTION_TEXT_SIZE;
import static com.life.app.client.dashboard.constants.Consts.GRAPHS_COLOR_BLUE_LIGHT;
import static com.life.app.client.dashboard.constants.Consts.GRAPHS_COLOR_WHITE;

public class BarChartSetup {

    public static void setBarChart(final BarChart mChart, boolean hasdescription, String description, float minValue, float maxValue) {
        mChart.clear();
        mChart.animateY(2000);
        mChart.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mChart.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LinearGradient linGrad = new LinearGradient(0, 0, 0,  mChart.getHeight(),
                        GRAPHS_COLOR_BLUE_LIGHT,
                        Color.parseColor("#BDBDBD"),
                        Shader.TileMode.REPEAT);
                Paint paint = mChart.getRenderer().getPaintRender();
                paint.setShader(linGrad);
                mChart.getRenderer().getPaintRender().setShader(linGrad);
            }
        });

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setPinchZoom(true);
        mChart.setScaleEnabled(true);

        Description descr = new Description();
        if(hasdescription) {
            descr.setText(description);
            descr.setTextColor(GRAPHS_COLOR_WHITE);
            descr.setTextSize(DESCRIPTION_TEXT_SIZE);
        }else
            descr.setText("");

        mChart.setDescription(descr);

        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(true);
        mChart.getLegend().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
        xAxis.setDrawLabels(false);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(minValue);
        leftAxis.setAxisMaximum(maxValue);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, false);
        rightAxis.setAxisMinimum(minValue);
        rightAxis.setAxisMaximum(maxValue);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        l.setEnabled(false);
        l.setTextColor(GRAPHS_COLOR_WHITE);
    }

    public static void setLineChart(final LineChart mChart, boolean hasdescription, String description, float minValue, float maxValue) {
        mChart.clear();
        mChart.animateY(2000);
        mChart.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mChart.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LinearGradient linGrad = new LinearGradient(0, 0, 0,  mChart.getHeight(),
                        GRAPHS_COLOR_BLUE_LIGHT,
                        Color.parseColor("#BDBDBD"),
                        Shader.TileMode.REPEAT);
                Paint paint = mChart.getRenderer().getPaintRender();
                paint.setShader(linGrad);
                mChart.getRenderer().getPaintRender().setShader(linGrad);
            }
        });

        Description descr = new Description();
        if(hasdescription) {
            descr.setText(description);
            descr.setTextColor(GRAPHS_COLOR_WHITE);
            descr.setTextSize(DESCRIPTION_TEXT_SIZE);
        }else
            descr.setText("");

        mChart.setDescription(descr);

        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(true);
        mChart.getLegend().setEnabled(false);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setPinchZoom(true);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
        xAxis.setDrawLabels(false);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(minValue);
        leftAxis.setAxisMaximum(maxValue);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, false);
        rightAxis.setAxisMinimum(minValue);
        rightAxis.setAxisMaximum(maxValue);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        l.setEnabled(false);
        l.setTextColor(GRAPHS_COLOR_WHITE);
    }

    public static void setBarChartWithLimitLine(final BarChart mChart, boolean hasdescription, String description, float minValue, float maxValue, float lineheight) {
        mChart.clear();
        mChart.animateY(2000);
        mChart.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mChart.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LinearGradient linGrad = new LinearGradient(0, 0, 0,  mChart.getHeight(),
                        GRAPHS_COLOR_BLUE_LIGHT,
                        Color.parseColor("#BDBDBD"),
                        Shader.TileMode.REPEAT);
                Paint paint = mChart.getRenderer().getPaintRender();
                paint.setShader(linGrad);
                mChart.getRenderer().getPaintRender().setShader(linGrad);
            }
        });

        Description descr = new Description();
        if(hasdescription) {

            descr.setText(description);
            descr.setTextColor(GRAPHS_COLOR_WHITE);
            descr.setTextSize(DESCRIPTION_TEXT_SIZE);
        }else
            descr.setText("");

        mChart.setDescription(descr);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setPinchZoom(true);

        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(true);
        mChart.getLegend().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
        xAxis.setDrawLabels(false);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(minValue);
        leftAxis.setAxisMaximum(maxValue);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(5, false);
        rightAxis.setAxisMinimum(minValue);
        rightAxis.setAxisMaximum(maxValue);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        LimitLine line = new LimitLine(lineheight);
        line.setLineColor(ColorTemplate.rgb("#E67E22"));
        leftAxis.addLimitLine(line);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        l.setEnabled(false);
        l.setTextColor(GRAPHS_COLOR_WHITE);
    }
}
