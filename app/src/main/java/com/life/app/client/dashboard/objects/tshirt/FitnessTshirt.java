package com.life.app.client.dashboard.objects.tshirt;

/*
 * Created by chara on 16-Jan-17.
 */

import com.life.services.bind.assets.Node;

import java.util.ArrayList;

public class FitnessTshirt extends Tshirt {

    public FitnessTshirt(ArrayList<Node> nodes){
        super.setNodes(nodes);
    }

    public boolean getSS2(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 2)
                return true;
        }

        return false;
    }

    public boolean getSS4(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 4)
                return true;
        }

        return false;
    }
}
