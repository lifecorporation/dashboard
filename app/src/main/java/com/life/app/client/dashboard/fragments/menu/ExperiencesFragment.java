package com.life.app.client.dashboard.fragments.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.adapters.ExperiencesPager;

import me.relex.circleindicator.CircleIndicator;

/*
 * Created by chkalog on 18/7/2016.
 */
public class ExperiencesFragment extends Fragment {

    View mLeak = null;

    ViewPager pager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_experiences, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        pager = (ViewPager) view.findViewById(R.id.pager);
        FragmentManager fm = getChildFragmentManager();
        ExperiencesPager pagerAdapter = new ExperiencesPager(fm);
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);

        CircleIndicator circleIndicator = (CircleIndicator) view.findViewById(R.id.indicator);
        circleIndicator.setViewPager(pager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLeak = null; // now cleaning up!
    }
}
