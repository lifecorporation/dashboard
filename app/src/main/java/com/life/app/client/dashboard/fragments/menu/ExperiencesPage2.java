package com.life.app.client.dashboard.fragments.menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.UUID;

/*
 * Created by chkalog on 30/9/2016.
 */
public class ExperiencesPage2 extends Fragment implements View.OnClickListener{

    ImageButton ibPosture = null, ibSleep = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_experiences_page2, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ibPosture = (ImageButton) view.findViewById(R.id.ibPosture);
        ibSleep   = (ImageButton) view.findViewById(R.id.ibSleep);

        ibPosture.setOnClickListener(this);
        ibSleep.setOnClickListener(this);

        if(SharedPrefDataHandler.getSleepModeStatus(getActivity())){
            ibSleep.setImageResource(R.mipmap.ic_action_sleep_active);
        }else{
            ibSleep.setImageResource(R.mipmap.ic_action_sleep);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ibPosture :
                launchPosture();
                break;
            case R.id.ibSleep:
                if(MainActivity.getStatus()!=null &&
                        (MainActivity.getStatus().getWearable_status() == 1 &&
                                (MainActivity.getStatus().getWearable_number() == 1 ||
                                MainActivity.getStatus().getWearable_number() == 49)))
                        sleepModeWarning();
                else
                    Toast.makeText(getActivity(), "Be aware to switch on your X10Y device and connect it to your tshirt", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void launchPosture(){
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(getResources().getString(R.string.package_posture));
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_posture_app_missing),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void sleepModeWarning(){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog_checkbox, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if(SharedPrefDataHandler.getSleepModeStatus(getActivity()))
            ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(getString(R.string.prompt_disable_sleepmode));
        else
            ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(getString(R.string.prompt_enable_sleepmode));


        CheckBox cbDismiss = (CheckBox)infoDialogView.findViewById(R.id.cbDismiss);
        cbDismiss.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefDataHandler.setSleepModeWarning(getActivity(), !b);
            }
        });


        AlertDialog infoDialogSleep = new AlertDialog.Builder(getActivity()).create();
        infoDialogSleep.setView(infoDialogView);
        infoDialogSleep.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.dialog_button_yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(SharedPrefDataHandler.getSleepModeStatus(getActivity()))
                            setSleepMode(false);
                        else
                            setSleepMode(true);
                    }
                });
        infoDialogSleep.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.dialog_button_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });


        infoDialogSleep.setCancelable(false);
        infoDialogSleep.show();
    }

    private void setSleepMode(boolean isSleep){
        SharedPrefDataHandler.setSleepModeStatus(getActivity(), isSleep);

        if(isSleep){
            stopBasicExperience();
            ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.RE_SLEEP_EXPERIENCE;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    //Request.startExperience(getActivity(), UUID.fromString(Uuid.RE_SLEEP_EXPERIENCE));
                    Utils.startExperience(UUID.fromString(Uuid.RE_SLEEP_EXPERIENCE));
                    Toast.makeText(getActivity(), getResources().getString(R.string.prompt_sleepmode_on), Toast.LENGTH_SHORT).show();
                    ibSleep.setImageResource(R.mipmap.ic_action_sleep_active);
                }
            }, Consts.START_EXPERIENCE_DELAY);
        }else{
            //Request.stopExperience(getActivity());
            Utils.stopExperience();
            ExperiencesUtils.resetBaseExperience(getActivity());
            ibSleep.setImageResource(R.mipmap.ic_action_sleep);
            Toast.makeText(getActivity(), getResources().getString(R.string.prompt_sleepmode_off), Toast.LENGTH_SHORT).show();
        }
    }

    public void stopBasicExperience(){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Utils.stopExperience();
                ExperiencesUtils.EXPERIENCE_RUNNING_KEY = null;
            }
        }, 500);
    }
}
