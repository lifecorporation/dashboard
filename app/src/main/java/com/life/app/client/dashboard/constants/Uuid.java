package com.life.app.client.dashboard.constants;

import com.life.services.bind.status.Status;

/*
 * Created by peppe on 15/07/2016.
 */
public class Uuid {

    public static String EMPTY_EXPERIENCE      = "00000000-0000-0000-0000-000000000000"; //0
    public static String BASE_EXPERIENCE_WEB   = "550e8400-e29b-baba-a716-446655440000"; //WB
    public static String BASE_EXPERIENCE_FIT   = "550e8400-e29b-41d4-a716-446655440000";    //FIT
    public static String BASE_EXPERIENCE_RACE  = "550e8400-e29b-41d4-a716-446655440000";    //FIT
    //public static String BASE_EXPERIENCE       = "11111111-0000-0000-1111-000000000002";    //test
    //public static String EXPERIENCE_RUNNING    = "";    //FIT
    public static String EXPERIENCE_STORE_DATA = "f7975e55-ee00-1028-2800-58418ce90100";    //SAVE DATA

    public static String ECG_RAW               = "00000003-0000-0000-0000-000000000015";
    public static String RESP_RAW              = "00000005-0000-0000-0000-000000000000";
    public static String FITNESS_RAW           = "582d96b2-aa10-1005-0001-437476744c76";
    public static String STATUS_INDICATOR      = "5857a5b3-aa10-1002-0003-576c6c426e67";

    public static String HEART_RATE            = "00000022-0000-0000-0000-000000000015";
    public static String RESP_RATE             = "00000044-0000-0000-0000-000000000001";

    public static String ACTIVITY_LEVEL        = "582d96b2-aa10-1005-0001-437476744c76"; // 00000088-0002-0000-0000-000000000000

    public static String EX_AEROBIC_POWER           = "11111111-0000-0000-1111-000000000000";
    public static String EX_AGILITY                 = "11111111-0000-0000-1111-000000000002";
    public static String EX_ANAEROBIC_CAPACITY      = "11111111-0000-0000-1111-000000000001";
    public static String EX_BALANCE                 = "11111111-0000-0000-1111-000000000004";
    public static String EX_EXPLOSIVENESS           = "11111111-0000-0000-1111-000000000003";
    public static String EX_REACTION_TIME           = "11111111-0000-0000-1111-000000000005";
    public static String EX_RHYTHM                  = "11111111-0000-0000-1111-000000000007";
    public static String EX_STRENGTH                = "11111111-0000-0000-1111-000000000006";
    public static String EX_SPEED                   = "11111111-0000-0000-1111-000000000008";

    public static String RE_AEROBIC_POWER_TEST      = "41d35e55-aa0d-1001-0003-581c973f0150";
    public static String RE_AEROBIC_POWER_OUT       = "6cac5e55-aa0d-1002-0002-581c98ca010f";
    public static String RE_AEROBIC_STEP_TEST       = "11111111-1111-1111-1111-000000000000";
    public static String RE_AEROBIC_STEP_RESULT     = "11111111-1111-1111-1111-000000000001";

    public static String RE_SPEED                   = "11111111-1111-1111-1111-000000000001";
    public static String RE_SPEED_RESULT            = "11111111-1111-1111-1111-000000000015";

    public static String RE_AGILITY_TIMER           = "11111111-1111-1111-1111-000000000000";
    public static String RE_AGILITY_JUMP            = "11111111-1111-1111-1111-000000000003";
    public static String RE_AGILITY_SWAY            = "11111111-1111-1111-1111-200000000000";
    public static String RE_AGILITY_OUTPUT          = "11111111-1111-1111-1111-000000000004";

    public static String RE_ANAEROBIC_FINALIZER     = "11111111-1111-1111-1111-000000000002";
    public static String RE_ANAEROBIC_C_TIMER       = "11111111-1111-1111-1111-000000000012";
    public static String RE_ANAEROBIC_C_TEST        = "11111111-1111-1111-1111-000000000016";

    public static String RE_BALANCE_OUT             = "11111111-1111-1111-1111-300000000000";
    public static String RE_BALANCE_TEST            = "11111111-1111-1111-1111-000000000006";

    public static String RE_EXPLOSIVENESS_OUT       = "11111111-1111-1111-1111-400000000000";
    public static String RE_EXPLOSIVENESS_TEST      = "11111111-1111-1111-1111-000000000005";

    public static String RE_JOINT_MOBILITY          = "00000044-1111-1111-0000-000000000001";

    public static String RE_REACTION_TIME_TEST      = "11111111-1111-1111-1111-000000000007";
    public static String RE_REACTION_TIME_OUTPUT    = "11111111-1111-1111-1111-500000000000";

    public static String RE_RHYTHM_EVALUATOR        = "11111111-1111-1111-1111-000000000011";
    public static String RE_RHYTHM_MOVE_CATCHER     = "11111111-1111-1111-1111-000000000013";
    public static String RE_RHYTHM_FINALIZER        = "11111111-1111-1111-1111-000000000014";
    public static String RE_RHYTHM_BEATS_COUNTER    = "11111111-1111-1111-1111-000000000010";

    public static String RE_STRENGTH_OUT            = "11111111-1111-1111-1111-000000000009";
    public static String RE_STRENGTH_TEST           = "11111111-1111-1111-1111-000000000008";

    public static String BICEPS_LEFT                = "581b514c-aa00-1001-1002-4d674e746e73";
    public static String BICEPS_RIGHT               = "581b5b0b-aa00-1001-1002-4d674e746e73";
    public static String FOREARM_LEFT               = "581c4a26-aa00-1001-1002-4d674e746e73";
    public static String FOREARM_RIGHT              = "581c4ad9-aa00-1001-1002-4d674e746e73";

    public static String BICEPS_LEFT_CALIBRATION    = "5889e0d6-eec0-1001-0100-15715ceb0000";
    public static String BICEPS_RIGHT_CALIBRATION   = "5889e0d6-eec0-1001-0100-ef7e61880000";
    public static String FOREARM_LEFT_CALIBRATION   = "5889e0d6-eec0-1001-0100-7a7041840000";
    public static String FOREARM_RIGHT_CALIBRATION  = "5889e0d6-eec0-1001-0100-ae317e5b0000";

    public static String BICEPS_LEFT_CALIBRATION_DATA    = "581b514c-aac0-1001-1002-4d674e746e73";
    public static String BICEPS_RIGHT_CALIBRATION_DATA   = "581b5b0b-aac0-1001-1002-4d674e746e73";
    public static String FOREARM_LEFT_CALIBRATION_DATA   = "581c4a26-aac0-1001-1002-4d674e746e73";
    public static String FOREARM_RIGHT_CALIBRATION_DATA  = "581c4ad9-aac0-1001-1002-4d674e746e73";

    public static String RE_SLEEP_EXPERIENCE        = "550e8400-e29b-bebe-a716-446655440000";



    /*
    public static String RE_EMG1_INTENSITY          = "5825ac98-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG2_INTENSITY          = "5825ad56-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG3_INTENSITY          = "5825ada0-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG4_INTENSITY          = "5825addc-aa0d-1001-1002-4d674e746e73";
    */

    /*
    public static String RE_EMG1_RAW                = "5829c22a-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG2_RAW                = "5829c277-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG3_RAW                = "5829c2e0-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG4_RAW                = "5829c2e0-aa0d-1001-1002-4d674e746e73";
    */

    public static String generalExperience(Status status){
        if(status == null)
            return "";

        if(status.getProduct_id() == 0 || status.getProduct_id() == 48){
            return BASE_EXPERIENCE_FIT;
        }else if(status.getProduct_id() == 1 || status.getProduct_id() == 49){
            return BASE_EXPERIENCE_WEB;
        }else if(status.getProduct_id() == 3 || status.getProduct_id() == 51){
            return BASE_EXPERIENCE_RACE;
        }

        return "";
    }
}