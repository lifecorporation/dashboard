package com.life.app.client.dashboard.fragments;

/*
 * Created by chara on 09-Dec-16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.fragments.menu.CalibrationFragment;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

import static com.life.app.client.dashboard.constants.Consts.*;
import static com.life.app.client.dashboard.utils.CustomAnimation.disableBody;
import static com.life.app.client.dashboard.utils.CustomAnimation.enableBody;

public class MainActivityMuscleFragment extends Fragment implements View.OnClickListener {

    static ImageView ivBodymap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_real_time_muscles, container, false);
    }

    TextView tvTitle = null;

    private static ImageView ivBcpteRight = null, ivBcpteLeft = null, ivForearmleft = null, ivForearmRight = null;

    private static MainActivityMuscleFragment thisContext;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ivBcpteRight      = (ImageView) view.findViewById(R.id.ivBcpteRight);
        ivBcpteLeft       = (ImageView) view.findViewById(R.id.ivBcpteLeft);
        ivForearmleft     = (ImageView) view.findViewById(R.id.ivForearmleft);
        ivForearmRight    = (ImageView) view.findViewById(R.id.ivForearmRight);
        ivBodymap         = (ImageView) view.findViewById(R.id.ivBodymap);

        thisContext = this;

        if(MainActivity.getStatus()==null ||
                 MainActivity.getStatus().getWearable_status()==0){
            disableBody(getActivity(), ivBodymap, 0, 0);
        }

        ivBcpteRight.setImageAlpha(toPercent(5));
        ivBcpteLeft.setImageAlpha(toPercent(5));
        ivForearmleft.setImageAlpha(toPercent(5));
        ivForearmRight.setImageAlpha(toPercent(5));

        tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utils.getIONIconFont(getActivity()), Typeface.BOLD);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    public static void clearMuscles(){
        if (ivForearmRight != null)
            ivForearmRight.setImageAlpha(toPercent(5));

        if (ivBcpteRight != null)
            ivBcpteRight.setImageAlpha(toPercent(5));

        if (ivForearmleft != null)
            ivForearmleft.setImageAlpha(toPercent(5));

        if (ivBcpteLeft != null)
            ivBcpteLeft.setImageAlpha(toPercent(5));

        if(lMusclegram!=null && tvGraphData!=null) {
            lMusclegram.setVisibility(View.GONE);
            tvGraphData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * EMG1
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean ForearmRightCalibrated = false;
    public static void updateForearmRight(final Activity activity, final String [] data){

        if(activity == null)
            return;

        if(ivForearmRight == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(data[3].equals("2.0")) {
                    ForearmRightCalibrated = true;
                    Glide.with(activity).load(R.drawable.ic_forearm_left).into(ivForearmRight);
                    //ivForearmRight.setImageResource(R.drawable.ic_forearm_left);
                    float f = Float.parseFloat(data[2]);
                    ivForearmRight.setImageAlpha(toPercent((int)f));
                }else{
                    ForearmRightCalibrated = false;
                    Glide.with(activity).load(R.drawable.ic_forearm_left_deactivated).into(ivForearmRight);
                    //ivForearmRight.setImageResource(R.drawable.ic_forearm_left_deactivated);
                    ivForearmRight.setImageAlpha(toPercent(100));
                }
            }
        });

        if(current_muscle_visible == 2 && data[3].equals("2.0"))
            addDataToGraph(activity, data);
    }

    /**
     * EMG2
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean BicepsRightCalibrated = false;
    public static void updateBicepsRight(final Activity activity, final String [] data){

        if(activity == null)
            return;

        if(ivBcpteRight == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(data[3].equals("2.0")) {
                    BicepsRightCalibrated = true;
                    Glide.with(activity).load(R.drawable.ic_arm_right).into(ivBcpteRight);
                    //ivBcpteRight.setImageResource(R.drawable.ic_arm_right);
                    float f = Float.parseFloat(data[2]);
                    ivBcpteRight.setImageAlpha(toPercent((int)f));
                }else{
                    BicepsRightCalibrated = false;
                    Glide.with(activity).load(R.drawable.ic_arm_right_deactivated).into(ivBcpteRight);
                    //ivBcpteRight.setImageResource(R.drawable.ic_arm_right_deactivated);
                    ivBcpteRight.setImageAlpha(toPercent(100));
                }
            }
        });

        if(current_muscle_visible == 0 && data[3].equals("2.0"))
            addDataToGraph(activity, data);
    }

    /**
     * EMG3
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean ForearmLeftCalibrated = false;
    public static void updateForearmLeft(final Activity activity, final String [] data){

        if(activity == null)
            return;

        if(ivForearmleft == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(data[3].equals("2.0")) {
                    ForearmLeftCalibrated = true;
                    Glide.with(activity).load(R.drawable.ic_forearm_right).into(ivForearmleft);
                    //ivForearmleft.setImageResource(R.drawable.ic_forearm_right);
                    float f = Float.parseFloat(data[2]);
                    ivForearmleft.setImageAlpha(toPercent((int)f));
                }else{
                    ForearmLeftCalibrated = false;
                    Glide.with(activity).load(R.drawable.ic_forearm_right_deactivated).into(ivForearmleft);
                    //ivForearmleft.setImageResource(R.drawable.ic_forearm_right_deactivated);
                    ivForearmleft.setImageAlpha(toPercent(100));
                }
            }
        });

        if(current_muscle_visible == 3 && data[3].equals("2.0"))
            addDataToGraph(activity, data);
    }

    /**
     * EMG4
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean BicepsLeftCalibrated = false;
    public static void updateBicepsLeft(final Activity activity, final String [] data){

        if(activity == null)
            return;

        if(ivBcpteLeft == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(data[3].equals("2.0")) {
                    BicepsLeftCalibrated = true;
                    Glide.with(activity).load(R.drawable.ic_arm_left).into(ivBcpteLeft);
                    //ivBcpteLeft.setImageResource(R.drawable.ic_arm_left);
                    float f = Float.parseFloat(data[2]);
                    ivBcpteLeft.setImageAlpha(toPercent((int) f));
                }else{
                    BicepsLeftCalibrated = false;
                    Glide.with(activity).load(R.drawable.ic_arm_left_deactivated).into(ivBcpteLeft);
                    //ivBcpteLeft.setImageResource(R.drawable.ic_arm_left_deactivated);
                    ivBcpteLeft.setImageAlpha(toPercent(100));
                }
            }
        });

        if(current_muscle_visible == 1 && BicepsLeftCalibrated)
            addDataToGraph(activity, data);
    }

    public static void enable(Context mContext){

        if(ivBcpteRight!=null){
            ivBcpteRight.setOnClickListener(thisContext);
        }
        if(ivBcpteLeft!=null){
            ivBcpteLeft.setOnClickListener(thisContext);
        }
        if(ivForearmleft!=null){
            ivForearmleft.setOnClickListener(thisContext);
        }
        if(ivForearmRight!=null){
            ivForearmRight.setOnClickListener(thisContext);
        }

        if(ivBodymap == null)
            return;

        enableBody(mContext, ivBodymap);
    }

    public static void disable(Context mContext){

        if(ivBcpteRight!=null)   ivBcpteRight.setOnClickListener(null);
        if(ivBcpteLeft!=null)    ivBcpteLeft.setOnClickListener(null);
        if(ivForearmleft!=null)  ivForearmleft.setOnClickListener(null);
        if(ivForearmRight!=null) ivForearmRight.setOnClickListener(null);

        if(ivBodymap == null)
            return;

        disableBody(mContext, ivBodymap, 1100, 500);

    }

    private static long firstdate  = 0;
    private static long currdate   = 0;
    private static boolean isFirst = true;
    private static List<PointValue> muscleValues = new ArrayList<>();
    private static int limit = 200;

    private static void setMusclesViewport() {
        int size = muscleValues.size();
        if (size > limit) {
            final Viewport viewport = new Viewport(lMusclegram.getMaximumViewport());
            viewport.left = size - limit;
            lMusclegram.setCurrentViewport(viewport);
        }
    }

    private static void addDataToGraph(Activity activity, final String [] data){

        if(tvGraphData!=null && lMusclegram!=null) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (tvGraphData.getVisibility() == View.VISIBLE) {
                        lMusclegram.setVisibility(View.VISIBLE);
                        tvGraphData.setVisibility(View.GONE);
                    }

                    if (isFirst) {
                        firstdate = Long.parseLong(data[1]);
                        currdate = Long.parseLong(data[1]);
                    } else {
                        currdate = Long.parseLong(data[1]);
                    }

                    float point = Float.parseFloat(data[2]);


                    int cardioValuesSize = muscleValues.size();
                    if (cardioValuesSize > 0){
                        PointValue prevPoint = muscleValues.get(cardioValuesSize - 1);
                        long currentPosX = currdate - firstdate;
                        if (currentPosX > prevPoint.getX()){
                            muscleValues.add(new PointValue(currentPosX, point));
                        }
                    }
                    else{
                        long currentPosX = currdate - firstdate;
                        muscleValues.add(new PointValue(currentPosX, point));
                    }

                    LineChartData data = lMusclegram.getLineChartData();
                    data.getLines().get(0).setValues(new ArrayList<>(muscleValues));
                    lMusclegram.setLineChartData(data);
                    setMusclesViewport();

                    if (muscleValues.size() > limit) {
                        muscleValues.remove(0);
                    }
                }
            });
        }
    }

    private static int toPercent(int value){
        return (value * 255) / 100;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBcpteRight:
                infoDialog(getString(R.string.prompt_bicep_right), 0);
                break;
            case R.id.ivBcpteLeft:
                infoDialog(getString(R.string.prompt_bicep_left), 1);
                break;
            case R.id.ivForearmRight:
                infoDialog(getString(R.string.prompt_forearm_right), 2);
                break;
            case R.id.ivForearmleft:
                infoDialog(getString(R.string.prompt_forearm_left), 3);
                break;
        }
    }

    private static LineChartView lMusclegram = null;
    private static TextView tvGraphData = null;
    private static int current_muscle_visible = -1;

    private void infoDialog(String title, int muscleFlag){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_muscles_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        tvGraphData = (TextView)infoDialogView.findViewById(R.id.tvGraphData);

        lMusclegram = (LineChartView)infoDialogView.findViewById(R.id.lMusclegram);

        if(muscleValues==null)
            muscleValues = new ArrayList<>();
        else
            muscleValues.clear();

        boolean calibrated = false;

        if(muscleFlag == 2 && ForearmRightCalibrated){
            calibrated = true;
        }else if(muscleFlag == 0 && BicepsRightCalibrated){
            calibrated = true;
        }else if(muscleFlag == 3 && ForearmLeftCalibrated){
            calibrated = true;
        }else if(muscleFlag == 1 && BicepsLeftCalibrated){
            calibrated = true;
        }

        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                current_muscle_visible = -1;
            }
        });

        if(!calibrated){
            tvGraphData.setText(getResources().getString(R.string.warning_emg_calibration));
            tvGraphData.setVisibility(View.VISIBLE);
            lMusclegram.setVisibility(View.GONE);
            infoDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.calibrate), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    infoDialog.dismiss();

                    calibrate(new CalibrationFragment());
                }
            });
        }else{
            tvGraphData.setVisibility(View.GONE);
            lMusclegram.setVisibility(View.VISIBLE);
            setSensorGraph(lMusclegram);
        }

        current_muscle_visible = muscleFlag;

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

    private void calibrate(final Fragment fragment){
        final Handler handler = new Handler();
        new Thread(new Runnable(){
            @Override
            public void run(){
                //long running code
                //this is running on a background thread
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        //since the handler was created on the UI thread,
                        //   this code will run on the UI thread
                        final FragmentManager manager = MainActivity.getInstance().getSupportFragmentManager();

                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.replace(R.id.content, fragment, fragment.getClass().getName());
                        transaction.commit();
                    }
                });
            }
        }).start();
    }

    private void setSensorGraph(LineChartView graph){

        graph.setInteractive(true);
        graph.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
        graph.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL);

        List<Line> pneumolines = new ArrayList<>();
        Line line = new Line();
        line.setHasLines(true);
        line.setHasPoints(false);
        line.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        line.setCubic(false);
        line.setStrokeWidth(1);
        pneumolines.add(line);

        LineChartData data = new LineChartData(pneumolines);
        data.setValueLabelBackgroundEnabled(false);
        data.setValueLabelBackgroundAuto(false);

        Axis axisX = new Axis().setName("Timestamp");
        Axis axisY = new Axis().setName("");
        data.setBaseValue(Float.NEGATIVE_INFINITY);
        axisX.setHasTiltedLabels(false);
        axisY.setHasTiltedLabels(false);
        axisX.setHasLines(false);
        axisY.setHasLines(false);

        graph.setLineChartData(data);
    }

    public static void updateMuscles(Activity mActivity, final boolean isFitness){
        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mActivity.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if(isFitness){
                    if(ivForearmleft!=null) ivForearmleft.setVisibility(View.VISIBLE);
                    if(ivForearmRight!=null) ivForearmRight.setVisibility(View.VISIBLE);
                }else{
                    if(ivForearmleft!=null) ivForearmleft.setVisibility(View.GONE);
                    if(ivForearmRight!=null) ivForearmRight.setVisibility(View.GONE);
                }
            }
        };
        mainHandler.postDelayed(myRunnable, 1000);
    }
}
