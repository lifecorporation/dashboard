package com.life.app.client.dashboard.fragments.radarChart.Balance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.BalanceActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;
import com.wang.avi.AVLoadingIndicatorView;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.UUID;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 02/08/2016.
 */
public class BalanceExperience extends Fragment implements CountDownAnimation.CountDownListener{

    RelativeLayout back;
    Button stop;

    public static Timer tBalance = null;

    View mLeak = null;

    static TextView tvAttempt, tvTime, messageTV;
    static ProgressBar hProgressbar1;

    public static Activity mInstance = null;
    static BarChart mChart;

    private static FragmentManager fragmentManager;

    //variabili per il grafico
    static int   fase       = 0;

    static ArrayList<BarEntry> entries = new ArrayList<>();
    static ArrayList<Entry> values     = new ArrayList<>();
    static ArrayList<Float> synthesis  = new ArrayList<>();

    static int currentProgress = 0;
    static boolean runTimer = true;

    static BarDataSet barSet1;
    static ArrayList<IBarDataSet> dataSets = new ArrayList<>();
    static BarData data;
    static ScatterDataSet scatterSet1;
    static ScatterChart scatterChart;

    static AVLoadingIndicatorView avIView;
    static RelativeLayout rlSaving;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_balance_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        mInstance = getActivity();

        fragmentManager = getActivity().getSupportFragmentManager();

        BalanceActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_balance));

        mChart       = (BarChart) view.findViewById(R.id.barChart);
        scatterChart = (ScatterChart) view.findViewById(R.id.scatterChart);
        tvAttempt    = (TextView) view.findViewById(R.id.tvAttempt);
        tvTime       = (TextView) view.findViewById(R.id.tvTime);
        messageTV    = (TextView) view.findViewById(R.id.messageTV);
        hProgressbar1= (ProgressBar) view.findViewById(R.id.hProgressBar);

        avIView = (AVLoadingIndicatorView) view.findViewById(R.id.avIView);
        rlSaving = (RelativeLayout) view.findViewById(R.id.rlSaving);

        stop = (Button) view.findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        BarChartSetup.setBarChart(mChart, false, null, 0f, 25f);
        initiateScatterChart(scatterChart);

        //addDataToChart(0,0);
        //addDataToScatterChart(0,0);

        cambiaMessaggio(fase);

        startCountDown(view);
    }

    @Override
    public void onResume(){
        super.onResume();
        BalanceActivity.hideTutorial();
    }

    private void startExperience() {
        //Request.startExperience(BalanceActivity.balanceContext, UUID.fromString(Uuid.EX_BALANCE));
        Utils.startExperience(UUID.fromString(Uuid.EX_BALANCE));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_BALANCE;

        updateTVAttempt(1);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private void resetValues(){
        mChart.clear();

        if(tBalance != null)
            tBalance.cancel();

        entries.clear();
        values.clear();

        synthesis.clear();

        if(barSet1!=null)   barSet1.clear();
        if(dataSets!=null)  dataSets.clear();
        if(data!=null)      data.clearValues();

        cambiaMessaggio(0);

        runTimer = false;
        currentProgress= 0;

        resetProgressBar();

        current_try = 1;

        if(shifts!=null) {
            if(shifts[0]!=null)    shifts[0].clear();
            if(shifts[1]!=null)    shifts[1].clear();
            if(shifts[2]!=null)    shifts[2].clear();

            shifts[0] = null;
            shifts[1] = null;
            shifts[2] = null;
        }

        shifts = null;
    }

    private static void resetProgressBar(){
        BalanceActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                hProgressbar1.setProgress(0);
                //resetto anche lo scatter chart
                //scatterChart.clearValues();
                addDataToScatterChart(0, 0);
            }
        });
    }

    private void initiateScatterChart(ScatterChart sChart){
        //sChart.setPinchZoom(true);
        sChart.setDrawGridBackground(false);
        sChart.setScaleEnabled(true);
        sChart.getLegend().setEnabled(false);

        XAxis xAxis = sChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(6);
        xAxis.setDrawLabels(true);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis leftAxis = sChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(0f);
        leftAxis.setAxisMinimum(-50f); // this replaces setStartAtZero(true)
        leftAxis.setAxisMaximum(50f);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = sChart.getAxisRight();
        rightAxis.setLabelCount(6, false);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        rightAxis.setSpaceTop(0f);
        rightAxis.setAxisMinimum(-50f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMaximum(50f);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        Legend l = sChart.getLegend();
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART_INSIDE);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(0f);
        l.setTextSize(11f);
        l.setTextColor(GRAPHS_COLOR_WHITE);
        l.setXEntrySpace(1f);
        l.setEnabled(false);

        sChart.getLegend().setEnabled(false);
        sChart.getXAxis().setAxisMinimum(-50f);
        sChart.getXAxis().setAxisMaximum(50f);
    }

    private static void updateProgressBar() {
        if (currentProgress <= 3000) {
            currentProgress += 1;
            hProgressbar1.setProgress(currentProgress);
        }
    }

    private static void addDataToChart(float tryNumber, float tryTime) {

        entries.add(new BarEntry(tryNumber, tryTime));

        barSet1 = new BarDataSet(entries, "");
        barSet1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        barSet1.setHighlightEnabled(false);
        barSet1.setDrawValues(false);

        dataSets.add(barSet1);

        data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.6f);

        BalanceActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                mChart.setData(data);
                mChart.invalidate();
            }
        });
    }

    private static void addDataToScatterChart(final float x, final float value) {
        values.add(new Entry(x, value));

        Collections.sort(values, new EntryXComparator());

        scatterSet1 = new ScatterDataSet(values, "DS 1");
        scatterSet1.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        scatterSet1.setColor(GRAPHS_COLOR_BLUE_LIGHT);

        scatterSet1.setScatterShapeSize(4f);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<>();
        dataSets.add(scatterSet1);

        ScatterData data = new ScatterData(dataSets);
        data.setValueTextSize(10f);

        scatterChart.setData(data);
        BalanceActivity.getInstance().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                    scatterChart.invalidate();
                }
            });
    }

    public static void onExperienceFinished(Activity activity, String [] data){



        BalanceActivity.getInstance().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                avIView.show();
                rlSaving.setVisibility(View.VISIBLE);
            }
        });



        synthesis.add(Float.parseFloat(data[2]));   //score normalizzato
        synthesis.add(Float.parseFloat(data[3]));   //valore massimo ottenuto nei tre tentativi
        if(shifts!=null) {

            if(shifts[0]!=null)
                synthesis.add((float) shifts[0].size());
            else
                synthesis.add(0f);

            if(shifts[1]!=null)
                synthesis.add((float) shifts[1].size());
            else
                synthesis.add(0f);

            if(shifts[2]!=null)
                synthesis.add((float) shifts[2].size());
            else
                synthesis.add(0f);
        }else{
            synthesis.add(0f);
            synthesis.add(0f);
            synthesis.add(0f);
        }
        SharedPrefDataHandler.addBest(activity, RadarChartConst.BALANCE_KEY , synthesis);
        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.BALANCE_KEY , synthesis);

        for(int i=0;i<3;i++){
            if(shifts[i]!=null) {
                for (int k = 0; k < shifts[i].size(); k++) {
                    SharedPrefDataHandler.addNewSet(activity, RadarChartConst.BALANCE_KEY + "_1_TRY_" + (i + 1) + "_SET_" + (k + 1),
                            shifts[i].get(k));
                }
            }else{
                SharedPrefDataHandler.addNewSet(activity, RadarChartConst.BALANCE_KEY + "_1_TRY_" + (i + 1) + "_SET_" + (1), new ArrayList<Float>());
            }
        }

        BalanceActivity.getInstance().runOnUiThread(new Runnable() {

            @Override
            public void run() {

                avIView.hide();
                rlSaving.setVisibility(View.GONE);
            }
        });


        gotoResults();
    }

    private static void gotoResults(){
        Bundle b = new Bundle();
        b.putInt("position", 1);
        Fragment f = new BalanceResults();
        f.setArguments(b);
        fragmentManager.beginTransaction().
                replace(R.id.balance_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    private void goBack(){
        BalanceActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), BalanceActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    /*
        A 3 row array, containing the list of the total shift.
     */
    private static ArrayList<ArrayList<Float>>[] shifts = null;
    private static int current_try = 1;

    public static void updateViews(String [] data){
        //update fase
        //data[0] = uuid
        //data[1] = timestamp
        //data[2] = fase del test
        //data[3] = tempo del tentativo attuale
        //data[4] = spostamento in x
        //data[5] = spostamento in y
        //data[6] = numero del tentativo

        if((int)Float.parseFloat(data[2]) == 3){

            BalanceActivity.getInstance().runOnUiThread(new Runnable(){

                @Override
                public void run() {
                    messageTV.setText(BalanceActivity.getInstance().getResources().getString(R.string.balanceFase3));
                }
            });

            if(shifts  == null)
                shifts = (ArrayList<ArrayList<Float>>[])new ArrayList[3];

            ArrayList<Float> xy1 = new ArrayList<>();
            xy1.add(Float.parseFloat(data[3]));
            xy1.add(Float.parseFloat(data[4]));
            xy1.add(Float.parseFloat(data[5]));

            if(shifts[current_try-1] == null)
                shifts[current_try-1] = new ArrayList<>();

            shifts[current_try-1].add(xy1);

            gestisciFase(Float.parseFloat(data[3]), Float.parseFloat(data[4]), Float.parseFloat(data[5]));

        }else if((int)Float.parseFloat(data[2]) == 4){
            onPhaseFinished();
            updateTVAttempt((int)Float.parseFloat(data[6]));

            current_try++;
        }else{
            cambiaMessaggio((int)Float.parseFloat(data[2]));
        }
    }

    private static void onPhaseFinished(){
        resetProgressBar();
        try {
            addDataToChart(current_try, shifts[current_try - 1].get(shifts[current_try - 1].size() - 1).get(0));
        }catch (NullPointerException n){
            n.printStackTrace();
        }

        BalanceActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvTime.setText("");
            }
        });
    }

    private static void cambiaMessaggio(int faseX){
        switch (faseX) {
            case 0:
                BalanceActivity.getInstance().runOnUiThread(new Runnable(){

                    @Override
                    public void run() {
                        messageTV.setText(BalanceActivity.getInstance().getResources().getString(R.string.balanceFase0));
                    }
                });
                break;
            case 1:
                BalanceActivity.getInstance().runOnUiThread(new Runnable(){

                    @Override
                    public void run() {
                        messageTV.setText(BalanceActivity.getInstance().getResources().getString(R.string.balanceFase1));
                    }
                });
                break;
            case 2:
                BalanceActivity.getInstance().runOnUiThread(new Runnable(){

                    @Override
                    public void run() {
                        messageTV.setText(BalanceActivity.getInstance().getResources().getString(R.string.balanceFase2));
                    }
                });
                break;

            case 3:
                BalanceActivity.getInstance().runOnUiThread(new Runnable(){

                    @Override
                    public void run() {
                        messageTV.setText(BalanceActivity.getInstance().getResources().getString(R.string.balanceFase3));
                    }
                });
                break;
        }
    }

    private static void gestisciFase(final float time, final float x, final float y){
        BalanceActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                updateProgressBar();

                addDataToScatterChart(x, y);
                String placeholder = (int)time + "''";
                tvTime.setText(placeholder);
            }
        });
    }

    private static void updateTVAttempt(final int attempt){
        BalanceActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                String placeholder = attempt + "/3";
                tvAttempt.setText(placeholder);
            }
        });
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        startExperience();
    }
}