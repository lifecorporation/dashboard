package com.life.app.client.dashboard.fragments.radarChart.AerobicPower;

/*
 * Created by chara on 16-Dec-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import java.util.ArrayList;

import static com.life.app.client.dashboard.constants.Consts.*;

public class RightFragment extends Fragment {

    static LineChart mChart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_aerobic_power_results_right, container, false);
    }

    ArrayList<Float> data = null;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChart  = (LineChart) view.findViewById(R.id.lineChart);

        data = new ArrayList<>();
        data = SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.AEROBIC_POWER_KEY + "_" + AerobicPowerResults.position +"_TRY_1_SET");

        BarChartSetup.setLineChart(mChart, true, "Heartbeats", 0f, 180f);
        addDataToChart();
    }

    private void addDataToChart() {

        ArrayList<Entry> entries = new ArrayList<>();

        for (int i = 0; i < data.size() ; i++) {
            entries.add(new Entry(i, data.get(i)));
        }

        LineDataSet dataset = new LineDataSet(entries, "");

        LineData data = new LineData(dataset);
        dataset.setDrawFilled(false);
        dataset.setValueTextColor(GRAPHS_COLOR_WHITE);

        mChart.setData(data);
        mChart.invalidate();
    }
}
