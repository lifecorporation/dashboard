package com.life.app.client.dashboard.fragments;

import android.app.Activity;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.utils.ColorTemplate;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

import static com.life.app.client.dashboard.constants.Consts.GRAPHS_COLOR_BLUE_LIGHT;

/*
 * Created by chara on 09-Dec-16.
 */

public class MainActivityRealTimeFragment extends Fragment {

    static GraphView graphHeart, graphResp;
    static LineGraphSeries<DataPoint> seriesHeart = null, seriesResp = null;
    static DataPoint [] points = null;
    static boolean firstDataHeart = true;
    static boolean firstDataResp = true;
    static int limit = 200;
    static TextView tvGraphHeart = null, tvGraphResp = null;

    private static LineChartView lPneumogram;
    private static LineChartView lCardiogram;
    private static List<PointValue> pneumoValues = new ArrayList<>();
    private static List<PointValue> cardioValues = new ArrayList<>();

    Handler mHandler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_real_time_2_real_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utils.getIONIconFont(getActivity()), Typeface.BOLD);

        graphHeart = (GraphView) view.findViewById(R.id.graphHeart);
        graphResp  = (GraphView) view.findViewById(R.id.graphResp);

        tvGraphHeart = (TextView) view.findViewById(R.id.tvGraphHeart);
        tvGraphResp  = (TextView) view.findViewById(R.id.tvGraphResp);

        graphHeart.getGridLabelRenderer().setGridStyle( GridLabelRenderer.GridStyle.NONE );
        graphResp.getGridLabelRenderer().setGridStyle( GridLabelRenderer.GridStyle.NONE );

        graphHeart.getGridLabelRenderer().setHorizontalLabelsVisible(false);// remove horizontal x labels and line
        graphHeart.getGridLabelRenderer().setVerticalLabelsVisible(false);

        graphResp.getGridLabelRenderer().setHorizontalLabelsVisible(false);// remove horizontal x labels and line
        graphResp.getGridLabelRenderer().setVerticalLabelsVisible(false);

        points = new DataPoint[0];

        Paint paintHeart = new Paint();
        Paint paintResp = new Paint();

        paintHeart.setStyle(Paint.Style.FILL_AND_STROKE);
        paintHeart.setStrokeWidth(4f);
        paintHeart.setColor(ColorTemplate.rgb("#BB3434"));
        paintHeart.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0));

        paintResp.setStyle(Paint.Style.FILL_AND_STROKE);
        paintResp.setStrokeWidth(4f);
        paintResp.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        paintResp.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0));

        if(seriesHeart==null)
            seriesHeart = new LineGraphSeries<>(points);

        if(seriesResp==null)
            seriesResp = new LineGraphSeries<>(points);
        //seriesResp.setDrawBackground(true);

        seriesHeart.setCustomPaint(paintHeart);
        seriesResp.setCustomPaint(paintResp);

        setGraph(graphHeart);
        graphHeart.addSeries(seriesHeart);
        setGraph(graphResp);
        graphResp.addSeries(seriesResp);

        ((TextView)view.findViewById(R.id.tvCardiogram)).setTypeface(Utils.getKnockoutFont(getActivity()));
        ((TextView)view.findViewById(R.id.tvPneumogram)).setTypeface(Utils.getKnockoutFont(getActivity()));


        lPneumogram = (LineChartView) view.findViewById(R.id.lPneumogram);
        lCardiogram = (LineChartView) view.findViewById(R.id.lCardiogram);

        lPneumogram.setInteractive(true);
        lPneumogram.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
        lPneumogram.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL);

        lCardiogram.setInteractive(true);
        lCardiogram.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
        lCardiogram.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL);

        mHandler = new Handler();

        List<Line> pneumolines = new ArrayList<>();
        Line line = new Line();
        line.setHasLines(true);
        line.setHasPoints(false);
        line.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        line.setCubic(false);
        line.setStrokeWidth(1);
        pneumolines.add(line);

        LineChartData data = new LineChartData(pneumolines);
        data.setValueLabelBackgroundEnabled(false);
        data.setValueLabelBackgroundAuto(false);

        Axis axisX = new Axis().setName("Timestamp");
        Axis axisY = new Axis().setName("");
        data.setBaseValue(Float.NEGATIVE_INFINITY);
        axisX.setHasTiltedLabels(false);
        axisY.setHasTiltedLabels(false);
        axisX.setHasLines(false);
        axisY.setHasLines(false);

        List<Line> cardiolines = new ArrayList<>();
        Line line2 = new Line();
        line2.setHasLines(true);
        line2.setHasPoints(false);
        line2.setColor(ColorTemplate.rgb("#BB3434"));
        line2.setCubic(true);
        line2.setStrokeWidth(1);
        cardiolines.add(line2);

        LineChartData data2 = new LineChartData(cardiolines);
        data2.setValueLabelBackgroundEnabled(false);
        data2.setValueLabelBackgroundAuto(false);

        Axis axisX2 = new Axis().setName("Timestamp");
        Axis axisY2 = new Axis().setName("");
        //data.setAxisXBottom(axisX);
        //data.setAxisYLeft(axisY);
        data2.setBaseValue(Float.NEGATIVE_INFINITY);
        axisX2.setHasTiltedLabels(false);
        axisY2.setHasTiltedLabels(false);
        axisX2.setHasLines(false);
        axisY2.setHasLines(false);

        lPneumogram.setLineChartData(data);
        lCardiogram.setLineChartData(data2);
    }

    private static void setPneumoViewport() {
        int size = pneumoValues.size();
        if (size > limit) {
            final Viewport viewport = new Viewport(lPneumogram.getMaximumViewport());
            viewport.left = size - limit;
            lPneumogram.setCurrentViewport(viewport);
        }
    }

    private static void setCardioViewport() {
        int size = cardioValues.size();
        if (size > limit) {
            final Viewport viewport = new Viewport(lCardiogram.getMaximumViewport());
            viewport.left = size - limit;
            lCardiogram.setCurrentViewport(viewport);
        }
    }

    private void setGraph(GraphView graph){
        // customize a little bit viewport
        graph.getGridLabelRenderer().setHighlightZeroLines(false);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(1700);

        graph.getViewport().setYAxisBoundsManual(false);

        // enable scaling and scrolling
        graph.getViewport().setScrollable(false);
        graph.getViewport().setScalable(false);
        graph.getViewport().setScalableY(false);
        graph.getViewport().scrollToEnd();
    }

    private static long prev_timestamp_heart = 0;
    private static long cur_timestamp_heart = 0;

    public synchronized static void addHeartRawData(final Activity activity, final String [] data){

        if (graphHeart != null){

            if(firstDataHeart){
                prev_timestamp_heart = Long.parseLong(data[1]);
                cur_timestamp_heart = Long.parseLong(data[1]);
                firstDataHeart = false;
            }else{
                cur_timestamp_heart = Long.parseLong(data[1]);
            }
            long currentPosX = cur_timestamp_heart - prev_timestamp_heart;

            float currentPosY = Float.parseFloat(data[2]);

            if(cardioValues.size() == 0){
                cardioValues.add(new PointValue(currentPosX, currentPosY));
            }else{
                if(cardioValues.size()<limit) {
                    PointValue prevPoint = cardioValues.get(cardioValues.size() - 1);
                    if (currentPosX > prevPoint.getX()) {
                        if (currentPosX == 0)
                            currentPosX = 1;
                        cardioValues.add(new PointValue(currentPosX, currentPosY));
                    }
                }
            }

            if(activity!=null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (cardioValues.size() >= limit){
                            cardioValues.remove(0);
                        }

                        LineChartData data = lCardiogram.getLineChartData();
                        data.getLines().get(0).setValues(new ArrayList<>(cardioValues));

                            lCardiogram.setLineChartData(data);

                        setCardioViewport();

                        if (lCardiogram.getVisibility() == View.GONE) {
                            lCardiogram.setVisibility(View.VISIBLE);
                            tvGraphHeart.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }
    }

    private static long prev_timestamp_resp = 0;
    private static long cur_timestamp_resp = 0;

    public synchronized static void addRespRawData(final Activity activity, final String [] data){

        if (graphResp != null){

            if(firstDataResp){
                prev_timestamp_resp = Long.parseLong(data[1]);
                cur_timestamp_resp = Long.parseLong(data[1]);
                firstDataResp = false;
            }else{
                cur_timestamp_resp = Long.parseLong(data[1]);
            }
            long currentPosX = cur_timestamp_resp - prev_timestamp_resp;

            float currentPosY = Float.parseFloat(data[2]);

            if(pneumoValues.size() == 0){
                pneumoValues.add(new PointValue(currentPosX, currentPosY));
            }else{
                if(pneumoValues.size()<limit) {
                    PointValue prevPoint = pneumoValues.get(pneumoValues.size() - 1);
                    if (currentPosX > prevPoint.getX()) {
                        pneumoValues.add(new PointValue(currentPosX, currentPosY));
                    }
                }
            }

            if(activity!=null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (pneumoValues.size() >= limit){
                            pneumoValues.remove(0);
                        }

                        LineChartData lData = lPneumogram.getLineChartData();
                        lData.getLines().get(0).setValues(new ArrayList<>(pneumoValues));
                        lPneumogram.setLineChartData(lData);

                        setPneumoViewport();

                        if (lPneumogram.getVisibility() == View.GONE) {
                            lPneumogram.setVisibility(View.VISIBLE);
                            tvGraphResp.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }
    }

    /*public void clearGraphs(){
        if(graphHeart!=null && getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DataPoint[] punti = new DataPoint[0];
                    seriesHeart = new LineGraphSeries<>(punti);
                    graphHeart.removeAllSeries();
                    graphHeart.addSeries(seriesHeart);

                    graphHeart.setVisibility(View.GONE);
                    tvGraphHeart.setVisibility(View.VISIBLE);
                }
            });
        }

        if(graphResp!=null && getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DataPoint[] punti = new DataPoint[0];
                    seriesResp = new LineGraphSeries<>(punti);
                    graphResp.removeAllSeries();
                    graphResp.addSeries(seriesResp);

                    graphResp.setVisibility(View.GONE);
                    tvGraphResp.setVisibility(View.VISIBLE);
                }
            });
        }
    }*/

    public static void resetGraphs(Activity mActivity){
        if(graphHeart!=null && mActivity!=null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DataPoint[] punti = new DataPoint[0];
                    seriesHeart = new LineGraphSeries<>(punti);
                    graphHeart.removeAllSeries();
                    graphHeart.addSeries(seriesHeart);

                    graphHeart.setVisibility(View.GONE);
                    lCardiogram.setVisibility(View.GONE);
                    tvGraphHeart.setVisibility(View.VISIBLE);
                }
            });
        }

        if(graphResp!=null && mActivity!=null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DataPoint[] punti = new DataPoint[0];
                    seriesResp = new LineGraphSeries<>(punti);
                    graphResp.removeAllSeries();
                    graphResp.addSeries(seriesResp);

                    graphResp.setVisibility(View.GONE);
                    lPneumogram.setVisibility(View.GONE);
                    tvGraphResp.setVisibility(View.VISIBLE);
                }
            });
        }
    }
}
