package com.life.app.client.dashboard.fragments.radarChart.Agility;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AgilityActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 28/9/2016.
 */
public class CenterFragment extends Fragment  implements OnChartValueSelectedListener {

    ScatterChart mChart;
    TextView tvHighscore, tvJumps, tvMonthlyTrend, tvMonthlyJumps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agility_results_center, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tvHighscore     = (TextView) view.findViewById(R.id.tvHighscore);
        tvJumps         = (TextView) view.findViewById(R.id.tvJumps);
        tvMonthlyTrend  = (TextView) view.findViewById(R.id.tvMonthlyTrend);
        tvMonthlyJumps  = (TextView) view.findViewById(R.id.tvMonthlyJumps);
        mChart          = (ScatterChart) view.findViewById(R.id.scatterChart);

        tvHighscore.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvJumps.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvMonthlyTrend.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvMonthlyJumps.setTypeface(Utils.getKnockoutFont(getActivity()));


        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position);
        if(synthesis!=null){
            String placeholderScore = String.format(Locale.US, "%.1f", synthesis.get(0));
            String placeholderJumps = Math.round(synthesis.get(1)) + "";
            tvHighscore.setText(placeholderScore);
            tvJumps.setText(placeholderJumps);
        }

        setChart();
        loadDataFromSharedPref();
    }

    private void setChart(){
        /*mChart.setDescription("Sway plot");
        mChart.setDescriptionColor(ColorTemplate.rgb("#F5F5F5"));
        mChart.setDescriptionTextSize(12f);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        mChart.setMaxHighlightDistance(50f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        mChart.setMaxVisibleValueCount(600);
        mChart.setPinchZoom(true);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXOffset(5f);

        YAxis yl = mChart.getAxisLeft();
        yl.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        yl.setTextColor(ColorTemplate.rgb("#F5F5F5"));
        yl.setAxisMaxValue(5f);
        yl.setAxisMinValue(-5f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xl = mChart.getXAxis();
        xl.setDrawGridLines(false);
        xl.setTextColor(ColorTemplate.rgb("#F5F5F5"));
        xl.setAxisMaxValue(5f);
        xl.setAxisMinValue(-5f);*/

        Description descr = new Description();
        descr.setText("Jump frequency (jumps/min)");
        descr.setTextColor(GRAPHS_COLOR_WHITE);
        descr.setTextSize(DESCRIPTION_TEXT_SIZE);
        mChart.setDescription(descr);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        //mChart.setMaxHighlightDistance(50f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        mChart.setMaxVisibleValueCount(120);
        mChart.setPinchZoom(true);

        Legend l = mChart.getLegend();
        l.setDrawInside(false);
        //l.setXOffset(50f);

        YAxis yl = mChart.getAxisLeft();
        yl.setTextColor(GRAPHS_COLOR_WHITE);
        yl.setAxisMaximum(160f);
        yl.setAxisMinimum(-5f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xl = mChart.getXAxis();
        xl.setDrawGridLines(true);
        xl.setTextColor(GRAPHS_COLOR_WHITE);
        xl.setAxisMaximum(30f);
        xl.setAxisMinimum(-5f);

    }

    private void loadDataFromSharedPref(){
        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position);
        int totaljumps = 0;
        if(synthesis!=null){
            totaljumps = Math.round(synthesis.get(0));
        }

        ArrayList<Float> frequencies = new ArrayList<>();

        for(int i=0; i<totaljumps;i++){
            ArrayList<Float> frequency = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position + "_TRY_" + (i+1));
            if(frequency!=null && frequency.size()>1)
                frequencies.add(frequency.get(1));
        }

        if(frequencies.size() == 0)
            frequencies.add(0f);

        addDataToChart(frequencies);

        /*int totaljumps = Math.round(SharedPrefDataHandler.getSynthesis(RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position).get(0));

        ArrayList<ArrayList<Float>> frequencies = new ArrayList<>();

        for(int i=0; i<totaljumps;i++){
            ArrayList<Float> frequency = SharedPrefDataHandler.getSets(RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position + "_TRY_" + (i+1) + "_SET_1");
            if(frequency.size()<=1)
                frequency.add(0f);
            frequencies.add(frequency);
        }
        addDataToChart(frequencies);*/
    }

    private void addDataToChart(ArrayList<Float> frequencies){

        ArrayList<Entry> yVals1 = new ArrayList<>();

        /*for (int i = 0; i < frequencies.size(); i++) {
            for(int j = 0; j < frequencies.get(i).size(); j+=2){
                yVals1.add(new Entry(frequencies.get(i).get(j), frequencies.get(i).get(j+1)));
            }
        }*/
        for (int i = 0; i < frequencies.size(); i++) {
            yVals1.add(new Entry((i+1), frequencies.get(i)));
        }

        Collections.sort(yVals1, new EntryXComparator());

        ScatterDataSet set1 = new ScatterDataSet(yVals1, "");
        set1.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        set1.setColor(Consts.GRAPHS_COLOR_BLUE_LIGHT);
        set1.setScatterShapeSize(12f);
        set1.setDrawValues(false);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the datasets

        ScatterData data = new ScatterData(dataSets);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        mChart.setData(data);
        AgilityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChart.invalidate();
            }
        });
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
