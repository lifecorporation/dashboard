package com.life.app.client.dashboard.fragments.radarChart.Strength;

/*
 * Created by chara on 28-Nov-16.
 */

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

public class LeftFragment extends Fragment {

    TextView tvPushups, tvSynthesis, tvPushupsMin;

    LineChart mChart;

    static int nFlessioni = 0;
    static int synthesisN = 0;
    static float ritmoFlessioni = 0;

    ArrayList<Float> data = null;

    ImageButton ibInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_strength_results_left, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tvPushups    = (TextView) view.findViewById(R.id.tvPushups);
        tvSynthesis  = (TextView) view.findViewById(R.id.tvSynthesis);
        tvPushupsMin = (TextView) view.findViewById(R.id.tvPushupsMin);
        ibInfo = (ImageButton) view.findViewById(R.id.ibInfo);
        ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_synthsesis_number), getString(R.string.prompt_synthesis_number));
            }
        });

        mChart = (LineChart) view.findViewById(R.id.lineChart);

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.STRENGTH_KEY + "_" + StrengthResults.position);
        if(synthesis!=null) {
            nFlessioni = Math.round(synthesis.get(0));
            synthesisN = Math.round(synthesis.get(1));
            ritmoFlessioni = synthesis.get(2);
        }

        String placeholder_flessioni = nFlessioni + "";
        String placeholder_synthesis = synthesisN + "";
        String placeholder_pushups   = String.format(Locale.US, "%.1f", (ritmoFlessioni * 60));

        tvPushups.setText(placeholder_flessioni);
        tvSynthesis.setText(placeholder_synthesis);
        tvPushupsMin.setText(placeholder_pushups);

        tvSynthesis.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);

        data = new ArrayList<>();
        for(int i=0;i<nFlessioni;i++) {
            ArrayList<Float> tries = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.STRENGTH_KEY + "_" + StrengthResults.position + "_TRY_" + (i+1));
            if(tries!=null)
                data.add(tries.get(0));
        }

        BarChartSetup.setLineChart(mChart, true, "Quality Index Trend", 0f, 105f);
        addDataToChart();
    }

    private void addDataToChart() {

        ArrayList<Entry> entries = new ArrayList<>();

        for (int i = 0; i < data.size() ; i++) {
            entries.add(new Entry(i, data.get(i)));
        }

        LineDataSet dataset = new LineDataSet(entries, "");

        LineData data = new LineData(dataset);
        dataset.setDrawFilled(true);
        dataset.setValueTextColor(GRAPHS_COLOR_WHITE);

        mChart.setData(data);
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }
}
