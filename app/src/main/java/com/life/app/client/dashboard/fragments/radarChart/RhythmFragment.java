package com.life.app.client.dashboard.fragments.radarChart;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.RhythmActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.fragments.radarChart.Rhythm.RhythmExperience;
import com.life.app.client.dashboard.fragments.radarChart.Rhythm.RhythmResults;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 21/07/2016.
 */
public class RhythmFragment extends Fragment {

    static TextView tvHighscore, tvResult, tvUnit, tvUnitMonthly, tvHighscoreMonthly, tvResultMonthly;
    RelativeLayout back;
    Button start;

    static BarChart mChart;

    View mLeak = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rhythm, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        RhythmActivity.showTutorial();

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_rhythm));

        view.findViewById(R.id.llCenter).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.llCenterM).setVisibility(View.INVISIBLE);

        start          = (Button) view.findViewById(R.id.start);
        back           = (RelativeLayout) view.findViewById(R.id.back);
        mChart         = (BarChart) view.findViewById(R.id.barChart);

        tvHighscoreMonthly  = (TextView) view.findViewById(R.id.tvHighscoreMonthly);
        tvResultMonthly     = (TextView) view.findViewById(R.id.tvResultMonthly);

        tvHighscore    = (TextView) view.findViewById(R.id.tvHighscore);
        tvResult       = (TextView) view.findViewById(R.id.tvResult);
        tvUnit         = (TextView) view.findViewById(R.id.tvUnit);
        tvUnitMonthly  = (TextView) view.findViewById(R.id.tvUnitMonthly);

        tvHighscore.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvResult.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvHighscoreMonthly.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvResultMonthly.setTypeface(Utils.getKnockoutFont(getActivity()));

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.getStatus()!=null
                        && MainActivity.getStatus().getWearable_status()==1)
                    startExperienceFragment();
                else
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_disconnected), Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.ibInfoExp).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ibInfoExp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_description),
                        getResources().getString(R.string.hint_description_rhythm));
            }
        });
        (view.findViewById(R.id.ibInfo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_description),
                        getResources().getString(R.string.hint_description_rhythm));
            }
        });

        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        showHistoryChart();
        setClickListenermChart();
    }

    private void goBack() {
        getActivity().finish();
        getActivity().getFragmentManager().popBackStack();
    }

    public void showHistoryChart() {
        // setup barchart
        if(mChart!=null) {
            BarChartSetup.setBarChart(mChart, false, null, 0f, 100f);
            loadSharedPrefValues();
        }
    }

    private void loadSharedPrefValues() {
        final float max = SharedPrefDataHandler.getBest(getActivity(), RadarChartConst.RHYTHM_KEY).get(0);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(tvHighscore!=null) {
                    String placeholder = String.format(Locale.US, "%.1f", max);
                    tvHighscore.setText(placeholder);
                }
            }
        });

        if(mChart!=null)
            displayHistoryValues(SharedPrefDataHandler.getHistoryValuesAsList(getActivity(), RadarChartConst.RHYTHM_KEY));
    }

    static BarDataSet set1;
    private static void displayHistoryValues(List<Float> results) {

        ArrayList<BarEntry> entries = new ArrayList<>();

        if(results != null) {

            float start = 0f;

            mChart.getXAxis().setAxisMinimum(start);
            mChart.getXAxis().setAxisMaximum(6);

            int counter = RadarChartConst.MAX_EXPERIENCE_VALUES - results.size() + 1;
            for (int i = 0; i < results.size(); i++) {
                entries.add(new BarEntry(counter, results.get(i)));
                counter++;
            }
        }

        set1 = new BarDataSet(entries, "");
        set1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        set1.setHighlightEnabled(true);
        set1.setValueTextColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(12f);
        //data.setValueTypeface(mTfLight);
        data.setBarWidth(0.5f);

        mChart.setData(data);
        mChart.invalidate();
    }

    private void startExperienceFragment() {
        //Request.stopExperience(getActivity());
        Utils.stopExperience();
        getFragmentManager().beginTransaction().
                replace(R.id.rhythm_placeholder, new RhythmExperience(), "RhythmExperience").
                addToBackStack("RhythmExperience").
                commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        start   = null;
        back    = null;
        tvHighscore = null;
        mLeak   = null; // now cleaning up!
    }

    private void setClickListenermChart(){
        mChart.setHighlightPerTapEnabled(true);
        // Make sure it's clickable.
        mChart.setClickable(true);
        mChart.setTouchEnabled(true);
        mChart.setEnabled(true);
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Bundle b = new Bundle();
                b.putInt("position", 6 - (int)e.getX());
                android.support.v4.app.Fragment f = new RhythmResults();
                f.setArguments(b);
                getFragmentManager().beginTransaction().
                        replace(R.id.rhythm_placeholder, f, "RhythmResults").
                        commit();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        RhythmActivity.showTutorial();
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }
}