package com.life.app.client.dashboard.constants;

/*
 * Created by chara on 30-Jan-17.
 */

public class TAG {

    public static String _TAG_REALTIME_FRAGMENT     = "com.life.app.client.dashboard.fragments.menu.RealTimeFragment";
    public static String _TAG_REALTIME_FRAGMENT_3   = "com.life.app.client.dashboard.fragments.MainActivityRealTimeFragment";

    public static String _TAG_CALIBRATION_          = "com.life.app.client.dashboard.fragments.menu.CalibrationFragment";

    /**
     * PHYSICAL ABILITIES
     */

    public static String _TAG_REACTION_TIME_        = "com.life.app.client.dashboard.fragments.radarChart.ReactionTime.ReactionTimeExperience";
    public static String _TAG_BALANCE_              = "com.life.app.client.dashboard.fragments.radarChart.Balance.BalanceExperience";
    public static String _TAG_RHYTHM_               = "com.life.app.client.dashboard.fragments.radarChart.Rhythm.RhythmExperience";
    public static String _TAG_STRENGTH_             = "com.life.app.client.dashboard.fragments.radarChart.Strength.StrengthExperience";
    public static String _TAG_AGILITY_              = "com.life.app.client.dashboard.fragments.radarChart.Agility.AgilityExperience";
    public static String _TAG_EXPLOSIVENESS_        = "com.life.app.client.dashboard.fragments.radarChart.Explosiveness.ExplosivenessExperience";
    public static String _TAG_AEROBIC_              = "com.life.app.client.dashboard.fragments.radarChart.Aerobic.AerobicPowerExperience";
    public static String _TAG_SPEED_                = "com.life.app.client.dashboard.fragments.radarChart.Speed.SpeedExperience";
    public static String _TAG_ANAEROBIC_CAPACITY_   = "com.life.app.client.dashboard.fragments.radarChart.AnaerobicCapacity.AnaerobicCapacityExperience";
}
