package com.life.app.client.dashboard.fragments.radarChart.Agility;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AgilityActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.HorizontalProgressBar;
import com.life.app.client.dashboard.utils.Percent;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/*
 * Created by peppe on 02/08/2016.
 */
public class AgilityExperience extends Fragment implements CountDownAnimation.CountDownListener {

    RelativeLayout back;

    View mLeak = null;

    private static TextView tvJumps, tvTime, tvFrequency, tvBest;
    private static View vMax;
    private static HorizontalProgressBar progressBarTime, progressBarJumps;

    public static Activity mInstance = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agility_experience, container, false);
    }

    private static FragmentManager fragmentManager;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        mLeak = view;
        mInstance = getActivity();

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_agility));

        tvJumps          = (TextView) view.findViewById(R.id.tvJumps);
        tvBest           = (TextView) view.findViewById(R.id.tvBest);
        vMax             = view.findViewById(R.id.vMax);
        tvTime           = (TextView) view.findViewById(R.id.tvTime);
        tvFrequency      = (TextView) view.findViewById(R.id.tvFrequency);
        progressBarTime  = (HorizontalProgressBar) view.findViewById(R.id.progressBarTime);
        progressBarJumps = (HorizontalProgressBar) view.findViewById(R.id.progressBarJumps);

        float best = SharedPrefDataHandler.getBest(getActivity(), RadarChartConst.AGILITY_KEY).get(1);
        tvBest.setText(String.format(Locale.US, "%.1f", best));
        tvBest.setTypeface(Utils.getKnockoutFont(getActivity()));
        setMax(best);

        tvTime.setText("0.0");

        fragmentManager = getActivity().getSupportFragmentManager();

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        startCountDown(view);
    }

    private void goBack(){
        AgilityActivity.showTutorial();

        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0){
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        Utils.startExperience(UUID.fromString(Uuid.EX_AGILITY));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_AGILITY;
    }

    private static int progress1 = 0;
    private static Timer timer = null;
    private static void startProgressBar(){
        progress1 = 0;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                AgilityActivity.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress1 += 3;
                        if(progress1>=100){
                            progressBarTime.setCurrentValue(new Percent(100));
                        }else{
                            progressBarTime.setCurrentValue(new Percent(progress1));
                        }
                    }
                });
            }
        },100,1000);
    }

    private void setMax(float best){

        int left = (300 * ((int)best)) / RadarChartConst.MAX_JUMPS;
        int left2 = 0;
        if(left>8) left2 = left - 8;

        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(vMax.getLayoutParams());
        ViewGroup.MarginLayoutParams marginParams2 = new ViewGroup.MarginLayoutParams(tvBest.getLayoutParams());
        marginParams.setMargins(left, 0, 0, 0);
        marginParams2.setMargins(left2, 0, 0, 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(marginParams2);
        vMax.setLayoutParams(layoutParams);
        tvBest.setLayoutParams(layoutParams2);
    }

    private static ArrayList<ArrayList<Float>> totalJumps = null;

    public static void addJump(final String [] data){
        if(totalJumps==null)
            totalJumps = new ArrayList<>();

        ArrayList<Float> newJump = new ArrayList<>();
        newJump.add(Float.parseFloat(data[2]));     // jumps
        newJump.add(Float.parseFloat(data[3]));     // frequency
        newJump.add(Float.parseFloat(data[4]));     // type
        newJump.add(Float.parseFloat(data[5]));     // left arm
        newJump.add(Float.parseFloat(data[6]));     // right arm
        newJump.add(Float.parseFloat(data[7]));     // medio laterale
        newJump.add(Float.parseFloat(data[8]));     // verticale
        newJump.add(Float.parseFloat(data[9]));     // antero-posteriore

        totalJumps.add(newJump);
        addToSways();

        AgilityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvJumps.setText(data[2]);
                tvFrequency.setText(data[3]);
                progressBarJumps.setCurrentValue(new Percent(totalJumps.size()));
            }
        });
    }

    private static boolean started = false;
    public static void updateTimeLeft(final String time){
        if(AgilityActivity.getInstance()!=null) {
            AgilityActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!started) {
                        startProgressBar();
                        started = true;
                    }
                    tvTime.setText(time);
                }
            });
        }
    }

    private static void addToSways(){
        if(sways==null)
            sways = new ArrayList<>();

        if(sway!=null) {
            sways.add(sway);
            sway = new ArrayList<>();
        }
    }

    private static ArrayList<ArrayList<Float>> sways = null;
    private static ArrayList<Float> sway = null;
    public static void addSway(final String [] data){
        if(sway == null)
            sway = new ArrayList<>();
        sway.add(Float.parseFloat(data[2]));
        sway.add(Float.parseFloat(data[3]));
    }

    static ArrayList<Float> synthesis = null;
    public static void onExperienceFinished(Activity activity, final String [] data){

        started = false;
        addToSways();

        final float score = Float.parseFloat(data[2]);
        final float jumps = Float.parseFloat(data[3]);

        saveToSharedPref(activity, score, jumps);

        AgilityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Bundle b = new Bundle();
                        b.putInt("position", 1);
                        Fragment f = new AgilityResults();
                        f.setArguments(b);
                        fragmentManager.beginTransaction().
                                replace(R.id.agility_placeholder, f, f.getClass().getName()).
                                addToBackStack(f.getClass().getName()).
                                commit();
                    }
                }, 500);
            }
        });
    }

    private static void saveToSharedPref(Activity activity, float score, float jumps){
        synthesis = new ArrayList<>();
        synthesis.add(score);
        synthesis.add(jumps);

        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.AGILITY_KEY, synthesis);
        SharedPrefDataHandler.addBest(activity, RadarChartConst.AGILITY_KEY, synthesis);
        if(totalJumps!=null) {
            for (int i = 0; i < totalJumps.size(); i++) {
                SharedPrefDataHandler.addNewTry(activity, RadarChartConst.AGILITY_KEY + "_1_TRY_" + (i + 1), totalJumps.get(i));
                SharedPrefDataHandler.addNewSet(activity, RadarChartConst.AGILITY_KEY + "_1_TRY_" + (i + 1) + "_SET_1", sways.get(i));
            }

            totalJumps.clear();
            totalJumps = null;
        }

        sways.clear();
        sways = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onResume(){
        super.onResume();
        AgilityActivity.hideTutorial();
    }

    private void resetValues(){
        if(timer!=null){
            timer.cancel();
            timer = null;
        }
        progress1 = 0;
    }
}