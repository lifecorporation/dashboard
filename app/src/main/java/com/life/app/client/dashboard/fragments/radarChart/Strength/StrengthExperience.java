package com.life.app.client.dashboard.fragments.radarChart.Strength;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.StrengthActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/*
 * Created by peppe on 02/08/2016.
 */
public class StrengthExperience extends Fragment implements CountDownAnimation.CountDownListener{

    View mLeak = null;

    public static Activity mInstance = null;

    static TextView tvPushups, tvTime, tvQuality, tvHeartrate;
    static ProgressBar schienaBar, rightBar, leftBar;

    ImageButton ibInfo;

    Button bStop;

    //Variabili da mostrare
    static int sbilanciamentoBraccia = 0;
    static int angoloSchiena = 0;
    public static ArrayList<Float> armSimValues = new ArrayList<>();
    public static ArrayList<Float> BK_rolFltValues = new ArrayList<>();
    public static ArrayList<Float> qIndexValues = new ArrayList<>();

    static  Timer timer = null;

    private static int time = 0;
    private static int lastPushUpNumber =0;

    private static FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_strength_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        mInstance = getActivity();

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        StrengthActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.strength));

        fragmentManager = getActivity().getSupportFragmentManager();

        tvPushups   = (TextView) view.findViewById(R.id.tvPushups);
        tvTime      = (TextView) view.findViewById(R.id.tvTime);
        tvQuality   = (TextView) view.findViewById(R.id.tvQuality);
        tvHeartrate = (TextView) view.findViewById(R.id.tvHeartrate);

        schienaBar  = (ProgressBar) view.findViewById(R.id.progessBarUp);
        rightBar    = (ProgressBar) view.findViewById(R.id.progessBarRight);
        leftBar     = (ProgressBar) view.findViewById(R.id.progessBarLeft);

        bStop = (Button) view.findViewById(R.id.stop);

        bStop.setTypeface(Utils.getIONIconFont(getActivity()), Typeface.BOLD);
        bStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        ibInfo = (ImageButton) view.findViewById(R.id.ibInfo);
        ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog("Quality Index", getString(R.string.prompt_quality_index));
            }
        });

        RelativeLayout back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        startCountDown(view);
    }

    @Override
    public void onResume(){
        super.onResume();
        StrengthActivity.hideTutorial();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    public static void onExperienceFinished(Activity activity, String [] data){
        float nFlessioni     = Float.parseFloat(data[2]);
        float value          = Float.parseFloat(data[3]);
        float ritmoFlessioni = Float.parseFloat(data[4]);

        ArrayList<Float> synthesis = new ArrayList<>();
        synthesis.add(nFlessioni);
        synthesis.add(value);
        synthesis.add(ritmoFlessioni);

        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.STRENGTH_KEY , synthesis);
        SharedPrefDataHandler.addBest(activity, RadarChartConst.STRENGTH_KEY , synthesis);

        for(int i=0;i<Math.round(nFlessioni);i++){
            ArrayList<Float> tries = new ArrayList<>();
            tries.add(qIndexValues.get(i));
            tries.add(armSimValues.get(i));
            tries.add(BK_rolFltValues.get(i));
            SharedPrefDataHandler.addNewTry(activity, RadarChartConst.STRENGTH_KEY + "_1_TRY_" + (i+1), tries);
        }

        gotoResults();
    }

    // update heart rate textview
    public static void updateHeartRate (final String [] data) {
        try {
            if (tvHeartrate != null) {
                StrengthActivity.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String heartrate = data[2] + "";
                        tvHeartrate.setText(heartrate);
                    }
                });
            }
        }catch (ExceptionInInitializerError e){
            e.printStackTrace();
        }
    }

    private static void gotoResults(){
        Bundle b = new Bundle();
        b.putInt("position", 1);
        Fragment f = new StrengthResults();
        f.setArguments(b);
        fragmentManager.beginTransaction().
                replace(R.id.strength_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    private void goBack(){
        StrengthActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), StrengthActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private static boolean isTimerRunning = false;
    public static void startTimer(){
        if(!isTimerRunning) {
            time = 0;
            final TimerTask timerTask = new TimerTask() {
                public void run() {
                    StrengthActivity.getInstance().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            time++;
                            String str_time = time + "''";
                            tvTime.setText(str_time);
                        }
                    });
                }
            };

            StrengthActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            timer = new Timer();
                            timer.scheduleAtFixedRate(timerTask, 0, 1000);
                        }
                    }, 500);
                }
            });

            isTimerRunning = true;
        }
    }

    public static void updateViews(String [] data){
        int pushUpDone = Math.round(Float.parseFloat(data[2]));
        sbilanciamentoBraccia = Math.round(Float.parseFloat(data[3]));
        angoloSchiena = Math.round(Float.parseFloat(data[4]));

        updateSchienaBar(angoloSchiena);
        updateBracciaBar(sbilanciamentoBraccia);

        if (pushUpDone > lastPushUpNumber){
            lastPushUpNumber = pushUpDone;
            armSimValues.add(Float.parseFloat(data[3]));
            BK_rolFltValues.add(Float.parseFloat(data[4]));
            qIndexValues.add(Float.parseFloat(data[5]));
            updatePushUpViews(pushUpDone, Float.parseFloat(data[5]));
        }
    }

    private static void updateSchienaBar(final int progress){
        StrengthActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                schienaBar.setProgress(progress);
            }
        });
    }

    private static void updateBracciaBar(final int sbilanciamento){

        if (sbilanciamento <= 0) {
            // devo settarlo a 1 altrimenti non si vedono le barre verticali
            rightBar.setProgress(1);

            int progress = calcolaProgress(-sbilanciamento);
            //Log.iLog.i("updateBracciaBar","updateBracciaBar progress " + progress);
            leftBar.setProgress(progress);
        }
        else{
            // devo settarlo a 1 altrimenti non si vedono le barre verticali
            leftBar.setProgress(1);

            int progress = calcolaProgress(sbilanciamento);
            //Log.i("updateBracciaBar","updateBracciaBar progress " + progress);
            rightBar.setProgress(progress);
        }
    }

    private static int calcolaProgress(int angolo){
        int maxBar = 25;
        int maxAngolo = 25;
        return (maxBar * angolo) / maxAngolo;
    }

    private static void updatePushUpViews(final int numberOfPushUps, final float quality){
        StrengthActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {
                DecimalFormat df = new DecimalFormat("#.#");
                String qualityindex = df.format(quality);
                String pushups_str = numberOfPushUps + "";
                tvQuality.setText(qualityindex);
                tvPushups.setText(pushups_str);
            }
        });
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {

        qIndexValues = new ArrayList<>();
        BK_rolFltValues = new ArrayList<>();
        armSimValues = new ArrayList<>();

        //Request.startExperience(getActivity(), UUID.fromString(Uuid.EX_STRENGTH));
        Utils.startExperience(UUID.fromString(Uuid.EX_STRENGTH));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_STRENGTH;
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(infoDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

    private void resetValues(){
        lastPushUpNumber = 0;
        isTimerRunning = false;

        if(timer!=null) {
            timer.purge();
            timer.cancel();
        }
    }
}