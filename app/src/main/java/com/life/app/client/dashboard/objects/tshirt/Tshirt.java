package com.life.app.client.dashboard.objects.tshirt;

/*
 * Created by chara on 16-Jan-17.
 */

import android.util.Log;

import com.life.services.bind.assets.Node;

import java.util.ArrayList;

public class Tshirt {

    private ArrayList<Node> nodes = null;

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public String toString(){
        if(this.getNodes() == null)
            return "";

        String output = "";
        for(int i=0;i<this.getNodes().size();i++) {
            output += "\n=====SENSOR=====" + "\n";
            output += "Node[" + i + "] : " + "\n"
                    + "\t" + "id : " + this.getNodes().get(i).getId() + "\n"
                    + "\t" + "sensors : " + this.getNodes().get(i).getSensorsNumber() + "\n";
            for(int k=0;k<this.getNodes().get(i).getSensorsList().size();k++){
                output += "\n" + "Sensor[" + k + "]" + "\n"
                    + "\t" + "id : " + this.getNodes().get(i).getSensorsList().get(k).getId() + "\n"
                    + "\t" + "type : " + this.getNodes().get(i).getSensorsList().get(k).getType() + "\n"
                    + "\t" + "position : " + this.getNodes().get(i).getSensorsList().get(k).getPosition() + "\n";
            }
        }
        Log.i("ASSET",output);
        return output;
    }

    public boolean getSS1(){
        if(this.getNodes() == null) {
            return false;
        }

        for(Node node: this.getNodes()){
            if(node.getId() == 1){
                return true;
            }
        }
        return false;
    }

    public boolean getSS3(){
        if(this.getNodes() == null)
            return false;

        for(Node node: this.getNodes()){
            if(node.getId() == 3)
                return true;
        }

        return false;
    }

    public boolean getSS5(){
        if(this.getNodes() == null)
            return false;

        for(Node node: this.getNodes()){
            if(node.getId() == 5)
                return true;
        }

        return false;
    }
}
