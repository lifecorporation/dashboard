package com.life.app.client.dashboard.activities.settings;

/*
 * Created by chara on 20-Feb-17.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.adapters.WifiAdapter;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.objects.WiFiFoundNetwork;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

public class WifiActivity extends MyFragmentActivity{

    private static SwitchCompat scSwitch;
    private static ListView lvWifis;
    ArrayList<WiFiFoundNetwork> mWiFis = new ArrayList<>();
    WifiAdapter wAdapter;
    SharedPreferences sharedpreferences;
    private static AVLoadingIndicatorView avLoader = null;
    private static Activity mActivity = null;

    private static boolean firstupdate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings_wifi);

        sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);

        avLoader = (AVLoadingIndicatorView)findViewById(R.id.avLoader);

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        scSwitch = (SwitchCompat) findViewById(R.id.scSwitch);
        scSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    Utils.switchWifiON();
                else
                    Utils.switchWifiOFF();
            }
        });

        wAdapter = new WifiAdapter(WifiActivity.this, R.layout.custom_listview_raw_text, mWiFis);

        lvWifis  = (ListView) findViewById(R.id.lvWifis);
        lvWifis.setAdapter(wAdapter);
        lvWifis.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(BTConnectionManagerService.getWiFiStatus().getSsid() == null || BTConnectionManagerService.getWiFiStatus().getSsid().trim().isEmpty() ) {
                    showConnectDialog(i);
                }else{

                    Log.i("connected", BTConnectionManagerService.getWiFiStatus().getSsid().substring(1, BTConnectionManagerService.getWiFiStatus().getSsid().length()-1));
                    Log.i("mWifiList", mWiFis.get(i).getSsid());

                    String connected = BTConnectionManagerService.getWiFiStatus().getSsid().substring(1, BTConnectionManagerService.getWiFiStatus().getSsid().length()-1);

                    if(mWiFis.get(i).getSsid().equals(connected))
                        showWifiDialog(BTConnectionManagerService.getWiFiStatus().getSsid(),
                                BTConnectionManagerService.getWiFiStatus().getIp(),
                                BTConnectionManagerService.getWiFiStatus().getMode());
                    else
                        showConnectDialog(i);
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        mActivity = WifiActivity.this;

        if(MainActivity.getStatus() == null){

            scSwitch.setEnabled(false);

            Toast.makeText(mActivity, getResources().getString(R.string.warning_device_not_found), Toast.LENGTH_LONG).show();
        }else{
            lvWifis.setVisibility(View.VISIBLE);
        }

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        firstupdate = true;
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    public void enable(){

        if(scSwitch == null)
            return;

        if(BTConnectionManagerService.WIFI_SWITCHED!=0x00)
            return;

        if(firstupdate)
            firstupdate = false;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(BTConnectionManagerService.getWiFiStatus()!=null){
                    if(BTConnectionManagerService.getWiFiStatus().getEnabled()){
                        Utils.scanWifi();
                        scSwitch.setChecked(true);
                    }else{
                        hideWifiList();
                        scSwitch.setChecked(false);
                    }
                }
            }
        });
    }

    private static void hideWifiList(){
        lvWifis.setVisibility(View.GONE);
    }

    private void update(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWiFis.clear();
                mWiFis.addAll(BTConnectionManagerService.getWiFiList());
                wAdapter.notifyDataSetChanged();
            }
        });
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){

            if (intent.getAction().equals(Consts.SERVICE_KEY)){

                if(intent.hasExtra(Consts.PREFERENCE_KEY_STATUS)) {

                    /*if(intent.getExtras().get(Consts.PREFERENCE_KEY_STATUS).equals(Consts.PREFERENCE_KEY_STATUS_OFF)){
                        deviceOFF();
                    }else{
                        if(MainActivity.getStatus().getWifi_status() == 1)
                            wifiON();
                        else
                            wifiOFF();
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_STATUS);*/
                }else if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {

                    isTimerBlocked = false;

                    if(avLoader!=null && avLoader.isShown())
                        avLoader.smoothToHide();

                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()) {
                        case "48":
                            Toast.makeText(WifiActivity.this, "" + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString(),
                                Toast.LENGTH_LONG ).show();
                            Log.i("WIFIACTIVITY", "(ERROR) SCAN");
                            break;
                        case "49":
                            Toast.makeText(WifiActivity.this, "" + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString(),
                                    Toast.LENGTH_LONG ).show();
                            Log.i("WIFIACTIVITY", "(ERROR) CONNECT");
                            break;
                        case "50":
                            Toast.makeText(WifiActivity.this, "" + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString(),
                                    Toast.LENGTH_LONG ).show();
                            Log.i("WIFIACTIVITY", "(ERROR) DISCONNECT");
                            break;
                        case "51":
                            Toast.makeText(WifiActivity.this, "" + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString(),
                                    Toast.LENGTH_LONG ).show();
                            Log.i("WIFIACTIVITY", "(ERROR) WIFI STATUS");
                            break;
                        case "55":
                            //Switch Wifi ON or OFF
                            Toast.makeText(WifiActivity.this, getResources().getString(R.string.warning_device_check), Toast.LENGTH_LONG).show();
                            BTConnectionManagerService.WIFI_SWITCHED = 0x00;
                            Log.i("WIFIACTIVITY", "(ERROR) SWITCHING WIFI");

                            break;
                        default:
                            Toast.makeText(WifiActivity.this, getResources().getString(R.string.error_sync_status), Toast.LENGTH_LONG).show();
                            break;
                    }

                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);

                    if(avLoader!=null && avLoader.isShown())
                        avLoader.smoothToHide();

                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "48" :
                            // SCAN WIFI
                            // 0x00 WiFi ON but not connected to a WiFi
                            // 0x01 WiFi ON and connected to WiFi
                            // 0x02 WiFi OFF

                            Log.i("WIFIACTIVITY", "SCAN");

                            update();
                            break;
                        case "49" :
                            //CONNECTED TO WIFI
                            Utils.requestWifiStatus();
                            Log.i("WIFIACTIVITY", "CONNECT");
                            break;
                        case "50" :
                            //DISCONNECT WIFI
                            clearWiFiPreferences(intent, sharedpreferences.edit());
                            Utils.requestWifiStatus();
                            Log.i("WIFIACTIVITY", "DISCONNECT");
                            Toast.makeText(WifiActivity.this, getString(R.string.prompt_wifi_disconnected), Toast.LENGTH_LONG).show();
                            break;
                        case "51" :
                            // WIFI STATUS
                            enable();
                            if(isTimerBlocked)
                                isTimerBlocked = false;

                            Log.i("WIFIACTIVITY", "WIFI STATUS");

                            break;
                        case "55":
                            // WiFi switched ON
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    BTConnectionManagerService.WIFI_SWITCHED = 0x00;
                                    Utils.requestWifiStatus();
                                }
                            }, 2000);
                            Log.i("WIFIACTIVITY", "SWITCHING WIFI");
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    private void clearWiFiPreferences(Intent intent, SharedPreferences.Editor edit){
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_SSID))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_SSID);
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_IP))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_IP);
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_MODE))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_MODE);

        edit.putString(Consts.PREFERENCE_KEY_WIFI_SSID, null);
        edit.putString(Consts.PREFERENCE_KEY_WIFI_IP, null);
        edit.putString(Consts.PREFERENCE_KEY_WIFI_MODE, null);
        edit.apply();
    }

    private void initiateWiFiPrefrences(Intent intent, SharedPreferences.Editor edit){
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_SSID))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_SSID, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_SSID).toString());
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_IP))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_IP, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_IP).toString());
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_MODE))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_MODE, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_MODE).toString());
        edit.apply();
    }

    private static AlertDialog wifiDialog = null;
    private void showWifiDialog(String ssid, String ip, String mode){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(WifiActivity.this);
        final View dialogView = View.inflate(WifiActivity.this, R.layout.dialog_wifi_details, null);
        dialogBuilder.setView(dialogView);
        if(ssid!=null)
            dialogBuilder.setMessage(ssid.substring(1, BTConnectionManagerService.getWiFiStatus().getSsid().length()-1));
        dialogBuilder.setCancelable(false);

        TextInputLayout tlIp   = (TextInputLayout) dialogView.findViewById(R.id.tvIp);
        TextInputLayout tlMode = (TextInputLayout) dialogView.findViewById(R.id.tvMode);

        if (tlIp.getEditText()!=null && ip != null)
            tlIp.getEditText().setText(ip);
        if (tlMode.getEditText()!=null && mode != null)
            tlMode.getEditText().setText(mode);

        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_disconnect), null);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        wifiDialog = dialogBuilder.create();
        wifiDialog.show();
        wifiDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isTimerBlocked = true;
                Utils.disconnectFromWifi();
                wifiDialog.dismiss();
            }
        });
    }

    private static AlertDialog connectDialog = null;
    private void showConnectDialog(final int position){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(WifiActivity.this);
        final View dialogView = View.inflate(WifiActivity.this, R.layout.dialog_wifi_connect, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(mWiFis.get(position).getSsid());
        dialogBuilder.setCancelable(false);

        final EditText mEtPassword = (EditText)dialogView.findViewById(R.id.etPassword);

        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_connect), null);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), null);
        dialogBuilder.setNeutralButton(getResources().getString(R.string.dialog_button_clear), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        connectDialog = dialogBuilder.create();
        connectDialog.show();
        connectDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEtPassword.getText().toString().trim().isEmpty()) {
                    mEtPassword.setError(getResources().getString(R.string.error_password_empty));
                }else{
                    isTimerBlocked = true;

                    Utils.connectToWifi(mWiFis.get(position).getSsid(), mEtPassword.getText().toString());

                    connectDialog.dismiss();

                    avLoader.smoothToShow();
                }
            }
        });

        connectDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEtPassword.getText().clear();
            }
        });
    }

    public static void stopWifi(Activity mActivity){

        if(scSwitch == null)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(avLoader!=null && avLoader.isShown())
                    avLoader.smoothToHide();

                scSwitch.setEnabled(false);

                lvWifis.setVisibility(View.GONE);

                if(connectDialog!=null && connectDialog.isShowing())
                    connectDialog.dismiss();

                if(wifiDialog!=null && wifiDialog.isShowing())
                    wifiDialog.dismiss();

                stopWifiUpdateTimer();
            }
        });

        firstupdate = true;
    }

    public static void startWifi(){

        if(mActivity == null)
            return;

        if(!firstupdate)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                scSwitch.setEnabled(true);

                lvWifis.setVisibility(View.VISIBLE);

                if(avLoader!=null && !avLoader.isShown())
                        avLoader.smoothToShow();

                if(BTConnectionManagerService.WIFI_SWITCHED == 0x00)
                    Utils.requestWifiStatus();

                startWifiUpdateTimer();
            }
        });
    }

    static Timer updateWifiTimer = null;
    private static boolean isTimerBlocked = false;
    //method to start BT connection thread
    private static void startWifiUpdateTimer() {

        if(mActivity == null)
            return;

        if (updateWifiTimer != null){
            //if (BuildConfig.DEBUG)
            //Log.e("provaAlarmBTConnection", "non era stato resettato il timer");
            updateWifiTimer.cancel();
            updateWifiTimer.purge();
            updateWifiTimer = null;
        }

        //Declare the timer
        updateWifiTimer = new Timer();
        //Set the schedule function and rate
        updateWifiTimer.scheduleAtFixedRate(new TimerTask() {
                                                @Override
                                                public void run() {
                                                    if(MainActivity.getStatus()!=null &&
                                                            BTConnectionManagerService.WIFI_SWITCHED == 0x00 && !isTimerBlocked) {
                                                        Utils.requestWifiStatus();
                                                    }
                                                }
                                            },1000,      //Set how long before to start calling the TimerTask (in milliseconds)
                7000);  //Set the amount of time between each execution (in milliseconds)
    }

    //method to stop BT connection thread
    private static void stopWifiUpdateTimer(){
        if(updateWifiTimer != null) {
            //if (BuildConfig.DEBUG)
            // Log.i("provaAlarmBTConnection", "elimino timer  " + btConnectionTimer.toString());
            updateWifiTimer.cancel();
            updateWifiTimer.purge();
            updateWifiTimer = null;
        }
    }
}
