package tutorial;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.Consts;

import java.util.ArrayList;

/*
 * Created by chara on 15-Dec-16.
 */

public class IntroFragmentPage2 extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tutorial_fragment_2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        PieChart pChart = (PieChart) view.findViewById(R.id.pcIndex);

        Description description = new Description();
        description.setText("");
        pChart.setDescription(description);

        pChart.setDrawHoleEnabled(false);
        pChart.setDrawMarkerViews(false);
        pChart.getLegend().setEnabled(false);
        pChart.setRotationEnabled(false);

        setDataForPieChart(pChart, 4);

        DecoView dvIntensity = (DecoView) view.findViewById(R.id.dvIntensity);

        SeriesItem seriesItemIntBackground = new SeriesItem.Builder(ContextCompat.getColor(getActivity(), R.color.grey_light_hlf))
                .setRange(0, 100, 0)
                .setLineWidth(6f)
                .build();

        SeriesItem seriesItemInt = new SeriesItem.Builder(ContextCompat.getColor(getActivity(), R.color.life_blue_normal))
                .setShadowSize(30f)
                .setRange(0, 100, 0)
                .setLineWidth(4f)
                .build();

        int serieIntIndexBackground = dvIntensity.addSeries(seriesItemIntBackground);
        dvIntensity.addEvent(new DecoEvent.Builder(100).setIndex(serieIntIndexBackground).setDelay(0).build());
        int serieIntIndex = dvIntensity.addSeries(seriesItemInt);

        dvIntensity.configureAngles(180, 0);
        dvIntensity.addEvent(new DecoEvent.Builder(67).setIndex(serieIntIndex).setDelay(0).build());
    }

    // we're going to display pie chart for school attendance
    private static int[] yValues = {10, 10, 10, 10, 10, 10};

    private static void setDataForPieChart(PieChart mChart, int status) {
        ArrayList<PieEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < yValues.length; i++)
            yVals1.add(new PieEntry(yValues[i], i));

        // create pieDataSet
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setDrawValues(false);
        dataSet.setValueLineWidth(5f);
        //dataSet.setValueLineColor(colors_state.get(status)[5]);

        // adding colors
        ArrayList<Integer> colors = new ArrayList<>();

        // Added My Own colors
        for (int c : Consts.colors_state.get(status))
            colors.add(c);


        dataSet.setColors(colors);

        //  create pie data object and set xValues and yValues and set it to the pieChart
        PieData data = new PieData(dataSet);
        //   data.setValueFormatter(new DefaultValueFormatter());
        //   data.setValueFormatter(new PercentFormatter());

        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        mChart.clear();
        mChart.setData(data);
        mChart.highlightValues(null);
        // refresh/update pie chart
        mChart.invalidate();
        // animate piechart
        mChart.animateY(0);
    }
}
