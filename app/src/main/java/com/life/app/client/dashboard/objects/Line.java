package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 23-Nov-16.
 */

public class Line {

    private int startX = 0;
    private int startY = 0;
    private int endX   = 0;
    private int endY   = 0;

    public Line(int startX, int startY, int endX, int endY){
        this.setStartX(startX);
        this.setStartY(startY);
        this.setEndX(endX);
        this.setEndY(endY);
    }

    public int getStartX() {
        return startX;
    }

    private void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    private void setStartY(int startY) {
        this.startY = startY;
    }

    public int getEndX() {
        return endX;
    }

    private void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return endY;
    }

    private void setEndY(int endY) {
        this.endY = endY;
    }
}
