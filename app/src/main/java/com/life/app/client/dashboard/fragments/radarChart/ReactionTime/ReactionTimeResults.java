package com.life.app.client.dashboard.fragments.radarChart.ReactionTime;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.ReactionTimeActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 26/9/2016.
 */
public class ReactionTimeResults extends Fragment {

    View mLeak = null;
    TextView tvBestTime, tvSynthesis;
    RelativeLayout back;

    BarChart mChart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reaction_time_results, container, false);
    }

    int position = 0;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.hint_reaction_time_title));

        tvBestTime  = (TextView) view.findViewById(R.id.tvBestTime);
        tvSynthesis = (TextView) view.findViewById(R.id.tvSynthesis);
        back        = (RelativeLayout) view.findViewById(R.id.back);
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        tvBestTime.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvSynthesis.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        mChart = (BarChart) view.findViewById(R.id.barChart);

        BarChartSetup.setBarChart(mChart, false, null, 0f, 3f);

        ArrayList<Float> attempts = new ArrayList<>();
        ArrayList<Float> try1 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.REACTION_TIME_KEY + "_" + position + "_TRY_1");
        ArrayList<Float> try2 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.REACTION_TIME_KEY + "_" + position + "_TRY_2");
        ArrayList<Float> try3 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.REACTION_TIME_KEY + "_" + position + "_TRY_3");

        if(try1!=null)
            attempts.add(try1.get(0));
        if(try2!=null)
            attempts.add(try2.get(0));
        if(try3!=null)
            attempts.add(try3.get(0));

        addDataToChart(mChart, attempts);

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.REACTION_TIME_KEY + "_" + position);

        if(synthesis!=null) {

            String placeholderSynt = Float.toString(synthesis.get(0));
            String placeholderScor = Float.toString(synthesis.get(1));

            tvSynthesis.setText(placeholderSynt);
            tvBestTime.setText(placeholderScor);
        }
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), ReactionTimeActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private void addDataToChart(BarChart chart, ArrayList<Float> results) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(4);

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = results.size()-1; i >=0 ; i--)
            yVals1.add(new BarEntry(i+1, results.get(i)/1000));

        BarDataSet set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(false);
        set1.setValueTextColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(14f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.7f);

        chart.setData(data);
    }

    @Override
    public void onResume(){
        super.onResume();
        ReactionTimeActivity.hideTutorial();
    }
}
