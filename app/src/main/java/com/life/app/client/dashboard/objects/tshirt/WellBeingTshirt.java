package com.life.app.client.dashboard.objects.tshirt;

/*
 * Created by chara on 16-Jan-17.
 */


import com.life.services.bind.assets.Node;

import java.util.ArrayList;

public class WellBeingTshirt extends Tshirt {

    public WellBeingTshirt(ArrayList<Node> nodes){
        super.setNodes(nodes);
    }

    public boolean getSSH(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 17)
                return true;
        }

        return false;
    }

    public boolean getSSA(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 10)
                return true;
        }

        return false;
    }

    public boolean getSS6(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 6)
                return true;
        }

        return false;
    }

    public boolean getSS7(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 7)
                return true;
        }

        return false;
    }

    public boolean getSS8(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 8)
                return true;
        }

        return false;
    }

    public boolean getSS9(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 9)
                return true;
        }

        return false;
    }
}
