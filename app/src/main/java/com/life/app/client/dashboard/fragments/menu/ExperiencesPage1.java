package com.life.app.client.dashboard.fragments.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.PhysicalAbilitiesActivity;

/*
 * Created by chkalog on 30/9/2016.
 */
public class ExperiencesPage1 extends Fragment implements View.OnClickListener{

    ImageButton ibPhysical, ibRunning, ibCycling, ibCore;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_experiences_page1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ibPhysical = (ImageButton) view.findViewById(R.id.ibPhysical);
        ibRunning  = (ImageButton) view.findViewById(R.id.ibRunning);
        ibCycling  = (ImageButton) view.findViewById(R.id.ibCycling);
        ibCore     = (ImageButton) view.findViewById(R.id.ibCore);

        ibPhysical.setOnClickListener(this);
        ibRunning.setOnClickListener(this);
        ibCycling.setOnClickListener(this);
        ibCore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.ibPhysical :
                intent = new Intent(getActivity(), PhysicalAbilitiesActivity.class);
                startActivity(intent);
                break;
            case R.id.ibRunning :
                launchRNB();
                break;
            case R.id.ibCycling :
                launchRNB();
                break;
            case R.id.ibCore :
                Toast.makeText(getActivity(), "Core Stability",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void launchRNB(){
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(getResources().getString(R.string.package_runandbike));
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_run_and_bike_missing),
                    Toast.LENGTH_LONG).show();
        }
    }
}
