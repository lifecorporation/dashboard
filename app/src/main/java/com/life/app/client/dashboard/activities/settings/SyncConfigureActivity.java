package com.life.app.client.dashboard.activities.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.utils.Utils;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 2/9/2016.
 */
public class SyncConfigureActivity extends MyFragmentActivity {

    TextInputLayout tvAutomatic, tvConnectionType, tvPowerStatus, tvBatteryLimit, tvTimeout;

    private boolean automatic = false;
    private String connection = null, powersupply = null;
    private int connection_id = 0, powersupply_id = 0;
    private int batterylimit = 10;
    private int timeout = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings_sync_configure);

        ((TextView)findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_sync_configuration));

        if(connection == null) {
            connection = getResources().getString(R.string.hint_wifi);
            connection_id = R.id.rbWifi;
        }

        if(powersupply == null) {
            powersupply = getResources().getString(R.string.hint_charging);
            powersupply_id = R.id.rbCharging;
        }

        tvAutomatic      = (TextInputLayout) findViewById(R.id.tvAutomatic);
        tvConnectionType = (TextInputLayout) findViewById(R.id.tvConnectionType);
        tvPowerStatus    = (TextInputLayout) findViewById(R.id.tvPowerStatus);
        tvBatteryLimit   = (TextInputLayout) findViewById(R.id.tvBatteryLimit);
        tvTimeout        = (TextInputLayout) findViewById(R.id.tvTimeout);

        String placeHolderAutomatic = automatic+"";
        String placeHolderBattery   = batterylimit + "%";
        String placeHolderTimeout   = timeout + getResources().getString(R.string.prompt_second);

        if(tvAutomatic.getEditText()!=null)         tvAutomatic.getEditText().setText(placeHolderAutomatic);
        if(tvConnectionType.getEditText()!=null)    tvConnectionType.getEditText().setText(connection);
        if(tvPowerStatus.getEditText()!=null)       tvPowerStatus.getEditText().setText(powersupply);
        if(tvBatteryLimit.getEditText()!=null)      tvBatteryLimit.getEditText().setText(placeHolderBattery);
        if(tvTimeout.getEditText()!=null)           tvTimeout.getEditText().setText(placeHolderTimeout);

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibSave)).setVisibility(View.VISIBLE);
        (findViewById(R.id.ibSave)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
    }

    private void save(){

        if(MainActivity.getStatus() == null){
            Toast.makeText(SyncConfigureActivity.this, getResources().getString(R.string.warning_device_not_found), Toast.LENGTH_LONG).show();
        }else if(tvTimeout.getEditText()!=null &&
                tvTimeout.getEditText().getText().toString().equals("0")){
            Toast.makeText(SyncConfigureActivity.this, getString(R.string.error_sync_timeout), Toast.LENGTH_LONG).show();
        }else {
            byte automatic_byte = 0x00;
            byte connection_byte = 0x00;
            byte power_byte = 0x00;

            if (automatic) automatic_byte = 0x01;

            if (connection.equals(getString(R.string.hint_4G))) connection_byte = 0x01;
            else if (connection.equals(getString(R.string.hint_both))) connection_byte = 0x02;

            if (powersupply.equals(getString(R.string.hint_battery))) power_byte = 0x01;

            if(batterylimit < 0x10){
                Toast.makeText(SyncConfigureActivity.this, getResources().getString(R.string.warning_sync_battery_limit), Toast.LENGTH_LONG).show();
            }else{
                Utils.configureSync(automatic_byte, connection_byte, power_byte, batterylimit, timeout);
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC) != null) {

                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC).toString()){
                        case "100" :

                            Toast.makeText(SyncConfigureActivity.this, getResources().getString(R.string.error_sync_configure), Toast.LENGTH_LONG).show();
                            break;
                    }


                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG_SYNC);
                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC)){
                    //sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC).toString()){
                        case "100" :

                            Toast.makeText(SyncConfigureActivity.this, getResources().getString(R.string.success_sync_configuration), Toast.LENGTH_SHORT).show();

                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC);
                }
            }
        }
    };

    public void setAutomatic(View v){
        if(tvAutomatic.getEditText()!=null)
            tvAutomatic.getEditText().requestFocus();
        tvAutomatic.getEditText().getBackground().setColorFilter(ContextCompat.getColor(SyncConfigureActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_automatic, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_automatic));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String placeHolder = automatic + "";

                tvAutomatic.getEditText().getBackground().clearColorFilter();
                tvAutomatic.getEditText().clearFocus();
                tvAutomatic.getEditText().setText(placeHolder);
                dialogInterface.dismiss();
            }
        });

        ((SwitchCompat)dialogView.findViewById(R.id.switchView)).setChecked(automatic);
        ((SwitchCompat)dialogView.findViewById(R.id.switchView)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                automatic = b;
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setConnection(View v){
        if(tvConnectionType.getEditText()!=null)
            tvConnectionType.getEditText().requestFocus();
        tvConnectionType.getEditText().getBackground().setColorFilter(ContextCompat.getColor(SyncConfigureActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_connection, null);

        dialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_connectiontype));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvConnectionType.getEditText().setText(connection);
                tvConnectionType.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                connection = ((RadioButton)dialogView.findViewById(i)).getText().toString();
                connection_id = i;
            }
        });

        if(connection==null) {
            ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).check(0);
            connection = getResources().getString(R.string.hint_wifi);
        }else{
            ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).check(connection_id);
        }

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setPowerSupply(View v){
        if(tvPowerStatus.getEditText()!=null)
            tvPowerStatus.getEditText().requestFocus();
        tvPowerStatus.getEditText().getBackground().setColorFilter(ContextCompat.getColor(SyncConfigureActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_power, null);

        dialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_powerstatus));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvPowerStatus.getEditText().setText(powersupply);
                tvPowerStatus.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        ((RadioGroup)dialogView.findViewById(R.id.rgPower)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                powersupply = ((RadioButton)dialogView.findViewById(i)).getText().toString();
                powersupply_id = i;
            }
        });

        if(powersupply==null) {
            ((RadioGroup)dialogView.findViewById(R.id.rgPower)).check(0);
            powersupply = getResources().getString(R.string.hint_wifi);
        }else{
            ((RadioGroup)dialogView.findViewById(R.id.rgPower)).check(powersupply_id);
        }

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setBatteryLimit(View v){
        if(tvBatteryLimit.getEditText()!=null)
            tvBatteryLimit.getEditText().requestFocus();
        tvBatteryLimit.getEditText().getBackground().setColorFilter(ContextCompat.getColor(SyncConfigureActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_battery, null);

        dialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        final SeekBar seekBar = (SeekBar) dialogView.findViewById(R.id.seekBar1);
        final TextView textView = (TextView) dialogView.findViewById(R.id.textView1);
        // Initialize the textview with '0'.

        String placeHolder = batterylimit + "%";
        textView.setText(placeHolder);
        seekBar.setProgress(batterylimit);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                batterylimit = seekBar.getProgress();

                String placeHolder = batterylimit + "%";

                textView.setText(placeHolder);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_powerstatus));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                String placeHolder = seekBar.getProgress() + "%";

                tvBatteryLimit.getEditText().setText(placeHolder);
                tvBatteryLimit.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setTimeout(View v){
        if(tvTimeout.getEditText()!=null)
            tvTimeout.getEditText().requestFocus();
        tvTimeout.getEditText().getBackground().setColorFilter(ContextCompat.getColor(SyncConfigureActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_timeout, null);

        dialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        final TextInputLayout tilTimeout = (TextInputLayout) dialogView.findViewById(R.id.tilTimeout);

        if(tilTimeout.getEditText()!=null) {
            tilTimeout.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    tilTimeout.setError(null);
                    return true;
                }
            });
        }

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.dialog_title_timeout));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.ok), null);

        final AlertDialog b = dialogBuilder.create();
        b.show();

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tilTimeout.getEditText()!=null){

                    if(tilTimeout.getEditText().getText().toString().equals("0")){
                        tilTimeout.setError(getString(R.string.error_sync_timeout));
                    }else{
                        if(tilTimeout.getEditText().getText().toString().trim().isEmpty())
                            timeout = 10;
                        else
                            timeout = Integer.parseInt(tilTimeout.getEditText().getText().toString());

                        String placeHolder = timeout + getResources().getString(R.string.prompt_second);

                        tvTimeout.getEditText().setText(placeHolder);
                        tvTimeout.getEditText().clearFocus();
                        b.dismiss();
                    }
                }else{
                    b.dismiss();
                }
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
