package com.life.app.client.dashboard.fragments.radarChart.AnaerobicCapacity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AnaerobicCapacityActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.HorizontalProgressBar;
import com.life.app.client.dashboard.utils.Percent;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;


/*
 * Created by peppe on 01/08/2016.
 */
public class AnaerobicCapacityExperience extends Fragment implements CountDownAnimation.CountDownListener{

    View mLeak = null;

    public static Activity mInstance = null;

    private static TextView tvInterval, tvTargetSpeed, tvMySpeed, tvHeartRate;

    private static LineChart mChart;

    private static HorizontalProgressBar progressBar;

    private static ArrayList<Float> synthesis = null;
    private static ArrayList<ArrayList<Float>> tries = null;
    private static ArrayList<Float> triesResults = null;

    static int tryNumber = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_anaerobic_capacity_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        mInstance = getActivity();

        fragmentManager = getChildFragmentManager();

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_anaerobic_capacity));

        mChart        = (LineChart) view.findViewById(R.id.lineChart);
        tvInterval    = (TextView) view.findViewById(R.id.tvInterval);
        tvTargetSpeed = (TextView) view.findViewById(R.id.tvTargetSpeed);
        tvMySpeed     = (TextView) view.findViewById(R.id.tvMySpeed);
        tvHeartRate   = (TextView) view.findViewById(R.id.tvHeartRate);
        progressBar   = (HorizontalProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setCurrentValue(new Percent(0));

        tvInterval.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvTargetSpeed.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvMySpeed.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvHeartRate.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);

        RelativeLayout back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        tries = new ArrayList<>();

        BarChartSetup.setLineChart(mChart, false, null, 0f, 2200f);

        startCountDown(view);
    }

    private void goBack(){
        AnaerobicCapacityActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), AnaerobicCapacityActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    public static void updateViews(final String [] data){

        final int phase    = Math.round(Float.parseFloat(data[2]));
        final int interval = Math.round(Float.parseFloat(data[3]));
        final float targetSpeed  = Float.parseFloat(data[4]);
        final float speedCheck = Float.parseFloat(data[5]);

        //if(interval>tryNumber)
        //    updateGraph(data);

        //Log.i("Anaerobic","ricevuti dati update phase: " + phase + "   intervallo: " + interval + "   speed: " + speed + "   speedCheck: " + speedCheck);

        AnaerobicCapacityActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {

                /*
                ArrayList<Float> newTry = new ArrayList<>();
                newTry.add(Float.parseFloat(data[2]));
                newTry.add(Float.parseFloat(data[3]));
                newTry.add(Float.parseFloat(data[4]));
                newTry.add(Float.parseFloat(data[5]));

                tries.add(newTry);
                */

                tvInterval.setText("" + interval);
                tvMySpeed.setText("" + speedCheck);
                tvTargetSpeed.setText("" + targetSpeed);

                progressBar.setCurrentValue(new Percent(interval * 10));
            }
        });
    }
;

    // update graph
    public static void updateGraph(final String [] data){
        tryNumber++;

        if (triesResults == null){
            triesResults = new ArrayList<>();
        }

        final float averageHeartRate = Float.parseFloat(data[2]);
        final float targetSpeed      = Float.parseFloat(data[3]);
        float flag                   = Float.parseFloat(data[4]);

        triesResults.add(averageHeartRate);
        triesResults.add(targetSpeed);
        triesResults.add(flag);

        ArrayList<Float> newtry = new ArrayList<>();
        newtry.add(averageHeartRate);
        newtry.add(targetSpeed);
        newtry.add(flag);

        tries.add(newtry);

        addDataToChart(averageHeartRate, targetSpeed);
    }

    // update heart rate textview
    public static void updateHeartRate (final String [] data) {
        if (tvHeartRate != null){
            AnaerobicCapacityActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String heartrate = data[2] + "";
                    tvHeartRate.setText(heartrate);
                }
            });
        }
    }

    public static void onExperienceFinished(Activity activity, String [] data){
        float score = Float.parseFloat(data[2]);
        float speedTreshold = Float.parseFloat(data[3]);

        if(synthesis==null)
            synthesis = new ArrayList<>();
        synthesis.add(score);
        synthesis.add(speedTreshold);

        saveToSharedPref(activity);
        goToResults();
    }

    private static void saveToSharedPref(Activity activity){
        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY, synthesis);
        SharedPrefDataHandler.addBest(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY, synthesis);
        for(int i=0;i < tries.size();i++){
            SharedPrefDataHandler.addNewTry(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_1_TRY_" + (i+1), tries.get(i));
            //SharedPrefDataHandler.addNewSet(RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_1_TRY_" + (i+1) + "_SET", triesResults);
        }
    }

    private static FragmentManager fragmentManager;
    private static void goToResults(){
        AnaerobicCapacityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Bundle b = new Bundle();
                        b.putInt("position", 1);
                        Fragment f = new AnaerobicCapacityResults();
                        f.setArguments(b);
                        fragmentManager.beginTransaction().
                                replace(R.id.anaerobic_capacity_placeholder, f, f.getClass().getName()).
                                addToBackStack(f.getClass().getName()).
                                commit();
                    }
                }, 500);
            }
        });
    }

    private static void addDataToChart(final float averageHeartRate, final float targetSpeed) {
        // add entries to the "data" object
        AnaerobicCapacityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                entries.add(new Entry(targetSpeed, averageHeartRate));
                Collections.sort(entries, new EntryXComparator());
                mChart.notifyDataSetChanged(); // let the chart know it's data changed
                mChart.invalidate(); // refresh
            }
        });
    }

    private static LineData data = null;
    private static LineDataSet dataset = null;
    private static ArrayList<Entry> entries = null;

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation;
        countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        //Request.startExperience(getActivity(), UUID.fromString(Uuid.EX_ANAEROBIC_CAPACITY));
        Utils.startExperience(UUID.fromString(Uuid.EX_ANAEROBIC_CAPACITY));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_ANAEROBIC_CAPACITY;
    }

    @Override
    public void onResume(){
        super.onResume();
        AnaerobicCapacityActivity.hideTutorial();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private void resetValues(){
        tryNumber = 0;
        mChart.clear();
        if(entries!=null)
            entries.clear();
        if(synthesis!=null)
            synthesis.clear();
        if(data!=null)
            data.clearValues();
    }
}