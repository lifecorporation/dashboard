package tutorial;

/*
 * Created by chara on 15-Dec-16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.utils.Utils;

public class IntroFragmentPage1 extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tutorial_fragment_1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView tvLogo = (TextView) view.findViewById(R.id.tvLogo);
        TextView tvWelcome = (TextView) view.findViewById(R.id.tvWelcome);

        tvLogo.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvWelcome.setTypeface(Utils.getKnockoutFont(getActivity()));

    }
}
