package com.life.app.client.dashboard.utils;

import android.app.Activity;
import android.content.SharedPreferences;

import com.life.app.client.dashboard.constants.RadarChartConst;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Created by peppe on 28/07/2016.
 */
public class SharedPrefDataHandler {

    public static void saveWellbeingStatus(Activity activity, float value){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat("wellbeing_status", value);
        editor.apply();
    }

    public static float getWellbeingStatus(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getFloat("wellbeing_status",0f);
    }

    /*
     * Start or stop SSleepExperience (only in case of WB)
     * @param activity
     * @param iswarning
     */
    public static void setSleepModeStatus(Activity activity, boolean iswarning){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("sleepmode_status", iswarning);
        editor.apply();
    }

    /*
     * Check if SleepExperience is enabled/running
     * @param activity
     * @return true if enabled, false if ndisabled
     */
    public static boolean getSleepModeStatus(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getBoolean("sleepmode_status",false);
    }

    /*
     * Set if warning dialog for SleepExperience needs to be shown or not
     * @param activity
     * @param iswarning
     */
    public static void setSleepModeWarning(Activity activity, boolean iswarning){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("sleepmode_warning", iswarning);
        editor.apply();
    }

    /*
     * Check if warning dialog for SleepExperience must be shown
     * @param activity
     * @return true if is active, false if not
     */
    public static boolean getSleepModeWarning(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getBoolean("sleepmode_warning",true);
    }

    public static void setWarning(Activity activity, boolean iswarning){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("warning_messages", iswarning);
        editor.apply();
    }

    public static boolean isWarning(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getBoolean("warning_messages",false);
    }

    private static void setTotalScore(Activity activity, int total){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("total_abilities", total);
        editor.apply();
    }

    public static int getTotalScore(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getInt("total_abilities",0);
    }

    public static String getTotalScoreToString(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return String.valueOf(prefs.getInt("total_abilities",0));
    }

    public static void tutorialStatus(Activity activity, boolean status){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("life_tutorial", status);
        editor.commit();
    }

    public static void clickStatus(Activity activity, boolean status){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("life_pikachu", status);
        editor.commit();
    }

    public static boolean getClickStatus(Activity activity){
        SharedPreferences preferences = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return preferences.getBoolean("life_pikachu", false);
    }

    public static boolean getTutorialStatus(Activity activity){
        SharedPreferences preferences = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return preferences.getBoolean("life_tutorial", false);
    }

    public static ArrayList<Float> getBest(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        ArrayList<Float> best = new ArrayList<>();
        try {
            JSONArray jsonArray2 = new JSONArray(prefs.getString(key+"Best", "[]"));
            if(jsonArray2.length()>0) {
                for (int i = 0; i < jsonArray2.length(); i++) {
                    //Double result = (Double)jsonArray2.get(i);
                    best.add((Float.parseFloat(jsonArray2.get(i).toString())));
                    //Float fX = new Float(result);
                    //best.add(new Float(result));
                }
            }else{
                best.add(0f);
                best.add(0f);
            }
        } catch (Exception e) {
            best.add(0f);
            best.add(0f);
            e.printStackTrace();
        }
        return best;
    }

    public static void addBest(Activity activity, String key, ArrayList<Float> values){
        if(getBest(activity, key).get(0) < values.get(0)) {
            SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
            SharedPreferences.Editor editor = prefs.edit();
            JSONArray jsonArray = new JSONArray();
            for(Float newvalue : values){
                jsonArray.put(newvalue);
            }
            editor.putString(key+"Best", jsonArray.toString());
            editor.commit();
        }
    }

    // Get all the values stored in sharedPreferences for the given key
    public static int [] getHistoryValues(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);

        int [] values = new int[5];
        for (int i=values.length-1; i>=1; i--){
            int element=prefs.getInt(key + "_" + i,0);
            values[i]=element;
        }
        values[0]=prefs.getInt(key,0);

        return values;
    }

    public static List<Float> getHistoryValuesAsList(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);

        List<Float> values = new ArrayList<>();
        for (int i = 1; i<=com.life.app.client.dashboard.constants.RadarChartConst.MAX_EXPERIENCE_VALUES; i++){
            if(prefs.contains(key + "_" + (i))) {
                ArrayList<Float> synthesis = getSynthesis(activity, key + "_" + (i));
                if (synthesis != null)
                    values.add(synthesis.get(0));
            }
        }

        Collections.reverse(values);
        return values;
    }

    /**
     * Every new value we retrieve from every experience, it's been stored in the SharedPreferences.
     * Example : retrieved 55 from RhythmExperience. Will be saved with the key RHYTHM_KEY_1
     * Next value will be saved with the key RHYTHM_KEY_2 and so on (...RHYTHM_KEY_N).
     *
     * @param key the key on which the value will be saved into the SharedPreferences.
     * @param values ArrayList of the values for the current experience running - including seconds where necessary.
     */
    public static void addNewSynthesis(Activity activity, String key, ArrayList<Float> values){    // 55
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();

        if(!prefs.contains(key+"_1")) {
            addSynthesis(activity, key+"_1", values);
        }else{
            moveValuesToNext(activity, key, values, prefs, editor);
        }
    }

    /**
     * Adds the newest synthesis number in the SharedPreferences.
     * Example : retrieved 55 from RhythmExperience. Will be saved with the key RHYTHM_KEY_1
     * Next value will be saved with the key RHYTHM_KEY_2 and so on (...RHYTHM_KEY_N).
     *
     * @param key the key on which the value will be saved into the SharedPreferences.
     * @param values ArrayList of the values for the current experience running - including seconds where necessary.
     */
    public static void addSynthesis(Activity activity, String key, ArrayList<Float> values){    // 55
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();

        JSONArray jsonArray = new JSONArray();
        for(Float newvalue : values){
            jsonArray.put(newvalue);
        }
        editor.putString(key, jsonArray.toString());
        editor.commit();

        calculateTotalScore(activity);
    }

    public static void calculateTotalScore(Activity activity){

        int totalScore = 0;
        int totalCounter = 0;
        for(int i=0;i<5;i++){
            if(getSynthesis(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.AEROBIC_POWER_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.AEROBIC_POWER_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.AEROBIC_POWER_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.REACTION_TIME_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.REACTION_TIME_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.REACTION_TIME_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.AGILITY_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.AGILITY_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.AGILITY_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.BALANCE_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.BALANCE_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.BALANCE_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.AEROBIC_POWER_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.EXPLOSIVENESS_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.EXPLOSIVENESS_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.RHYTHM_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.RHYTHM_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.RHYTHM_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.SPEED_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.SPEED_KEY + "_" + (i+1)).get(0) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.SPEED_KEY + "_" + (i+1)).get(0);
                totalCounter++;
            }

            if(getSynthesis(activity, RadarChartConst.STRENGTH_KEY + "_" + (i+1))!=null &&
                    getSynthesis(activity, RadarChartConst.STRENGTH_KEY + "_" + (i+1)).get(1) != 0){
                totalScore += getSynthesis(activity, RadarChartConst.STRENGTH_KEY + "_" + (i+1)).get(1);
                totalCounter++;
            }
        }

        if(totalCounter == 0)
            setTotalScore(activity, 0);
        else
            setTotalScore(activity, (int)(totalScore/totalCounter));
    }

    /**
     * Returns an ArrayList where stored the synthesis number and seconds - where required
     *
     * @param key the key on which the list of the sub-values is saved into the SharedPreferences.
     * @return an ArrayList containing the synthesis value of the experience
     */
    public static ArrayList<Float> getSynthesis(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        ArrayList<Float> synthesis = new ArrayList<>();

        try {
            JSONArray jsonArray2 = new JSONArray(prefs.getString(key, "[]"));
            if(jsonArray2.length()!=0){
                for (int i = 0; i < jsonArray2.length(); i++)
                    synthesis.add((Float.parseFloat(jsonArray2.get(i).toString())));
            }else{
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return synthesis;
    }

    /**
     * Returns the current value of the running experience
     *
     * @param key the key where is stored the value in the SharedPreferences
     * @return the current output of the running experience as int
     */
    public static int getExperienceValue(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        return prefs.getInt(key, 0);
    }

    /**
     * Returns the list of the last 5 values of the running experience
     *
     * @param key the key where is stored the value in the SharedPreferences
     * @return list including the last 5 results of the running experience
     */
    public static List<Integer> getExperienceValueAsList(Activity activity, String key){
        List<Integer> results = new ArrayList<>();
        for(int i = 0; i< com.life.app.client.dashboard.constants.RadarChartConst.MAX_EXPERIENCE_VALUES; i++){
                results.add(Math.round(getSynthesis(activity, key + "_" +(i+1)).get(0)));
        }

        return results;
    }

    /**
     * On every attempt of saving data, must me be check if the current key already exists. The first time of execution the 1st position
     * is always free.
     * This function is for moving the old data by 1 (next position).
     *
     * @param key the current key we want to save the new value (ex RHYTHM_KEY_1).
     * @param newsynthesis ArrayList of the new value - among the seconds if provided - stored in the current key.
     * @param prefs the SharedPreferences object
     * @param editor the Editor object
     * @see com.life.app.client.dashboard.constants.Consts for maximum possible data saved
     */
    private static void moveValuesToNext(Activity activity, String key, ArrayList<Float> newsynthesis, SharedPreferences prefs, SharedPreferences.Editor editor){
        ArrayList<Float> oldsynthesis;
        ArrayList<Float> oldTry1, oldTry2, oldTry3;
        ArrayList<ArrayList<Float>> oldTries;
        ArrayList<ArrayList<Float>> oldSets;
        ArrayList<Float> set1, set2, set3;

        ArrayList<ArrayList<Float>> oldBalanceSet1;
        ArrayList<ArrayList<Float>> oldBalanceSet2;
        ArrayList<ArrayList<Float>> oldBalanceSet3;

        for(int k=com.life.app.client.dashboard.constants.RadarChartConst.MAX_EXPERIENCE_VALUES;k>1;k--){

            oldTries = new ArrayList<>();
            oldTry1 = new ArrayList<>();
            oldTry2 = new ArrayList<>();
            oldTry3 = new ArrayList<>();
            oldSets = new ArrayList<>();
            set1 = new ArrayList<>();
            set2 = new ArrayList<>();
            set3 = new ArrayList<>();

            oldBalanceSet1 = new ArrayList<>();
            oldBalanceSet2 = new ArrayList<>();
            oldBalanceSet3 = new ArrayList<>();

            oldsynthesis = getSynthesis(activity, key + "_" + (k-1));                     // 55

            if(key.equals(RadarChartConst.BALANCE_KEY)) {
                int i = 1;
                while(prefs.contains(key + "_" + (k - 1) + "_TRY_1_SET_"+i)){
                    oldBalanceSet1.add(getSets(activity, key + "_" + (k - 1) + "_TRY_1_SET_" + i));
                    i++;
                }

                i = 1;
                while(prefs.contains(key + "_" + (k - 1) + "_TRY_2_SET_"+i)){
                    oldBalanceSet2.add(getSets(activity, key + "_" + (k - 1) + "_TRY_2_SET_" + i));
                    i++;
                }

                i = 1;
                while(prefs.contains(key + "_" + (k - 1) + "_TRY_2_SET_"+i)){
                    oldBalanceSet3.add(getSets(activity, key + "_" + (k - 1) + "_TRY_2_SET_" + i));
                    i++;
                }
            }else if(key.equals(RadarChartConst.AGILITY_KEY) || key.equals(RadarChartConst.STRENGTH_KEY)) {
                int i = 1;
                while(prefs.contains(key + "_" + (k - 1) + "_TRY_"+i)){
                    oldTries.add(getTry(activity, key + "_" + (k - 1) + "_TRY_"+i));
                    oldSets.add(getSets(activity, key + "_" + (k - 1) + "_TRY_" + i + "_SET_1"));

                    i++;
                }
            }else{
                oldTry1 = getTry(activity, key + "_" + (k - 1) + "_TRY_1");
                oldTry2 = getTry(activity, key + "_" + (k - 1) + "_TRY_2");
                oldTry3 = getTry(activity, key + "_" + (k - 1) + "_TRY_3");

                set1 = getSets(activity, key + "_" + (k - 1) + "_TRY_1_SET");
                set2 = getSets(activity, key + "_" + (k - 1) + "_TRY_2_SET");
                set3 = getSets(activity, key + "_" + (k - 1) + "_TRY_3_SET");
            }

            if(oldsynthesis!=null)
                addSynthesis(activity, key + "_" + (k), oldsynthesis);

            if(key.equals(RadarChartConst.BALANCE_KEY)) {
                for(int i=0;i<oldBalanceSet1.size();i++){
                    addNewSet(activity, key + "_" + (k) + "_TRY_1_SET_" + (i+1), oldBalanceSet1.get(i));
                }

                for(int i=0;i<oldBalanceSet2.size();i++){
                    addNewSet(activity, key + "_" + (k) + "_TRY_2_SET_" + (i+1), oldBalanceSet2.get(i));
                }

                for(int i=0;i<oldBalanceSet3.size();i++){
                    addNewSet(activity, key + "_" + (k) + "_TRY_3_SET_" + (i+1), oldBalanceSet3.get(i));
                }
            }else if(key.equals(RadarChartConst.AGILITY_KEY) || key.equals(RadarChartConst.STRENGTH_KEY)) {
                for(int i=0;i<oldTries.size();i++){

                    addNewTry(activity, key + "_" + (k) + "_TRY_"+(i+1), oldTries.get(i));
                    addNewSet(activity, key + "_" + (k) + "_TRY_"+(i+1) + "_SET_1", oldSets.get(i));
                }
            }else{
                addNewTry(activity, key + "_" + (k) + "_TRY_1", oldTry1);
                addNewTry(activity, key + "_" + (k) + "_TRY_2", oldTry2);
                addNewTry(activity, key + "_" + (k) + "_TRY_3", oldTry3);

                addNewSet(activity, key + "_" + (k) + "_TRY_1_SET", set1);
                addNewSet(activity, key + "_" + (k) + "_TRY_2_SET", set2);
                addNewSet(activity, key + "_" + (k) + "_TRY_3_SET", set3);
            }
        }
        addSynthesis(activity, key + "_1", newsynthesis);
        editor.commit();                          //BALANCE_KEY_1
    }

    /**
     * For each value retrieved from the running experience, is needed to store the values for every try.
     * (Usually every value is composed by 3 tries)
     * Example : Considering running the Rhythm experience. Having retrieved 55 from this experience, this value is composed by 3 tries.
     * Those values are 54, 79, 33 and the will be saved into the SharedPreferences with the following keys :
     * RHYTHM_KEY_1_TRY_1, RHYTHM_KEY_1_TRY_2 and RHYTHM_KEY_1_TRY_3.
     * In case there are no such as values/tries will be simple stored the retrieved values with the same keys.
     *
     * @param key the key on which the value of the specific try will be saved into the SharedPreferences.
     * @param values ArrayList that contains the effort of the current try -and the seconds where given - of the user.
     */
    public static void addNewTry(Activity activity, String key, ArrayList<Float> values){    // 54, 79, 33
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();

        JSONArray jsonArray = new JSONArray();

        if(values!=null)
            for(Float newvalue : values) {
                jsonArray.put(newvalue);
            }

        editor.putString(key, jsonArray.toString());
        editor.commit();

    }

    /**
     * Returns the value for the current try of the running experience
     *
     * @param key the key where is stored the value in the SharedPreferences
     * @return the value of the try as int
     */
    public static ArrayList<Float> getTry(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);

        ArrayList<Float> tries = new ArrayList<>();
        try {
            JSONArray jsonArray2 = new JSONArray(prefs.getString(key, "[]"));
            // questo if non funziona. va sempre nell'else
            if(jsonArray2.length()==0){
                return null;
            }else{
                for (int i = 0; i < jsonArray2.length(); i++) {
                    tries.add(Float.parseFloat(jsonArray2.get(i).toString()));
                }
                if(tries.size()<3){
                    for(int i=tries.size(); i < 4;i++)
                        tries.add(0f);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return tries;
    }

    /**
     * For every try, need to save the list with the sub-values that form the result.
     * In this case we need to save to it as a Set since it's about a list of values.
     * Example : Considering the last tries with keys RHYTHM_KEY_1_TRY_1, RHYTHM_KEY_1_TRY_2, RHYTHM_KEY_1_TRY_3
     * the keys for every try and its list will be RHYTHM_KEY_1_TRY_1_SET, RHYTHM_KEY_1_TRY_2_SET, RHYTHM_KEY_1_TRY_3_SET
     *
     * @param key the key on which the list of the sub-values will be saved into the SharedPreferences.
     * @param values a list containing all of the sub-values retrieved
     */
    public static void addNewSet(Activity activity, String key, ArrayList<Float> values){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        SharedPreferences.Editor editor = prefs.edit();

        float[] floatArray = new float[values.size()];
        int i = 0;

        for (Float f : values) {
            floatArray[i++] = (f != null ? f : Float.NaN); // Or whatever default you want.
        }

        JSONArray jsonArray = new JSONArray();
        for(Float newvalue : floatArray){
            //Log.i("CURRENT_VALUE_SET", newvalue  + "");
            jsonArray.put(newvalue);
        }
        editor.putString(key, jsonArray.toString());
        editor.commit();
    }

    /**
     * Returns the ArrayList of the values for every try
     *
     * @param key the key on which the list of the sub-values is saved into the SharedPreferences.
     * @return an ArrayList containing the sub-values for every try
     */
    public static ArrayList<Float> getSets(Activity activity, String key){
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        ArrayList<Float> data = new ArrayList<>();
        try {
            JSONArray jsonArray2 = new JSONArray(prefs.getString(key, "[]"));
            if(jsonArray2.length()==0){
                data.add(0f);
                data.add(0f);
                data.add(0f);
            }else {
                for (int i = 0; i < jsonArray2.length(); i++) {
                    data.add(Float.parseFloat(jsonArray2.get(i).toString()));
                    //data.add((float) BigDecimal.valueOf(jsonArray2.getDouble(i)).floatValue());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            data.add(0f);
            data.add(0f);
            data.add(0f);
        }
        return data;
    }
}
