package com.life.app.client.dashboard.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.github.mikephil.charting.charts.RadarChart;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AerobicPowerActivity;
import com.life.app.client.dashboard.activities.abilities.AgilityActivity;
import com.life.app.client.dashboard.activities.abilities.AnaerobicCapacityActivity;
import com.life.app.client.dashboard.activities.abilities.BalanceActivity;
import com.life.app.client.dashboard.activities.abilities.ExplosivenessActivity;
import com.life.app.client.dashboard.activities.abilities.ReactionTimeActivity;
import com.life.app.client.dashboard.activities.abilities.RhythmActivity;
import com.life.app.client.dashboard.activities.abilities.SpeedActivity;
import com.life.app.client.dashboard.activities.abilities.StrengthActivity;
import com.life.app.client.dashboard.graphs.RadarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 13/7/2016.
 */
public class PhysicalAbilitiesActivity extends MyFragmentActivity {

    TextView tvTotalSynthesis;
    Button bAnaerobicCpacity, bAerobicPower, bReactionTime, bAgility, bBalance, bExplosiveness, bRhythm, bSpeed, bStrength;
    RelativeLayout ibInfoExp, back;
    ImageButton ibInfo2;
    // Radarchart
    private static RadarChart radarChart = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.fragment_dashboard);

        ((TextView) findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.hint_physical_abilities));

        tvTotalSynthesis = (TextView) findViewById(R.id.tvTotalSynthesis);
        tvTotalSynthesis.setTypeface(Utils.getKnockoutFont(this));

        ibInfoExp = (RelativeLayout) findViewById(R.id.ibInfoExp);
        ibInfo2   = (ImageButton) findViewById(R.id.ibInfo2);
        back      = (RelativeLayout) findViewById(R.id.back);
        ibInfoExp.setVisibility(View.GONE);

        ibInfo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_my_abilities),
                        getResources().getString(R.string.hint_my_abilities_info));
            }
        });
        back.setVisibility(View.VISIBLE);


        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        radarChart = (RadarChart) findViewById(R.id.radar_chart);

        bAnaerobicCpacity    = (Button) findViewById(R.id.bAnaerobicCpacity);
        bAerobicPower        = (Button) findViewById(R.id.bAerobicPower);
        bReactionTime        = (Button) findViewById(R.id.bReactionTime);
        bAgility             = (Button) findViewById(R.id.bAgility);
        bBalance             = (Button) findViewById(R.id.bBalance);
        bExplosiveness       = (Button) findViewById(R.id.bExplosivness);
        bRhythm              = (Button) findViewById(R.id.bRhythm);
        bSpeed               = (Button) findViewById(R.id.bSpeed);
        bStrength            = (Button) findViewById(R.id.bStrength);

        setRadarChartButtonListener();
    }

    @Override
    public void onResume(){
        super.onResume();

        if(tvTotalSynthesis!=null)
            tvTotalSynthesis.setText(SharedPrefDataHandler.getTotalScoreToString(PhysicalAbilitiesActivity.this));

        new Handler().postDelayed(new Runnable(){
            public void run(){
                showRadarChart();
            }
        },200);
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(this, R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(this).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(PhysicalAbilitiesActivity.this, R.color.life_blue_normal));
    }

    public void showRadarChart(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RadarChartSetup.setup(PhysicalAbilitiesActivity.this, radarChart);
                RadarChartSetup.addEntries(PhysicalAbilitiesActivity.this, radarChart);
            }
        });
    }

    private void setRadarChartButtonListener() {

        bAnaerobicCpacity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(50);
            }
        });

        bAerobicPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(51);
            }
        });

        bReactionTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(53);
            }
        });

        bAgility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(54);
            }
        });

        bBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(55);
            }
        });

        bExplosiveness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(56);
            }
        });

        bRhythm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(57);
            }
        });

        bSpeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(58);
            }
        });

        bStrength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectExperience(59);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    public void selectExperience(int position) {
        // update the main content by replacing fragments
        Intent intent = null;
        switch (position) {
            // position sended by RadarMarkerView
            case 50:
                intent = new Intent(this, AnaerobicCapacityActivity.class);
                break;
            case 51:
                intent = new Intent(this, AerobicPowerActivity.class);
                break;
            case 52:
                //ex joint  mobility
                break;
            case 53:
                intent = new Intent(this, ReactionTimeActivity.class);
                break;
            case 54:
                intent = new Intent(this, AgilityActivity.class);
                break;
            case 55:
                intent = new Intent(this, BalanceActivity.class);
                break;
            case 56:
                intent = new Intent(this, ExplosivenessActivity.class);
                break;
            case 57:
                intent = new Intent(this, RhythmActivity.class);
                break;
            case 58:
                intent = new Intent(this, SpeedActivity.class);
                break;
            case 59:
                intent = new Intent(this, StrengthActivity.class);
                break;
        }
        startActivity(intent);
    }
}