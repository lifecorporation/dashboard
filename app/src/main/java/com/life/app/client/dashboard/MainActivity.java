package com.life.app.client.dashboard;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.activities.settings.SettingsActivity;
import com.life.app.client.dashboard.activities.settings.WifiActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.fragments.MainActivityMuscleFragment;
import com.life.app.client.dashboard.fragments.MainActivityRealTimeFragment;
import com.life.app.client.dashboard.fragments.MainActivitySensorsFragment;
import com.life.app.client.dashboard.fragments.menu.CalibrationFragment;
import com.life.app.client.dashboard.fragments.menu.DeviceDiagnosisFragment;
import com.life.app.client.dashboard.fragments.menu.ExperiencesFragment;
import com.life.app.client.dashboard.fragments.menu.RealTimeFragment;
import com.life.app.client.dashboard.objects.Asset;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.BindService;
import com.life.services.bind.assets.Node;
import com.life.services.bind.assets.ShirtSensorNetworkListener;
import com.life.services.bind.status.StatusListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.fabric.sdk.android.Fabric;

import static com.life.app.client.dashboard.constants.TAG.*;

/*
 * Created by chara on 06-Dec-16.
 */

public class MainActivity extends MyFragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static Activity activity      = null;

    protected Life mLife                 = null;

    private LinearLayout llRightBarPanel;
    private boolean isDrawerOpen         = false;

    private static MainActivity instance = null;

    BindService bindService = null;

    AVLoadingIndicatorView avLoader = null;

    private static com.life.services.bind.status.Status status = null;

    boolean alert5Shown = false, alert10Shown = false;

    public static com.life.services.bind.status.Status getStatus(){
        return status;
    }

    public void setStatus(com.life.services.bind.status.Status newStatus){
        status = newStatus;
    }

    // Graphs variables
    static int count = 0;

    ImageButton ibDevice = null;
    ImageButton ibTShirt = null;

    DrawerLayout dLayout;
    RelativeLayout drawer;
    ArrayAdapter<String> adapter;
    ListView navList;

    ImageView ivBattery, ivShirt, ivCloud, ivStorage;
    TextView tvBattery, tvTshirt, tvCloud, tvStorage;


    private int dialogCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);

        List<String> Lines = Arrays.asList(getResources().getStringArray(R.array.menu_drawer));
        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if(fragment!=null) {
                    if(CalibrationFragment.calibrationRunning){
                        calibrationWarningDialog(fragment);
                    }else{
                        switchFragment(fragment);

                        if(fragment instanceof RealTimeFragment) {
                            if (avLoader != null && status == null)
                                avLoader.smoothToShow();
                        }else{
                            if(avLoader!=null && avLoader.isShown())
                                avLoader.smoothToHide();
                        }
                    }
                }
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        drawer = (RelativeLayout) findViewById(R.id.drawer);

        avLoader = (AVLoadingIndicatorView) findViewById(R.id.avLoader);

        adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,Lines);

        navList = (ListView) findViewById(R.id.navList);
        navList.setAdapter(adapter);
        navList.setOnItemClickListener(this);

        mLife = new Life();
        activity = this;

        llRightBarPanel = (LinearLayout)findViewById(R.id.llRightBarPanel);

        ibTShirt = (ImageButton) findViewById(R.id.ibTShirt);
        ibTShirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTShirtSelected();
            }
        });

        ibDevice = (ImageButton) findViewById(R.id.ibDevice);
        ibDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getStatus()!=null)
                    infoStatusDialog("");
            }
        });

        instance = MainActivity.this;

        /*
         * The account manager used to request and add account.
         */
        AccountManager mAccountManager = AccountManager.get(this);

        setTshirt();

        //stop already running experiences

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else{
            Account[] accountsFromFirstApp = mAccountManager.getAccountsByType(Consts.ACCOUNT_TYPE);
            for(Account acct: accountsFromFirstApp){
                ((TextView) findViewById(R.id.tvName)).setText(acct.name);
            }
        }

        startServiceConnection();

        bindService = new BindService(MainActivity.this);
        bindService.registerStatus(new StatusListener() {
            @Override
            public void onStatusReceived(com.life.services.bind.status.Status newstatus) {
                setStatus(newstatus);
                mLife.setStatus(newstatus);
                if(newstatus!=null){

                    if(Log.isLoggable("Status", Log.INFO))
                        Log.i("Status", newstatus.toString());

                    dialogCounter++;

                    if(status.getStorage_percentage()<20 && dialogCounter > 30){

                        dialogCounter = 0;

                        infoDialog(getString(R.string.warning), getString(R.string.hint_warning_message_storage));
                    }

                    activateDashboard(newstatus);

                    if(status.getWearable_status()==1){
                        activateTShirtConnection();
                    }else{
                        deactivateTShirtConnection();
                    }
                }else{
                    deactivateDashboard();
                }
            }
        });

        bindService.registerShirtSNStatusReceiver(new ShirtSensorNetworkListener() {
            @Override
            public void onStatusReceived(ArrayList<Node> nodes) {
                Asset.setTshirt(getStatus(), nodes);
                MainActivitySensorsFragment.refreshSensors();

            }
        });

        infoRepeatDialog(getString(R.string.warning), getString(R.string.hint_warning_message));

        //Utils.LogDensity(MainActivity.this);

        avLoader.smoothToShow();
    }

    Fragment fragment = null;

    public void openDrawer(View v){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dLayout.openDrawer(drawer);
            }
        }, 100);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    @Override
    public void onClick(View view) {

    }

    public void disable(){
        /*
         * fade out animation for the left panel and the bodymap
         */
        ((RealTimeFragment) (MainActivity.this).getSupportFragmentManager().findFragmentByTag(_TAG_REALTIME_FRAGMENT)).fadeOut(MainActivity.this);

        /*
         * clear raw data graphs
         */

        /*
         * sets to 0 all data
         */
        clearDisplay();

        Asset.setTshirt(null, null);
    }

    public void setDeviceIcon(){
        ibDevice.setImageResource(R.mipmap.ic_actionbar_device);
    }

    public void hideTshirt(){
        ibTShirt.setVisibility(View.INVISIBLE);
    }

    public void showDeviceAndTshirt(){
        if(llRightBarPanel.getVisibility() == View.INVISIBLE) {
            Animation mLoadAnimation = AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_in);
            mLoadAnimation.setDuration(Consts.DURATION_FADE_IN_OUT);
            mLoadAnimation.setFillAfter(true);
            llRightBarPanel.startAnimation(mLoadAnimation);
            llRightBarPanel.setVisibility(View.VISIBLE);
        }
    }

    public void hideDeviceAndTshirt(){
        if(llRightBarPanel.getVisibility() == View.VISIBLE) {
            Animation mLoadAnimation = AnimationUtils.loadAnimation(MainActivity.this, android.R.anim.fade_out);
            mLoadAnimation.setDuration(Consts.DURATION_FADE_IN_OUT);
            mLoadAnimation.setFillAfter(true);
            llRightBarPanel.startAnimation(mLoadAnimation);
            llRightBarPanel.setVisibility(View.INVISIBLE);
        }
    }

    private boolean launchDiagnosis(){
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(getResources().getString(R.string.package_diagnosis));
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
            return true;
        }
        return false;
    }

    private void showFCButton(){
        //findViewById(R.id.llRightBarPanel).setVisibility(View.VISIBLE);
        findViewById(R.id.ibInfo).setVisibility(View.GONE);
    }

    private void hideFCButton(){
        //findViewById(R.id.llRightBarPanel).setVisibility(View.GONE);
        findViewById(R.id.ibInfo).setVisibility(View.VISIBLE);
    }

    private void hideInfoButton(){
        findViewById(R.id.ibInfo).setVisibility(View.GONE);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(isDrawerOpen)
            dLayout.closeDrawer(drawer);

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);

        SharedPrefDataHandler.calculateTotalScore(MainActivity.this);

        if(SharedPrefDataHandler.getSleepModeStatus(MainActivity.this))
            ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.RE_SLEEP_EXPERIENCE;

        if(ExperiencesUtils.EXPERIENCE_RUNNING_KEY!=null &&
                ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.RE_SLEEP_EXPERIENCE)){

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getResources().getString(R.string.prompt_stop_sleepmode))
                    .setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SharedPrefDataHandler.setSleepModeStatus(MainActivity.this, false);
                            ExperiencesUtils.resetBaseExperience(MainActivity.this);
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.prompt_sleepmode_off), Toast.LENGTH_SHORT).show();
                        }})
                    .setNegativeButton(getResources().getString(R.string.dialog_button_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }})
                    .setCancelable(false)
                    .show();
        }

        /*
         * Sending request for the status of the sensors only if the device is switched on and connected
         * to the tshirt
         */
        if(getSupportFragmentManager().findFragmentById(R.id.content) instanceof RealTimeFragment &&
                (getStatus()!=null && getStatus().getWearable_status()==1))
            Utils.getAsset();
    }

    private void calibrationWarningDialog(final Fragment fragment){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getResources().getString(R.string.warning_calibration_ongoing_title)).
                setMessage(getResources().getString(R.string.warning_calibration_ongoing_descr))
                .setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //CalibrationFragment.resetOnGoingDialog(MainActivity.this);

                        ExperiencesUtils.resetBaseExperience(MainActivity.this);

                        if(fragment!=null)
                            switchFragment(fragment);
                    }})
                .setNegativeButton(getResources().getString(R.string.dialog_button_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }})
                .setCancelable(false)
                .show();
    }

    private void switchFragment(final Fragment fragment){
        final Handler handler = new Handler();
        new Thread(new Runnable(){
            @Override
            public void run(){
                //long running code
                //this is running on a background thread
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        //since the handler was created on the UI thread,
                        //   this code will run on the UI thread
                        final FragmentManager manager = getSupportFragmentManager();

                        if (manager.getBackStackEntryCount() > 0) {
                            manager.popBackStack();
                        }

                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.replace(R.id.content, fragment, fragment.getClass().getName());
                        //transaction.addToBackStack(fragment.getClass().getName());
                        transaction.commit();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        stopServiceConnection();
        super.onDestroy();

        dialogCounter = 0;

        if(ExperiencesUtils.EXPERIENCE_RUNNING_KEY != null &&
                (!ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.BASE_EXPERIENCE_WEB) &&
                        !ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.BASE_EXPERIENCE_FIT) &&
                        !ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.RE_SLEEP_EXPERIENCE))){
            stopBasicExperience();
        }

        if (bindService != null) {
            bindService.unregisterShirtSNStatusReceiver();
            bindService.unregisterStatus();
            bindService.unbindService();
        }
    }

    @Override
    public void onBackPressed() {
        // resetto il valore del count
        resetCount();
        if(isDrawerOpen){
            dLayout.closeDrawer(drawer);
        }else if (getSupportFragmentManager().findFragmentById(R.id.content) instanceof CalibrationFragment
                && (CalibrationFragment.calibrationRunning)){
            /*
             * Warning message in case of Calibration ongoing
             */
            calibrationWarningDialog(null);
        }else if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStack();
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.quite_message))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.dialog_button_yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            MainActivity.this.finish();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.dialog_button_no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void setBatteryLevel(int level){
        if(ivBattery == null || tvBattery == null)
            return;

        if(level == -1){
            ivBattery.setImageResource(R.mipmap.ic_action_battery_undefined);
        }else{
            if (level < 20) {
                ivBattery.setImageResource(R.mipmap.ic_battery_empty);
            } else if (level < 40) {
                ivBattery.setImageResource(R.mipmap.ic_battery_low);
            } else if (level < 80) {
                ivBattery.setImageResource(R.mipmap.ic_battery_charged);
                alert10Shown = false;
                alert5Shown = false;
            } else {
                ivBattery.setImageResource(R.mipmap.ic_battery_full);
                alert10Shown = false;
                alert5Shown = false;
            }
        }
    }

    public void setTshirtStatus(int status){
        if (ivShirt == null || tvTshirt == null)
            return;

        switch (status) {
            case -1:
                tvTshirt.setText(getString(R.string.hint_tshirt_missing));
                ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_undefined));
                break;
            case 0:
                tvTshirt.setText(getString(R.string.hint_tshirt_missing));
                if (getStatus().getProduct_id() == 0 || getStatus().getProduct_id() == 48) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_undefined));
                } else if (getStatus().getProduct_id() == 1 || getStatus().getProduct_id() == 49) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_wb_undefined));
                } else if (getStatus().getProduct_id() == 3 || getStatus().getProduct_id() == 51) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_undefined));
                } else {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_undefined));
                }
                break;
            case 1:
                if (getStatus().getProduct_id() == 0 || getStatus().getProduct_id() == 48) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_connected));
                    tvTshirt.setText(getString(R.string.hint_tshirt_fitness));
                } else if (getStatus().getProduct_id() == 1 || getStatus().getProduct_id() == 49) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_wb_connected));
                    tvTshirt.setText(getString(R.string.hint_tshirt_welbeing));
                } else if (getStatus().getProduct_id() == 3 || getStatus().getProduct_id() == 51) {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_connected));
                    tvTshirt.setText(getString(R.string.hint_tshirt_fireproof));
                } else {
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.mipmap.ic_action_fitness_connected));
                    tvTshirt.setText("");
                }
                break;
        }
    }

    public void setCloudStatus(int status){
        if(ivCloud==null || tvCloud == null)
            return;

        switch (status) {
            case -1:
                ivCloud.setImageResource(R.mipmap.ic_cloud_undefined);
                tvCloud.setText(getResources().getString(R.string.hint_cloud_undefined));
                break;
            case 0:
                ivCloud.setImageResource(R.mipmap.ic_cloud_waiting);
                tvCloud.setText(getResources().getString(R.string.hint_cloud_waiting));
                break;
            case 1:
                ivCloud.setImageResource(R.mipmap.ic_cloud_connected);
                tvCloud.setText(getResources().getString(R.string.hint_cloud_connected));
                break;
            case 2:
                ivCloud.setImageResource(R.mipmap.ic_cloud_disconnected);
                tvCloud.setText(getResources().getString(R.string.hint_cloud_disconnected));
                break;
        }
    }

    public void setStorageStatus(int storage){

        if(tvStorage == null || ivStorage == null)
            return;

        if(storage == -1){
            String placeholder = "-%";
            tvStorage.setText(placeholder);
        }else{
            String placeholder = storage + "% available storage";
            tvStorage.setText(placeholder);

            if (status.getStorage_percentage() > 70)
                ivStorage.setImageResource(R.mipmap.ic_storage_empty);
            else if (status.getStorage_percentage() > 20)
                ivStorage.setImageResource(R.mipmap.ic_storage_warning);
            else if (status.getStorage_percentage() >= 0)
                ivStorage.setImageResource(R.mipmap.ic_storage_full);
        }
    }

    private void startServiceConnection(){

        Intent intent = new Intent();
        intent.setAction(BTConnectionManagerService.startServiceString);

        this.sendBroadcast(intent);
    }

    public void stopServiceConnection(){
        //stopService(new Intent(this, BTConnectionManagerService.class));
    }

    public void startBasicExperience(){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                String currentUUID = Uuid.generalExperience(status);
                if(!currentUUID.isEmpty()){

                    Utils.startExperience(UUID.fromString(Uuid.generalExperience(status)));
                    ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.generalExperience(status);

                    //startServiceConnection();
                }
            }
        }, Consts.START_EXPERIENCE_DELAY);
    }

    public void stopBasicExperience(){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Utils.stopExperience();
                ExperiencesUtils.EXPERIENCE_RUNNING_KEY = null;
            }
        }, 500);
    }

    public static MainActivity getInstance(){
        return instance;
    }

    private void resetCount(){
        count = 0;
    }

    private void clearDisplay(){
        RealTimeFragment.clearAll(getInstance());
    }

    private void onTShirtSelected(){
        RealTimeFragment.goToSensors();
    }

    public void setTshirt(){
        if(ibTShirt!=null && getStatus()!=null){
            CalibrationFragment.updateMuscles();
            if(getStatus().getProduct_id() == 0 || getStatus().getProduct_id() == 48) {
                ibTShirt.setVisibility(View.VISIBLE);
                ibTShirt.setImageResource(R.mipmap.ic_actionbar_tshirt_fitness);
                Utils.getAsset();
                MainActivityMuscleFragment.updateMuscles(MainActivity.this, true);
                MainActivitySensorsFragment.updateSesnors(0);
            }else if(getStatus().getProduct_id() == 1 || getStatus().getProduct_id() == 49) {
                ibTShirt.setVisibility(View.VISIBLE);
                ibTShirt.setImageResource(R.mipmap.ic_actionbar_tshirt_wellbeing);
                Utils.getAsset();
                MainActivityMuscleFragment.updateMuscles(MainActivity.this, false);
                MainActivitySensorsFragment.updateSesnors(1);
            }else if(getStatus().getProduct_id() == 3 || getStatus().getProduct_id() == 51) {
                ibTShirt.setVisibility(View.VISIBLE);
                ibTShirt.setImageResource(R.mipmap.ic_actionbar_tshirt_fitness);
                Utils.getAsset();
                MainActivityMuscleFragment.updateMuscles(MainActivity.this, true);
                MainActivitySensorsFragment.updateSesnors(3);
            }else{
                ibTShirt.setVisibility(View.INVISIBLE);
            }
        }
    }

    private AlertDialog infoDialog = null;
    public void infoDialog(final String title, final String description){
        final View infoDialogView = View.inflate(this, R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);

        if(infoDialog == null || !infoDialog.isShowing()){
            infoDialog = new AlertDialog.Builder(this).create();
            infoDialog.setView(infoDialogView);
            infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogCounter = 0;
                        }
                    });

            infoDialog.setCancelable(false);
            infoDialog.show();
            infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.life_blue_normal));
        }
    }

    private AlertDialog infoRepeatDialog = null;
    public void infoRepeatDialog(final String title, final String description){
        final View infoDialogView = View.inflate(this, R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);

        if(infoRepeatDialog == null)
            infoRepeatDialog = new AlertDialog.Builder(this).create();

        if(!infoRepeatDialog.isShowing()) {
            infoRepeatDialog.setView(infoDialogView);
            infoRepeatDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            infoRepeatDialog(title, description);
                        }
                    });


            if (getStatus() == null && SharedPrefDataHandler.isWarning(MainActivity.this)) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (!isFinishing()) {
                            infoRepeatDialog.setCancelable(false);
                            infoRepeatDialog.show();
                            infoRepeatDialog.getButton(-1).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.life_blue_normal));
                        }
                    }
                }, 20000);
            }
        }
    }

    AlertDialog infoStatusDialog = null;
    public void infoStatusDialog(final String title){
        final View infoDialogView = View.inflate(this, R.layout.info_dialog_status, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if(title == null || title.trim().isEmpty())
            (infoDialogView.findViewById(R.id.tvTitle)).setVisibility(View.GONE);
        else
            ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);

        ivBattery = (ImageView)infoDialogView.findViewById(R.id.ivBattery);
        tvBattery = (TextView) infoDialogView.findViewById(R.id.tvBattery);
        ivShirt   = (ImageView)infoDialogView.findViewById(R.id.ivShirt);
        tvTshirt  = (TextView) infoDialogView.findViewById(R.id.tvTshirt);
        ivCloud   = (ImageView)infoDialogView.findViewById(R.id.ivCloud);
        tvCloud   = (TextView) infoDialogView.findViewById(R.id.tvCloud);
        ivStorage = (ImageView)infoDialogView.findViewById(R.id.ivStorage);
        tvStorage = (TextView) infoDialogView.findViewById(R.id.tvStorage);

        if(getStatus()!=null) {

            setBatteryLevel(status.getBattery_percentage());
            setTshirtStatus(status.getWearable_status());
            setCloudStatus(status.getCloud_connected());
            setStorageStatus(status.getStorage_percentage());

            String placeholderBattery = getStatus().getBattery_percentage() + " %";
            tvBattery.setText(placeholderBattery);
        }else{
            setBatteryLevel(-1);
            setTshirtStatus(-1);
            setCloudStatus(-1);
            setStorageStatus(-1);

            String placeholderBattery = " - %";
            tvBattery.setText(placeholderBattery);
        }

        infoStatusDialog = new AlertDialog.Builder(this).create();
        infoStatusDialog.setView(infoDialogView);
        infoStatusDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });


        infoStatusDialog.setCancelable(false);
        infoStatusDialog.show();
        infoStatusDialog.getButton(-1).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.life_blue_normal));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                showFCButton();
                fragment = new RealTimeFragment();
                if(getStatus()!=null && getStatus().getWearable_status()==1)
                    ((RealTimeFragment) (MainActivity.this).getSupportFragmentManager().findFragmentByTag(_TAG_REALTIME_FRAGMENT)).enable(MainActivity.this);
                break;
            case 1:
                hideFCButton();
                hideInfoButton();
                fragment = new ExperiencesFragment();
                break;
            case 2:
                if(!launchDiagnosis()) {
                    hideFCButton();
                    hideInfoButton();
                    fragment = new DeviceDiagnosisFragment();
                }
                break;
            case 3:
                hideFCButton();
                hideInfoButton();

                CalibrationFragment.updateMuscles();

                fragment = new CalibrationFragment();
                break;
            case 4:
                fragment = null;
                hideFCButton();
                hideInfoButton();
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                break;
            case 5:

                fragment = null;
                SharedPrefDataHandler.tutorialStatus(this, false);
                Intent intent = new Intent(this, TutorialActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
                startActivity(intent);
                break;
            default:
                showFCButton();
                fragment = new RealTimeFragment();
                break;
        }


        // update selected item and title, then close the drawer
        //mDrawerList.setItemChecked(position, true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dLayout.closeDrawer(drawer);
            }
        }, 100);
    }

    private void activateDashboard(com.life.services.bind.status.Status status){

        if(avLoader!=null && avLoader.isShown())
            avLoader.smoothToHide();

        setDeviceIcon();

        showDeviceAndTshirt();

        setBatteryLevel(status.getBattery_percentage());
        setTshirtStatus(status.getWearable_status());
        setCloudStatus(status.getCloud_connected());
        setStorageStatus(status.getStorage_percentage());

        WifiActivity.startWifi();
    }

    private void deactivateDashboard(){
        if(avLoader!=null)
            avLoader.smoothToShow();

        infoRepeatDialog(getString(R.string.warning), getString(R.string.hint_warning_message));

        if(infoStatusDialog!=null && infoStatusDialog.isShowing())
            infoStatusDialog.dismiss();

        disable();

        hideDeviceAndTshirt();

        MainActivitySensorsFragment.resetSensors();
        MainActivityRealTimeFragment.resetGraphs(MainActivity.this);

        WifiActivity.stopWifi(MainActivity.this);

        setBatteryLevel(-1);
        setTshirtStatus(-1);
        setCloudStatus(-1);
        setStorageStatus(-1);

        if(ExperiencesUtils.EXPERIENCE_RUNNING_KEY!=null &&
                ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.RE_SLEEP_EXPERIENCE)) {
            stopBasicExperience();
            SharedPrefDataHandler.setSleepModeStatus(MainActivity.this, false);
            stopServiceConnection();
        }
    }

    private void activateTShirtConnection(){
        setTshirt();

        ((RealTimeFragment) (MainActivity.this).getSupportFragmentManager().findFragmentByTag(_TAG_REALTIME_FRAGMENT)).enable(MainActivity.this);

        if(getSupportFragmentManager().findFragmentById(R.id.content) instanceof RealTimeFragment){

            if(status.getExp_uuid().equals(UUID.fromString(Uuid.EMPTY_EXPERIENCE))){
                startBasicExperience();
            }else{
                if(status.getExp_uuid().equals(UUID.fromString(Uuid.RE_SLEEP_EXPERIENCE)) ||
                        status.getExp_uuid().equals(UUID.fromString(Uuid.generalExperience(status)))){
                }else{
                    if(!status.getExp_uuid().equals(UUID.fromString(Uuid.EMPTY_EXPERIENCE))){
                        stopBasicExperience();
                    }
                    startBasicExperience();
                }
            }
        }
    }

    private void deactivateTShirtConnection(){

        disable();
        hideTshirt();
        MainActivitySensorsFragment.resetSensors();
        MainActivityRealTimeFragment.resetGraphs(MainActivity.this);

        if(status.getExp_uuid().equals(UUID.fromString(Uuid.EMPTY_EXPERIENCE)))
            SharedPrefDataHandler.setSleepModeStatus(MainActivity.this, false);
    }
}
