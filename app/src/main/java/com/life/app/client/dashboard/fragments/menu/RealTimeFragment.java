package com.life.app.client.dashboard.fragments.menu;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.FCActivity;
import com.life.app.client.dashboard.adapters.MainViewPager;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.fragments.MainActivityMuscleFragment;
import com.life.app.client.dashboard.fragments.MainActivitySensorsFragment;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.text.DecimalFormat;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

import static com.life.app.client.dashboard.utils.CustomAnimation.fadeInViewAnimation;
import static com.life.app.client.dashboard.utils.CustomAnimation.fadeOutViewAnimation;
import static com.life.app.client.dashboard.utils.Showcase.hideSequence;

/*
 * Created by chkalog on 29/9/2016.
 */
public class RealTimeFragment extends Fragment {

    View mLeak = null;

    TextView tvBottomTitle;

    private static DecoView dvIntensity;
    private static int serieIntIndex, serieIntIndexBackground;

    private static TextView tvHeartRate, tvRespirationRate, tvActivityIndex, tvFC, tvFitness;

    private static PieChart mChart;

    private static ImageView ivQualityActivity, ivQualityResp, ivQualityHeart, ivStatusIndicator;

    static ImageView ibFC = null;

    private static LinearLayout llFirstLayout;
    private static LinearLayout llSecondLayout;
    private static LinearLayout llThirdLayout;
    private static LinearLayout llFourthLayout;

    private static LinearLayout llHeartrate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_real_time, container, false);
    }

    // we're going to display pie chart for school attendance
    private static int[] yValues = {10, 10, 10, 10, 10, 10};

    private static ViewPager pager = null;
    private static ImageView ivHeartRate, ivRespirationRate;
    private static View rlFitness, rlFC;

    MainViewPager pagerAdapter;

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLeak = view;

        ibFC = (ImageView) view.findViewById(R.id.ibFC);

        tvFC              = (TextView) view.findViewById(R.id.tvFC);
        tvFitness         = (TextView) view.findViewById(R.id.tvFitness);
        tvHeartRate       = (TextView) view.findViewById(R.id.tvHeartrate);
        tvRespirationRate = (TextView) view.findViewById(R.id.tvRespirationrate);
        tvActivityIndex   = (TextView) view.findViewById(R.id.tvActivityIndex);
        tvBottomTitle     = (TextView) view.findViewById(R.id.tvBottomTitle);
        mChart            = (PieChart) view.findViewById(R.id.pcIndex);

        ivQualityHeart    = (ImageView) view.findViewById(R.id.ivQualityHeart);
        ivQualityResp     = (ImageView) view.findViewById(R.id.ivQualityResp);
        ivQualityActivity = (ImageView) view.findViewById(R.id.ivQualityActivity);
        ivStatusIndicator = (ImageView) view.findViewById(R.id.ivQualityWB);

        llFirstLayout     = (LinearLayout) view.findViewById(R.id.llFirstLayout);
        llSecondLayout    = (LinearLayout) view.findViewById(R.id.llSecondLayout);
        llThirdLayout     = (LinearLayout) view.findViewById(R.id.llThirdLayout);
        llFourthLayout    = (LinearLayout) view.findViewById(R.id.llFourthLayout);

        llHeartrate       = (LinearLayout) view.findViewById(R.id.llHeartrate);

        rlFitness         = view.findViewById(R.id.rlFitness);
        rlFC              = view.findViewById(R.id.rlFC);

        Description descr = new Description();
        descr.setText("");
        mChart.setDescription(descr);

        mChart.setDrawHoleEnabled(false);
        mChart.setDrawMarkers(false);
        mChart.getLegend().setEnabled(false);
        mChart.setRotationEnabled(false);

        ivHeartRate       = (ImageView) view.findViewById(R.id.ivHeartRate);
        ivRespirationRate = (ImageView) view.findViewById(R.id.ivRespirationRate);

        dvIntensity = (DecoView) view.findViewById(R.id.dvIntensity);

        tvFC.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvFitness.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvHeartRate.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvRespirationRate.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvActivityIndex.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvBottomTitle.setTypeface(Utils.getKnockoutFont(getActivity()));

        rlFitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MainActivity.increaseCounter();
                infoDialog(getResources().getString(R.string.prompt_wellbeing_index), getString(R.string.prompt_fitness_index));
            }
        });

        rlFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MainActivity.increaseCounter();
                startActivity(new Intent(getActivity(), FCActivity.class));
            }
        });

        pager = (ViewPager) view.findViewById(R.id.pager);

        final Handler handler = new Handler();
        new Thread(new Runnable(){
            @Override
            public void run(){
                //long running code
                //this is running on a background thread
                handler.post(new Runnable(){
                    @Override
                    public void run(){
                        //since the handler was created on the UI thread,
                        //this code will run on the UI thread
                        FragmentManager fm = getChildFragmentManager();
                        pagerAdapter = new MainViewPager(fm);
                        // Here you would declare which page to visit on creation
                        pager.setAdapter(pagerAdapter);
                        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {

                                switch (position){

                                    case 0 :

                                        if(MainActivity.getStatus()!=null &&
                                                MainActivity.getStatus().getWearable_status() == 1) {
                                            MainActivityMuscleFragment.enable(getActivity());
                                        }

                                        break;
                                    case 1:
                                        /*
                                         * Sending a request to get the status for each sensor
                                         */
                                        Request.getAsset(getActivity());

                                        if(MainActivity.getStatus()!=null &&
                                                MainActivity.getStatus().getWearable_status()==1){
                                            MainActivitySensorsFragment.initiateEnabled();
                                        }else
                                            MainActivitySensorsFragment.initiateDisabled();
                                        break;
                                }
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                        pager.setCurrentItem(0);

                        ivHeartRate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //isHeartRate = true;
                                //MainActivity.increaseCounter();
                                pager.setCurrentItem(2, true);
                            }
                        });

                        ivRespirationRate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //isHeartRate = false;
                                //MainActivity.increaseCounter();
                                pager.setCurrentItem(2, true);
                            }
                        });

                        CircleIndicator circleIndicator = (CircleIndicator) view.findViewById(R.id.indicator);
                        circleIndicator.setViewPager(pager);

                        SeriesItem seriesItemIntBackground = new SeriesItem.Builder(ContextCompat.getColor(getActivity(), R.color.grey_light_hlf))
                                .setRange(0, 100, 0)
                                .setLineWidth(6f)
                                .build();

                        SeriesItem seriesItemInt = new SeriesItem.Builder(ContextCompat.getColor(getActivity(), R.color.color_fc_light_pressed))
                                .setShadowSize(30f)
                                .setRange(0, 100, 0)
                                .setLineWidth(4f)
                                .build();

                        serieIntIndexBackground = dvIntensity.addSeries(seriesItemIntBackground);
                        dvIntensity.addEvent(new DecoEvent.Builder(100).setIndex(serieIntIndexBackground).setDelay(0).build());
                        serieIntIndex = dvIntensity.addSeries(seriesItemInt);
                        dvIntensity.configureAngles(180, 0);
                        addProgressListener(seriesItemInt, tvActivityIndex, "%.0f");

                        setDataForPieChart(0);
                    }
                });
            }
        }).start();



        //presentShowcaseSequence(ivHeartRate, ivRespirationRate, getActivity(),1200);
    }

    public void fadeOut(Context mContext){

        if(llFirstLayout.getVisibility() == View.INVISIBLE ||
                llFirstLayout.getVisibility() == View.GONE)
            return;

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                MainActivityMuscleFragment.disable(getActivity());
                MainActivitySensorsFragment.disable();

                ivHeartRate.setOnClickListener(null);
                ivRespirationRate.setOnClickListener(null);

                rlFitness.setOnClickListener(null);
                rlFC.setOnClickListener(null);
            }
        };
        mainHandler.postAtTime(myRunnable, 0);

        fadeOutViewAnimation(mContext, llFirstLayout,  500);
        fadeOutViewAnimation(mContext, llSecondLayout, 600);
        fadeOutViewAnimation(mContext, llThirdLayout,  700);
        fadeOutViewAnimation(mContext, llFourthLayout, 800);

        hideSequence();
    }

    public void enable(Context mContext){

        if(llFirstLayout.getVisibility() == View.VISIBLE)
            return;

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                //long running code
                //this is running on a background thread

                MainActivityMuscleFragment.enable(getActivity());
                MainActivitySensorsFragment.enable();

                ivHeartRate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //isHeartRate = true;
                        //MainActivity.increaseCounter();
                        pager.setCurrentItem(2, true);
                    }
                });

                ivRespirationRate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //isHeartRate = false;
                        //MainActivity.increaseCounter();
                        pager.setCurrentItem(2, true);
                    }
                });

                rlFitness.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //MainActivity.increaseCounter();
                        infoDialog(getActivity().getString(R.string.prompt_wellbeing_index),
                                getActivity().getString(R.string.prompt_fitness_index));
                    }
                });

                rlFC.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //MainActivity.increaseCounter();
                        getActivity().startActivity(new Intent(getActivity(), FCActivity.class));
                    }
                });
            }
        };
        mainHandler.post(myRunnable);

        //presentShowcaseSequence(rlFitness, rlFC, ivHeartRate, ivRespirationRate, dvIntensity, getActivity(), 1200);

        fadeInViewAnimation(mContext, llFirstLayout,  500);
        fadeInViewAnimation(mContext, llSecondLayout, 600);
        fadeInViewAnimation(mContext, llThirdLayout,  700);
        fadeInViewAnimation(mContext, llFourthLayout, 800);
    }

    private static void setDataForPieChart(int status) {
        ArrayList<PieEntry> yVals1 = new ArrayList<>();

        for (int i = 0; i < yValues.length; i++)
            yVals1.add(new PieEntry(yValues[i], i));

        // create pieDataSet
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setDrawValues(false);
        dataSet.setValueLineWidth(5f);
        //dataSet.setValueLineColor(colors_state.get(status)[5]);

        // adding colors
        ArrayList<Integer> colors = new ArrayList<>();

        // Added My Own colors
        if(status == 0)
            for (int c : Consts.colors_state.get(status))
                colors.add(c);
        else {
            if(status<6) {
                for (int c : Consts.colors_state.get(status - 1))
                    colors.add(c);
            }else{
                for (int c : Consts.colors_state.get(0))
                    colors.add(c);
            }
        }
        dataSet.setColors(colors);

        //  create pie data object and set xValues and yValues and set it to the pieChart
        PieData data = new PieData(dataSet);
        //   data.setValueFormatter(new DefaultValueFormatter());
        //   data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        mChart.clear();
        mChart.setData(data);
        mChart.highlightValues(null);
        // refresh/update pie chart
        mChart.invalidate();
        // animate piechart
        mChart.animateY(0);
    }

    // dynamic  activity index loading
    protected boolean mUpdateListeners = true;
    protected void addProgressListener(@NonNull final SeriesItem seriesItem, @NonNull final TextView view, @NonNull final String format) {
        if (format.length() <= 0) {
            throw new IllegalArgumentException("String formatter can not be empty");
        }

        seriesItem.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                if (mUpdateListeners) {
                    if (format.contains("%%")) {
                        // We found a percentage so we insert a percentage
                        float percentFilled = ((currentPosition - seriesItem.getMinValue()) / (seriesItem.getMaxValue() - seriesItem.getMinValue()));
                        view.setText(String.format(format, percentFilled * 100f));
                    } else {
                        view.setText(String.format(format, currentPosition));
                    }
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLeak = null; // now cleaning up!
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    public static void clearAll(final Activity activity){

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (tvRespirationRate != null)
                    tvRespirationRate.setText("0");

                if (tvHeartRate != null)
                    tvHeartRate.setText("0");

                if (dvIntensity != null)
                    dvIntensity.addEvent(new DecoEvent.Builder(0).setIndex(serieIntIndex).setDelay(0).build());

                MainActivityMuscleFragment.clearMuscles();

            }
        });
    }

    /**
     * Update activity index
     * @param activity context of MainActivity
     * @param data input values
     * data[2] valore identificativo del tipo di tocco
     */
    public static void updateActivityLevel(Activity activity, final String [] data){

        if (dvIntensity != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int arValue = (int)(100 * Float.valueOf(data[2]));
                    if (arValue > 100){
                        arValue = 100;
                    }else if(arValue < 0){
                        arValue = 0;
                    }
                    dvIntensity.addEvent(new DecoEvent.Builder(arValue).setIndex(serieIntIndex).setDelay(0).build());

                    if(ivQualityActivity!=null) {
                        if (data.length >= 4){
                            switch (data[3]) {
                                case "0.0":
                                    ivQualityActivity.setImageResource(R.mipmap.ic_indicator_poor);
                                    break;
                                case "1.0":
                                    ivQualityActivity.setImageResource(R.mipmap.ic_indicator_medium);
                                    break;
                                case "2.0":
                                    ivQualityActivity.setImageResource(R.mipmap.ic_indicator_strong);
                                    break;
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * Update respiration data
     * @param activity context of MainActivity
     * @param data input values
     * data[2] frequenza respiratoria istantanea
     * data[3] indice di qualità [0,1,2]
     */
    public static void updateRespRate(final Activity activity, final String [] data){
        if (tvRespirationRate != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String placeholder = ((int)Float.parseFloat(data[2])) + "";
                    tvRespirationRate.setText(placeholder);
                    if(ivQualityResp!=null) {
                        if (data.length >= 4){
                            switch (data[3]) {
                                case "0.0":
                                    ivQualityResp.setImageResource(R.mipmap.ic_indicator_poor);
                                    break;
                                case "1.0":
                                    ivQualityResp.setImageResource(R.mipmap.ic_indicator_medium);
                                    break;
                                case "2.0":
                                    ivQualityResp.setImageResource(R.mipmap.ic_indicator_strong);
                                    break;
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * Update heart data
     * @param activity context of MainActivity
     * @param data input values
     * data[3] : heart frequency percentage
     * data[4] : quality indicator
     */
    public static void updateHeartRate(final Activity activity, final String [] data){

        if (tvHeartRate != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String heartrate = ((int)Float.parseFloat(data[2])) + "";
                    tvHeartRate.setText(heartrate);
                }
            });
        }

        final float hfValue = Float.valueOf(data[3]);

        if (tvFC != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String placeholder = new DecimalFormat(".#").format(hfValue);
                    tvFC.setText(placeholder);
                }
            });
        }

        if (ibFC != null && !activity.isDestroyed()){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(hfValue <= 50f){
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_rest)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_rest));
                    }else if (hfValue <= 60f){
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_vlight)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_vlight));
                    }else if (hfValue <= 70f){
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_light)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_light));
                    }else if (hfValue <= 80f){
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_moderate)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_moderate));
                    }else if (hfValue <= 90f){
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_hard)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_hard));
                    }else {
                        Glide.with(activity)
                                .load("")
                                .placeholder(R.mipmap.fc_max)
                                .into(ibFC);
                        //ibFC.setImageDrawable(ContextCompat.getDrawable(MainActivity.getInstance(), R.mipmap.fc_max));
                    }
                }
            });
        }

        if (ivQualityHeart != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (data.length >= 5){
                        switch (data[4]) {
                            case "0.0":
                                ivQualityHeart.setImageResource(R.mipmap.ic_indicator_poor);
                                break;
                            case "1.0":
                                ivQualityHeart.setImageResource(R.mipmap.ic_indicator_medium);
                                break;
                            case "2.0":
                                ivQualityHeart.setImageResource(R.mipmap.ic_indicator_strong);
                                break;
                        }
                    }
                }
            });
        }
    }

    /**
     * update status indicator
     * @param activity context of MainActivity
     * @param data input values
     * data[2] : indice di forma, ovvero un valore da 0 a 6
     * data[3] : flag che indica lo stato dell’indice di forma [0,1,2]
     * data[4] : Valore di heart rate medio su cui è stato calcolato l’indice di forma
     */
    public void updatStatusIndicator(final Activity activity, final String [] data){

        if(ivStatusIndicator == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    switch (data[3]) {
                        case "0.0":
                            ivStatusIndicator.setImageResource(R.mipmap.ic_indicator_poor);
                            updateWBText(activity, ".", false);
                            break;
                        case "1.0":
                            ivStatusIndicator.setImageResource(R.mipmap.ic_indicator_medium);
                            updateWBText(activity, data[2], true);
                            break;
                        case "2.0":
                            ivStatusIndicator.setImageResource(R.mipmap.ic_indicator_strong);
                            updateWBText(activity, data[2], true);
                            break;
                    }
                }catch (ArrayIndexOutOfBoundsException a){
                    ivStatusIndicator.setImageResource(R.mipmap.ic_indicator_undefined);
                }
            }
        });

        SharedPrefDataHandler.saveWellbeingStatus(getActivity(), Float.valueOf(data[2]));
    }

    private static void updateWBText(Activity activity, final String input, final boolean update){

        if(tvFitness == null)
            return;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvFitness.setText(input);
                if(update)  setDataForPieChart((int)((float)Float.valueOf(input)));
            }
        });
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);

        infoDialogView.findViewById(R.id.llWellbeingStates).setVisibility(View.VISIBLE);

        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

    public static void goToSensors(){

        if(pager == null)
            return;

        pager.setCurrentItem(1);
    }
}