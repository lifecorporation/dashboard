package com.life.app.client.dashboard.services;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.LoginActivity;
import com.life.app.client.dashboard.activities.NotificationDialogActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.Intents;
import com.life.app.client.dashboard.constants.StatusConsts;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.constants.UuidDummy;
import com.life.app.client.dashboard.fragments.MainActivityMuscleFragment;
import com.life.app.client.dashboard.fragments.MainActivityRealTimeFragment;
import com.life.app.client.dashboard.fragments.menu.CalibrationFragment;
import com.life.app.client.dashboard.fragments.menu.RealTimeFragment;
import com.life.app.client.dashboard.fragments.radarChart.AerobicPower.AerobicPowerExperience;
import com.life.app.client.dashboard.fragments.radarChart.Agility.AgilityExperience;
import com.life.app.client.dashboard.fragments.radarChart.AnaerobicCapacity.AnaerobicCapacityExperience;
import com.life.app.client.dashboard.fragments.radarChart.Balance.BalanceExperience;
import com.life.app.client.dashboard.fragments.radarChart.Explosiveness.ExplosivenessExperience;
import com.life.app.client.dashboard.fragments.radarChart.ReactionTime.ReactionTimeExperience;
import com.life.app.client.dashboard.fragments.radarChart.Rhythm.RhythmExperience;
import com.life.app.client.dashboard.fragments.radarChart.Speed.SpeedExperience;
import com.life.app.client.dashboard.fragments.radarChart.Strength.StrengthExperience;
import com.life.app.client.dashboard.managers.PreferencesManager;
import com.life.app.client.dashboard.models.RegistrationInfo;
import com.life.app.client.dashboard.objects.Date;
import com.life.app.client.dashboard.protocols.BTProtocolFrame;
import com.life.app.client.dashboard.threads.ReaderThreadArchitecture;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.Status;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.objects.SyncStatus;
import com.life.services.bind.objects.WiFiFoundNetwork;
import com.life.services.bind.objects.WiFiStatus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import io.fabric.sdk.android.Fabric;

import static com.life.app.client.dashboard.Life.isAppForeground;
import static com.life.app.client.dashboard.constants.TAG._TAG_REACTION_TIME_;


/*
 * Created by Giuseppe Piccione on 24/06/2016.
 * background services to handle incoming messages from apps
 * and phone module
 */
public class BTConnectionManagerService extends Service{

    public static Context btConnManagerContext = null;
    public static BluetoothSocket serviceSocket = null;
    private static boolean connesso = false;
    public static boolean btConnected = false;

    static String TAG = "BindMessageHandler";
    static Messenger mClients = null;
    static ReaderThreadArchitecture rt = null;
    static BluetoothAdapter btAdapter = null;

    static boolean showDialogBatteryLow = true;

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    private static boolean connectedToOtherDevices = false;

    // status timer variables
    private static int statusTimeDelay = 5000;
    private static int getStatusCC     = 0x02;
    static Timer statusTimer           = null;

    // bt connection variables
    public static final UUID DEFAULT_SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static String macAddress;
    static Timer btConnectionTimer = null;
    private static int maxConnectionTries = 8;
    private static int actualTry = 1;

    public static String startServiceString ="com.life.app.dashbpard.service.BTCONNECTIONMANAGERSTART";

    // check app in foreground variables
    private static Timer foregroundTimer = null;
    private static int foregroundAppCheckDelay = 5000;

    private static String [] dataReceived = null;

    private static Activity mainActivity = null;

    // 0x00 no operation
    // 0x01 switched on
    // 0x02 switched off
    public static int WIFI_SWITCHED = 0x00;

    @Override
    public IBinder onBind(Intent intent){
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent){
        if (mClients != null ){
            mClients = null;
        }
        return true;
    }

    @Override
        public void onCreate() {
            super.onCreate();

        btConnManagerContext = this;

        unsetSocket();

        Fabric.with(this, new Crashlytics());

        btConnManagerContext=this;

        AccountManager accountManager = AccountManager.get(btConnManagerContext);
        Account[] arrayAccount= accountManager.getAccountsByType("com.life.x10y.account.X10YACCOUNT");

        //controllo che esista almeno un account
        if (arrayAccount.length < 1){
            stopSelf();

            //start login Activity to save a new account
            Intent myIntent = new Intent(btConnManagerContext, LoginActivity.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            btConnManagerContext.startActivity(myIntent);

            //stop service and exit
            return;
        }

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        showDisconnectedPhoneNotification();
    }

    @Override
    public void onTaskRemoved(Intent i){
        //Log.i("Service","service removed");

        Intent intent = new Intent();
        intent.setAction(startServiceString);

        btConnManagerContext.sendBroadcast(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        // check if BT is enabled, if not enable it
        if (btAdapter != null){
            if (btAdapter.isEnabled()){
                startTimerConnessioneBT();
            }
            registerReceiver(this);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        //Log.i("Service", "service distrutto");
        if (btConnectionTimer != null) {
            //Log.i("Service", "fermo anche il thread di connessione");
            stopTimerConnessioneBT();
            unregisterReceiver(this);
        }
    }

    // method to set the BT socket with the phone module
    public static void setSocket (BluetoothSocket btSocket){
        mainActivity = MainActivity.activity;
        serviceSocket = btSocket;

        avviaReaderThread();

        startGetStatusTimer();
        //startCheckAppForegroundTimer();
    }

    // method to release BT socket
    public static void unsetSocket (){

        if (rt != null){
            stoppaReaderThread();
        }

        stopGetStatusTimer();

        stopCheckAppForegroundTimer();

        // close socket
        if (serviceSocket != null && serviceSocket.isConnected()){
            try{
                serviceSocket.close();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        serviceSocket = null;
    }

    // method to get BT Socket
    public static BluetoothSocket getSocket (){
        if (serviceSocket != null){
            return serviceSocket;
        }
        else {
            return null;
        }
    }

    // Broadcast receiver for BT changes
    public final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            //Log.i("Check bt", "action " + action);

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        actualTry = 1;
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        unsetSocket();
                        stopTimerConnessioneBT();
                        // dismiss status notification
                        showDisconnectedPhoneNotification();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        actualTry = 1;
                        startTimerConnessioneBT();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED) ) {

                connectedToOtherDevices = true;
                // handling pairing with different type devices
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //Log.i("Check connessione"," mi sono connesso ma devo verificare il tipo di device connectedToOtherDevices " + connectedToOtherDevices);
                //Log.i("Check connessione"," mi sono connesso ma devo verificare il tipo device.getBluetoothClass().getDeviceClass() " + device.getBluetoothClass().getDeviceClass());
                // if connected with a smartphone and registered
                if (device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.PHONE_SMART){
                    //Log.i("Check connessione"," connesso con il device corretto");
                }

            }
            // if Bt status changes into disconnected
            else if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED) ) {
                connectedToOtherDevices = false;
                btConnected = false;
                // disconnected from a smartphone
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.PHONE_SMART){
                    connesso = false;

                    unsetSocket();

                    //send disconnected intent
                    sendDisconnectedIntent();

                    showDisconnectedPhoneNotification();

                    // if BT status changes into disconnected and BT connection thread is not running I start thread,
                    // otherwise i'm already seraching and I
                    startTimerConnessioneBT();
                }
            } else if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)) {
                if (Log.isLoggable("Check connessione", Log.INFO))
                    Log.i("Check connessione", " richiesta disconnessione");
            }
        }
    };


    // register broadcast receiver
    public void registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        //filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);

        context.registerReceiver(mReceiver, filter);
    }

    public void unregisterReceiver(Context context){
        context.unregisterReceiver(mReceiver);
    }

    private static ArrayList<WiFiFoundNetwork>   mWiFiList  = new ArrayList<>();

    public static ArrayList<WiFiFoundNetwork> getWiFiList() {
        return mWiFiList;
    }

    // handle incoming messages from the phone module
    public static void gestisciFrame(final BTProtocolFrame frame){
        // if Control code = event
        if (frame.getControl() == BTProtocolFrame.CTRL_EVENT ){
            // send broadcast with telephone number of incoming call
            Intent intent = new Intent();
            switch (frame.getCommandCode()){
                case BTProtocolFrame.CC_CALIBRATION_PROGRESS:
                    rispondiBinded(1, frame);
                    break;
                case BTProtocolFrame.CC_DATA:
                    if (false){
                        sendDataToDashboard(frame);
                    }
                    else{
                        if (Log.isLoggable(TAG, Log.INFO))
                            Log.i(TAG, "send message");
                        rispondiBinded(2, frame);
                    }
                    break;
                case BTProtocolFrame.CC_NEW_CALL:
                    frame.moveToData();
                    //int numberLength = frame.getFrame().getShort(3);
                    //Log.i("BTConnManager","numberLength " + numberLength);
                    frame.getFrame().position(4);
                    int nLength = frame.getFrame().get() & 0xFF;
                    byte [] b_number = new byte[nLength];
                    frame.getFrame().get();
                    frame.getFrame().get(b_number);
                    StringBuilder number = new StringBuilder("");

                    for (int i=0; i<nLength; i++){
                        //Log.i("BTConnManager","Chiamata in ingresso da " + ((char)b_number[i]));
                        number.append(((char)b_number[i]));
                    }
                    //Log.i("BTConnManager","numero " + number );

                    intent.putExtra("Data", number.toString());
                    intent.setAction(Intents.intentNewCall);
                    intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                    btConnManagerContext.sendBroadcast(intent);
                    break;
                case BTProtocolFrame.CC_HANG_UP:
                    intent.putExtra("start", 1);
                    intent.setAction(Intents.intentHangUp);
                    btConnManagerContext.sendBroadcast(intent);

                    break;
                case BTProtocolFrame.CC_GET_EXPERIENCE_STATUS:
                    intent.putExtra("data", frame.getFrame().array());
                    intent.setAction(Intents.intentExperienceStatus);
                    btConnManagerContext.sendBroadcast(intent);

                    break;
                case BTProtocolFrame.CC_SN_HARD_RESET:

                    intent.putExtra("data", frame.getFrame().array());
                    intent.setAction(Intents.intentSnHardReset);
                    btConnManagerContext.sendBroadcast(intent);

                    break;
                case BTProtocolFrame.CC_STRING_DATA:

                    break;
            }
        } else if (frame.getControl() == BTProtocolFrame.CTRL_ACK){
            Intent intent = new Intent();
            ByteBuffer bf;
            switch (frame.getCommandCode()){
                case BTProtocolFrame.CC_GETSTATUS:
                    frame.moveToData();

                    bf = ByteBuffer.allocate(frame.getDataSize());

                    frame.getFrame().get(bf.array());
                    //Mando in broadcast il pacchetto
                    // creo una istanza dello status
                    Status status = new Status(bf.array());
                    handleNotificationStatus(status);

                    intent.putExtra("Data",bf.array());
                    intent.setAction(Intents.intentStatus);
                    btConnManagerContext.sendBroadcast(intent);
                    break;

                case BTProtocolFrame.CC_GET_DATETIME:
                    generateDateTime(frame);
                    rispondiBinded(4, frame);
                    setIntentSuccess(BTProtocolFrame.CC_GET_DATETIME, "Date and time received successfully");
                    break;
                case BTProtocolFrame.CC_SET_DATETIME:
                    rispondiBinded(4, frame);
                    setIntentSuccess(BTProtocolFrame.CC_GET_DATETIME, "Date and time were set successfully");
                    break;
                case BTProtocolFrame.CC_LIST_SYNC:
                    rispondiBinded(3, frame);
                    break;
                case BTProtocolFrame.CC_START_SYNC:
                    rispondiBinded(3, frame);
                    setIntentSuccess(BTProtocolFrame.CC_START_SYNC, "Starting synchronising");
                    break;
                case BTProtocolFrame.CC_STOP_SYNC:
                    rispondiBinded(3, frame);
                    setIntentSuccess(BTProtocolFrame.CC_STOP_SYNC, "Stopping synchronising");
                    break;
                case BTProtocolFrame.CC_SYNC_STATUS:
                    rispondiBinded(3, frame);
                    setSyncStatus(frame);
                    setIntentSuccess(BTProtocolFrame.CC_SYNC_STATUS, "Sync status received");
                    break;
                case BTProtocolFrame.CC_GET_SYNC_CONFIGURE:
                    rispondiBinded(3, frame);
                    setIntentSuccess(BTProtocolFrame.CC_GET_SYNC_CONFIGURE, "Configuration was completed successfully");
                    break;
                case BTProtocolFrame.CC_START_CALIBRATION:
                    rispondiBinded(6, frame);
                    break;
                case BTProtocolFrame.CC_STOP_CALIBRATION:
                    rispondiBinded(6, frame);
                    break;
                case BTProtocolFrame.CC_GET_LOCALE:
                    rispondiBinded(7, frame);
                    break;
                case BTProtocolFrame.CC_SET_LOCALE:
                    rispondiBinded(7, frame);
                    break;
                case BTProtocolFrame.CC_PLACE_CALL:
                    intent.setAction(Intents.intentPlaceCall);
                    btConnManagerContext.sendBroadcast(intent);
                    break;
                case BTProtocolFrame.CC_HANG_UP:
                    intent.setAction(Intents.intentHangUp);
                    btConnManagerContext.sendBroadcast(intent);
                    break;
                case BTProtocolFrame.CC_GET_CALL_STATUS:
                    bf = ByteBuffer.allocate(frame.getDataSize());

                    intent.setAction(Intents.intentCallStatus);
                    intent.putExtra("Data",bf.array());
                    btConnManagerContext.sendBroadcast(intent);
                    break;
                case BTProtocolFrame.CC_ACTIVATE_AP:
                    rispondiBinded(8, frame);
                    break;
                case BTProtocolFrame.CC_GET_ASSET:

                    bf = ByteBuffer.allocate(frame.getFrameSize());
                    frame.getFrame().get(bf.array());

                    intent.putExtra("Data",bf.array());
                    intent.setAction(Intents.intentShirtSNStatus);
                    btConnManagerContext.sendBroadcast(intent);

                    break;
                case BTProtocolFrame.CC_SCAN_WIFI:
                    if (isAppForeground){
                        if (mWiFiList == null || mWiFiList.size() > 0)
                            mWiFiList = new ArrayList<>();
                        getWifis(frame);
                        setIntentSuccess(BTProtocolFrame.CC_SCAN_WIFI, "Scan was completed successfully");
                    }
                    else{
                        rispondiBinded(5, frame);
                    }


                    break;
                case BTProtocolFrame.CC_CONNECT_WIFI:
                    setIntentSuccess(BTProtocolFrame.CC_CONNECT_WIFI, "Connected successfully");
                    break;
                case BTProtocolFrame.CC_DISCONNECT_WIFI:
                    setIntentSuccess(BTProtocolFrame.CC_DISCONNECT_WIFI, "WiFi was successfully disconnected");
                    break;
                case BTProtocolFrame.CC_GET_WIFISTATUS:
                    if (isAppForeground){
                        getWifiStatus(frame);
                        setIntentSuccess(BTProtocolFrame.CC_GET_WIFISTATUS, "Wifi connection established");
                    }
                    else{
                        rispondiBinded(5, frame);
                    }
                    break;
                case BTProtocolFrame.CC_SWITCH_WIFI:
                    setIntentSuccess(BTProtocolFrame.CC_SWITCH_WIFI, "Wifi switched on");
                    break;
                default:
                    rispondiBinded(99, frame);
                    break;
            }
        } else if(frame.getControl() == BTProtocolFrame.CTRL_NACK){
            switch (frame.getCommandCode()){
                case BTProtocolFrame.CC_CONNECT_WIFI:
                    setIntentErr(BTProtocolFrame.CC_CONNECT_WIFI, "Failed to obtain IP. Please confirm that the password is correct");
                    break;
                case BTProtocolFrame.CC_DISCONNECT_WIFI:                                // NACK
                    setIntentErr(BTProtocolFrame.CC_DISCONNECT_WIFI, "Please check your WiFi connection");
                    break;
                case BTProtocolFrame.CC_SWITCH_WIFI:
                    setIntentErr(BTProtocolFrame.CC_SWITCH_WIFI, "Wifi switched on");
                    break;
                case BTProtocolFrame.CC_START_SYNC:
                    setIntentErr(BTProtocolFrame.CC_START_SYNC, "Error while attempting to start the synchronisation");
                    break;
                case BTProtocolFrame.CC_STOP_SYNC:
                    setIntentErr(BTProtocolFrame.CC_STOP_SYNC, "Error while attempting to stop the synchronisation");
                    break;
                case BTProtocolFrame.CC_SYNC_STATUS:
                    mSyncStatus = null;
                    setIntentErr(BTProtocolFrame.CC_SYNC_STATUS, "Error while attempting to get the sync status");
                    break;
                case BTProtocolFrame.CC_GET_SYNC_CONFIGURE:
                    rispondiBinded(3, frame);
                    setIntentErr(BTProtocolFrame.CC_GET_SYNC_CONFIGURE, "Configuration was completed successfully");
                    break;
                case BTProtocolFrame.CC_GET_DATETIME:
                    rispondiBinded(4, frame);
                    setIntentErr(BTProtocolFrame.CC_GET_DATETIME, "Date and time received successfully");
                    break;
                case BTProtocolFrame.CC_SET_DATETIME:
                    rispondiBinded(4, frame);
                    setIntentErr(BTProtocolFrame.CC_GET_DATETIME, "Date and time were set successfully");
                    break;
                default:
                    rispondiBinded(50,frame);
                    if (Log.isLoggable("ControlloFrame", Log.ERROR))
                        Log.e("ControlloFrame","codice errore " + frame.bytes()[0]+" " + frame.bytes()[1]+" " + frame.bytes()[2]+" " + frame.bytes()[3]+" " + frame.bytes()[4]+" " + frame.bytes()[5]);
                    break;
            }
        } /*else{
            if (Debug.isDebuggerConnected()) {
                for (int i = 0; i < frame.bytes().length; i++) {
                    Log.i("BTFrame", "byte " + i + ": " + frame.bytes()[i]);
                }
            }
        }*/
    }

    private static void rispondiBinded(int code, BTProtocolFrame frame){
        if(Log.isLoggable(TAG, Log.INFO))
            Log.i(TAG, "rispondiBinded");

        if (mClients != null) {
            try {
                Log.i(TAG, "rispondiBinded / mClients not null");
                Bundle b = new Bundle();
                Message msg1 = Message.obtain(null, code);
                byte [] bframe = new byte[frame.getFrameSize()];
                for (int i=0; i < bframe.length; i++){
                    bframe[i] = frame.bytes()[i];
                }
                b.putByteArray("Data",bframe);
                msg1.setData(b);
                mClients.send(msg1);
            } catch (Exception e) {
                //if (BuildConfig.DEBUG)
                e.printStackTrace();
                if(Log.isLoggable(TAG, Log.INFO))
                    Log.i(TAG, "Unable to send response");
            }
        }
    }

    private static Date date = null;

    public static Date getDate(){
        return date;
    }

    private static void generateDateTime(BTProtocolFrame frame){
        frame.moveToData();

        int automatic = frame.getFrame().get();
        int len = frame.getFrame().get();

        ByteBuffer buf = ByteBuffer.allocate(len);

        for(int i=0;i<len;i++){
            buf.put(frame.getFrame().get());
        }
        try {
            date = new Date(btConnManagerContext, automatic, new String(buf.array(), "ASCII"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Decodes the BTProtocolFrame object and generates the SyncStatus object where all the necessary info regarding
     * the Sync, is stored
     *
     * @param  frame  Message object from the device to the smartscreen
     * @see    com.life.services.bind.protocols.BTProtocolFrame
     * @see    SyncStatus
     */
    private static SyncStatus mSyncStatus = null;
    private static void setSyncStatus(BTProtocolFrame frame) {
        frame.moveToData();
        frame.getFrame().position(5);

        byte status             =  frame.getFrame().get();
        byte percentage         =  frame.getFrame().get();
        int speed               =  frame.getFrame().getInt();
        int estimatedTime       =  frame.getFrame().getInt();
        int filesCount          =  frame.getFrame().getInt();
        int currentFileNumber   =  frame.getFrame().getInt();

        mSyncStatus = new SyncStatus(status, percentage, speed, estimatedTime, filesCount, currentFileNumber);
    }

    /**
     * Returns the SyncStatus object
     *
     * @return mListSync
     * @see SyncStatus
     */
    public static SyncStatus getSyncStatus(){
        return mSyncStatus;
    }

    private static void rispondiBindedErrore(String messaggio){
        if (mClients != null) {
            try {
                Bundle b = new Bundle();
                Message msg1 = Message.obtain(null, 51);
                b.putString("Data",messaggio);
                msg1.setData(b);
                mClients.send(msg1);
            } catch (Exception e) {
                //if (BuildConfig.DEBUG)
                    //Log.i(TAG, "accidenti non ci sono riuscito");
                //Log.e(TAG, e.getMessage());
            }
        }
    }

    private class IncomingHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {

            mClients= msg.replyTo;

            if ( connesso && serviceSocket!= null && serviceSocket.isConnected()){
                //Log.i("Service"," Messaggio what "  + msg.what);
                if (Log.isLoggable("Service", Log.INFO))
                    Log.i("Service"," stato socket "  + serviceSocket.isConnected());

                switch (msg.what) {
                    case 5:
                        send(msg.getData().getByteArray("Data"));
                        ReaderThreadArchitecture.mesaggiRicevuti=0;
                        break;
                    // testing case
                    case 6:

                        Uri path = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+ "");
                        File file = new File(path + "/contacts.vcf");

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file) );

                        //list of apps that can handle our intent
                        PackageManager pm = getPackageManager();
                        List<ResolveInfo> appsList = pm.queryIntentActivities( intent, 0);

                        if(appsList.size() > 0) {
                            //select bluetooth
                            String packageName = null;
                            String className = null;
                            boolean found = false;

                            for(ResolveInfo info: appsList){
                                packageName = info.activityInfo.packageName;
                                if( packageName.equals("com.android.bluetooth")){
                                    className = info.activityInfo.name;
                                    found = true;
                                    break;// found
                                }
                            }
                            if(! found){
                                if (Log.isLoggable("Trasferimento", Log.ERROR))
                                    Log.e("Trasferimento", "BT non trovato per il trasferimento");
                            }
                            else {
                                if (Log.isLoggable("Trasferimento", Log.ERROR))
                                    Log.e("Trasferimento", "Invio il file");

                                //set our intent to launch Bluetooth
                                intent.setClassName(packageName, className);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    break;
                    default:
                        super.handleMessage(msg);
                }
            }
            else {
                rispondiBindedErrore("Problemi di socket");
                //if (BuildConfig.DEBUG)
                    //Log.i(TAG, "socket non pronta");
            }
        }
    }

    // prepare messages for WDL
    private byte[] prepareRequest(int cc, byte[] payload) {
        ByteBuffer frame;
        if (payload != null) frame = ByteBuffer.allocate(8 + payload.length);
        else frame = ByteBuffer.allocate(8);

        frame.order(ByteOrder.BIG_ENDIAN);
        frame.put((byte) 0x02);
        frame.put((byte) 0x00);
        frame.put((byte) cc);
        if (payload != null) {
            frame.putShort((short) payload.length);
            frame.put(payload);
        } else {
            frame.putShort((byte) 0x00);
        }
        frame.putShort((short) 0);          // crc16
        frame.put((byte) 0x03);             // ETX
        return frame.array();
    }

    //method to start BT connection thread
    private static void startTimerConnessioneBT() {

        if (btConnectionTimer != null){
            if(Log.isLoggable("provaAlarmBTConnection", Log.ERROR))
                Log.e("provaAlarmBTConnection", "non era stato resettato il timer");
            btConnectionTimer.cancel();
            btConnectionTimer.purge();
            btConnectionTimer = null;
        }

        //Declare the timer
        btConnectionTimer = new Timer();
        //Set the schedule function and rate
        btConnectionTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //Called each time when 1000 milliseconds (1 second) (the period parameter)
                    if(Log.isLoggable("provaAlarmBTConnection", Log.INFO))
                        Log.i("provaAlarmBTConnection","invio richiesta connessione");
                    connettiDevices();
                }
            },100,      //Set how long before to start calling the TimerTask (in milliseconds)
                5000);  //Set the amount of time between each execution (in milliseconds)
    }

    //method to stop BT connection thread
    public static void stopTimerConnessioneBT(){
        if(btConnectionTimer != null) {
            //if (BuildConfig.DEBUG)
               // Log.i("provaAlarmBTConnection", "elimino timer  " + btConnectionTimer.toString());
            btConnectionTimer.cancel();
            btConnectionTimer.purge();
            btConnectionTimer = null;
        }
    }

    private static void connettiDevices(){
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_UUID);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        BTConnectionManagerService.btConnManagerContext.registerReceiver(actionFoundReceiver, filter);

        macAddress = PreferencesManager.getMac((BTConnectionManagerService.btConnManagerContext));

        boolean connettiConPaired = false;
        BluetoothDevice myDevice = null;
        // get list of paired devices
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        for (BluetoothDevice device : pairedDevices) {
            if(Log.isLoggable("BTCOnnection", Log.INFO))
                Log.i("BTCOnnection", " ottengo la lista dei device pairati " + device.getName() + "  address: " + device.getAddress() + "   tipo " + device.getType() + "  mio device " + macAddress);
            if( device.getType() == 1 && device.getAddress().equals(macAddress) ){
                myDevice = device;
                connettiConPaired = true;
            }
        }
        if (connettiConPaired){
            stopTimerConnessioneBT();
            try {
                connectDirect(myDevice, DEFAULT_SPP_UUID);
                // make the thread sleep to not overload the device
                //Thread.sleep(8000);
            } catch (Exception e) {
                //if (BuildConfig.DEBUG)
                    //Log.e("BTConnThread", "dispositivi pairati errore nella connsessione");
            }
        }
        else{
            if (!btAdapter.isDiscovering()) {
                btAdapter.startDiscovery();
                // wait until the next try to not overload the device
                try {
                    Thread.sleep(8000);
                } catch (Exception e) {

                }
            }
        }
    }

    public static final BroadcastReceiver actionFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(Log.isLoggable("BtManager", Log.INFO))
                    Log.i("BtManager", "mac dispositivo trovato: " + device.getName() + "    " + device.getAddress() + "     mac mio: " + macAddress);
                try {
                    if (device.getAddress().equals(macAddress)) {
                        //stopTimerConnessioneBT();
                        connectDirect(device, DEFAULT_SPP_UUID);
                        stopTimerConnessioneBT();
                    }
                } catch (Exception e) {
                    if (Log.isLoggable("BTConnThread", Log.ERROR))
                        Log.e("BTConnThread", "Broadcast errore nella connsessione");
                    BTConnectionManagerService.restartDiscovery();
                }
            }/*else {
                if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                    if (BuildConfig.DEBUG)
                        Log.i("Service", "\nDiscovery Started...");
                }
                else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                    if (BuildConfig.DEBUG)
                        Log.i("Service", "\nDiscovery Finished");
                }
            }*/
        }
    };

    private static void connectDirect(final BluetoothDevice device, final UUID serviceUuid) {

        try {
            if(serviceSocket!=null){
                serviceSocket.close();
                if (Log.isLoggable("serviceSocket", Log.INFO))
                    Log.i("serviceSocket", "serviceSocket is not null and closing");
            }/*else{
                if (BuildConfig.DEBUG)
                    Log.i("serviceSocket", "serviceSocket is null");
            }*/

            serviceSocket = device.createRfcommSocketToServiceRecord(serviceUuid);
            if(Log.isLoggable("serviceSocket", Log.INFO))
                Log.i("serviceSocket", "called createRfcommSocketToServiceRecord");
            btAdapter.cancelDiscovery();
            if(Log.isLoggable("serviceSocket", Log.INFO))
                Log.i("serviceSocket", "called cancelDiscovery");
            serviceSocket.connect();
            if(Log.isLoggable("Service", Log.INFO))
                Log.i("Service", "sono dentro il connect direct");
            //Toast.makeText(BTConnectionManagerService.btConnManagerContext,"Mi sono connesso",Toast.LENGTH_LONG).show();

            if (register()) {
                return;
            }

        } catch (IOException eConnectException) {
            if(Log.isLoggable("Connessione", Log.ERROR))
                Log.e("Connessione", "CouldNotConnectToSocket", eConnectException);
            closeSocket(serviceSocket);
            BTConnectionManagerService.restartDiscovery();

            // increase tries counter
            actualTry++;

            if(Log.isLoggable("check connessione", Log.INFO))
                Log.i("check connessione","actualTry " + actualTry);
        } catch (Exception e){
            e.printStackTrace();
            if(Log.isLoggable("check connessione", Log.INFO))
                Log.i("check connessione","actualTry " + actualTry);
        }
    }

    private static void closeSocket(BluetoothSocket socket) {
        try {
            //Log.i("Service", "chiudo la socket");
            socket.close();
        } catch (Exception e) {
            if(Log.isLoggable("Service", Log.INFO))
                Log.i("Service", "non sono riuscito a chiudere la socket");
        }
    }

    private static boolean register() {

        OutputStream out = null;
        InputStream in = null;

        if (serviceSocket != null) {

            AccountManager accountManager = AccountManager.get(BTConnectionManagerService.btConnManagerContext);
            if (ActivityCompat.checkSelfPermission(BTConnectionManagerService.btConnManagerContext, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            Account[] arrayAccount = accountManager.getAccountsByType("com.life.x10y.account.X10YACCOUNT");
            //Log.i("BTThread","AarrayAccount " + arrayAccount.length);

            //qui username e password li devo recuperare dai dati dell'account quindi adesso la devo sistemare
            //byte[] username=null;
            //byte[] password=null;

            RegistrationInfo ri = PreferencesManager.getRegistrationInfo(btConnManagerContext);

            //TODO inserire la gestione dei dati criptati
            for (int i=0; i<arrayAccount.length; i++){
                //Log.i("BTThread","user " + arrayAccount[i].name  + " password  " + accountManager.getPassword(arrayAccount[i]));
                //username = arrayAccount[i].name.getBytes();
                //password = accountManager.getPassword(arrayAccount[i]).getBytes();
            }


            byte[] imei      = PreferencesManager.getImei((BTConnectionManagerService.btConnManagerContext)).getBytes();
            int userid       = PreferencesManager.getUserId(BTConnectionManagerService.btConnManagerContext);
            String password  = PreferencesManager.getPass(BTConnectionManagerService.btConnManagerContext);
            String username  = PreferencesManager.getUsername(BTConnectionManagerService.btConnManagerContext);

            // Payload
            if(Log.isLoggable("connectionThread", Log.INFO))
                Log.i("connectionThread","USERNAME: " + username + "  PASSWORD: " + password + "  USERID: " + userid + "  IMEI: " + imei);

            try{
                ByteBuffer payload = ByteBuffer.allocate(1024);
                payload.order(ByteOrder.BIG_ENDIAN);
                payload.putInt(userid); //userId
                payload.put(imei); // imei
                payload.put((byte) (username.getBytes().length & 0xff)); //user name len
                payload.put(username.getBytes()); //user name
                payload.put((byte) (password.getBytes().length & 0xff)); //password len
                payload.put(password.getBytes()); // password name

                int payloadLen = payload.position();

                ByteBuffer frame = ByteBuffer.allocate(payloadLen + 8);
                frame.order(ByteOrder.BIG_ENDIAN);
                frame.put((byte) 0x02);
                frame.put((byte) 0x00);
                frame.put((byte) 0x01); // register
                frame.putShort((short) payloadLen); // payload len
                payload.rewind();
                frame.put(payload.array(), 0, payloadLen); // payload
                frame.putShort((short) 0x0); // crc16
                frame.put((byte) 0x03); // ETX


                out = serviceSocket.getOutputStream();
                in = serviceSocket.getInputStream();

                out.write(frame.array(), 0, frame.position());

                final byte[] read = new byte[1024];

                in.read(read);
                //if (BuildConfig.DEBUG)
                    //Log.i("connectionThread","leggo ack o nack");
                /*
                Log.i("Service", "Vediamo quanto è grande questo array " + read.length + " e vediamo il contenuto " + read[1]);
                Log.i("Service", "Errore coneesione  e vediamo il contenuto " + read[4]);
                Log.i("Service", "Vediamo quanto è grande questo array " + read.length);
                */

                //checkif register successfull
                if (read[1] == BTProtocolFrame.CTRL_ACK){
                    // Ho verificato che la connessione è stata autenticata e quindi ora passo la socket al servizio di gestione del BT
                    BTConnectionManagerService.setSocket(serviceSocket);
                    // stop BT connection thread
                    //if (BuildConfig.DEBUG)
                        //Log.i("connectionThread","ricevuto un ack");
                    actualTry = 1;
                    setWeAreConnected();
                    return true;
                }
                else {
                    //if (BuildConfig.DEBUG)
                        //Log.i("connectionThread","ricevuto un nack");
                    Toast.makeText(BTConnectionManagerService.btConnManagerContext,"Errore nel register",Toast.LENGTH_LONG).show();
                    serviceSocket.close();
                    //unsetSocket();
                    return false;
                }
            } catch (Exception e) {
                if(Log.isLoggable("Service", Log.ERROR))
                    Log.e("Service","C'è stato qualche errore in fase di registrazione");
                return false;
            }
        } else {
            if(Log.isLoggable("Service", Log.ERROR))
                Log.e("Service","il socket non esiste più e non è una cosa buona");
            return false;
        }

    }

    private static void setWeAreConnected(){
        stopTimerConnessioneBT();

        dismissDisconnectedNotification();

        connesso = true;

        btConnected = true;

//        btConnectionTimer.cancel();
//        btConnectionTimer.purge();
//        btConnectionTimer = null;

        actualTry = 1;

    }

    //method to start get status thread
    private static void startGetStatusTimer(){

        //Declare the timer
        statusTimer = new Timer();
        //Set the schedule function and rate
        statusTimer.scheduleAtFixedRate(new TimerTask() {
              @Override
              public void run() {
                  //Called each time when 1000 milliseconds (1 second) (the period parameter)
                  //Log.i("provaAlarmStatus","invio richiesta");
                  byte[] data = prepareStatusRequest(getStatusCC);
                  send(data);
              }
          },    1000,//Set how long before to start calling the TimerTask (in milliseconds)
                statusTimeDelay);//Set the amount of time between each execution (in milliseconds)
    }

    //method to stop get status thread
    private static void stopGetStatusTimer(){
        if(statusTimer != null) {
            statusTimer.cancel();
            statusTimer.purge();
            statusTimer = null;
        }
    }

    //method to start get status thread
    private static void startCheckAppForegroundTimer(){

        //Declare the timer
        foregroundTimer = new Timer();
        //Set the schedule function and rate
        foregroundTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //Called each time when 1000 milliseconds (1 second) (the period parameter)
                    //Log.i("provaAlarmStatus","invio richiesta");
                    /*
                    if (isAppOnForeground(btConnManagerContext)){
                        Log.i("Service","app in foreground");
                        mainActivity = MainActivity.activity;
                        isForeground = true;
                    }
                    else {
                        Log.i("Service","app in background");
                        isForeground = false;
                        mainActivity = null;

                    }
                    */
                }
            },1000,                         //Set how long before to start calling the TimerTask (in milliseconds)
                foregroundAppCheckDelay);   //Set the amount of time between each execution (in milliseconds)
    }

    //method to stop get status thread
    private static void stopCheckAppForegroundTimer(){
        if(foregroundTimer != null) {
            foregroundTimer.cancel();
            foregroundTimer.purge();
            foregroundTimer = null;
        }
    }

    //method to start data reader thread
    private static void avviaReaderThread() {
        if (rt == null || rt.getState().toString().equals("TERMINATED")) {

            if(Log.isLoggable("Service", Log.INFO))
                Log.i("Service", "avviaThread di lettura");
            rt = new ReaderThreadArchitecture(serviceSocket);
            rt.running = true;
            rt.start();
        } else {
            rt.running = true;
            rt.start();
        }

        //openFileOutput();
    }

    //method to stop data reader thread
    private static void stoppaReaderThread(){
        if (rt != null) {
            rt.interrupt();
            rt.running = false;
        }

        //closeFileOutput();
    }

    static OutputStreamWriter osw = null;

    public static boolean openFileOutput(){
        //if (BuildConfig.DEBUG)
            //Log.i("ScriviFile","devo scrivere il log");

        try {
            Uri path = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+ "/");
            File file = new File(path + "statistiche.txt");
            FileOutputStream out = new FileOutputStream(path + "tempi_pacchetti.txt", true);
            osw = new OutputStreamWriter(out);

            return true;
        } catch (IOException e) {
            if(Log.isLoggable("Exception", Log.ERROR))
                Log.e("Exception", "File write failed: " + e.toString());
            return false;
        }
    }

    public static boolean closeFileOutput() {
        try{
            osw.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private static void writeToFile(UUID uuid, long timestampDato, long timestampRicezione) {

        //if (BuildConfig.DEBUG)
            //Log.i("ScriviFile","devo scrivere il log");

        try {
            //Uri path = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+ "/");
            //File file = new File(path + "statistiche.txt");
            //FileOutputStream out = new FileOutputStream(path + "tempi_pacchetti.txt", true);
            //OutputStreamWriter osw = new OutputStreamWriter(out);
            osw.write("uuid: " + uuid.toString() + "    timestampDato: " + timestampDato + "    timestampRicezione: " + timestampRicezione + "\n");
            //osw.close();

        } catch (IOException e) {
            if(Log.isLoggable("Exception", Log.ERROR))
                Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void sendDisconnectedIntent(){
        if(Log.isLoggable("BTConnManager", Log.INFO))
            Log.i("BTConnManager","mando intent disconnesso ");
        Intent intent = new Intent();
        intent.setAction(Intents.intentActionDisconnected);
        btConnManagerContext.sendBroadcast(intent);
    }

    public static void send(byte [] data){

        try {

            OutputStream out = serviceSocket.getOutputStream();

            out.write(data);

            if(Log.isLoggable(TAG, Log.INFO))
                Log.i(TAG, "ho inviato il messaggio");

        } catch (IOException e) {
            if(Log.isLoggable(TAG, Log.INFO))
                Log.i(TAG, "accidenti non ci sono riuscito");
            e.printStackTrace();
            if(Log.isLoggable(TAG, Log.ERROR))
                Log.e(TAG, e.getStackTrace().toString());
        }
    }

    // function to stop all running thread and restart BT discovery
    public static void restartDiscovery(){
        stopGetStatusTimer();

        stopCheckAppForegroundTimer();

        stoppaReaderThread();

        btConnected = false;

        if(Log.isLoggable("Restarta", Log.INFO))
            Log.i("Restarta", "riparte il discovery");

        try {
            serviceSocket.getInputStream().close();
            serviceSocket.getOutputStream().close();
            serviceSocket.close();

            serviceSocket = null;

        } catch (IOException e){
            e.printStackTrace();
        }

        // if we reached the maximum bt connection tries means we are too far from the phone so we can turn off the BT if anything else is connected
        /*
        if(actualTry >= maxConnectionTries && ! connectedToOtherDevices){
            btAdapter.disable();
        }
*/
        startTimerConnessioneBT();

    }

    // build status messages and notify
    private static void handleNotificationStatus(Status s){

        dismissDisconnectedNotification();

        Notification.Builder n;

        // notification for battery level
        int batteryLevel = s.getBattery_percentage();
        if (batteryLevel > 80){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Phone Battery Level")
                    .setContentText(s.getBattery_percentage() + "%")
                    .setSmallIcon(R.mipmap.ic_notification_battery_full)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setColor(0x00AAEC);

            showDialogBatteryLow = true;
        } else if (batteryLevel > 40){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Phone Battery Level")
                    .setContentText(s.getBattery_percentage() + "%")
                    .setSmallIcon(R.mipmap.ic_notification_battery_charged)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setColor(0x00AAEC);

            showDialogBatteryLow = true;
        } else if (batteryLevel > 20){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Phone Battery Level")
                    .setContentText(s.getBattery_percentage() + "%")
                    .setSmallIcon(R.mipmap.ic_notification_battery_low)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setColor(0x00AAEC);

            showDialogBatteryLow = true;
        } else {
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Phone Battery Level")
                    .setContentText(s.getBattery_percentage() + "%")
                    .setSmallIcon(R.mipmap.ic_notification_battery_empty)
                    .setAutoCancel(true)
                    .setOngoing(true)
                    .setColor(0x00AAEC);

            // send dialog battery low
            if (showDialogBatteryLow){

                /*
                AlertDialog alertDialog = new AlertDialog.Builder(BTConnectionManagerService.btConnManagerContext).create();
                alertDialog.setTitle("Battery low");
                alertDialog.setMessage("Battery is running low. Please charge");
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                //alertDialog.setIcon(R.drawable.icon);

                alertDialog.show();
                */

                showDialogBatteryLow = false;
            }
        }
        // Creates an Intent for the Activity
        Intent notifyIntent =
                new Intent(BTConnectionManagerService.btConnManagerContext, NotificationDialogActivity.class);
        // Sets the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Creates the PendingIntent
        PendingIntent notifyPendingIntent =
                PendingIntent.getActivity(
                        BTConnectionManagerService.btConnManagerContext,
                        0,
                        notifyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        // Puts the PendingIntent into the notification builder
        n.setContentIntent(notifyPendingIntent);

        // Notifications are issued by sending them to the
        // NotificationManager system service.
        NotificationManager mNotificationManager = (NotificationManager)
                BTConnectionManagerService.btConnManagerContext.getSystemService(Context.NOTIFICATION_SERVICE);
        // Builds an anonymous Notification object from the builder, and
        // passes it to the NotificationManager
        mNotificationManager.notify(StatusConsts.NOTIFICATION_BATTERY_LEVEL, n.build());

        NotificationDialogActivity.setBatteryLevel(batteryLevel);
        NotificationDialogActivity.setTshirtStatus(s);
        NotificationDialogActivity.setCloudStatus(s);
        NotificationDialogActivity.setStorageStatus(s);


        /*
        // notification for wearable
        if(s.getWearable_status() != 0) {

// build notification
// the addAction re-use the same intent to keep the example short
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Wearable")
                    .setContentText("Connected to a shirt")
                    .setSmallIcon(R.drawable.shirt)
                    .setAutoCancel(true)
                    .setOngoing(true).build();

            notificationManager.notify(StatusConsts.NOTIFICATION_WEARABLE_STATUS, n);
        }
        else {
            // Dismiss wearable notify
            //NotificationManager mNotificationManager = (NotificationManager)  BTConnectionManagerService.btConnManagerContext.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(StatusConsts.NOTIFICATION_WEARABLE_STATUS);
        }

        // notification for SIM status
        if (s.getPhone_sim_ready() != TelephonyManager.SIM_STATE_READY) {
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("SIM problem")
                    .setContentText("Unable to access SIM")
                    .setSmallIcon(R.drawable.sim_error)
                    .setAutoCancel(true)
                    .setOngoing(true).build();

            notificationManager.notify(StatusConsts.NOTIFICATION_SIM_STATUS, n);
        }
        else{
            notificationManager.cancel(StatusConsts.NOTIFICATION_SIM_STATUS);

            // notification for GSM signal status
            int gsmLevel = s.getPhone_level();

            switch(gsmLevel){
                case 0:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_0)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case 1:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_1)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case 2:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_2)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case 3:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_3)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case 4:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;

            }


        }

        // notification for mobile data connection and type
        //TODO Sistemare le icone del type che vanno integrate a quelle del livello
        /*
        if (s.getPhone_connected() == 1){

            int networkType = s.getPhone_network_type();

            switch (networkType){
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("GSM Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.signal_status_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS, n);
                    break;
            }
        }
        */

        /*
        // notification for wifi status
        int wifiStatus = s.getWifi_status();
        if (wifiStatus == 1){
            int wifiLevel = s.getWifi_level();
            switch (wifiLevel){
                case 0:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("WiFi Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.wifi_level_0)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_WIFI_STATUS, n);
                    break;
                case 1:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("WiFi Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.wifi_level_1)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_WIFI_STATUS, n);
                    break;
                case 2:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("WiFi Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.wifi_level_2)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_WIFI_STATUS, n);
                    break;
                case 3:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("WiFi Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.wifi_level_3)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_WIFI_STATUS, n);
                    break;
                case 4:
                    n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                            .setContentTitle("WiFi Level")
                            //.setContentText("Unable to access cloud")
                            .setSmallIcon(R.drawable.wifi_level_4)
                            .setAutoCancel(true)
                            .setOngoing(true).build();
                    notificationManager.notify(StatusConsts.NOTIFICATION_WIFI_STATUS, n);
                    break;
            }
        }
        else{
            notificationManager.cancel(StatusConsts.NOTIFICATION_WIFI_STATUS);
        }

        // notification for sorage status
        if (s.getStorage_percentage()<= 20){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Storage Alert")
                    .setContentText("Phone module storage almost full")
                    .setSmallIcon(R.drawable.storage_full)
                    .setAutoCancel(true)
                    .setOngoing(true).build();
            notificationManager.notify(StatusConsts.NOTIFICATION_STORAGE_STATUS, n);
        }
        else{
            notificationManager.cancel(StatusConsts.NOTIFICATION_STORAGE_STATUS);
        }

        // notification for cloud status
        if(s.getCloud_connected() == 0){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Cloud not connected")
                    .setContentText("Unable to access cloud")
                    .setSmallIcon(R.drawable.cloud_not_connected)
                    .setAutoCancel(true)
                    .setOngoing(true).build();
            notificationManager.notify(StatusConsts.NOTIFICATION_CLOUD_STATUS, n);
        }
        else if(s.getCloud_connected() == 2){
            n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                    .setContentTitle("Cloud error")
                    .setContentText("Error accessing cloud")
                    .setSmallIcon(R.drawable.cloud_error)
                    .setAutoCancel(true)
                    .setOngoing(true).build();
            notificationManager.notify(StatusConsts.NOTIFICATION_CLOUD_STATUS, n);
        }
        else{
            notificationManager.cancel(StatusConsts.NOTIFICATION_CLOUD_STATUS);
        }

*/

    }

    // dismiss all status notification
    private static void dismissStatusNotification(){
        NotificationManager mNotificationManager = (NotificationManager)  BTConnectionManagerService.btConnManagerContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Dismiss wearable notify
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_WEARABLE_STATUS);

        //Log.i("dismissStatusNotifica","elimino notifica batteria");
        // dismiss battery notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_BATTERY_LEVEL);
        //Log.i("dismissStatusNotifica","notifica batteria eliminata");

        // dismiss SIM status notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_SIM_STATUS);

        // dismiss cloud status notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_CLOUD_STATUS);

        // dismiss storage status notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_STORAGE_STATUS);

        // dismiss GSM status notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_GSM_LEVEL_STATUS);

        // dismiss Wifi status notification
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_WIFI_STATUS);
    }

    // show the notification icon when the phone module and the smartscreen are disconnected
    private void showDisconnectedPhoneNotification(){

        dismissStatusNotification();

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification n = new Notification.Builder(BTConnectionManagerService.btConnManagerContext)
                .setContentTitle("Phone Module")
                .setContentText("Phone Module not connected")
                .setSmallIcon(R.mipmap.ic_device_disconnected)
                .setAutoCancel(true)
                .setOngoing(true)
                .setColor(0x00AAEC)
                .build();

        mNotificationManager.notify(StatusConsts.NOTIFICATION_SMARTSCREEN_DISCONNECTED, n);

        NotificationDialogActivity.setBatteryLevel(-1);
        NotificationDialogActivity.setTshirtStatus(null);
        NotificationDialogActivity.setCloudStatus(null);
        NotificationDialogActivity.setStorageStatus(null);
    }

    // dismiss the disconnected notification
    private static void dismissDisconnectedNotification(){
        //Log.i("BTConnectionService","DIsmiss disconnected notification");
        NotificationManager mNotificationManager = (NotificationManager) btConnManagerContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(StatusConsts.NOTIFICATION_SMARTSCREEN_DISCONNECTED);
    }

    // prepare status requestMessage
    private static byte[] prepareStatusRequest(int cc) {
        ByteBuffer frame = ByteBuffer.allocate(8);

        frame.order(ByteOrder.BIG_ENDIAN);
        frame.put((byte) 0x02);
        frame.put((byte) 0x00);
        frame.put((byte) cc);

        frame.putShort((byte) 0x00);
        frame.putShort((short) 0);          // crc16
        frame.put((byte) 0x03);             // ETX
        return frame.array();
    }

    private static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        //Log.i("pacckageName", packageName );
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equalsIgnoreCase(packageName)) {
                return true;
            }
        }
        return false;
    }

    private static void sendDataToDashboard(BTProtocolFrame frame){
        handleData(frame);
        addEntry(mainActivity, dataReceived);
    }

    private static void handleData(BTProtocolFrame frame){

        try {
            frame.moveToData();
            byte[] b_uuid = new byte[16];
            frame.getFrame().get(b_uuid);
            UUID uuid= Utils.getUUIDFromByte(b_uuid);

            long timestamp = frame.getFrame().getLong();
            //int timestamp = frame.getFrame()
            int dataCount = frame.getFrame().get()& 0xff;

            String[] data = new String[dataCount+2];
            data[0] = uuid+"";
            data[1] = timestamp + "";
            for(int i=2;i<dataCount+2;i++)
                data[i] = frame.getFrame().getFloat()+"";

            dataReceived = data;

            //mEntries.offer(data);

            /*switch (frame.getCommandCode()){
                case BTProtocolFrame.CC_DATA:
                    decodeData(frame);
                    break;
                default:
                    break;
            }*/
        } catch (Exception e){
            //setIntentErr(Consts.MESSAGE_KO_REMOTE, "Communication error");
            e.printStackTrace();
        }
    }

    private static int counter =0;

    // Funzione chiamata dal readerThread per mandare i dati letti
    private static void addEntry(Activity activity, final String [] dati){
        if(Log.isLoggable("data", Log.INFO))
            Log.i("data", Arrays.toString(dati));

        if (dati[0].equals(Uuid.RESP_RATE)){
            //Log.i("RESPIRATION_RATE", Arrays.toString(dati));
            //PhysicalAbilitiesActivity.addRespRateData(instance, dati[VALUE_POSITION]);
            //RespirationRateFragment.addRespRateData(MainActivity.getInstance(),dati);
            RealTimeFragment.updateRespRate(activity, dati);
        }else if(dati[0].equals(Uuid.HEART_RATE)) {
            //Log.i("HEART_RATE", Arrays.toString(dati) );
            RealTimeFragment.updateHeartRate(activity, dati);
            StrengthExperience.updateHeartRate(dati);
            AnaerobicCapacityExperience.updateHeartRate(dati);
            AerobicPowerExperience.updateHeartRate(dati);
        }else if (dati[0].equals(Uuid.ACTIVITY_LEVEL)){
            //Log.i("ACTIVITY_LEVEL", Arrays.toString(dati));
            RealTimeFragment.updateActivityLevel(activity, dati);
        }else if (dati[0].equals(Uuid.ECG_RAW)){
            //Log.i("ECG_RAW", Arrays.toString(dati));
            MainActivityRealTimeFragment.addHeartRawData(activity,dati);

            /*
            counter++;
            if(counter == 1){
                Log.i("TIMESTAMP_init", System.currentTimeMillis() + "");
            }

            if(counter >= 50){
                Log.i("TIMESTAMP_end", System.currentTimeMillis() + "");
                counter = 0;
            }
            */

        }else if (dati[0].equals(Uuid.RESP_RAW)) {
            //Log.i("RESP_RAW", Arrays.toString(dati));
            //counter++;
            MainActivityRealTimeFragment.addRespRawData(activity, dati);

            /*if(counter == 1){
                Log.i("TIMESTAMP_init", System.currentTimeMillis() + "");
            }

            if(counter >= 50){
                Log.i("TIMESTAMP_end", System.currentTimeMillis() + "");
                counter = 0;
            }*/
        }

        if(ExperiencesUtils.EXPERIENCE_RUNNING_KEY!=null) {

            if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_REACTION_TIME)) {
                if (dati[0].equals(Uuid.RE_REACTION_TIME_OUTPUT)) {
                    //Log.i("RE_REACTION_TIME_OUTPUT", Arrays.toString(dati));
                    //ReactionTimeExperience.finalize(dati);
                    ReactionTimeExperience.finalize(activity, dati);
                } else if (dati[0].equals(Uuid.RE_REACTION_TIME_TEST)) {
                    //Log.i("RE_REACTION_TIME_TEST", Arrays.toString(dati));
                    ReactionTimeExperience.updateViews(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_BALANCE)) {
                if (dati[0].equals(Uuid.RE_BALANCE_OUT)) {
                    //Log.i("RE_BALANCE_OUT", Arrays.toString(dati) );
                    //ExperiencesUtils.experienceFinished(BalanceExperience.mInstance, RadarChartConst.BALANCE_KEY, (int)(Double.parseDouble(dati[VALUE_POSITION])));
                    //RespirationRateFragment.addRawData(instance,count,dati[1]);
                    //BalanceExperience.onExperienceFinished(dati);
                    BalanceExperience.onExperienceFinished(activity, dati);
                } else if (dati[0].equals(Uuid.RE_BALANCE_TEST)) {
                    //Log.i("RE_BALANCE_TEST", Arrays.toString(dati) );
                    BalanceExperience.updateViews(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_RHYTHM)) {
                if (dati[0].equals(Uuid.RE_RHYTHM_EVALUATOR)) {
                    //Log.i("RE_RHYTHM_EVALUATOR", Arrays.toString(dati) );
                    RhythmExperience.updateViews(dati);
                }else if (dati[0].equals(Uuid.RE_RHYTHM_FINALIZER)) {
                    //Log.i("RE_RHYTHM_FINALIZER", Arrays.toString(dati) );
                    //RhythmExperience.finalize(dati);
                    RhythmExperience.finalize(dati);
                }else if (dati[0].equals(Uuid.RE_RHYTHM_MOVE_CATCHER)) {
                    //Log.i("RE_RHYTHM_MOVE_CATCHER", Arrays.toString(dati) );
                    RhythmExperience.moveCatcher(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_STRENGTH)) {
                if (dati[0].equals(Uuid.RE_STRENGTH_OUT)) {
                    //Log.i("RE_STRENGTH_OUT", Arrays.toString(dati) );
                    //ExperiencesUtils.experienceFinished(StrengthExperience.mInstance, RadarChartConst.STRENGHT_KEY, (int)(Double.parseDouble(dati[VALUE_POSITION])));
                    //RespirationRateFragment.addRawData(instance,count,dati[1]);
                    //StrengthExperience.onExperienceFinished(dati);
                    StrengthExperience.onExperienceFinished(activity, dati);
                } else if (dati[0].equals(Uuid.RE_STRENGTH_TEST)) {
                    //Log.i("RE_STRENGTH_TEST", Arrays.toString(dati) );
                    StrengthExperience.updateViews(dati);
                    if (Math.round(Float.parseFloat(dati[2])) == 0)
                        StrengthExperience.startTimer();
                    //Log.i("strength", "strength ");
                    //ExperiencesUtils.experienceFinished(StrengthExperience.mInstance, RadarChartConst.STRENGHT_KEY, (int)(Double.parseDouble(dati[VALUE_POSITION])));
                    //RespirationRateFragment.addRawData(instance,count,dati[1]);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_AGILITY)) {
                if (dati[0].equals(Uuid.RE_AGILITY_TIMER)) {
                    //Log.i("RE_AGILITY_TIMER", Arrays.toString(dati));
                    AgilityExperience.updateTimeLeft(dati[2]);
                } else if (dati[0].equals(Uuid.RE_AGILITY_JUMP)) {
                    //Log.i("RE_AGILITY_JUMP", Arrays.toString(dati));
                    AgilityExperience.addJump(dati);
                } else if (dati[0].equals(Uuid.RE_AGILITY_SWAY)) {
                    //Log.i("RE_AGILITY_SWAY", Arrays.toString(dati));
                    AgilityExperience.addSway(dati);
                } else if (dati[0].equals(Uuid.RE_AGILITY_OUTPUT)) {
                    //Log.i("RE_AGILITY_OUTPUT", Arrays.toString(dati));
                    AgilityExperience.onExperienceFinished(activity, dati);
                    //AgilityExperience.onExperienceFinished(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_EXPLOSIVENESS)) {
                if (dati[0].equals(UuidDummy.RE_EXPLOSIVENESS_TEST)) {
                    //Log.i("RE_EXPLOSIVNESS_TEST", Arrays.toString(dati));
                    ExplosivenessExperience.updateViews(dati);
                } else if (dati[0].equals(UuidDummy.RE_EXPLOSIVENESS_OUT)) {
                    //Log.i("RE_EXPLOSIVNESS_OUT", Arrays.toString(dati));
                    //ExplosivenessExperience.onExperienceFinished(dati);
                    ExplosivenessExperience.onExperienceFinished(activity, dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_AEROBIC_POWER)) {
                if (dati[0].equals(UuidDummy.RE_AEROBIC_POWER_OUT)) {
                    //Log.i("RE_AEROBIC_POWER_OUT", Arrays.toString(dati) );
                    AerobicPowerExperience.onExperienceFinished(activity, dati);
                    //AerobicPowerExperience.experienceFinished(dati);
                } else if (dati[0].equals(UuidDummy.RE_AEROBIC_POWER_TEST)) {
                    //Log.i("RE_AEROBIC_POWER_TEST", Arrays.toString(dati));
                    AerobicPowerExperience.updateViews(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_SPEED)) {
                if (dati[0].equals(Uuid.RE_SPEED)) {
                    //Log.i("RE_SPEED", Arrays.toString(dati));
                    SpeedExperience.updateProgress(dati);
                } else if (dati[0].equals(Uuid.RE_SPEED_RESULT)) {
                    //Log.i("RE_SPEED_RESULT", Arrays.toString(dati));
                    SpeedExperience.onExperienceFinished(activity, dati);
                    //SpeedExperience.onExperienceFinished(dati);
                }
            } else if (ExperiencesUtils.EXPERIENCE_RUNNING_KEY.equals(Uuid.EX_ANAEROBIC_CAPACITY)) {
                if (dati[0].equals(UuidDummy.RE_ANAEROBIC_C_TIMER)) {
                    //Log.i("RE_ANAEROBIC_C_TIMER", Arrays.toString(dati));
                    AnaerobicCapacityExperience.updateViews(dati);
                    //AnaerobicCapacityExperience.updateGraph(dati);
                } else if (dati[0].equals(UuidDummy.RE_ANAEROBIC_C_TEST)) {
                    //Log.i("RE_ANAEROBIC_C_TEST", Arrays.toString(dati));
                    AnaerobicCapacityExperience.updateGraph(dati);
                } else if (dati[0].equals(UuidDummy.RE_ANAEROBIC_FINALIZER)) {
                    //Log.i("RE_ANAEROBIC_FINALIZER", Arrays.toString(dati));
                    AnaerobicCapacityExperience.onExperienceFinished(activity, dati);
                    //AnaerobicCapacityExperience.onExperienceFinished(dati);
                }
            }
        }

        if(dati[0].equals(Uuid.BICEPS_LEFT)){
            //Log.i("BICEPS_LEFT", Arrays.toString(dati));
            //MainActivitySensorsFragment.updateEMG1(activity, dati);
            MainActivityMuscleFragment.updateBicepsLeft(activity, dati);
            CalibrationFragment.updateBicepsLeft(activity, dati);
        }
        if(dati[0].equals(Uuid.BICEPS_RIGHT)){
            //Log.i("BICEPS_RIGHT", Arrays.toString(dati));
            MainActivityMuscleFragment.updateBicepsRight(activity, dati);
            CalibrationFragment.updateBicepsRight(activity, dati);
        }
        if(dati[0].equals(Uuid.FOREARM_LEFT)){
            //Log.i("FOREARM_LEFT", Arrays.toString(dati));
            MainActivityMuscleFragment.updateForearmLeft(activity, dati);
            CalibrationFragment.updateForearmLeft(activity, dati);
        }
        if(dati[0].equals(Uuid.FOREARM_RIGHT)){
            //Log.i("FOREARM_RIGHT", Arrays.toString(dati));
            MainActivityMuscleFragment.updateForearmRight(activity, dati);
            CalibrationFragment.updateForearmRight(activity, dati);
        }

        /*else if(dati[0].equals(Uuid.RE_EMG1_INTENSITY)){                // avambraccio destro
            //Log.i("RE_EMG1_INTENSITY", Arrays.toString(dati));
            MainActivitySensorsFragment.updateEMG1(MainActivity.getInstance(), dati);
        }else if(dati[0].equals(Uuid.RE_EMG2_INTENSITY)){               // bicipite destro
            //Log.i("RE_EMG2_INTENSITY", Arrays.toString(dati));
            MainActivitySensorsFragment.updateEMG2(MainActivity.getInstance(), dati);
        }else if(dati[0].equals(Uuid.RE_EMG3_INTENSITY)){               // avambraccio sinistro
            //Log.i("RE_EMG3_INTENSITY", Arrays.toString(dati));
            MainActivitySensorsFragment.updateEMG3(MainActivity.getInstance(), dati);
        }else if(dati[0].equals(Uuid.RE_EMG4_INTENSITY)){               // bicipite sinistro
           // Log.i("RE_EMG4_INTENSITY", Arrays.toString(dati));
            MainActivitySensorsFragment.updateEMG4(MainActivity.getInstance(), dati);
        }*/

        if(dati[0].equals(Uuid.STATUS_INDICATOR)) {
            //Log.i("STATUS_INDICATOR", Arrays.toString(dati));
            if((((MainActivity) activity).getSupportFragmentManager().findFragmentByTag(_TAG_REACTION_TIME_))!=null)
                ((RealTimeFragment) ((MainActivity) activity).getSupportFragmentManager().findFragmentByTag(_TAG_REACTION_TIME_)).updatStatusIndicator(activity, dati);
            //RealTimeFragment.updatStatusIndicator(this.activity, dati);
        }

        if(dati[0].equals(Uuid.BICEPS_LEFT_CALIBRATION_DATA)){
            //Log.i("BICEPS_L_CALIBRATION", Arrays.toString(dati));
            CalibrationFragment.onCalibrationReceived(activity, dati);
        }else if(dati[0].equals(Uuid.BICEPS_RIGHT_CALIBRATION_DATA)){
            //Log.i("BICEPS_R_CALIBRATION", Arrays.toString(dati));
            CalibrationFragment.onCalibrationReceived(activity, dati);
            //CalibrationFragment.onCalibrationReceived(dati);
        }else if(dati[0].equals(Uuid.FOREARM_LEFT_CALIBRATION_DATA)){
            //Log.i("FOREARM_L_CALIBRATION", Arrays.toString(dati));
            CalibrationFragment.onCalibrationReceived(activity, dati);
            //CalibrationFragment.onCalibrationReceived(dati);
        }else if(dati[0].equals(Uuid.FOREARM_RIGHT_CALIBRATION_DATA)){
            //Log.i("FOREARM_R_CALIBRATION", Arrays.toString(dati));
            CalibrationFragment.onCalibrationReceived(activity, dati);
            //CalibrationFragment.onCalibrationReceived(dati);
        }
    }



    private static WiFiStatus wifi;

    public static WiFiStatus getWiFiStatus() {
        return wifi;
    }

    /**
     * Inserts into the LocalBroadcastManager success messages and the cc of each operation
     *
     * @param cc byte of the operation
     * @param success_msg default info message depending the operation
     * @see   LocalBroadcastManager
     */
    private static void setIntentSuccess(int cc, String success_msg){
        //Log.i("intent_suc", cc+"");
        if(mainActivity!=null) {
            Intent intent = new Intent();
            intent.setAction(Consts.SERVICE_KEY);

            if(cc == 0x63 || cc == 0x64) {
                intent.putExtra("success_sync_code", cc);
                intent.putExtra("success_sync_msg", success_msg);
            }else {
                intent.putExtra("success_code", cc);
                intent.putExtra("success_msg", success_msg);
            }

            if(cc == BTProtocolFrame.CC_GET_WIFISTATUS && wifi!=null) {

                intent.putExtra("wifi_ssid", wifi.getSsid());
                intent.putExtra("wifi_ip", wifi.getIp());
                intent.putExtra("wifi_mode", wifi.getMode());
            }

            LocalBroadcastManager.getInstance(mainActivity).sendBroadcast(intent);
        }
    }

    /**
     * Inserts into the LocalBroadcastManager failure messages and the cc of each operation
     *
     * @param cc byte of the operation
     * @param err_msg default info message depending the operation
     * @see   LocalBroadcastManager
     */
    private static void setIntentErr(int cc, String err_msg){
        //Log.i("intent_err", cc+"");
        if(mainActivity!=null) {
            Intent intent = new Intent();
            intent.setAction("com.life.services.bind.messages");

            if(cc == 0x63 || cc == 0x64) {
                intent.putExtra("error_sync_code", cc);
                intent.putExtra("error_sync_msg", err_msg);
            }else {
                intent.putExtra("error_code", cc);
                intent.putExtra("error_msg", err_msg);
            }

            LocalBroadcastManager.getInstance(mainActivity).sendBroadcast(intent);
        }
    }

    /**
     * Reads the BTProtocolFrame byte by byte in order to populate the list of the wifi names and their SSID
     *
     * @param  frame  BTProtocolFrame object from the device to the smartscreen where the data are stored
     * @see    com.life.services.bind.protocols.BTProtocolFrame
     */
    private static void getWifis(BTProtocolFrame frame) {
        frame.moveToData();
        short payloadLen = frame.getFrame().getShort(3);
        frame.getFrame().position(5);

        while (frame.getFrame().position() < payloadLen + 5) {

            byte ssid_len = frame.getFrame().get();
            byte[] ssid = new byte[ssid_len];
            frame.getFrame().get(ssid, 0, ssid_len);
            byte caps_len = frame.getFrame().get();
            byte[] caps = new byte[caps_len];
            frame.getFrame().get(caps, 0, caps_len);

            byte db = frame.getFrame().get();

            WiFiFoundNetwork wiFiNetowrk = new WiFiFoundNetwork(ssid, caps, db);

            String name = wiFiNetowrk.getSsid();
            if(!checkAvailability(name)) {
                mWiFiList.add(wiFiNetowrk);
            }
        }
    }

    /**
     * Checks the wifi list in case of duplicated wifis comparing their SSIDs
     *
     * @param  ssid  BTProtocolFrame object from the device to the smartscreen where the data is stored
     * @return true if wifi has been already stored, either false
     */
    private static boolean checkAvailability(String ssid){
        for (int i=0; i < mWiFiList.size(); i++){
            if (mWiFiList.get(i).getSsid().equals(ssid)){
                return true;
            }
        }
        return false;
    }

    /**
     * Decodes the info from the BTProtocolFrame and generates the Wifi object
     *
     * @param  frame BTProtocolFrame object from the device to the smartscreen where the info of a specific is stored
     * @see    WiFiStatus
     */
    private static void getWifiStatus(BTProtocolFrame frame){

        frame.moveToData();

        //String mode = "STA", name, ip;
        byte mode = frame.getFrame().get();
        byte status = frame.getFrame().get();
        byte signalLevel = frame.getFrame().get();
        int ssidLen = frame.getFrame().get();
        byte[] ssid = new byte[ssidLen];
        frame.getFrame().get(ssid);
        int ipLen = frame.getFrame().get();
        byte[] ip = new byte[ipLen];
        frame.getFrame().get(ip);

        wifi = new WiFiStatus(mode, status, signalLevel, ssid, ip);
    }
}
