package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 13-Mar-17.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class LineGraph {

    private TimeSeries data = new TimeSeries("Data");
    private XYMultipleSeriesDataset multiData = new XYMultipleSeriesDataset();
    private XYSeriesRenderer renderer = new XYSeriesRenderer();
    private XYMultipleSeriesRenderer multipleSeriesRenderer = new XYMultipleSeriesRenderer();
    private GraphicalView graphView;

    public LineGraph(){
        //add time series data to xy multi data
        multiData.addSeries(data);

        //customize renderer
        renderer.setColor(Color.BLACK);
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setFillPoints(true);

        multipleSeriesRenderer.setZoomEnabled(false);
        multipleSeriesRenderer.setZoomButtonsVisible(false);
        multipleSeriesRenderer.setXTitle("");
        multipleSeriesRenderer.setYTitle("");
        multipleSeriesRenderer.setXLabelsColor(Color.BLUE);
        multipleSeriesRenderer.setYLabelsColor(0, Color.BLUE);
        multipleSeriesRenderer.setTextTypeface(Typeface.DEFAULT);
        multipleSeriesRenderer.setAxisTitleTextSize(16f);

        //add series render to multi renderer
        multipleSeriesRenderer.addSeriesRenderer(renderer);
    }

    public GraphicalView getView(Context context) {
        //create graphical view

        graphView = ChartFactory.getLineChartView(context, multiData, multipleSeriesRenderer);
        return graphView ;
    }

    public void addNewPoint(Point point){
        data.add(point.getX(), point.getY());
    }
}
