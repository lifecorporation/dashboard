package com.life.app.client.dashboard.fragments.radarChart.Strength;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.StrengthActivity;
import com.life.app.client.dashboard.adapters.StrengthResultsPager;

import me.relex.circleindicator.CircleIndicator;

/*
 * Created by chkalog on 26/9/2016.
 */
public class StrengthResults extends Fragment {

    View mLeak = null;
    RelativeLayout back;

    ViewPager pager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_strength_results, container, false);
    }

    public static int position = 1;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);

        StrengthActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_strength));

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        pager = (ViewPager) view.findViewById(R.id.pager);
        FragmentManager fm = getChildFragmentManager();
        StrengthResultsPager pagerAdapter = new StrengthResultsPager(fm);
        // Here you would declare which page to visit on creation
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);

        CircleIndicator circleIndicator = (CircleIndicator) view.findViewById(R.id.indicator);
        circleIndicator.setViewPager(pager);
    }

    @Override
    public void onResume(){
        super.onResume();
        StrengthActivity.hideTutorial();
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), StrengthActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }
}
