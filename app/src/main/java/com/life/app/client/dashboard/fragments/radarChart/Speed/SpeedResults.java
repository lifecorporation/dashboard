package com.life.app.client.dashboard.fragments.radarChart.Speed;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.SpeedActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;

/*
 * Created by chkalog on 26/9/2016.
 */
public class SpeedResults extends Fragment {

    View mLeak = null;
    TextView tvBest, tvSynthesis;
    RelativeLayout back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speed_results, container, false);
    }

    int position = 0;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        Bundle bundle = getArguments();
        position = bundle.getInt("position");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_speed));

        tvBest      = (TextView) view.findViewById(R.id.tvBest);
        tvSynthesis = (TextView) view.findViewById(R.id.tvSynthesis);
        back        = (RelativeLayout) view.findViewById(R.id.back);

        tvBest.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvSynthesis.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.SPEED_KEY + "_" + position);
        if(synthesis!=null) {
            String placeholderBest = Float.toString(synthesis.get(1));
            String placeholderSynt = Float.toString(synthesis.get(0));
            tvBest.setText(placeholderBest);
            tvSynthesis.setText(placeholderSynt);
        }
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), SpeedActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        SpeedActivity.hideTutorial();
    }
}
