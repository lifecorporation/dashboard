package tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.life.app.client.dashboard.R;

/*
 * Created by chara on 15-Dec-16.
 */

public class IntroFragmentPage3 extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tutorial_fragment_3, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


    }
}
