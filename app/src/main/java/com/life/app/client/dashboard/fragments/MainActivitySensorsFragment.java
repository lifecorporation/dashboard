package com.life.app.client.dashboard.fragments;

/*
 * Created by chara on 09-Dec-16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.objects.Asset;
import com.life.app.client.dashboard.objects.tshirt.FitnessTshirt;
import com.life.app.client.dashboard.objects.tshirt.RacingTshirt;
import com.life.app.client.dashboard.objects.tshirt.WellBeingTshirt;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.assets.Node;

public class MainActivitySensorsFragment extends Fragment  implements View.OnClickListener{

    private static ImageView ivBodymap;

    public static boolean toUpdate = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_real_time_sensors, container, false);
    }

    private static LinearLayout llEMG1 = null;
    private static LinearLayout llEMG2 = null;
    private static LinearLayout llEMG3 = null;
    private static LinearLayout llEMG4 = null;

    private static ImageView ivEMG1     = null;
    private static ImageView ivEMG2     = null;
    private static ImageView ivEMG3     = null;
    private static ImageView ivEMG4     = null;

    private static ImageView ivSS1      = null;
    private static ImageView ivSS2      = null;
    private static ImageView ivSS3      = null;
    private static ImageView ivSS4      = null;
    private static ImageView ivSS5      = null;
    private static ImageView ivSS6      = null;
    private static ImageView ivSS7      = null;
    private static ImageView ivSS8      = null;
    private static ImageView ivSS9      = null;
    private static ImageView ivSSA      = null;
    private static ImageView ivSSH      = null;

    private static int[] EMGStatus      = {0,0,0,0};

    private static Context mContext;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utils.getIONIconFont(getActivity()), Typeface.BOLD);

        mContext = getActivity();

        ivBodymap = (ImageView) view.findViewById(R.id.ivBodymap);

        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.2f);
        animation1.setDuration(0);
        animation1.setFillAfter(true);
        ivBodymap.startAnimation(animation1);

        llEMG1 = (LinearLayout) view.findViewById(R.id.llEMG1);
        llEMG2 = (LinearLayout) view.findViewById(R.id.llEMG2);
        llEMG3 = (LinearLayout) view.findViewById(R.id.llEMG3);
        llEMG4 = (LinearLayout) view.findViewById(R.id.llEMG4);

        llEMG1.setOnClickListener(this);
        llEMG3.setOnClickListener(this);

        llEMG2.setOnClickListener(this);
        llEMG4.setOnClickListener(this);

        view.findViewById(R.id.llSS9).setOnClickListener(this);
        view.findViewById(R.id.llSS7).setOnClickListener(this);

        ivEMG1  = (ImageView) view.findViewById(R.id.ivEMG1);
        ivEMG2  = (ImageView) view.findViewById(R.id.ivEMG2);
        ivEMG3  = (ImageView) view.findViewById(R.id.ivEMG3);
        ivEMG4  = (ImageView) view.findViewById(R.id.ivEMG4);

        ivSS1   = (ImageView) view.findViewById(R.id.ivSS1);
        ivSS2   = (ImageView) view.findViewById(R.id.ivSS2);
        ivSS3   = (ImageView) view.findViewById(R.id.ivSS3);
        ivSS4   = (ImageView) view.findViewById(R.id.ivSS4);
        ivSS5   = (ImageView) view.findViewById(R.id.ivSS5);
        ivSS6   = (ImageView) view.findViewById(R.id.ivSS6);
        ivSS7   = (ImageView) view.findViewById(R.id.ivSS7);
        ivSS8   = (ImageView) view.findViewById(R.id.ivSS8);
        ivSS9   = (ImageView) view.findViewById(R.id.ivSS9);
        ivSSA   = (ImageView) view.findViewById(R.id.ivSSA);
        ivSSH   = (ImageView) view.findViewById(R.id.ivSSH);

        ivEMG1.setOnClickListener(this);
        ivEMG2.setOnClickListener(this);
        ivEMG3.setOnClickListener(this);
        ivEMG4.setOnClickListener(this);
        ivSS1.setOnClickListener(this);
        ivSS2.setOnClickListener(this);
        ivSS3.setOnClickListener(this);
        ivSS4.setOnClickListener(this);
        ivSS5.setOnClickListener(this);
        ivSS6.setOnClickListener(this);
        ivSS7.setOnClickListener(this);
        ivSS8.setOnClickListener(this);
        ivSS9.setOnClickListener(this);
        ivSSA.setOnClickListener(this);
        ivSSH.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivEMG2:
                infoDialog(Node._SENSOR_EMG2 , "-", true, 1);
                break;
            case R.id.llEMG2:
                infoDialog(Node._SENSOR_EMG2 , "-", true, 1);
                break;
            case R.id.ivEMG1:
                infoDialog(Node._SENSOR_EMG1 , "-", true, 0);
                break;
            case R.id.llEMG1:
                infoDialog(Node._SENSOR_EMG1 , "-", true, 0);
                break;
            case R.id.ivEMG4:
                infoDialog(Node._SENSOR_EMG4, "-", true, 3);
                break;
            case R.id.llEMG4:
                infoDialog(Node._SENSOR_EMG4, "-", true, 3);
                break;
            case R.id.ivEMG3:
                infoDialog(Node._SENSOR_EMG3, "-", true, 2);
                break;
            case R.id.llEMG3:
                infoDialog(Node._SENSOR_EMG3, "-", true, 2);
                break;
            case R.id.ivSS1:
                infoDialog(Node._SENSOR_SS1, Node._DESCRIPTION_SS1, false, -1);
                break;
            case R.id.ivSS2:
                infoDialog(Node._SENSOR_SS2, Node._DESCRIPTION_SS2, false, -1);
                break;
            case R.id.ivSS3:
                infoDialog(Node._SENSOR_SS3, Node._DESCRIPTION_SS3, false, -1);
                break;
            case R.id.ivSS4:
                infoDialog(Node._SENSOR_SS4, Node._DESCRIPTION_SS4, false, -1);
                break;
            case R.id.ivSS5:
                infoDialog(Node._SENSOR_SS5, Node._DESCRIPTION_SS5, false, -1);
                break;
            case R.id.llSS5:
                infoDialog(Node._SENSOR_SS5, Node._DESCRIPTION_SS5, false, -1);
                break;
            case R.id.ivSS6:
                infoDialog(Node._SENSOR_SS6, Node._DESCRIPTION_SS6, false, -1);
                break;
            case R.id.llSS6:
                infoDialog(Node._SENSOR_SS6, Node._DESCRIPTION_SS6, false, -1);
                break;
            case R.id.ivSS7:
                infoDialog(Node._SENSOR_SS7, Node._DESCRIPTION_SS7, false, -1);
                break;
            case R.id.llSS7:
                infoDialog(Node._SENSOR_SS7, Node._DESCRIPTION_SS7, false, -1);
                break;
            case R.id.ivSS8:
                infoDialog(Node._SENSOR_SS8, Node._DESCRIPTION_SS8, false, -1);
                break;
            case R.id.llSS8:
                infoDialog(Node._SENSOR_SS8, Node._DESCRIPTION_SS8, false, -1);
                break;
            case R.id.ivSS9:
                infoDialog(Node._SENSOR_SS9, Node._DESCRIPTION_SS9, false, -1);
                break;
            case R.id.llSS9:
                infoDialog(Node._SENSOR_SS9, Node._DESCRIPTION_SS9, false, -1);
                break;
            case R.id.ivSSA:
                infoDialog(Node._SENSOR_SSA, Node._DESCRIPTION_SSA, false, -1);
                break;
            case R.id.llSSA:
                infoDialog(Node._SENSOR_SSA, Node._DESCRIPTION_SSA, false, -1);
                break;
            case R.id.ivSSH:
                infoDialog(Node._SENSOR_SSH, Node._DESCRIPTION_SSH, false, -1);
                break;
            case R.id.llSSH:
                infoDialog(Node._SENSOR_SSH, Node._DESCRIPTION_SSH, false, -1);
                break;
        }
    }

    public static float alpha = 0.2f;
    public static void enable(){
        if(alpha == 0.2f && ivBodymap!=null) {
            AlphaAnimation animation1 = new AlphaAnimation(alpha, 1.0f);
            animation1.setDuration(2000);
            animation1.setFillAfter(true);
            ivBodymap.startAnimation(animation1);

            alpha = 1.0f;
        }
    }

    public static void disable(){
        if(alpha == 1.0f && ivBodymap!=null) {
            AlphaAnimation animation2 = new AlphaAnimation(alpha, 0.2f);
            animation2.setDuration(2000);
            animation2.setFillEnabled(true);
            animation2.setFillAfter(true);
            ivBodymap.startAnimation(animation2);

            alpha = 0.2f;
        }
    }

    public static void initiateDisabled(){
        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.2f);
        animation1.setDuration(0);
        animation1.setFillAfter(true);
        ivBodymap.startAnimation(animation1);
        alpha = 0.2f;
    }

    public static void initiateEnabled(){
        AlphaAnimation animation1 = new AlphaAnimation(0.2f, 1.0f);
        animation1.setDuration(0);
        animation1.setFillAfter(true);
        ivBodymap.startAnimation(animation1);
        alpha = 1.0f;
    }

    public static void refreshSensors(){

        if(Asset.getTshirt()!=null) {

            if ((Asset.getTshirt()).getSS1()) {
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS1);
                Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ok).into(ivEMG1);
                //ivSS1.setImageResource(R.mipmap.ic_action_sensor_ok);
                //ivEMG1.setImageResource(R.mipmap.ic_action_emg_right_ok);
            } else {
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS1);
                Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ko).into(ivEMG1);
                //ivSS1.setImageResource(R.mipmap.ic_action_sensor_ko);
                //ivEMG1.setImageResource(R.mipmap.ic_action_emg_right_ko);
            }

            if ((Asset.getTshirt()).getSS3()) {
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS3);
                Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ok).into(ivEMG3);
                //ivSS3.setImageResource(R.mipmap.ic_action_sensor_ok);
                //ivEMG3.setImageResource(R.mipmap.ic_action_emg_left_ok);
            } else {
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS3);
                Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ko).into(ivEMG3);
                //ivSS3.setImageResource(R.mipmap.ic_action_sensor_ko);
                //ivEMG3.setImageResource(R.mipmap.ic_action_emg_left_ko);
            }

            if ((Asset.getTshirt()).getSS5())
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS5);
                //ivSS5.setImageResource(R.mipmap.ic_action_sensor_ok);
            else
                Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS5);
                //ivSS5.setImageResource(R.mipmap.ic_action_sensor_ko);

            if (Asset.getTshirt() instanceof WellBeingTshirt) {

                if (((WellBeingTshirt) Asset.getTshirt()).getSS6())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS6);
                    //ivSS6.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS6);
                    //ivSS6.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((WellBeingTshirt) Asset.getTshirt()).getSS7())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS7);
                    //ivSS7.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS7);
                    //ivSS7.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((WellBeingTshirt) Asset.getTshirt()).getSS8())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS8);
                    //ivSS8.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS8);
                    //ivSS8.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((WellBeingTshirt) Asset.getTshirt()).getSS9())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS9);
                    //ivSS9.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS9);
                    //ivSS9.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((WellBeingTshirt) Asset.getTshirt()).getSSA())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSSA);
                    //ivSSA.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSSA);
                    //ivSSA.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((WellBeingTshirt) Asset.getTshirt()).getSSH())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSSH);
                    //ivSSH.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSSH);
                    //ivSSH.setImageResource(R.mipmap.ic_action_sensor_ko);
            } else if(Asset.getTshirt() instanceof FitnessTshirt) {

                if (((FitnessTshirt) Asset.getTshirt()).getSS2())
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ok).into(ivEMG2);
                    //ivEMG2.setImageResource(R.mipmap.ic_action_emg_right_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ko).into(ivEMG2);
                    //ivEMG2.setImageResource(R.mipmap.ic_action_emg_right_ko);

                if (((FitnessTshirt) Asset.getTshirt()).getSS4())
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ok).into(ivEMG4);
                    //ivEMG4.setImageResource(R.mipmap.ic_action_emg_left_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ko).into(ivEMG4);
                    //ivEMG4.setImageResource(R.mipmap.ic_action_emg_left_ko);

            }else if(Asset.getTshirt() instanceof RacingTshirt) {

                if (((RacingTshirt) Asset.getTshirt()).getSS1()){
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS1);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ok).into(ivEMG1);
                    //ivSS1.setImageResource(R.mipmap.ic_action_sensor_ok);
                    //ivEMG1.setImageResource(R.mipmap.ic_action_emg_right_ok);
                } else {
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS1);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ko).into(ivEMG1);
                    //ivSS1.setImageResource(R.mipmap.ic_action_sensor_ko);
                    //ivEMG1.setImageResource(R.mipmap.ic_action_emg_right_ko);
                }

                if (((RacingTshirt) Asset.getTshirt()).getSS2()) {
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ok).into(ivEMG2);
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS2);
                    //ivEMG2.setImageResource(R.mipmap.ic_action_emg_right_ok);
                    //ivSS2.setImageResource(R.mipmap.ic_action_sensor_ok);
                }else {
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_right_ko).into(ivEMG2);
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS2);
                    //ivEMG2.setImageResource(R.mipmap.ic_action_emg_right_ko);
                    //ivSS2.setImageResource(R.mipmap.ic_action_sensor_ko);
                }

                if (((RacingTshirt) Asset.getTshirt()).getSS3()){
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS3);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ok).into(ivEMG3);
                    //ivSS3.setImageResource(R.mipmap.ic_action_sensor_ok);
                    //ivEMG3.setImageResource(R.mipmap.ic_action_emg_left_ok);
                } else {
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS3);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ko).into(ivEMG3);
                    //ivSS3.setImageResource(R.mipmap.ic_action_sensor_ko);
                    //ivEMG3.setImageResource(R.mipmap.ic_action_emg_left_ko);
                }

                if (((RacingTshirt) Asset.getTshirt()).getSS4()) {
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS4);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ok).into(ivEMG4);
                    //ivSS4.setImageResource(R.mipmap.ic_action_sensor_ok);
                    //ivEMG4.setImageResource(R.mipmap.ic_action_emg_left_ok);
                }else {
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS4);
                    Glide.with(mContext).load(R.mipmap.ic_action_emg_left_ko).into(ivEMG4);
                    //ivSS4.setImageResource(R.mipmap.ic_action_sensor_ko);
                    //ivEMG4.setImageResource(R.mipmap.ic_action_emg_left_ko);
                }

                if (((RacingTshirt) Asset.getTshirt()).getSS6())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS6);
                    //ivSS6.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS6);
                    //ivSS6.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((RacingTshirt) Asset.getTshirt()).getSS7())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS7);
                    //ivSS7.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS7);
                    //ivSS7.setImageResource(R.mipmap.ic_action_sensor_ko);

                if (((RacingTshirt) Asset.getTshirt()).getSS9())
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ok).into(ivSS9);
                    //ivSS9.setImageResource(R.mipmap.ic_action_sensor_ok);
                else
                    Glide.with(mContext).load(R.mipmap.ic_action_sensor_ko).into(ivSS9);
                    //ivSS9.setImageResource(R.mipmap.ic_action_sensor_ko);

            }
        }
    }

    public static void resetSensors(){
        Glide.with(mContext).load(R.mipmap.ic_action_emg_right).into(ivEMG1);
        Glide.with(mContext).load(R.mipmap.ic_action_emg_right).into(ivEMG2);
        Glide.with(mContext).load(R.mipmap.ic_action_emg_left).into(ivEMG3);
        Glide.with(mContext).load(R.mipmap.ic_action_emg_left).into(ivEMG4);
        ivEMG1.setImageResource(R.mipmap.ic_action_emg_right);
        ivEMG2.setImageResource(R.mipmap.ic_action_emg_right);
        ivEMG3.setImageResource(R.mipmap.ic_action_emg_left);
        ivEMG4.setImageResource(R.mipmap.ic_action_emg_left);

        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS1);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS2);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS3);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS4);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS5);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS6);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS7);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS8);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSS9);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSSA);
        Glide.with(mContext).load(R.mipmap.ic_action_sensor_white).into(ivSSH);

        ivSS1.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS2.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS3.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS4.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS5.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS6.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS7.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS8.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSS9.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSSA.setImageResource(R.mipmap.ic_action_sensor_white);
        ivSSH.setImageResource(R.mipmap.ic_action_sensor_white);

        toUpdate = true;
    }

    private void infoDialog(String name, String description, boolean isEMG, int position){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_sensors_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        if(isEMG){
            switch (EMGStatus[position]) {
                case 0:
                    ((TextView) infoDialogView.findViewById(R.id.tvDescription)).setText(getString(R.string.calibration_error));
                    break;
                case 1:
                    ((TextView) infoDialogView.findViewById(R.id.tvDescription)).setText(getString(R.string.calibration_old));
                    break;
                case 2:
                    ((TextView) infoDialogView.findViewById(R.id.tvDescription)).setText(getString(R.string.calibration_ok));
                    break;
            }
            (infoDialogView.findViewById(R.id.llDescription)).setVisibility(View.VISIBLE);
        }else{
            ((TextView) infoDialogView.findViewById(R.id.tvDescription)).setText(getString(R.string.calibration_missing_data));
        }

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(name);
        ((TextView)infoDialogView.findViewById(R.id.tvNodeDescription)).setText(description);

        GraphView graph = (GraphView)infoDialogView.findViewById(R.id.graphSensor);
        setSensorGraph(graph);
        graph.setVisibility(View.GONE);

        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

    static DataPoint [] points = null;
    static LineGraphSeries<DataPoint> series = null;

    private void setSensorGraph(GraphView graph){
        graph.getGridLabelRenderer().setGridStyle( GridLabelRenderer.GridStyle.NONE );
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);// remove horizontal x labels and line
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);

        points = new DataPoint[0];
        series = new LineGraphSeries<>(points);
        series.setThickness(2);

        graph.getGridLabelRenderer().setHighlightZeroLines(false);
        graph.getViewport().setScalable(false);
        graph.getViewport().setScrollable(true);

        graph.getViewport().setMaxX(150);
        graph.addSeries(series);
    }

    /**
     *
     * @param activity MainActivity
     * @param data bytes received
     *
     * data[3] = 0 : never calibrated
     * data[3] = 1 : very old calibration, provide though with value
     * data[3] = 2 : calibration up to date
     */
    public static void updateEMG1(final Activity activity, final String [] data){
        if (ivEMG1 != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float f = Float.parseFloat(data[3]);
                    switch ((int)f){
                        case 0:
                            EMGStatus[0] = 0;
                            ivEMG1.setImageResource(R.mipmap.ic_action_sensor_ko);
                            break;
                        case 1:
                            EMGStatus[0] = 1;
                            ivEMG1.setImageResource(R.mipmap.ic_action_sensor_white);
                            break;
                        case 2:
                            EMGStatus[0] = 2;
                            ivEMG1.setImageResource(R.mipmap.ic_action_sensor_ok);
                            break;
                    }
                }
            });
        }
    }

    /**
     *
     * @param activity MainActivity
     * @param data bytes received
     *
     * data[3] = 0 : never calibrated
     * data[3] = 1 : very old calibration, provide though with value
     * data[3] = 2 : calibration up to date
     */
    public static void updateEMG2(final Activity activity, final String [] data){
        if (ivEMG1 != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float f = Float.parseFloat(data[3]);
                    switch ((int)f){
                        case 0:
                            EMGStatus[1] = 0;
                            ivEMG2.setImageResource(R.mipmap.ic_action_sensor_ko);
                            break;
                        case 1:
                            EMGStatus[1] = 1;
                            ivEMG2.setImageResource(R.mipmap.ic_action_sensor_white);
                            break;
                        case 2:
                            EMGStatus[1] = 2;
                            ivEMG2.setImageResource(R.mipmap.ic_action_sensor_ok);
                            break;
                    }
                }
            });
        }
    }
    /**
     *
     * @param activity MainActivity
     * @param data bytes received
     *
     * data[3] = 0 : never calibrated
     * data[3] = 1 : very old calibration, provide though with value
     * data[3] = 2 : calibration up to date
     */
    public static void updateEMG3(final Activity activity, final String [] data){
        if (ivEMG3 != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float f = Float.parseFloat(data[3]);
                    switch ((int)f){
                        case 0:
                            EMGStatus[2] = 0;
                            ivEMG3.setImageResource(R.mipmap.ic_action_sensor_ko);
                            break;
                        case 1:
                            EMGStatus[2] = 1;
                            ivEMG3.setImageResource(R.mipmap.ic_action_sensor_white);
                            break;
                        case 2:
                            EMGStatus[2] = 2;
                            ivEMG3.setImageResource(R.mipmap.ic_action_sensor_ok);
                            break;
                    }
                }
            });
        }
    }

    /**
     *
     * @param activity MainActivity
     * @param data bytes received
     *
     * data[3] = 0 : never calibrated
     * data[3] = 1 : very old calibration, provide though with value
     * data[3] = 2 : calibration up to date
     */
    public static void updateEMG4(final Activity activity, final String [] data){
        if (ivEMG4 != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float f = Float.parseFloat(data[3]);
                    switch ((int)f){
                        case 0:
                            EMGStatus[3] = 0;
                            ivEMG4.setImageResource(R.mipmap.ic_action_sensor_ko);
                            break;
                        case 1:
                            EMGStatus[3] = 1;
                            ivEMG4.setImageResource(R.mipmap.ic_action_sensor_white);
                            break;
                        case 2:
                            EMGStatus[3] = 2;
                            ivEMG4.setImageResource(R.mipmap.ic_action_sensor_ok);
                            break;
                    }
                }
            });
        }
    }

    /**
     * Type of the tshirt
     * 0 : Fitness
     * 1 : Welbeing
     * 2 : Medical
     * 3 : Racing
     */
    private static int type = 0;

    public static void updateSesnors(int type){
        toUpdate = false;

        switch (type){
            case 0:

                if(ivEMG2!=null) ivEMG2.setVisibility(View.VISIBLE);
                if(ivEMG4!=null) ivEMG4.setVisibility(View.VISIBLE);

                if(ivSS6!=null)  ivSS6.setVisibility(View.GONE);
                if(ivSS7!=null)  ivSS7.setVisibility(View.GONE);
                if(ivSS8!=null)  ivSS8.setVisibility(View.GONE);
                if(ivSS9!=null)  ivSS9.setVisibility(View.GONE);
                if(ivSSA!=null)  ivSSA.setVisibility(View.GONE);
                if(ivSSH!=null)  ivSSH.setVisibility(View.GONE);

                break;

            case 1:

                if(ivEMG2!=null) ivEMG2.setVisibility(View.GONE);
                if(ivEMG4!=null) ivEMG4.setVisibility(View.GONE);

                if(ivSS2!=null)  ivSS2.setVisibility(View.GONE);
                if(ivSS4!=null)  ivSS4.setVisibility(View.GONE);

                if(ivSS6!=null)  ivSS6.setVisibility(View.VISIBLE);
                if(ivSS7!=null)  ivSS7.setVisibility(View.VISIBLE);
                if(ivSS8!=null)  ivSS8.setVisibility(View.VISIBLE);
                if(ivSS9!=null)  ivSS9.setVisibility(View.VISIBLE);
                if(ivSSA!=null)  ivSSA.setVisibility(View.VISIBLE);
                if(ivSSH!=null)  ivSSH.setVisibility(View.VISIBLE);

                break;

            case 3:

                if(ivEMG2!=null) ivEMG2.setVisibility(View.VISIBLE);
                if(ivEMG4!=null) ivEMG4.setVisibility(View.VISIBLE);

                if(ivSS5!=null)  ivSS5.setVisibility(View.VISIBLE);
                if(ivSS6!=null)  ivSS6.setVisibility(View.VISIBLE);
                if(ivSS7!=null)  ivSS7.setVisibility(View.VISIBLE);
                if(ivSS9!=null)  ivSS9.setVisibility(View.VISIBLE);

                if(ivSS8!=null)  ivSS8.setVisibility(View.GONE);
                if(ivSSA!=null)  ivSSA.setVisibility(View.GONE);
                if(ivSSH!=null)  ivSSH.setVisibility(View.GONE);


                break;
        }
    }
}
