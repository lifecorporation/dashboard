package com.life.app.client.dashboard.fragments.radarChart.Agility;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/*
 * Created by chkalog on 28/9/2016.
 */
public class AgilityResultsPager extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    public AgilityResultsPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {

        switch (arg0) {
            case 0:
                return new CenterFragment();
            case 1:
                return new LeftFragment();
            default:
                return new CenterFragment();
        }

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
