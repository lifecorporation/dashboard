package com.life.app.client.dashboard.fragments.radarChart.Rhythm;

/*
 * Created by chara on 06-Dec-16.
 */

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

public class LeftFragment extends Fragment {

    TextView tvScore1, tvScore2, tvScore3;
    TextView tvSNumber1, tvSNumber2, tvSNumber3;

    BarChart mChart1, mChart2, mChart3;
    ArrayList<Float> try1 = null, try2 = null, try3 = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rhythm_results_left, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tvScore1 = (TextView) view.findViewById(R.id.tvScore1);
        tvScore2 = (TextView) view.findViewById(R.id.tvScore2);
        tvScore3 = (TextView) view.findViewById(R.id.tvScore3);
        tvSNumber1  = (TextView) view.findViewById(R.id.tvSNumber1);
        tvSNumber1.setOnClickListener(null);
        tvSNumber2  = (TextView) view.findViewById(R.id.tvSNumber2);
        tvSNumber2.setOnClickListener(null);
        tvSNumber3  = (TextView) view.findViewById(R.id.tvSNumber3);
        tvSNumber3.setOnClickListener(null);

        view.findViewById(R.id.llresultsHeader).setOnClickListener(null);

        tvScore1.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvScore1.setOnClickListener(null);

        tvScore2.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvScore2.setOnClickListener(null);

        tvScore3.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);
        tvScore3.setOnClickListener(null);

        mChart1 = (BarChart) view.findViewById(R.id.barChart1);
        mChart2 = (BarChart) view.findViewById(R.id.barChart2);
        mChart3 = (BarChart) view.findViewById(R.id.barChart3);

        setClickListenermChart(mChart1);
        setClickListenermChart(mChart2);
        setClickListenermChart(mChart3);

        try1 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_1");
        try2 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_2");
        try3 = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_3");

        if(try1!=null) {
            BarChartSetup.setBarChartWithLimitLine(mChart1, false, "", (try1.get(2) / 1000) - 1f, (try1.get(2) / 1000) + 1f, (try1.get(2)) / 1000);
        }

        if(try2!=null){
            BarChartSetup.setBarChartWithLimitLine(mChart2, false, "", (try2.get(2) / 1000) - 1f, (try2.get(2) / 1000) + 1f, (try2.get(2)) / 1000);
        }

        if(try3!=null){
            BarChartSetup.setBarChartWithLimitLine(mChart3, false, "", (try3.get(2) / 1000) - 1f, (try3.get(2) / 1000) + 1f, (try3.get(2)) / 1000);
        }

        addDataToChart(mChart1, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_1_SET"));
        addDataToChart(mChart2, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_2_SET"));
        addDataToChart(mChart3, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.RHYTHM_KEY + "_" + RhythmResults.position + "_TRY_3_SET"));

        String placeholder1 = String.format(Locale.US, "%.1f", try1.get(3));
        String placeholder2 = String.format(Locale.US, "%.1f", try2.get(3));
        String placeholder3 = String.format(Locale.US, "%.1f", try3.get(3));

        tvScore1.setText(placeholder1);
        tvScore2.setText(placeholder2);
        tvScore3.setText(placeholder3);

        ImageButton ibInfo = (ImageButton) view.findViewById(R.id.ibInfo);
        ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_synthsesis_number), getString(R.string.prompt_synthesis_number_rhythm));
            }
        });
    }

    private void addDataToChart(BarChart chart, ArrayList<Float> results) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(results.size() + 1);

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        for (int i = results.size()-1; i >=0 ; i--) {
            yVals1.add(new BarEntry(i+1, results.get(i)));
        }

        Collections.sort(yVals1, new EntryComparator());

        BarDataSet set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(true);
        set1.setValueTextColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(12f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.6f);

        chart.setData(data);
    }

    private void setClickListenermChart(BarChart mChart){
        mChart.setHighlightPerTapEnabled(true);
        // Make sure it's clickable.
        mChart.setClickable(true);
        mChart.setTouchEnabled(true);
        mChart.setEnabled(true);
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                showAsToast(e);
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    Toast mToast = null;

    public void showAsToast (Entry e){
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(getActivity(),
                e.getY() + "",
                Toast.LENGTH_SHORT);
        mToast.show();
    }

    public class EntryComparator implements Comparator<Entry> {
        @Override
        public int compare(Entry entry1, Entry entry2) {
            return (int)entry1.getX() - (int)entry2.getX();
        }
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

}
