package com.life.app.client.dashboard.activities.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.utils.Utils;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chara on 21-Feb-17.
 */

public class SyncActivity extends MyFragmentActivity {

    private static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings_sync);

        ((TextView)findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_synchronize));

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.llSync, new SyncPreferenceFragment())
                    .commit();
        }

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        mContext = SyncActivity.this;

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, quitFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    public static class SyncPreferenceFragment extends PreferenceFragment implements android.preference.Preference.OnPreferenceClickListener{
        @Override
        public void onCreate(final Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.sync);
            (findPreference("start")).setOnPreferenceClickListener(this);
            (findPreference("stop")).setOnPreferenceClickListener(this);
            (findPreference("status")).setOnPreferenceClickListener(this);
            (findPreference("configure")).setOnPreferenceClickListener(this);
        }

        @Override
        public boolean onPreferenceClick(android.preference.Preference preference) {
            String key = preference.getKey();
            switch (key){
                case "status" :
                    startActivity(new Intent(mContext, SyncStatusActivity.class));
                    break;
                case "configure" :
                    startActivity(new Intent(mContext, SyncConfigureActivity.class));
                    break;
                case "start" :
                    start();
                    break;
                case "stop" :
                    stop();
                    break;
                case "list" :
                    break;
            }
            return false;
        }
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()) {
                        case "97" :
                            Toast.makeText(SyncActivity.this, getResources().getString(R.string.error_sync_start), Toast.LENGTH_LONG).show();
                            break;
                        case "98" :
                            Toast.makeText(SyncActivity.this, getResources().getString(R.string.error_sync_stop), Toast.LENGTH_LONG).show();
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "97" :
                            Toast.makeText(SyncActivity.this, getResources().getString(R.string.success_sync_start), Toast.LENGTH_LONG).show();
                            break;
                        case "98" :
                            Toast.makeText(SyncActivity.this, getResources().getString(R.string.success_sync_stop), Toast.LENGTH_LONG).show();
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    private static void start(){
        if(MainActivity.getStatus() != null) {
            Utils.startSync();
        }else{
            Toast.makeText(mContext, mContext.getResources().getString(R.string.warning_remote), Toast.LENGTH_LONG).show();
        }
    }

    private static void stop(){
        if(MainActivity.getStatus() != null) {
            Utils.stopSync();
        }else{
            Toast.makeText(mContext, mContext.getResources().getString(R.string.warning_remote), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
