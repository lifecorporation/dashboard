package com.life.app.client.dashboard.activities.settings;

/*
 * Created by chara on 23-Feb-17.
 */

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.Life;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class ProfileActivity extends MyFragmentActivity {

    private Account mAccount = null;

    private String authToken = null;

    TextInputLayout tilFirstname, tilLastname, tilEmail, tilPassword, tilImei, tilMac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_settings_profile);

        tilFirstname = (TextInputLayout) findViewById(R.id.tilFirstname);
        tilLastname  = (TextInputLayout) findViewById(R.id.tilLastname);
        tilEmail     = (TextInputLayout) findViewById(R.id.tilEmail);
        tilPassword  = (TextInputLayout) findViewById(R.id.tilPassword);
        tilImei      = (TextInputLayout) findViewById(R.id.tilImei);
        tilMac       = (TextInputLayout) findViewById(R.id.tilMac);

        if(mAccount!=null){

            if(tilPassword.getEditText()!=null){
                tilPassword.getEditText().setText("12345");
            }
        }

        ((TextView)findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_profile));

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);

        getAccount();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    private void getAccount(){
        /*
         * The account manager used to request and add account.
         */
        AccountManager mAccountManager = AccountManager.get(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else{

            Account[] accountsFromFirstApp = mAccountManager.getAccountsByType(Consts.ACCOUNT_TYPE);
            for(Account acct: accountsFromFirstApp){

                mAccount = acct;

                authToken = mAccountManager.peekAuthToken(acct, Consts.ACCOUNT_TYPE);
                //if(authToken!=null)
                //    Log.i("_ACCOUNT_TOKEN", authToken);

                if(tilFirstname.getEditText()!=null){
                    tilFirstname.getEditText().setText(((Life)getApplicationContext()).getUser().getFirstname());
                }

                if(tilLastname.getEditText()!=null){
                    tilLastname.getEditText().setText(((Life)getApplicationContext()).getUser().getLastname());
                }

                if(tilEmail.getEditText()!=null){
                    tilEmail.getEditText().setText(((Life)getApplicationContext()).getUser().getEmail());
                }

                if(tilImei.getEditText()!=null){
                    tilImei.getEditText().setText(((Life)getApplicationContext()).getUser().getImei());
                }

                if(tilMac.getEditText()!=null){
                    tilMac.getEditText().setText(((Life)getApplicationContext()).getUser().getMac());
                }
            }
        }
    }

    public void setFirstname(View v){

    }

    public void setLastname(View v){

    }

    public void setEmail(View v){

    }

    public void setImei(View v){

    }

    public void setMac(View v){

    }
    public void setPassword(View v){
        if(authToken == null) {
            Toast.makeText(ProfileActivity.this, "Token has expired", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Utils.openLoginPage(ProfileActivity.this, mAccount.name);
                }
            }, 1000);
        }else
            editPasswordDialog();
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC) != null) {
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG_SYNC);
                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC)){
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC);
                }
            }
        }
    };

    private void editPasswordDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the
        // dialog layout
        View view = View.inflate(ProfileActivity.this, R.layout.dialog_profile_edit_password, null);

        TextInputLayout tvOldPassword = (TextInputLayout) view.findViewById(R.id.tvOldPassword);
        final TextInputLayout tvNewPassword = (TextInputLayout) view.findViewById(R.id.tvNewPassword);
        final TextInputLayout tvConfirmPassword = (TextInputLayout) view.findViewById(R.id.tvConfirmPassword);

        builder.setTitle(getResources().getString(R.string.hint_change_password));
        builder.setCancelable(false);
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(getResources().getString(R.string.hint_save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if(tvNewPassword.getEditText()!=null && tvConfirmPassword.getEditText()!=null &&
                                confirmPasswords(tvNewPassword.getEditText().getText().toString(),
                                        tvConfirmPassword.getEditText().getText().toString()))
                            AccountManager.get(ProfileActivity.this).setPassword(mAccount,
                                    tvNewPassword.getEditText().getText().toString());
                        else
                            if( tvConfirmPassword.getEditText()!=null)
                                tvConfirmPassword.getEditText().setError(getResources().getString(R.string.error_password_confirmation));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create();
        builder.show();
    }

    private boolean confirmPasswords(String pass1, String pass2){
        if(Utils.isEmpty(pass1) &&
                Utils.isEmpty(pass2))
            if(pass1.equals(pass2))
                return true;
        return false;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
