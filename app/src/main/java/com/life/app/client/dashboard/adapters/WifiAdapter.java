package com.life.app.client.dashboard.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.services.bind.objects.WiFiFoundNetwork;

import java.util.ArrayList;

/*
 * Created by chkalog on 2/8/2016.
 */
public class WifiAdapter extends BaseAdapter {

    private Context context;
    private int resource;
    private ArrayList<WiFiFoundNetwork> data = null;

    public WifiAdapter(Context context, int resource, ArrayList<WiFiFoundNetwork> data) {
        super();
        this.resource = resource;
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        if(null != this.data){
            return this.data.size();
        }
        return 0;
    }

    @Override
    public WiFiFoundNetwork getItem(int arg0) {
        if(null != this.data){
            return this.data.get(arg0);
        }
        return null;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if(row == null){
            LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
            row = inflater.inflate(this.resource, parent, false);

            holder = new ViewHolder();
            holder.tvTitle  = (TextView)row.findViewById(R.id.text1);
            row.setTag(holder);
        }
        else{
            holder = (ViewHolder)row.getTag();
        }

        String ssid = getItem(position).getSsid();

        if(ssid!=null) {
            holder.tvTitle.setText(ssid);

            if (BTConnectionManagerService.getWiFiStatus() != null && BTConnectionManagerService.getWiFiStatus().getSsid().length() > 1 &&
                    ssid.equals(BTConnectionManagerService.getWiFiStatus().getSsid().substring(1, BTConnectionManagerService.getWiFiStatus().getSsid().length()-1))) {
                holder.tvTitle.setTextColor(ContextCompat.getColor(this.context, R.color.life_green));
            } else {
                holder.tvTitle.setTextColor(ContextCompat.getColor(this.context, R.color.white));
            }
        }

        return row;
    }

    private class ViewHolder{
        TextView tvTitle;
    }
}
