package com.life.app.client.dashboard.activities.settings;

/*
 * Created by chara on 23-Jan-17.
 */
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import io.fabric.sdk.android.Fabric;

public class SettingsActivity extends MyFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.llSettings, new SettingsFragment())
                    .commit();
        }

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.hint_settings));

        RelativeLayout back = (RelativeLayout) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public static class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener,
            Preference.OnPreferenceChangeListener {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);

            ((CheckBoxPreference)findPreference("warning")).setChecked(SharedPrefDataHandler.isWarning(getActivity()));
            (findPreference("warning")).setOnPreferenceChangeListener(this);
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            switch (preference.getKey()){
                case "warning":
                    break;
            }
            return true;
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {

            SharedPrefDataHandler.setWarning(getActivity(), Boolean.parseBoolean(o.toString()));

            return true;
        }
    }
}
