package tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.life.app.client.dashboard.R;

/*
 * Created by chara on 15-Dec-16.
 */

public class IntroFragmentPage4 extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tutorial_fragment_4, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView tvDescription = (TextView)view.findViewById(R.id.tvDescription);
        String redString = getResources().getString(R.string.prompt_tutorial_sensor_activity2);
        tvDescription.setText(Html.fromHtml(redString));

    }
}
