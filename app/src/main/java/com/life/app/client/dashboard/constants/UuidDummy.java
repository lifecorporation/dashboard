package com.life.app.client.dashboard.constants;

/*
 * Created by peppe on 15/07/2016.
 */
public class UuidDummy {

    //public static String BASE_EXPERIENCE       = "582b2eac-ee0d-1012-0013-476e726c5870";

    public static String ECG_RAW               = "5825da03-aa0d-1001-0001-436367446d6d";
    public static String RESP_RAW              = "5825e0be-aa0d-1001-0001-527370527744";
    public static String FITNESS_RAW           = "58204aba-aa00-1001-1002-576c6c426e67";

    public static String HEART_RATE            = "5825cccb-aa0d-1001-0003-487274427449";
    public static String RESP_RATE             = "5825cf39-aa0d-1001-0002-527370727472";
    public static String WELLBEING_INDEX       = "5825ef23-aa0d-1001-0001-576c6c426e67";
    public static String INTENSITY_LEVEL_NEW   = "00000088-0002-0000-0000-000000000000";

    public static String ACTIVITY_LEVEL        = "5829a33e-aa0d-1001-0001-437476744e74";
    public static String ENERGY_EXPENDITURE    = "5825d08f-aa0d-1001-0002-4e726758706e";

    public static String EX_AEROBIC_POWER      = "ba355e55-ee0d-1003-0303-581c9c28010d";
    /*public static String EX_AGILITY            = "";
    public static String EX_ANAEROBIC_CAPACITY = "a5415e55-ee0d-1003-0301-582065ad015d";
    public static String EX_BALANCE            = "";
    public static String EX_EXPLOSIVNESS       = "";
    public static String EX_REACTION_TIME      = "";
    public static String EX_RHYTHM             = "";
    public static String EX_STRENGTH           = "11111111-0000-0000-1111-100000000006";
    public static String EX_SPEED              = "21e95e55-ee0d-1001-0101-5820665f0106";*/

    public static String RE_AEROBIC_POWER_TEST = "41d35e55-aa0d-1001-0003-581c973f0150";
    public static String RE_AEROBIC_POWER_OUT  = "6cac5e55-aa0d-1002-0002-581c98ca010f";
    public static String RE_AEROBIC_POWER_HEART= "00000022-0000-0000-0000-000000000015";

    public static String RE_SPEED              = "11111111-1111-1111-1111-000000000001";
    public static String RE_SPEED_RESULT       = "11111111-1111-1111-1111-000000000001";

    public static String RE_AGILITY_TIMER      = "11111111-1111-1111-1111-100000000000";
    public static String RE_AGILITY_JUMP       = "11111111-1111-1111-1111-000000000003";
    public static String RE_AGILITY_SWAY       = "11111111-1111-1111-1111-200000000000";
    public static String RE_AGILITY_OUTPUT     = "11111111-1111-1111-1111-000000000004";


    public static String RE_BALANCE_OUT        = "11111111-1111-1111-1111-300000000000";
    public static String RE_BALANCE_TEST       = "11111111-1111-1111-1111-000000000006";

    public static String RE_EXPLOSIVENESS_OUT  = "11111111-1111-1111-1111-400000000000";
    public static String RE_EXPLOSIVENESS_TEST = "11111111-1111-1111-1111-000000000005";

    public static String RE_JOINT_MOBILITY     = "00000044-1111-1111-0000-000000000001";

    public static String RE_REACTION_TIME_TEST      = "11111111-1111-1111-1111-000000000007";
    public static String RE_REACTION_TIME_OUTPUT    = "11111111-1111-1111-1111-500000000000";

    public static String RE_RHYTHM_EVALUATOR        = "11111111-1111-1111-1111-000000000011";
    public static String RE_RHYTHM_MOVE_CATCHER     = "11111111-1111-1111-1111-000000000013";
    public static String RE_RHYTHM_FINALIZER        = "11111111-1111-1111-1111-000000000014";
    public static String RE_RHYTHM_BEATS_COUNTER    = "11111111-1111-1111-1111-000000000010";

    public static String RE_STRENGTH_OUT            = "11111111-1111-1111-1111-100000000009";
    public static String RE_STRENGTH_TEST           = "11111111-1111-1111-1111-100000000008";

    public static String RE_EMG1_RAW                = "5829c22a-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG2_RAW                = "5829c277-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG3_RAW                = "5829c2e0-aa0d-1001-1002-4d674e746e73";
    public static String RE_EMG4_RAW                = "5829cc5f-aa0d-1001-1002-4d674e746e73";

    public static String RE_BICEP_LEFT              = "00000006-0000-0000-0000-000000000001";
    public static String RE_BICEP_RIGHT             = "00000006-0000-0000-0000-000000000003";
    public static String RE_TRICEPS_LEFT            = "00000006-0000-0000-0000-000000000002";
    public static String RE_TRICEPS_RIGHT           = "00000006-0000-0000-0000-000000000004";

    public static String RE_TOUCHPOINT_LEFT         = "582ac987-aa0d-1001-0001-546368506e74";
    public static String RE_TOUCHPOINT_RIGHT        = "582aca55-aa0d-1001-0001-546368506e74";
    public static String RE_TAP_POINT               = "582adfa5-aa0d-1001-0001-5470506e7444";

    public static String RE_ANAEROBIC_FINALIZER= "3d865e55-aa0d-1001-0002-582050ed0155";
    public static String RE_ANAEROBIC_C_TIMER  = "0cb85e55-aa0d-1001-0004-58203fe70100";
    public static String RE_ANAEROBIC_C_TEST   = "9dd35e55-aa0d-1002-0003-582046530104";
}