package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 24-Feb-17.
 */

import android.content.Context;

import com.life.app.client.dashboard.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

public class Date {

    private String date = "";
    private String time = "";
    private int automatic = 0;

    public Date(Context mContext, int automatic, String datetime){
        this.setAutomatic(automatic);

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-mm-dd'T'HH:mm:ss.SSSZZ");
        DateTime dt = formatter.parseDateTime(datetime);

        this.setDate(dt.getDayOfMonth() + " " + Utils.getMonthName(mContext, dt.getMonthOfYear()) +
                " " + dt.getYear());

        this.setTime(dt.getHourOfDay() + ":" + String.format(Locale.US, "%02d", dt.getMinuteOfHour()) + ":"
                + String.format(Locale.US, "%02d", dt.getSecondOfMinute()));
    }

    public String getDate() {
        return date;
    }

    private void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAutomatic() {
        return automatic;
    }

    private void setAutomatic(int automatic) {
        this.automatic = automatic;
    }
}
