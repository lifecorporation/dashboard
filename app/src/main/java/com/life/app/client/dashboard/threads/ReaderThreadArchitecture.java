package com.life.app.client.dashboard.threads;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.life.app.client.dashboard.protocols.BTProtocolFrame;
import com.life.app.client.dashboard.protocols.BTProtocolNak;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.services.bind.constants.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/*
 * Created by peppe on 30/06/2016.
 */
public class ReaderThreadArchitecture extends Thread {

    public boolean running = false;
    private byte read;
    private int count = 0;
    private BluetoothSocket rtSocket = null;
    private InputStream inputStream = null;
    private BTProtocolFrame currentFrame = null;

    private boolean puoiEspandere = false;

    String TAG = "ReaderThread";

    public static int mesaggiRicevuti =0;

    public ReaderThreadArchitecture(BluetoothSocket socket){
        if (socket.isConnected()){
            rtSocket = socket;
            try {
                inputStream = rtSocket.getInputStream();
            }
            catch( Exception e){
                // TODO vedere come e se gestire le eccezioni
            }
        }
        else {
            rtSocket = null;
        }
    }

    public void run() {

        mesaggiRicevuti=0;

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        //Log.i("btpicc","currentsocket 1 " + currentSocket.getName().toString());
        //Log.i(TAG, "readerThread started! valore running " + running);
        while (running) {
            //Log.i("TAG"," sto girando Socket: " + rtSocket.isConnected() + " inputStream: ");
            // provo a leggere dal buffer di input
            try {
                // verifico che per un qualsiasi motivo non sia caduta la connessione
                if ( rtSocket!= null && rtSocket.isConnected() && inputStream != null)
                {

                    mesaggiRicevuti++;

                    read = (byte) inputStream.read();

                    if (read == BTProtocolFrame.STX) {

                        puoiEspandere = true;

                        currentFrame = new BTProtocolFrame();
                        currentFrame.addData(read);
                        count = 1;

                        try {
                            while (!currentFrame.addData((byte) inputStream.read())) {
                                count++;
                                //Log.i("BTProtocolFrame", "leggo bytes");


                                if (puoiEspandere && count % BTProtocolFrame.STD_PACKET_LEN == 0){
                                    puoiEspandere = false;

                                    //currentFrame = new BTProtocolFrame(currentFrame.getDataSize());


                                    //Log.i("BTProtocolFrame", "estendo il frame  " + currentFrame.bytes().length);

                                    // save currentFrame data into a temporary frame
                                    ByteBuffer btemp = ByteBuffer.allocate(BTProtocolFrame.STD_PACKET_LEN );
                                    btemp.put(currentFrame.bytes());
                                    //Log.i("BTProtocolFrame", "creato il bytebuffer " + Arrays.toString(currentFrame.bytes()));

                                    //Log.i("BTProtocolFrame", "verifico dimensioni currentFrame payload " + currentFrame.getDataSizeInt());
                                    currentFrame = new BTProtocolFrame(currentFrame.getDataSizeInt());

                                    // copia il contenuto del temp frame dentro il current frame
                                    int len = btemp.array().length;
                                    for (int i =0; i < len; i++){
                                        //Log.i("BTProtocolFrame", "ricopio il frame temporaneo " + btemp.get(i) );
                                        currentFrame.addData(btemp.get(i));
                                        //Log.i("BTProtocolFrame", "for " + Arrays.toString(currentFrame.bytes()));
                                    }

                                    puoiEspandere = false;
                                }
                            }

                            // leggo il frame e lo giro al service di gestione per vedere se mandarlo in broadcast o restituirlo come messaggio
                            // visto ceh si tratta di dati
                            //Log.i(TAG,"Ricevuto messaggio uuid " + Utils.getUUIDFromByte(currentFrame.getUUID()) );

                            //Log.i(TAG,"Ricevuto messaggio byte corretto "  + Utils.bytesToHex(currentFrame.bytes(), count - 1));

                            BTConnectionManagerService.gestisciFrame(currentFrame);

                        } catch (BTProtocolFrame.BTProtocolInvalidLengthException e) {
                            e.printStackTrace();
                            if(Log.isLoggable(TAG, Log.ERROR))
                                Log.e(TAG, "frame with invalid length! " + Utils.bytesToHex(currentFrame.getFrame().array(), currentFrame.getFrameSize()));
                            sendNak(currentFrame.getCommandCode(), BTProtocolFrame.REASON_INVALID_LENGHT);
                            currentFrame = null;
                        } catch (BTProtocolFrame.BTProtocolInvalidCRCException e) {
                            e.printStackTrace();
                            if(Log.isLoggable(TAG, Log.ERROR))
                                Log.e(TAG, "frame with invalid CRC! " + Utils.bytesToHex(currentFrame.getFrame().array(), currentFrame.getFrameSize()));
                            sendNak(currentFrame.getCommandCode(), BTProtocolFrame.REASON_INVALID_CRC);
                            currentFrame = null;
                        } catch (IOException e) {
                            e.printStackTrace();
                            if(Log.isLoggable(TAG, Log.ERROR))
                                Log.e(TAG, "frame with unknown error! " + Utils.bytesToHex(currentFrame.getFrame().array(), currentFrame.getFrameSize()));
                            sendNak(currentFrame.getCommandCode(), BTProtocolFrame.REASON_GENERIC);
                            currentFrame = null;
                        }
                    }

                } else {
                    Thread.sleep(1000);
                }
            } catch (IOException e) {
                if(Log.isLoggable(TAG, Log.ERROR))
                    Log.e(TAG, "readerThread.run()", e);
                //currentSocket = null;
                inputStream=null;
            } catch (Exception e) {
                if(Log.isLoggable(TAG, Log.ERROR))
                    Log.e(TAG, "readerThread.run()", e);
            }
        }
        //Log.i(TAG, "readerThread exit!!!");
    }

    private void sendNak(byte CommandCode, byte Reason) {
        BTProtocolNak nak = new BTProtocolNak(CommandCode, Reason);
        try {
            OutputStream out = rtSocket.getOutputStream();
            //currentSocket.write(nak.bytes(), 0, nak.getFrameSize());
            out.write(nak.bytes(), 0, nak.getFrameSize());
            //Log.d(TAG, "sendNAK:" + Utils.bytesToHex(nak.bytes(), nak.getFrameSize()));
            out.close();
        } catch (Exception e) {
            if(Log.isLoggable(TAG, Log.ERROR))
                Log.e(TAG, "sendNAK", e);
        }
    }
}