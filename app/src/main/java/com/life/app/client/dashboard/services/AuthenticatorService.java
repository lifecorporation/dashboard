package com.life.app.client.dashboard.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.life.app.client.dashboard.account.CustomAuthenticator;


/**
 * Created by peppe on 28/06/2016.
 */
public class AuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        CustomAuthenticator authenticator = new CustomAuthenticator(this);
        return authenticator.getIBinder();
    }
}