package com.life.app.client.dashboard.constants;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Created by Vittorio on 22/04/15.
 */
public  class Consts {

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE         = "com.life.x10y.account.X10YACCOUNT";

    public static final String AUTHTOKEN_TYPE       = "com.life.x10y.account.X10YACCOUNT";

    public static String SERVICE_KEY                = "com.life.services.bind.messages";

    public static String PREFERENCE_KEY_ERROR_CODE  = "error_code";
    public static String PREFERENCE_KEY_ERROR_MSG   = "error_msg";

    public static String PREFERENCE_KEY_SUCCESS_CODE= "success_code";
    public static String PREFERENCE_KEY_SUCCESS_MSG = "success_msg";

    public static String PREFERENCE_KEY_ERROR_CODE_SYNC  = "error_sync_code";
    public static String PREFERENCE_KEY_ERROR_MSG_SYNC   = "error_sync_msg";

    public static String PREFERENCE_KEY_SUCCESS_CODE_SYNC= "success_sync_code";
    public static String PREFERENCE_KEY_SUCCESS_MSG_SYNC = "success_sync_msg";

    public static final byte CC_START_EXPERIENCE    = 0x21;
    public static final byte CC_STOP_EXPERIENCE     = 0x22;

    public static final byte CC_GET_ASSETS           = 0x43;

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_SCARSO = {
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255),
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.rgb(192,57,43)
    };

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_SOTTO_MEDIA = {
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255),
            Color.argb(10, 255, 255, 255), Color.rgb(211,84,0), Color.rgb(211,84,0)
    };

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_MEDIA = {
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255),
            Color.rgb(211,84,0), Color.rgb(211,84,0), Color.rgb(211,84,0)
    };

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_SOPRA_MEDIA = {
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.rgb(230,126,34),
            Color.rgb(230,126,34), Color.rgb(230,126,34), Color.rgb(230,126,34)
    };

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_BUONO = {
            Color.argb(10, 255, 255, 255), Color.argb(10, 255, 255, 255), Color.rgb(46,204,113),
            Color.rgb(46,204,113), Color.rgb(46,204,113), Color.rgb(46,204,113)
    };

    // colors for different sections in pieChart
    private static final int[] MY_COLORS_EXCELLENT = {
            Color.rgb(39,174,96), Color.rgb(39,174,96), Color.rgb(39,174,96),
            Color.rgb(39,174,96), Color.rgb(39,174,96), Color.rgb(39,174,96)
    };

    public static ArrayList<int []> colors_state = new ArrayList<>(Arrays.asList(MY_COLORS_SCARSO,
            MY_COLORS_SOTTO_MEDIA, MY_COLORS_SOTTO_MEDIA,
            MY_COLORS_MEDIA, MY_COLORS_BUONO, MY_COLORS_EXCELLENT ));

    public static int DURATION_FADE_IN_OUT   = 1200;


    /**
     *  GRAPHS SISE
     */

    public static float DESCRIPTION_TEXT_SIZE   = 15f;
    public static float VALUE_TEXT_SIZE         = 15f;

    /**
     * GRAPHS COLORS
     */

    public static int GRAPHS_COLOR_WHITE        = 0xFFF5F5F5;
    public static int GRAPHS_COLOR_GREEN        = 0xFF009688;
    public static int GRAPHS_COLOR_ORANGE       = 0xFFFF9800;
    public static int GRAPHS_COLOR_RED          = 0xFFF44336;
    public static int GRAPHS_COLOR_RED_DARK     = 0xFFC0392B;
    public static int GRAPHS_COLOR_BLUE_LIGHT   = 0xFF2196F3; //00AAEC
    public static int GRAPHS_COLOR_BLUE_DARK    = 0xFF3F51B5;


    public static int START_EXPERIENCE_DELAY    = 1750;


    public static String PREFERENCE_KEY                   = "MyPrefs";
    public static String PREFERENCE_KEY_WIFI_SSID         = "wifi_ssid";
    public static String PREFERENCE_KEY_WIFI_IP           = "wifi_ip";
    public static String PREFERENCE_KEY_WIFI_MODE         = "wifi_mode";
    public static String PREFERENCE_KEY_WIFI_STATUS       = "disconnected";
    public static String PREFERENCE_KEY_WIFI_CONNECTED    = "connected";
    public static String PREFERENCE_KEY_WIFI_DISCONNECTED = "disconnected";
    public static String PREFERENCE_KEY_AUTO_DATETIME     = "automatic";

    public static String PREFERENCE_KEY_STATUS            = "status";
    public static String PREFERENCE_KEY_STATUS_ON         = "1";
    public static String PREFERENCE_KEY_STATUS_OFF        = "2";
}
