package com.life.app.client.dashboard.fragments.radarChart;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.fragments.radarChart.Balance.BalanceExperience;
import com.life.app.client.dashboard.fragments.radarChart.Balance.BalanceResults;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.ArrayList;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 21/07/2016.
 */
public class BalanceFragment extends Fragment {

    RelativeLayout back;
    Button start;

    static BarChart mChart;

    TextView tvHighscore, tvResult, tvUnit, tvUnitMonthly, tvHighscoreMonthly, tvResultMonthly;

    View mLeak = null;

    static int expTry = 5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_balance, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.balance));

        start  = (Button) view.findViewById(R.id.start);
        back   = (RelativeLayout) view.findViewById(R.id.back);
        mChart = (BarChart) view.findViewById(R.id.barChart);

        setClickListenermChart();

        tvHighscore   = (TextView) view.findViewById(R.id.tvHighscore);
        tvResult      = (TextView) view.findViewById(R.id.tvResult);
        tvUnit        = (TextView) view.findViewById(R.id.tvUnit);
        tvUnitMonthly = (TextView) view.findViewById(R.id.tvUnitMonthly);

        tvHighscoreMonthly  = (TextView) view.findViewById(R.id.tvHighscoreMonthly);
        tvResultMonthly     = (TextView) view.findViewById(R.id.tvResultMonthly);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.getStatus()!=null
                        && MainActivity.getStatus().getWearable_status()==1)
                    startExperienceFragment();
                else
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_disconnected), Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.ibInfoExp).setVisibility(View.VISIBLE);
        view.findViewById(R.id.ibInfoExp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_description),
                        getResources().getString(R.string.hint_description_balance));
            }
        });
        (view.findViewById(R.id.ibInfo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog(getResources().getString(R.string.hint_description),
                        getResources().getString(R.string.hint_description_balance));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        String placeholderScore = String.format(Locale.US, "%.1f", SharedPrefDataHandler.getBest(getActivity(), RadarChartConst.BALANCE_KEY).get(0));
        String placeholderResult = Float.toString(SharedPrefDataHandler.getBest(getActivity(), RadarChartConst.BALANCE_KEY).get(1));

        tvHighscore.setText(placeholderScore);
        tvResult.setText(placeholderResult);
        tvUnit.setText(getString(R.string.unit_balance));
        tvUnitMonthly.setText(getString(R.string.unit_balance));

        tvHighscore.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvResult.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvHighscoreMonthly.setTypeface(Utils.getKnockoutFont(getActivity()));
        tvResultMonthly.setTypeface(Utils.getKnockoutFont(getActivity()));

        showHistoryChart();
    }

    private void goBack() {
        getActivity().finish();
        getActivity().getFragmentManager().popBackStack();
    }

    public void showHistoryChart() {
        // setup barchart
        if(mChart!=null) {
            BarChartSetup.setBarChart(mChart, false, null, 0f, 100f);
            addDataToChart(mChart);
        }
    }

    private void addDataToChart(BarChart chart) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(6);

        ArrayList<BarEntry> entries = new ArrayList<>();

        for (int i=0; i< expTry; i++){
            ArrayList<Float> tries = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.BALANCE_KEY + "_" + (expTry - i));
            if(tries!=null)
                entries.add(new BarEntry(i + 1, tries.get(0)));
        }

        BarDataSet set1 = new BarDataSet(entries, "");
        set1.setHighlightEnabled(true);
        set1.setDrawValues(true);
        set1.setColor(GRAPHS_COLOR_WHITE);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setHighlightEnabled(true);
        data.setBarWidth(0.6f);
        data.setValueTextSize(16f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);

        chart.setData(data);
        chart.invalidate();
    }

    private void startExperienceFragment() {
        //Request.stopExperience(getActivity());
        Utils.stopExperience();
        BalanceExperience f = new BalanceExperience();
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.balance_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        start   = null;
        back    = null;
        tvHighscore = null;
        tvResult   = null;
        tvUnit   = null;
        mLeak   = null; // now cleaning up!
    }

    private void setClickListenermChart(){
        mChart.setHighlightPerTapEnabled(true);
        // Make sure it's clickable.
        mChart.setClickable(true);
        mChart.setTouchEnabled(true);
        mChart.setEnabled(true);
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Bundle b = new Bundle();
                b.putInt("position", 6 - (int)e.getX());
                Fragment f = new BalanceResults();
                f.setArguments(b);
                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.balance_placeholder, f, f.getClass().getName()).
                        addToBackStack(f.getClass().getName()).
                        commit();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void infoDialog(String title, String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);
        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }
}