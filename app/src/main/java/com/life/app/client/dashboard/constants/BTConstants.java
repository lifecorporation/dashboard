package com.life.app.client.dashboard.constants;


import com.life.app.client.dashboard.protocols.BTProtocolFrame;

/**
 * Created by peppe on 27/06/2016.
 */
public class BTConstants {

    public static final byte ACK = 0x01;
    public static final byte NACK = 0x02;
    public static final int BT_RX_BUFF_SIZE = 2048;
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_SEARCHING = 1; // now listening for incoming
    // connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing
    // connection
    public static final int STATE_CONNECTED = 3; // now connected to a remote
    // device

    public static final int MESSAGE_STATE_CHANGE = 1;
    //    public static final int MESSAGE_READ = 2;
//    public static final int MESSAGE_WRITE = 3;
//    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_STATUS = BTProtocolFrame.CC_GETSTATUS;
}

