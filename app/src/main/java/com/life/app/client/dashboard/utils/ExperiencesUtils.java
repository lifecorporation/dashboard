package com.life.app.client.dashboard.utils;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Handler;

import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.services.bind.messages.Request;

import java.util.UUID;

/*
 * Created by chkalog on 9/8/2016.
 */
public class ExperiencesUtils {

    public static String EXPERIENCE_RUNNING_KEY = null;

    /*public static void stopExperience(Activity activity) {
        if(activity!=null && activity.getFragmentManager().getBackStackEntryCount()>0) {
            activity.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = null;
        Request.stopExperience(activity.getApplicationContext());
    }*/

    public static void experienceFinished(Activity activity, String experienceKey, int value){
        savePreferencesData(activity, experienceKey, value);
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = null;
        if(activity!=null && activity.getFragmentManager().getBackStackEntryCount()>0) {
            activity.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private static void savePreferencesData(Activity activity, String experienceKey, int value) {
        // Otteniamo il riferimento alle Preferences
        SharedPreferences prefs = activity.getSharedPreferences(RadarChartConst.MY_PREFERENCES, 0);
        // Otteniamo il corrispondente Editor
        SharedPreferences.Editor editor = prefs.edit();
        //int value = ((int) (Math.random() * 70) + 30);
        if(value>100)   value = 100;
        //SharedPrefDataHandler.setHistoryValues(experienceKey, value);
        editor.apply();
    }

    /**stop current running experience and restart base experience
     *
     */
    public static void resetBaseExperience(final Activity activity){

        Utils.stopExperience();

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if(!Uuid.generalExperience(MainActivity.getStatus()).equals("")) {
                    //Request.startExperience(activity, UUID.fromString(Uuid.generalExperience(MainActivity.getStatus())));
                    Utils.startExperience(UUID.fromString(Uuid.generalExperience(MainActivity.getStatus())));
                    ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.generalExperience(MainActivity.getStatus());
                }
            }
        }, Consts.START_EXPERIENCE_DELAY);
    }
}
