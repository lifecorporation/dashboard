package com.life.app.client.dashboard.fragments.menu;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.life.app.client.dashboard.R;

/*
 * Created by chkalog on 18/7/2016.
 */
public class DeviceDiagnosisFragment extends Fragment {

    View mLeak = null;

    ImageView ivDownload;

    CoordinatorLayout coordinatorLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_diagnosis, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.main_content);

        ivDownload = (ImageView) view.findViewById(R.id.ivDownload);
        ivDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, "Link coming soon", Snackbar.LENGTH_LONG)
                        .setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });

                snackbar.getView().setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
                snackbar.setActionTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                snackbar.show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLeak = null; // now cleaning up!
    }
}
