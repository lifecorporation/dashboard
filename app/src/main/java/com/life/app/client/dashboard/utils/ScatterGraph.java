package com.life.app.client.dashboard.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/*
 * Created by chkalog on 14/10/2016.
 */
public class ScatterGraph {

    public Intent getIntent(Context context){

        float[] x = {1f,2.1f,3,0.7f,5,6f,0.7f,8,9.9f};
        float[] values = {1.1f,1.2f,1.3f,1.1f,1.5f,6.1f,7.3f,0.8f,0.9f};

        XYSeries series = new XYSeries("Series 1");
        int length = x.length;
        for(int k=0;k<length;k++){
            series.add(x[k], values[k]);
        }

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(series);

        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setColor(Color.WHITE);
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setLineWidth(0);

        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        mRenderer.addSeriesRenderer(renderer);

        return ChartFactory.getScatterChartIntent(context, dataset, mRenderer);
    }

}
