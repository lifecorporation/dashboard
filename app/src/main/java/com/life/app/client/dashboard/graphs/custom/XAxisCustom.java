package com.life.app.client.dashboard.graphs.custom;

import android.content.DialogInterface;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.components.XAxis;

/**
 * Created by peppe on 20/07/2016.
 */
public class XAxisCustom extends XAxis implements View.OnClickListener {

    @Override
    public void onClick(View v) {

        Log.i("Radarchart","cliccato "+ v.getId());

    }
}
