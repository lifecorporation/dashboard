package com.life.app.client.dashboard.fragments.radarChart.Agility;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AgilityActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import java.util.ArrayList;
import java.util.Collections;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 28/9/2016.
 */
public class RightFragment extends Fragment implements OnChartValueSelectedListener {

    ScatterChart mChart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agility_results_right, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChart = (ScatterChart) view.findViewById(R.id.scatterChart);

        setChart();
        loadDataFromSharedPref();
    }

    private void setChart(){

        Description descr = new Description();
        descr.setText("Jump frequency (jumps/min)");
        descr.setTextColor(GRAPHS_COLOR_WHITE);
        descr.setTextSize(16f);
        mChart.setDescription(descr);

        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawGridBackground(false);
        mChart.setTouchEnabled(true);
        mChart.setMaxHighlightDistance(50f);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        mChart.setMaxVisibleValueCount(60);
        mChart.setPinchZoom(true);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXOffset(50f);

        YAxis yl = mChart.getAxisLeft();
        yl.setTextColor(GRAPHS_COLOR_WHITE);
        yl.setAxisMaxValue(160f);
        yl.setAxisMinValue(-5f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xl = mChart.getXAxis();
        xl.setDrawGridLines(true);
        xl.setTextColor(GRAPHS_COLOR_WHITE);
        xl.setAxisMaxValue(30f);
        xl.setAxisMinValue(-5f);
    }

    private void loadDataFromSharedPref(){

        int totaljumps = Math.round(SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position).get(0));

        ArrayList<Float> frequencies = new ArrayList<>();

        for(int i=0; i<totaljumps;i++){
            ArrayList<Float> frequency = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position + "_TRY_" + (i+1));
            if(frequency!=null)
                frequencies.add(frequency.get(1));
        }
        addDataToChart(frequencies);
    }

    private void addDataToChart(ArrayList<Float> frequencies){

        ArrayList<Entry> yVals1 = new ArrayList<>();

        for (int i = 0; i < frequencies.size(); i++) {
            yVals1.add(new Entry((i+1), frequencies.get(i)));
        }

        Collections.sort(yVals1, new EntryXComparator());

        ScatterDataSet set1 = new ScatterDataSet(yVals1, "");
        set1.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        set1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        set1.setScatterShapeSize(8f);
        set1.setDrawValues(false);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the datasets

        ScatterData data = new ScatterData(dataSets);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        mChart.setData(data);
        AgilityActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChart.invalidate();
            }
        });
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
