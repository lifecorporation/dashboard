package com.life.app.client.dashboard.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

/**
 * Created by peppe on 28/06/2016.
 */
public class BackPressActivity extends MyFragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
    }
}
