package com.life.app.client.dashboard.utils;

/*
 * Created by chara on 30-Mar-17.
 */

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import tourguide.tourguide.ChainTourGuide;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Sequence;
import tourguide.tourguide.ToolTip;

public class Showcase{

    private static Sequence sequence = null;

    private static ChainTourGuide tourGuide = null;


    public static void presentShowcaseSequence(final View view1, final View view2, final View view3,
                                               final View view4, final View view5, final Activity mActivity, int delay){

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mActivity.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                /* setup enter and exit animation */
                Animation enterAnimation = new AlphaAnimation(0f, 0.4f);
                enterAnimation.setDuration(500);
                enterAnimation.setFillAfter(true);

                Animation exitAnimation = new AlphaAnimation(0.4f, 0f);
                exitAnimation.setDuration(500);
                exitAnimation.setFillAfter(true);

                // the return handler is used to manipulate the cleanup of all the tutorial elements
                ChainTourGuide tourGuide1 = ChainTourGuide.init(mActivity)
                        .setToolTip(new ToolTip()
                                .setTitle("ContinueMethod.OVERLAY")
                                .setDescription("This is textView1")
                                .setGravity(Gravity.BOTTOM | Gravity.RIGHT)
                                .setBackgroundColor(Color.parseColor("#039DCC"))
                        )
                        .setOverlay(new Overlay(false, Color.parseColor("#455A64"), Overlay.Style.Circle)
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        // note that there is no Overlay here, so the default one will be used
                        .playLater(view1);

                ChainTourGuide tourGuide2 = ChainTourGuide.init(mActivity)
                        .setToolTip(new ToolTip()
                                .setTitle("Tip")
                                .setDescription("This is textView2")
                                .setGravity(Gravity.BOTTOM | Gravity.RIGHT)
                                .setBackgroundColor(Color.parseColor("#039DCC"))
                        )
                        .setOverlay(new Overlay()
                                .setBackgroundColor(Color.parseColor("#CC2c3e50"))
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        .playLater(view2);

                ChainTourGuide tourGuide3 = ChainTourGuide.init(mActivity)
                        .setToolTip(new ToolTip()
                                .setTitle("ContinueMethod.OVERLAY")
                                .setDescription("This is view 3")
                                .setGravity(Gravity.BOTTOM | Gravity.RIGHT)
                                .setBackgroundColor(Color.parseColor("#039DCC"))
                        )
                        .setOverlay(new Overlay()
                                .setBackgroundColor(Color.parseColor("#CC2c3e50"))
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        // note that there is no Overlay here, so the default one will be used
                        .playLater(view3);

                ChainTourGuide tourGuide4 = ChainTourGuide.init(mActivity)
                        .setToolTip(new ToolTip()
                                .setTitle("ContinueMethod.OVERLAY")
                                .setDescription("This is view 4")
                                .setGravity(Gravity.TOP | Gravity.RIGHT)
                                .setBackgroundColor(Color.parseColor("#039DCC"))
                        )
                        .setOverlay(new Overlay()
                                .setBackgroundColor(Color.parseColor("#CC2c3e50"))
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        // note that there is no Overlay here, so the default one will be used
                        .playLater(view4);

                ChainTourGuide tourGuide5 = ChainTourGuide.init(mActivity)
                        .setToolTip(new ToolTip()
                                .setTitle("ContinueMethod.OVERLAY")
                                .setDescription("This is view 5")
                                .setGravity(Gravity.TOP | Gravity.RIGHT)
                                .setBackgroundColor(Color.parseColor("#039DCC"))
                        )
                        .setOverlay(new Overlay()
                                .setBackgroundColor(Color.parseColor("#CC2c3e50"))
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        // note that there is no Overlay here, so the default one will be used
                        .playLater(view5);

                sequence = new Sequence.SequenceBuilder()
                        .add(tourGuide1, tourGuide2, tourGuide3, tourGuide4, tourGuide5)
                        .setDefaultOverlay(new Overlay()
                                .setEnterAnimation(enterAnimation)
                                .setExitAnimation(exitAnimation)
                        )
                        .setDefaultPointer(null)
                        .setContinueMethod(Sequence.ContinueMethod.Overlay)
                        .build();


                tourGuide = ChainTourGuide.init(mActivity);
                tourGuide.playInSequence(sequence);
            }
        };
        mainHandler.postDelayed(myRunnable,1200);
    }

    public static void hideSequence(){
        if(tourGuide == null)
            return;

        tourGuide.cleanUp();
    }
}
