package com.life.app.client.dashboard.constants;


/*
 * Created by peppe on 21/07/2016.
 */
public class RadarChartConst {

    final public static int POS_ANAEROBIC_CAPACITY             = 0;
    final public static int POS_AEROBIC_POWER                  = 1;
    final public static int POS_JOINT_MOBILITY                 = 2;
    final public static int POS_REACTION_TIME                  = 3;
    final public static int POS_AGILITY                        = 4;
    final public static int POS_BALANCE                        = 5;
    final public static int POS_EXPLOSIVENESS                   = 6;
    final public static int POS_RHYTHM                         = 7;
    final public static int POS_SPEED                          = 8;
    final public static int POS_STRENGHT                       = 9;

    // Identificatore delle preferenze dell'applicazione
    final public static String MY_PREFERENCES                  = "MyPref";

    /*
    final public static String BALANCE_KEY                     = "BalanceValue";
    final public static String BALANCE_KEY_1                   = "BalanceValue1";
    final public static String BALANCE_KEY_2                   = "BalanceValue2";
    final public static String BALANCE_KEY_3                   = "BalanceValue3";
    final public static String BALANCE_KEY_4                   = "BalanceValue4";
    final public static String BALANCE_KEY_BEST                = "BalanceValueBest";

    final public static String AEROBIC_POWER_KEY               = "AerobicPowerValue";
    final public static String AEROBIC_POWER_KEY_1             = "AerobicPowerValue1";
    final public static String AEROBIC_POWER_KEY_2             = "AerobicPowerValue2";
    final public static String AEROBIC_POWER_KEY_3             = "AerobicPowerValue3";
    final public static String AEROBIC_POWER_KEY_4             = "AerobicPowerValue4";
    final public static String AEROBIC_POWER_KEY_BEST          = "AerobicPowerValueBest";

    final public static String ANAEROBIC_CAPACITY_KEY          = "AnaerobicCapacityValue";
    final public static String ANAEROBIC_CAPACITY_KEY_1        = "AnaerobicCapacityValue1";
    final public static String ANAEROBIC_CAPACITY_KEY_2        = "AnaerobicCapacityValue2";
    final public static String ANAEROBIC_CAPACITY_KEY_3        = "AnaerobicCapacityValue3";
    final public static String ANAEROBIC_CAPACITY_KEY_4        = "AnaerobicCapacityValue4";
    final public static String ANAEROBIC_CAPACITY_KEY_BEST     = "AnaerobicCapacityValueBest";

    final public static String AGILITY_KEY                     = "AgilityValue";
    final public static String AGILITY_KEY_1                   = "AgilityValue1";
    final public static String AGILITY_KEY_2                   = "AgilityValue2";
    final public static String AGILITY_KEY_3                   = "AgilityValue3";
    final public static String AGILITY_KEY_4                   = "AgilityValue4";
    final public static String AGILITY_KEY_BEST                = "AgilityValueBest";

    final public static String EXPLOSIVNESS_KEY                = "ExplosivnessValue";
    final public static String EXPLOSIVNESS_KEY_1              = "ExplosivnessValue1";
    final public static String EXPLOSIVNESS_KEY_2              = "ExplosivnessValue2";
    final public static String EXPLOSIVNESS_KEY_3              = "ExplosivnessValue3";
    final public static String EXPLOSIVNESS_KEY_4              = "ExplosivnessValue4";
    final public static String EXPLOSIVNESS_KEY_BEST           = "ExplosivnessValueBest";

    final public static String REACTION_TIME_KEY               = "ReactionTimeValue";
    final public static String REACTION_TIME_KEY_1             = "ReactionTimeValue1";
    final public static String REACTION_TIME_KEY_2             = "ReactionTimeValue2";
    final public static String REACTION_TIME_KEY_3             = "ReactionTimeValue3";
    final public static String REACTION_TIME_KEY_4             = "ReactionTimeValue4";
    final public static String REACTION_TIME_KEY_BEST          = "ReactionTimeValueBest";

    final public static String JOINT_MOBILITY_KEY              = "JointMobilityValue";
    final public static String JOINT_MOBILITY_KEY_1            = "JointMobilityValue1";
    final public static String JOINT_MOBILITY_KEY_2            = "JointMobilityValue2";
    final public static String JOINT_MOBILITY_KEY_3            = "JointMobilityValue3";
    final public static String JOINT_MOBILITY_KEY_4            = "JointMobilityValue4";
    final public static String JOINT_MOBILITY_KEY_BEST         = "JointMobilityValueBest";

    final public static String RHYTHM_KEY                      = "RhythmValue";
    final public static String RHYTHM_KEY_1                    = "RhythmValue1";
    final public static String RHYTHM_KEY_2                    = "RhythmValue2";
    final public static String RHYTHM_KEY_3                    = "RhythmValue3";
    final public static String RHYTHM_KEY_4                    = "RhythmValue4";
    final public static String RHYTHM_KEY_BEST                 = "RhythmValueBest";

    final public static String SPEED_KEY                       = "SpeedValue";
    final public static String SPEED_KEY_1                     = "SpeedValue1";
    final public static String SPEED_KEY_2                     = "SpeedValue2";
    final public static String SPEED_KEY_3                     = "SpeedValue3";
    final public static String SPEED_KEY_4                     = "SpeedValue4";
    final public static String SPEED_KEY_BEST                  = "SpeedValueBest";

    final public static String STRENGHT_KEY                    = "StrenghtValue";
    final public static String STRENGHT_KEY_1                  = "StrenghtValue1";
    final public static String STRENGHT_KEY_2                  = "StrenghtValue2";
    final public static String STRENGHT_KEY_3                  = "StrenghtValue3";
    final public static String STRENGHT_KEY_4                  = "StrenghtValue4";
    final public static String STRENGHT_KEY_BEST               = "StrenghtValueBest";
    */

    final public static String ANAEROBIC_CAPACITY_KEY = "ANAEROBIC_CAPACITY_KEY";
    final public static String AEROBIC_POWER_KEY      = "AEROBIC_POWER_KEY";
    final public static String REACTION_TIME_KEY      = "REACTION_TIME_KEY";
    final public static String AGILITY_KEY            = "AGILITY_KEY";
    final public static String BALANCE_KEY            = "BALANCE_KEY";
    final public static String EXPLOSIVENESS_KEY      = "EXPLOSIVENESS_KEY";
    final public static String RHYTHM_KEY             = "RHYTHM_KEY";
    final public static String SPEED_KEY              = "SPEED_KEY";
    final public static String STRENGTH_KEY           = "STRENGTH_KEY";

    final public static int MAX_EXPERIENCE_VALUES     = 5;
    final public static int MAX_JUMPS                 = 150;

}
