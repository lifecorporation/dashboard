package com.life.app.client.dashboard.activities.abilities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.utils.Utils;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 23/8/2016.
 */
public class BalanceActivity  extends MyFragmentActivity {

    BottomSheetBehavior bottom_sheet;
    private static View tutorial;

    static Activity instance = null;
    public static Context balanceContext = null;

    LinearLayout llSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_balance);

        instance = this;
        balanceContext = this;

        tutorial = findViewById(R.id.bottom_sheet);
        llSheet  = (LinearLayout)findViewById(R.id.llSheet);
        bottom_sheet = BottomSheetBehavior.from(tutorial);
        bottom_sheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
        Utils.setPeekHeight(this, bottom_sheet);

        llSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tutorial.post(new Runnable(){
                    public void run(){
                        if(BottomSheetBehavior.from(tutorial).getState() == BottomSheetBehavior.STATE_EXPANDED){
                            // Right here!
                            BottomSheetBehavior.from(tutorial)
                                    .setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }else{
                            BottomSheetBehavior.from(tutorial)
                                    .setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        BalanceActivity.showTutorial();
    }

    public static void hideTutorial(){
        tutorial.setVisibility(View.GONE);
    }

    public static void showTutorial(){
        tutorial.setVisibility(View.VISIBLE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    public static Activity getInstance(){
        return instance;
    }
}
