package com.life.app.client.dashboard.graphs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.Log;
import android.view.View;

import com.life.app.client.dashboard.objects.Line;

import java.util.ArrayList;

/*
 * Created by chara on 21-Nov-16.
 */

public class DrawView extends View {
    Paint paint = null;
    ArrayList<Path> paths = null;
    ArrayList<Line> lines = null;
    Canvas canvas = null;

    private boolean cc = true;

    public DrawView(Context context, ArrayList<Line> nLines){
        super(context);

        cc = true;

        paint = new Paint();
        lines = nLines;
        paths = new ArrayList<>();

        /*Log.i("DrawView", "startX = " + startX + "\n" +
                "startY = " + startY + "\n" +
                "endX = " + endX + "\n" +
                "endY = " + endY + "\n");*/

        paint.setColor(Color.WHITE);
    }

    public DrawView(Context context){
        super(context);

        cc = true;

        paint = new Paint();
        paths = new ArrayList<>();

        paint.setColor(Color.WHITE);
    }

    @Override
    public void onDraw(Canvas canvas){

        if(cc) {

            for (Line line : lines) {
                Path newpath = new Path();
                newpath.moveTo(line.getStartX(), line.getStartY());
                newpath.lineTo(line.getEndX(), line.getEndY());
                paths.add(newpath);
            }

            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            paint.setDither(true);
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStrokeWidth(1);

            for (Path path : paths)
                canvas.drawPath(path, paint);
            this.canvas = canvas;
        }else{
            Path path = new Path();
            Paint clearPaint = new Paint();
            clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            canvas.drawRect(0, 0, 0, 0, clearPaint);



            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }

    public void clear(){
        if (this.canvas != null){
            super.draw(this.canvas);
            cc = false;
        }
    }
}