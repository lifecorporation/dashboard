package com.life.app.client.dashboard.receivers;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.util.Log;
import android.widget.Toast;

import com.life.app.client.dashboard.activities.LoginActivity;
import com.life.app.client.dashboard.services.BTConnectionManagerService;


/**
 * Created by peppe on 28/06/2016.
 */
public class StartupReceiver extends BroadcastReceiver {

    Context ctx = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        ctx = context;

        //Log.i("Service","rilevato fine avvio   intent: " + intent.toString());

        AccountManager accountManager = AccountManager.get(context);
        Account[] arrayAccount= accountManager.getAccountsByType("com.life.x10y.account.X10YACCOUNT");


        //controllo che esista almeno un account
        if (arrayAccount.length > 0){
            //Log.i("Service","trovato account");
            //faccio partire il service per la connessione con il telefono via BT
            if (!isMyServiceRunning(BTConnectionManagerService.class)){
                Intent i = new Intent(context, BTConnectionManagerService.class);
                i.setFlags(ServiceInfo.FLAG_EXTERNAL_SERVICE);

                context.startService(i);
                //Log.i("Service","Service morto e quindi riavvio");
            }
        }
        else {

            Intent myIntent = new Intent(context, LoginActivity.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);

        }

        //Toast.makeText(context,"avvio completato", Toast.LENGTH_LONG).show();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
