package com.life.app.client.dashboard.objects;

/*
 * Created by chara on 16-Jan-17.
 */

import com.life.app.client.dashboard.objects.tshirt.FitnessTshirt;
import com.life.app.client.dashboard.objects.tshirt.RacingTshirt;
import com.life.app.client.dashboard.objects.tshirt.Tshirt;
import com.life.app.client.dashboard.objects.tshirt.WellBeingTshirt;
import com.life.services.bind.assets.Node;
import com.life.services.bind.status.Status;

import java.util.ArrayList;

public class Asset {

    private static Tshirt tshirt = null;

    public static void setTshirt(Status status, ArrayList<Node> _nodes){

        if(status == null)
            return;

        ArrayList<Node> nodes = new ArrayList<>();
        for(Node node : _nodes){
            nodes.add(new Node(node.getId(), node.getSensorsNumber(), node.getSensorsList()));
        }

        switch (status.getWearable_status()){
            case 1:
                if(status.getProduct_id() == 0 || status.getProduct_id() == 48){
                    tshirt = new FitnessTshirt(nodes);
                }else if (status.getProduct_id() == 1 || status.getProduct_id() == 49){
                    tshirt = new WellBeingTshirt(nodes);
                }else if (status.getProduct_id() == 3 || status.getProduct_id() == 51){
                    tshirt = new RacingTshirt(nodes);
                }
                break;
            case 0:
                tshirt = null;
                break;
            default:
                tshirt = null;
                break;
        }
    }

    public static Tshirt getTshirt() {
        return tshirt;
    }
}
