package com.life.app.client.dashboard.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.life.app.client.dashboard.R;

/*
 * Created by chkalog on 13/7/2016.
 */
public class HeartRateFragment extends Fragment {

    static TextView tvHeartrate;
    static GraphView graph = null;
    static DataPoint [] punti = null;
    static LineGraphSeries<DataPoint> serie = null;

    static int limit = 200;

    static boolean firstData = true;

    public static ImageView SS5;

    View mLeak = null;

    static int timestampPosition = 1;
    static int valuePosition = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_heartrate, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        tvHeartrate = (TextView) view.findViewById(R.id.tvHeartrateFrag);
        graph = (GraphView) view.findViewById(R.id.graphHeart);
        SS5 = (ImageView) view.findViewById(R.id.SS5);
        SS5.setImageResource(R.mipmap.ic_record_yellow);

        punti = new DataPoint[0];
        serie = new LineGraphSeries<DataPoint>(punti);

        firstData = true;

        if (graph != null){
            graph.getViewport().setScalable(true);
            graph.getViewport().setMaxX(limit);
            graph.addSeries(serie);
            //Log.i("HeartRateFragment","grafico istanziato");
        }
        else{
            //Log.i("HeartRateFragment","grafico non istanziato");
        }
    }

    public static void addRawData(final Activity activity, String [] data){
        if (graph == null) {
            graph = (GraphView) activity.findViewById(R.id.graphHeart);
        }

        final DataPoint punto = new DataPoint(Float.parseFloat(data[valuePosition]),Float.parseFloat(data[timestampPosition]));
        if (graph != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (firstData){
                        setGraph();
                    }
                    serie.appendData(punto, true, limit);
                }
            });
        }
    }

    public static void addHeartRateData(Activity activity, final String [] data){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(tvHeartrate!=null)   tvHeartrate.setText(data[valuePosition]);
            }
        });
    }

    private static void setGraph(){
        graph.getViewport().setScalable(true);
        graph.getViewport().setMaxX(limit);
        graph.addSeries(serie);
        firstData = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tvHeartrate = null;
        graph = null;
        SS5 = null;
        mLeak = null; // now cleaning up!
    }
}
