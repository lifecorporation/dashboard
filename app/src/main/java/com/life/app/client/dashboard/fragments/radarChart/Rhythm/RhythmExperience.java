package com.life.app.client.dashboard.fragments.radarChart.Rhythm;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.RhythmActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.HorizontalProgressBar;
import com.life.app.client.dashboard.utils.Percent;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 02/08/2016.
 */
public class RhythmExperience extends Fragment implements CountDownAnimation.CountDownListener{

    RelativeLayout back;

    private static TextView tvAttempt;

    private static ImageView ivLed;

    private static BarChart mChart;
    public static Activity mInstance = null;

    private static HorizontalProgressBar pbListen, pbRepeat;

    private static FragmentManager fragmentManager;

    private static ArrayList<ArrayList<Float>> rhythm_sAttempts = null; //number of beats
    private static ArrayList<Float> [] rhythm_results   = null; //values for every beat in ms (from 4 to 20)
    private static ArrayList<Float> synthesis = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rhythm_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        mInstance = getActivity();
        fragmentManager = getActivity().getSupportFragmentManager();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_rhythm));

        pbListen  = (HorizontalProgressBar) view.findViewById(R.id.pbListen);
        pbRepeat  = (HorizontalProgressBar) view.findViewById(R.id.pbRepeat);
        tvAttempt = (TextView) view.findViewById(R.id.tvAttempt);
        mChart    = (BarChart) view.findViewById(R.id.barChart);

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        ivLed = (ImageView) view.findViewById(R.id.ivIndicator);
        setShadow(R.drawable.ic_action_circle_white);

        rhythm_sAttempts = new ArrayList<>();
        rhythm_results   = (ArrayList<Float>[])new ArrayList[3];

        yVals1 = new ArrayList<>();
        historyValues = new ArrayList<>();

        BarChartSetup.setBarChart(mChart, false, null, 0f, 100f);
        addDataToChart();

        startCountDown(view);
    }

    private void goBack(){
        RhythmActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), RhythmActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private static int currentProgress1 = 0;
    private static int currentProgress2 = 0;

    private static boolean running1 = true;
    private static boolean running2 = false;
    private static boolean isTimerRunning = false;

    private static int exCounter = 1;
    public static void updateViews(final String [] data){
        /**
         *  data[0] :   UUID
         *  data[1] :   timestamp
         *  data[2] :   exCounter
         *  data[3] :   warning
         *  data[4] :   error
         *  data[5] :   reference value
         *  data[6] :   sNumber
         *  data[7] :   numBeats
         *  data[....] :   intValues
         */

        exCounter = (Double.valueOf(data[2]).intValue());

        if(data[3].equals("5.0")){
            isTimerRunning = true;

            RhythmActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String placeholder = Integer.toString(exCounter);
                    tvAttempt.setText(placeholder);
                    resetValues();
                }
            });
        }else {
            RhythmActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(exCounter<=3)
                        isWarning(RhythmActivity.getInstance(), (Double.valueOf(data[3]).intValue()) == 1);
                    //synthesis Number
                    updateDataToChart((Double.valueOf(data[6]).intValue()));
                }
            });

            //synthesis Number
            if(rhythm_sAttempts == null)
                rhythm_sAttempts = new ArrayList<>();
            ArrayList<Float> newtry = new ArrayList<>();
            newtry.add(((Float.valueOf(data[3]))));
            newtry.add(((Float.valueOf(data[4]))));
            newtry.add(((Float.valueOf(data[5]))));
            newtry.add(((Float.valueOf(data[6]))));
            newtry.add(((Float.valueOf(data[7]))));
            rhythm_sAttempts.add(newtry);

            //intValues
            rhythm_results[exCounter - 1] = new ArrayList<>();
            if ((Double.valueOf(data[7]).intValue()) != 1) {
                for (int k = 8; k < 7 + ((Double.valueOf(data[7]).intValue())); k++) {
                    rhythm_results[exCounter - 1].add((Float.valueOf(data[k])) / 1000);
                }
            }
        }
    }

    private static void isWarning(Activity activity, boolean isWarning){
        if(isWarning)
            Toast.makeText(activity, activity.getResources().getString(R.string.warning_experience_rhythm),
                    Toast.LENGTH_LONG).show();
    }

    private static void resetValues(){
        running1 = true;
        running2 = false;
        currentProgress1 = 0;
        currentProgress2 = 0;
        pbRepeat.setCurrentValue(new Percent(0));
        pbListen.setCurrentValue(new Percent(0));
    }

    public static void finalize(String [] data){
        synthesis = new ArrayList<>();
        //Log.i("RHYTHM", "SYNTHESIS : " + data[2]);
        try {
            if (Float.parseFloat(data[2]) > 100)
                synthesis.add(Float.parseFloat(Integer.toString(100)));
            else
                synthesis.add(Float.valueOf(data[2]));
        }catch (NumberFormatException n){
            synthesis.add(Float.parseFloat(Integer.toString(0)));
        }
        saveToSharedPref();
        gotoResults();
    }

    public static void moveCatcher(final String [] data){
        RhythmActivity.getInstance().runOnUiThread(new Thread(new Runnable() {
            @Override
            public void run() {
                addLedData(Double.valueOf(data[2]).intValue());
            }
        }));
    }

    public void initializeTimerTask() {
        if(isTimerRunning) {
            if (running1) {
                if(currentProgress1 < 100){
                    currentProgress1 += 10;
                    pbListen.setCurrentValue(new Percent(currentProgress1));
                }
                if(currentProgress1 >= 100){
                    running1 = false;
                    running2 = true;
                    currentProgress1 = 0;
                }
            }
            if (running2) {
                if(currentProgress2 < 100){
                    currentProgress2 += 10;
                    pbRepeat.setCurrentValue(new Percent(currentProgress2));
                }
                if(currentProgress2 >= 100){
                    running2 = false;
                    currentProgress2 = 0;
                }
            }
        }
    }

    private static Timer mTimer1;
    private Handler mTimerHandler = new Handler();
    private void startExperience(){
        mTimer1 = new Timer();
        TimerTask mTt1 = new TimerTask() {
            public void run() {
                mTimerHandler.post(new Runnable() {
                    public void run(){
                        initializeTimerTask();
                    }
                });
            }
        };

        mTimer1.schedule(mTt1, 1, 1100);

        Utils.startExperience(UUID.fromString(Uuid.EX_RHYTHM));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_RHYTHM;
        tvAttempt.setText("1");
    }


    private static void gotoResults(){
        Bundle b = new Bundle();
        b.putInt("position", 1);
        Fragment f = new RhythmResults();
        f.setArguments(b);
        fragmentManager.beginTransaction().
                replace(R.id.rhythm_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    private static void saveToSharedPref(){
        SharedPrefDataHandler.addNewSynthesis(RhythmActivity.getInstance(), RadarChartConst.RHYTHM_KEY, synthesis);
        SharedPrefDataHandler.addBest(RhythmActivity.getInstance(), RadarChartConst.RHYTHM_KEY, synthesis);
        for(int i=0;i<3;i++){
            SharedPrefDataHandler.addNewTry(RhythmActivity.getInstance(), RadarChartConst.RHYTHM_KEY + "_1_TRY_" + (i+1), rhythm_sAttempts.get(i));
            SharedPrefDataHandler.addNewSet(RhythmActivity.getInstance(), RadarChartConst.RHYTHM_KEY + "_1_TRY_" + (i+1) + "_SET", rhythm_results[i]);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());

        isTimerRunning = false;

        if(mTimer1 != null){
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());

        isTimerRunning = false;

        if(mTimer1 != null){
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());

        isTimerRunning = false;

        if(mTimer1 != null){
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    private static void setShadow(int image){
        final Bitmap srcBitmap = BitmapFactory.decodeResource(mInstance.getResources(), image);
        // Display the bitmap in ImageView
        ivLed.setImageBitmap(srcBitmap);
        // Initialize a new Paint instance
        Paint paint = new Paint();

        // Get source bitmap width and height
        int srcBitmapWidth = srcBitmap.getWidth();
        int srcBitmapHeight = srcBitmap.getHeight();

        // Define border and shadow width
        int borderWidth = 12;
        int shadowWidth = 10;

        // destination bitmap width
        int dstBitmapWidth = Math.min(srcBitmapWidth,srcBitmapHeight)+borderWidth*2;
        //float radius = Math.min(srcBitmapWidth,srcBitmapHeight)/2;

        // Initializing a new bitmap to draw source bitmap, border and shadow
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);

        // Initialize a new canvas
        Canvas canvas = new Canvas(dstBitmap);

        // Draw a solid color to canvas
        canvas.drawColor(Color.WHITE);

        // Draw the source bitmap to destination bitmap by keeping border and shadow spaces
        canvas.drawBitmap(srcBitmap, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);

        // Use Paint to draw border
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);

        // Draw the border in destination bitmap
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);

        // Use Paint to draw shadow
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);

        // Draw the shadow on circular bitmap
        canvas.drawCircle(canvas.getWidth()/2,canvas.getHeight()/2,canvas.getWidth()/2,paint);

        /*
            RoundedBitmapDrawable
                A Drawable that wraps a bitmap and can be drawn with rounded corners. You
                can create a RoundedBitmapDrawable from a file path, an input stream, or
                from a Bitmap object.
        */
        // Initialize a new RoundedBitmapDrawable object to make ImageView circular
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mInstance.getResources(), dstBitmap);

        // Make the ImageView image to a circular image
        roundedBitmapDrawable.setCircular(true);

        /*
            setAntiAlias(boolean aa)
                Enables or disables anti-aliasing for this drawable.
        */
        roundedBitmapDrawable.setAntiAlias(true);

        // Set the ImageView image as drawable object
        ivLed.setImageDrawable(roundedBitmapDrawable);
    }

    private static void updateDataToChart(int input) {
        historyValues.add(input);
        yVals1.add(new BarEntry(yVals1.size(), input));

        data.notifyDataChanged(); // NOTIFIES THE DATA OBJECT
        RhythmActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChart.notifyDataSetChanged();   // let the chart know it's data changed
                mChart.invalidate();             // refresh
            }
        });
    }

    private static void addDataToChart() {
        float start = 0f;

        mChart.getXAxis().setAxisMinimum(start);
        mChart.getXAxis().setAxisMaximum(4);

        yVals1.add(new BarEntry(0, 0));

        set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_WHITE);
        set1.setHighlightEnabled(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        data = new BarData(dataSets);
        data.setDrawValues(true);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setValueTextSize(10f);
        data.setBarWidth(0.7f);

        mChart.setData(data);
        RhythmActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChart.notifyDataSetChanged();   // let the chart know it's data changed
                mChart.invalidate();             // refresh
            }
        });
    }

    public static void addLedData(final int data){
        if (ivLed != null){
            RhythmActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(data>=1) {
                        setShadow(R.drawable.ic_action_circle_blue);
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                setShadow(R.drawable.ic_action_circle_white);
                            }
                        }, 150);
                    }else
                        setShadow(R.drawable.ic_action_circle_white);
                }
            });
        }
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation;
        countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        startExperience();
    }

    static BarDataSet set1;
    private static ArrayList<BarEntry> yVals1 = null;
    private static ArrayList<Integer>  historyValues = null;
    private static BarData data;

    @Override
    public void onResume(){
        super.onResume();
        RhythmActivity.hideTutorial();
    }
}