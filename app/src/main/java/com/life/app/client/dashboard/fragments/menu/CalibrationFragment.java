package com.life.app.client.dashboard.fragments.menu;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.life.app.client.dashboard.MainActivity;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.UUID;

import static com.life.app.client.dashboard.constants.Consts.GRAPHS_COLOR_BLUE_LIGHT;

/*
 * Created by chkalog on 18/7/2016.
 */
public class CalibrationFragment extends Fragment implements View.OnClickListener{

    public static boolean calibrationRunning = false;

    static RelativeLayout rlForearmRight, rlBcpteLeft, rlForearmleft, rlBcpteRight;
    static ImageView ivForearmRight, ivBcpteLeft, ivForearmleft, ivBcpteRight;

    static TextView tvPhase;

    View mLeak = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        calibrationRunning = false;
        return inflater.inflate(R.layout.fragment_calibration, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        rlForearmRight = (RelativeLayout) mLeak.findViewById(R.id.rlForearmRight);
        rlBcpteLeft    = (RelativeLayout) mLeak.findViewById(R.id.rlBcpteLeft);
        rlForearmleft  = (RelativeLayout) mLeak.findViewById(R.id.rlForearmleft);
        rlBcpteRight   = (RelativeLayout) mLeak.findViewById(R.id.rlBcpteRight);

        ivForearmRight = (ImageView) mLeak.findViewById(R.id.ivForearmRight);
        ivBcpteLeft    = (ImageView) mLeak.findViewById(R.id.ivBcpteLeft);
        ivForearmleft  = (ImageView) mLeak.findViewById(R.id.ivForearmleft);
        ivBcpteRight   = (ImageView) mLeak.findViewById(R.id.ivBcpteRight);

        rlForearmRight.setOnClickListener(this);
        rlBcpteLeft.setOnClickListener(this);
        rlForearmleft.setOnClickListener(this);
        rlBcpteRight.setOnClickListener(this);

        updateMuscles();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLeak = null; // now cleaning up!
        calibrationRunning = false;
        values = null;
        haveFinalResult = false;

        if(graph!=null && series!=null)
            graph.removeSeries(series);

        graph = null;
        series = null;
    }

    public static void updateMuscles(){
        if(MainActivity.getStatus()!=null) {
            if (MainActivity.getStatus().getProduct_id() == 0 || MainActivity.getStatus().getProduct_id() == 48) {
                if(rlForearmleft!=null) rlForearmleft.setVisibility(View.VISIBLE);
                if(rlForearmRight!=null) rlForearmRight.setVisibility(View.VISIBLE);
            } else if (MainActivity.getStatus().getProduct_id() == 1 || MainActivity.getStatus().getProduct_id() == 49) {
                if(rlForearmleft!=null) rlForearmleft.setVisibility(View.GONE);
                if(rlForearmRight!=null) rlForearmRight.setVisibility(View.GONE);
            } else if (MainActivity.getStatus().getProduct_id() == 3 || MainActivity.getStatus().getProduct_id() == 51) {
                if(rlForearmleft!=null) rlForearmleft.setVisibility(View.VISIBLE);
                if(rlForearmRight!=null) rlForearmRight.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(MainActivity.getStatus()!=null
                && MainActivity.getStatus().getWearable_status()==1) {

            switch (view.getId()){
                case R.id.rlForearmRight:

                    startCalibrationDialog(getString(R.string.forearm_right), UUID.fromString(Uuid.FOREARM_RIGHT_CALIBRATION));

                    break;
                case R.id.rlBcpteLeft:

                    startCalibrationDialog(getString(R.string.biceps_left), UUID.fromString(Uuid.BICEPS_LEFT_CALIBRATION));

                    break;
                case R.id.rlForearmleft:

                    startCalibrationDialog(getString(R.string.forearm_left), UUID.fromString(Uuid.FOREARM_LEFT_CALIBRATION));

                    break;
                case R.id.rlBcpteRight:

                    startCalibrationDialog(getString(R.string.biceps_right), UUID.fromString(Uuid.BICEPS_RIGHT_CALIBRATION));

                    break;
            }
        }else {
            infoDialog(getString(R.string.warning), getString(R.string.hint_warning_message));
        }
    }

    private static GraphView graph = null;
    private static TextView tvDescription = null;
    private static AlertDialog calibrationDialog = null;

    private void startCalibrationDialog(final String muscle, final UUID uuid){

        final View infoDialogView = View.inflate(getActivity(), R.layout.dialog_calibration, null);
        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        ((TextView)infoDialogView.findViewById(R.id.tvMuscle)).setText(muscle);
        graph = (GraphView)infoDialogView.findViewById(R.id.graphMuscles);
        tvPhase = (TextView) infoDialogView.findViewById(R.id.tvPhase);
        tvDescription = (TextView) infoDialogView.findViewById(R.id.tvDescription);
        setSensorGraph(graph);

        calibrationDialog = new AlertDialog.Builder(getActivity())
            .setView(infoDialogView)
            .setCancelable(false)
            .setPositiveButton(R.string.start, null) //Set to null. We override the onclick
            .setNegativeButton(R.string.stop, null)
            .setNeutralButton(R.string.dismiss, null)
            .create();

        calibrationDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                final Button start = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                final Button stop = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                final Button dismiss = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEUTRAL);

                stop.setEnabled(false);

                start.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        haveFinalResult = false;
                        calibrationRunning = true;
                        Utils.stopExperience();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                Utils.startExperience(uuid);
                            }
                        }, Consts.START_EXPERIENCE_DELAY);

                        if(tvPhase!=null) tvPhase.setText("-");
                        if(tvDescription!=null) tvDescription.setText("");

                        start.setEnabled(false);
                        dismiss.setEnabled(false);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                stop.setEnabled(true);
                            }
                        }, 2500);
                    }
                });

                stop.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        calibrationRunning = false;
                        stop.setEnabled(false);
                        dismiss.setEnabled(true);

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                if(tvPhase!=null)
                                    tvPhase.setText("-");

                                if(tvDescription!=null)
                                    tvDescription.setText("");
                            }
                        }, 1000);

                        ExperiencesUtils.resetBaseExperience(getActivity());

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                start.setEnabled(true);
                            }
                        }, 2500);
                    }
                });

                dismiss.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        calibrationRunning = false;

                        if(tvPhase!=null) tvPhase.setText("-");
                        if(tvDescription!=null) tvDescription.setText("");

                        calibrationDialog.dismiss();


                    }
                });
            }
        });

        calibrationDialog.show();
    }

    private static void resultsDialog(Activity activity, int result, String description){
        final View resultsDialogView = View.inflate(activity, R.layout.dialog_calibration_results, null);
        resultsDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        TextView tvHighest = (TextView) resultsDialogView.findViewById(R.id.tvHighest);
        TextView tvAverage = (TextView) resultsDialogView.findViewById(R.id.tvAverage);
        TextView tvLowest  = (TextView) resultsDialogView.findViewById(R.id.tvLowest);
        TextView tvResult  = (TextView) resultsDialogView.findViewById(R.id.tvResult);
        ImageView ivResult = (ImageView) resultsDialogView.findViewById(R.id.ivResult);

        tvResult.setText(description);

        String placeholderH = String.format(Locale.US, "%.3f", 0.000);
        String placeholderA = String.format(Locale.US, "%.3f", 0.000);
        String placeholderL = String.format(Locale.US, "%.3f", 0.000);

        if(values!=null && values.size()>0) {
            placeholderH = String.format(Locale.US, "%.3f", Collections.max(values));
            placeholderA = String.format(Locale.US, "%.3f", calculateAverage(values));
            placeholderL = String.format(Locale.US, "%.3f", Collections.min(values));
        }

        tvHighest.setText(placeholderH);
        tvAverage.setText(placeholderA);
        tvLowest.setText(placeholderL);

        switch (result){
            case 0:
                ivResult.setImageResource(R.mipmap.ic_failure);
                break;
            case 1:
                ivResult.setImageResource(R.mipmap.ic_success);
                break;
        }

        AlertDialog resultsDialog = new AlertDialog.Builder(activity)
                .setView(resultsDialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        resultsDialog.show();
    }

    private static double calculateAverage(ArrayList <Double> list) {
        double sum = 0;
        if(!list.isEmpty()) {
            for (double mark : list) {
                sum += mark;
            }
            return sum / list.size();
        }
        return sum;
    }

    static DataPoint [] points = null;
    static LineGraphSeries<DataPoint> series = null;

    private void setSensorGraph(GraphView graph){
        graph.getGridLabelRenderer().setGridStyle( GridLabelRenderer.GridStyle.NONE );
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);// remove horizontal x labels and line
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);

        points = new DataPoint[0];
        series = new LineGraphSeries<>(points);
        series.setColor(GRAPHS_COLOR_BLUE_LIGHT);

        graph.getGridLabelRenderer().setHighlightZeroLines(false);
        graph.getViewport().setScalable(false);
        graph.getViewport().setScrollable(false);
        graph.addSeries(series);
    }

    public void infoDialog(final String title, final String description){
        final View infoDialogView = View.inflate(getActivity(), R.layout.info_dialog, null);

        infoDialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        ((TextView)infoDialogView.findViewById(R.id.tvTitle)).setText(title);
        ((TextView)infoDialogView.findViewById(R.id.tvDescription)).setText(description);

        final AlertDialog infoDialog = new AlertDialog.Builder(getActivity()).create();
        infoDialog.setView(infoDialogView);
        infoDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        infoDialog.setCancelable(false);
        infoDialog.show();
        infoDialog.getButton(-1).setTextColor(ContextCompat.getColor(getActivity(), R.color.life_blue_normal));
    }

    private static ArrayList<Double> values = null;
    private static boolean haveFinalResult = false;
    public static void onCalibrationReceived(final Activity activity, final String [] data){

        if(calibrationRunning) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String placeholder = data[3];
                    if (tvPhase != null) tvPhase.setText(placeholder);

                    switch ((int) Float.parseFloat(data[3])) {
                        case 0:
                            if (tvDescription != null)
                                tvDescription.setText(activity.getString(R.string.hint_calibration_step0));
                            break;
                        case 1:
                            if (tvDescription != null)
                                tvDescription.setText(activity.getString(R.string.hint_calibration_step1));
                            break;
                        case 2:
                            if (tvDescription != null)
                                tvDescription.setText(activity.getString(R.string.hint_calibration_step2));
                            break;
                        default:
                            if (tvDescription != null) tvDescription.setText("");
                            break;
                    }
                }
            });

            if (data[3].equals("1.0")) { //during calibration
                if (values == null)
                    values = new ArrayList<>();
                values.add(Double.parseDouble(data[2]));
            }

            if (!haveFinalResult && data[3].equals("3.0")) {
                haveFinalResult = true;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ExperiencesUtils.resetBaseExperience(activity);

                        if (calibrationDialog != null && calibrationDialog.isShowing()) {
                            calibrationDialog.dismiss();
                        }

                        calibrationRunning = false;
                    }
                });

                if (data[4].equals("1.0")) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resultsDialog(activity, 1, activity.getString(R.string.calibration_success));
                        }
                    });
                } else if (data[4].equals("0.0")) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resultsDialog(activity, 0, activity.getString(R.string.calibration_failure));
                        }
                    });
                }
            } else {
                addDataToGraph(activity, data);
            }
        }
    }

    private static long firstdate  = 0;
    private static long currdate   = 0;
    private static boolean isFirst = true;
    private static void addDataToGraph(Activity activity, final String [] data){

        if(graph!=null) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (isFirst) {
                        firstdate = Long.parseLong(data[1]);
                        currdate = Long.parseLong(data[1]);
                    } else {
                        currdate = Long.parseLong(data[1]);
                    }

                    float point = Float.parseFloat(data[2]);
                    if (point == 0)
                        point = 1;

                    final DataPoint punto = new DataPoint((currdate - firstdate), point);

                    if(graph!=null && series!=null){
                        if(isFirst){
                            graph.addSeries(series);
                            isFirst = false;
                        }else{
                            series.appendData(punto, true, 200);
                        }
                    }
                }
            });
        }
    }

    /**
     * EMG1
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean ForearmRightCalibrated = false;
    public static void updateForearmRight(final Activity activity, final String [] data){
        if (ivForearmRight != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(data[3].equals("2.0")) {
                        ForearmRightCalibrated = true;
                        ivForearmRight.setImageResource(R.drawable.ic_forearm_left);
                    }else{
                        ForearmRightCalibrated = false;
                        ivForearmRight.setImageResource(R.drawable.ic_forearm_left_deactivated);
                    }
                }
            });
        }
    }

    /**
     * EMG2
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean BicepsRightCalibrated = false;
    public static void updateBicepsRight(final Activity activity, final String [] data) {
        if (ivBcpteRight != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (data[3].equals("2.0")) {
                        BicepsRightCalibrated = true;
                        ivBcpteRight.setImageResource(R.drawable.ic_arm_right);
                    } else {
                        BicepsRightCalibrated = false;
                        ivBcpteRight.setImageResource(R.drawable.ic_arm_right_deactivated);
                    }
                }
            });
        }
    }

    /**
     * EMG3
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean ForearmLeftCalibrated = false;
    public static void updateForearmLeft(final Activity activity, final String [] data){
        if (ivForearmleft != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(data[3].equals("2.0")) {
                        ForearmLeftCalibrated = true;
                        ivForearmleft.setImageResource(R.drawable.ic_forearm_right);
                    }else{
                        ForearmLeftCalibrated = false;
                        ivForearmleft.setImageResource(R.drawable.ic_forearm_right_deactivated);
                    }
                }
            });
        }
    }

    /**
     * EMG4
     * @param activity MainActivity
     * @param data bytes received
     * data[2] : intensità segnale [0,100]
     * data[3] : flag calibrazione [0,1,2]
     */
    private static boolean BicepsLeftCalibrated = false;
    public static void updateBicepsLeft(final Activity activity, final String [] data){
        if (ivBcpteLeft != null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(data[3].equals("2.0")) {
                        BicepsLeftCalibrated = true;
                        ivBcpteLeft.setImageResource(R.drawable.ic_arm_left);
                    }else{
                        BicepsLeftCalibrated = false;
                        ivBcpteLeft.setImageResource(R.drawable.ic_arm_left_deactivated);
                    }
                }
            });
        }
    }
}
