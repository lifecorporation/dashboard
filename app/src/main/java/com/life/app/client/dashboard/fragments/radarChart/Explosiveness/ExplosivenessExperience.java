package com.life.app.client.dashboard.fragments.radarChart.Explosiveness;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.ExplosivenessActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.ArrayList;
import java.util.UUID;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 02/08/2016.
 */
public class ExplosivenessExperience extends Fragment implements CountDownAnimation.CountDownListener{

    View mLeak = null;

    public static Activity mInstance = null;

    RelativeLayout back = null;

    static BarChart mChart = null;
    static TextView tvAttempt, tvHeight;

    static ArrayList<BarEntry> entries = new ArrayList<>();
    static BarDataSet barSet1;
    static ArrayList<IBarDataSet> dataSets = new ArrayList<>();
    static BarData data;

    static ArrayList<Float> [] tries     = (ArrayList<Float>[])new ArrayList[3];
    static ArrayList<Float> [] results   = (ArrayList<Float>[])new ArrayList[3];
    static ArrayList<Float> synthesis      = new ArrayList<>();

    private static FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_explosiveness_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        mInstance = getActivity();

        fragmentManager = getActivity().getSupportFragmentManager();

        ExplosivenessActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_explosivness));

        mChart       = (BarChart) view.findViewById(R.id.barChart);
        tvAttempt    = (TextView) view.findViewById(R.id.tvAttempt);
        tvHeight     = (TextView) view.findViewById(R.id.tvHeight);

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        initializeArrays();

        BarChartSetup.setBarChart(mChart, false, null, 0f, 80f);
        addDataToChart(0,0);

        startCountDown(view);
    }

    @Override
    public void onResume(){
        super.onResume();
        ExplosivenessActivity.hideTutorial();
    }

    private void startExperience() {
        //Request.startExperience(ExplosivenessActivity.explosivenessContext, UUID.fromString(Uuid.EX_EXPLOSIVENESS));
        Utils.startExperience(UUID.fromString(Uuid.EX_EXPLOSIVENESS));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_EXPLOSIVENESS;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private static void addDataToChart(float x, float value) {
        //prepare data to save
        if ( x != 0){
            tries[(int)x - 1].add(value);
            results[(int)x - 1].add(value);
        }

        entries.add(new BarEntry(x, value));

        //set1.setBarShadowColor();

        barSet1 = new BarDataSet(entries, "");
        barSet1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        barSet1.setHighlightEnabled(false);

        dataSets.add(barSet1);

        data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.6f);

        mChart.setData(data);
        ExplosivenessActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {
                mChart.invalidate();
            }
        });
    }

    public static void onExperienceFinished(final Activity activity, String [] data){
        float tentativo = Float.parseFloat(data[2]);
        float height  = Float.parseFloat(data[3]);

        synthesis.add(tentativo);   //score normalizzato
        synthesis.add(height);

        SharedPrefDataHandler.addBest(activity, RadarChartConst.EXPLOSIVENESS_KEY , synthesis);
        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.EXPLOSIVENESS_KEY  , synthesis);
        for(int i=0;i<3;i++){
            SharedPrefDataHandler.addNewTry(activity, RadarChartConst.EXPLOSIVENESS_KEY + "_1_TRY_" + (i+1), tries[i]);
            SharedPrefDataHandler.addNewSet(activity, RadarChartConst.EXPLOSIVENESS_KEY + "_1_TRY_" + (i+1) + "_SET", results[i]);
        }

        ExplosivenessActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        goToResults();
                    }
                }, 500);
            }
        });
    }

    private static void goToResults(){
        Bundle b = new Bundle();
        b.putInt("position", 1);
        Fragment f = new ExplosivenessResults();
        f.setArguments(b);
        fragmentManager.beginTransaction().
                replace(R.id.explosivness_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    private void goBack(){
        ExplosivenessActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), ExplosivenessActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    public static void updateViews(String [] data){
        final int tentativo  = (int)Float.parseFloat(data[2]);
        final double height  =  round(Float.parseFloat(data[3]),2);

        addDataToChart(tentativo, (float)height);

        ExplosivenessActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {

                String placeholderAttempt = tentativo + "/3";
                String placeholderHeight  = height + " cm";

                tvAttempt.setText(placeholderAttempt);
                tvHeight.setText(placeholderHeight);
            }
        });
    }

    private void initializeArrays(){
        // initialize tries and results arrays
        for (int i=0; i< tries.length; i++){
            tries[i] = new ArrayList<>();
            results[i] = new ArrayList<>();
        }
    }

    private void resetValues(){
        mChart.clear();

        entries.clear();

        synthesis.clear();

        barSet1.clear();
        dataSets .clear();
        data.clearValues();
    }

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        startExperience();
    }
}