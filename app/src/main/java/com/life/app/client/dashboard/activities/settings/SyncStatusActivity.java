package com.life.app.client.dashboard.activities.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.objects.SyncStatus;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 1/9/2016.
 */
public class SyncStatusActivity extends MyFragmentActivity {

    SyncStatus status = null;

    TextInputLayout tvSpeed, tvPercentage, tvEstimatedTime, tvCurrentFileNumber, tvFilesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings_sync_status);

        ((TextView)findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_sync_status));

        tvSpeed             = (TextInputLayout) findViewById(R.id.tvSpeed);
        tvPercentage        = (TextInputLayout) findViewById(R.id.tvPercentage);
        tvEstimatedTime     = (TextInputLayout) findViewById(R.id.tvEstimatedTime);
        tvCurrentFileNumber = (TextInputLayout) findViewById(R.id.tvCurrentFileNumber);
        tvFilesCount        = (TextInputLayout) findViewById(R.id.tvFilesCount);

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, quitFilter);

        Utils.syncStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){

                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC)!=null ) {
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC).toString()) {
                        case "99" :
                            Toast.makeText(SyncStatusActivity.this, getResources().getString(R.string.error_sync_status), Toast.LENGTH_LONG).show();
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG_SYNC);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC).toString()){
                        case "99" :

                            status = BTConnectionManagerService.getSyncStatus();

                            if(status!=null) {

                                String placholderSpeed              =   status.getSpeed() + "";
                                String placholderPercentage         =   status.getPercentage() + "";
                                String placholderEstimatedTime      =   status.getEstimatedTime() + "";
                                String placholderCurrentFileNumber  =   status.getCurrentFileNumber() + "";
                                String placholderFilesCount         =   status.getFilesCount() + "";

                                if (tvSpeed.getEditText() != null)
                                    tvSpeed.getEditText().setText(placholderSpeed);
                                if (tvPercentage.getEditText() != null)
                                    tvPercentage.getEditText().setText(placholderPercentage);
                                if (tvEstimatedTime.getEditText() != null)
                                    tvEstimatedTime.getEditText().setText(placholderEstimatedTime);
                                if (tvCurrentFileNumber.getEditText() != null)
                                    tvCurrentFileNumber.getEditText().setText(placholderCurrentFileNumber);
                                if (tvFilesCount.getEditText() != null)
                                    tvFilesCount.getEditText().setText(placholderFilesCount);
                            }

                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE_SYNC);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG_SYNC);
                }
            }
        }
    };

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
