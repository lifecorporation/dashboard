package com.life.app.client.dashboard.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/*
 * Created by chkalog on 2/8/2016.
 */
public class CustomProgressDialog extends ProgressDialog {
    private Context context = null;
    public CustomProgressDialog(Context context) {
        super(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = this.getWindow();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        window.getDecorView().setSystemUiVisibility(uiOptions);
    }
}
