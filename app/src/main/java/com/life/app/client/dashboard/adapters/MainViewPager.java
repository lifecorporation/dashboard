package com.life.app.client.dashboard.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.life.app.client.dashboard.fragments.MainActivitySensorsFragment;
import com.life.app.client.dashboard.fragments.MainActivityMuscleFragment;
import com.life.app.client.dashboard.fragments.MainActivityRealTimeFragment;

/*
 * Created by chkalog on 28/9/2016.
 */
public class MainViewPager extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 3;

    public MainViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {

        switch (arg0) {
            case 0:
                return new MainActivityMuscleFragment();
            case 1:
                return new MainActivitySensorsFragment();
            case 2:
                return new MainActivityRealTimeFragment();
            default:
                return new MainActivityMuscleFragment();
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
