package com.life.app.client.dashboard.objects.tshirt;

/*
 * Created by chara on 27-Feb-17.
 */

import com.life.services.bind.assets.Node;

import java.util.ArrayList;

public class RacingTshirt extends Tshirt{

    public RacingTshirt(ArrayList<Node> nodes){
        super.setNodes(nodes);
    }

    public boolean getSS1(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 1) //0x28
                return true;
        }

        return false;
    }

    public boolean getSS2(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 2) //0x2C
                return true;
        }

        return false;
    }

    public boolean getSS3(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 3) //0x08
                return true;
        }

        return false;
    }

    public boolean getSS4(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 4) //0x0C
                return true;
        }

        return false;
    }

    public boolean getSS6(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 6) //0x24
                return true;
        }

        return false;
    }

    public boolean getSS7(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 7) //0x04
                return true;
        }

        return false;
    }

    public boolean getSS9(){
        if(super.getNodes() == null)
            return false;

        for(Node node: super.getNodes()){
            if(node.getId() == 9) //0xC8
                return true;
        }

        return false;
    }
}
