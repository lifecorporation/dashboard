package com.life.app.client.dashboard.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.life.app.client.dashboard.models.RegistrationInfo;


/**
 * Created by Jonathan Muller on 4/21/16.
 */
public class PreferencesManager
{

    private static final String SAVED_PREF = "SAVED_PREF";

    private static final String REGISTRATION_INFO_KEY = "REGISTRATION_INFO_KEY";

    //TODO Devi criptare i dati http://stackoverflow.com/questions/30148729/how-to-secure-android-shared-preferences

    public static void saveRegistrationInfo(RegistrationInfo info, Context context)
    {
        String infoJson = new Gson().toJson(info);

        SharedPreferences preferences = context.getSharedPreferences(SAVED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(REGISTRATION_INFO_KEY, infoJson);
        edit.apply();
    }

    public static RegistrationInfo getRegistrationInfo(Context context)
    {
        SharedPreferences preferences =context.getSharedPreferences(SAVED_PREF, Context.MODE_PRIVATE);
        String jsonInfo = preferences.getString(REGISTRATION_INFO_KEY, "");

        return new Gson().fromJson(jsonInfo, RegistrationInfo.class);
    }

    public static String getUsername(Context context){

        RegistrationInfo info=getRegistrationInfo(context);

        try {
            return info.username;
        }catch (NullPointerException n){
            return "";
        }
    }

    public static String getImei(Context context){

        RegistrationInfo info=getRegistrationInfo(context);

        try {
            return info.imei;
        }catch (NullPointerException n){
            return "";
        }
    }

    public static String getMac(Context context){

        RegistrationInfo info=getRegistrationInfo(context);
        try {
            return info.mac;
        }catch (NullPointerException n){
            return "";
        }
    }

    public static String getPass(Context context){

        RegistrationInfo info=getRegistrationInfo(context);
        try {
            return info.pass;
        }catch (NullPointerException n){
            return "";
        }
    }

    public static int getUserId(Context context){

        RegistrationInfo info=getRegistrationInfo(context);

        try {
            return info.userId;
        }catch (NullPointerException n){
            return 0;
        }
    }

    /*
    public static String getUsername(Context context){

        RegistrationInfo info=getRegistrationInfo(context);

        return info.username;
    }

    public static String getPassword(Context context){

        RegistrationInfo info=getRegistrationInfo(context);

        return info.password;
    }
    */
}
