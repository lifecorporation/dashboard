package com.life.app.client.dashboard.activities;

/*
 * Created by chara on 20-Mar-17.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.utils.Status;

public class NotificationDialogActivity extends MyFragmentActivity{

    private static ImageView ivBattery, ivShirt, ivCloud, ivStorage;
    private static TextView tvBattery, tvTshirt, tvCloud, tvStorage;

    private static Activity mActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        View dialogView = View.inflate(this, R.layout.info_notification_dialog_status, null);

        dialogView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        ivBattery = (ImageView) dialogView.findViewById(R.id.ivBattery);
        tvBattery = (TextView)  dialogView.findViewById(R.id.tvBattery);
        ivShirt   = (ImageView) dialogView.findViewById(R.id.ivShirt);
        tvTshirt  = (TextView)  dialogView.findViewById(R.id.tvTshirt);
        ivCloud   = (ImageView) dialogView.findViewById(R.id.ivCloud);
        tvCloud   = (TextView)  dialogView.findViewById(R.id.tvCloud);
        ivStorage = (ImageView) dialogView.findViewById(R.id.ivStorage);
        tvStorage = (TextView)  dialogView.findViewById(R.id.tvStorage);

        AlertDialog alert = dialogBuilder.create();
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
        alert.getButton(-1).setTextColor(ContextCompat.getColor(NotificationDialogActivity.this, R.color.life_blue_normal));
    }

    @Override
    public void onResume(){
        super.onResume();

        mActivity = this;
    }

    public static void setBatteryLevel(final int level){

        if(mActivity == null)
            return;

        if (ivBattery == null || tvBattery == null)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String placeholder = level + "%";
                if (level == -1)
                    placeholder = "-%";

                tvBattery.setText(placeholder);

                if (level == -1) {
                    ivBattery.setImageResource(R.mipmap.ic_action_battery_undefined);
                } else {
                    if (level < 20) {
                        ivBattery.setImageResource(R.mipmap.ic_battery_empty);
                    } else if (level < 40) {
                        ivBattery.setImageResource(R.mipmap.ic_battery_low);
                    } else if (level < 80) {
                        ivBattery.setImageResource(R.mipmap.ic_battery_charged);
                    } else {
                        ivBattery.setImageResource(R.mipmap.ic_battery_full);
                    }
                }
            }
        });
    }

    public static void setTshirtStatus(final Status status){

        if(mActivity == null)
            return;

        if (ivShirt == null || tvTshirt == null)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == null) {
                    tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_missing));
                    ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_undefined));
                } else {
                    switch (status.getWearable_status()) {
                        case -1:
                            tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_missing));
                            ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_undefined));
                            break;
                        case 0:
                            tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_missing));
                            if (status.getProduct_id() == 0 || status.getProduct_id() == 48) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_undefined));
                            } else if (status.getProduct_id() == 1 || status.getProduct_id() == 49) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_wb_undefined));
                            } else if (status.getProduct_id() == 3 || status.getProduct_id() == 51) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_undefined));
                            } else {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_undefined));
                            }
                            break;
                        case 1:
                            if (status.getProduct_id() == 0 || status.getProduct_id() == 48) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_connected));
                                tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_fitness));
                            } else if (status.getProduct_id() == 1 || status.getProduct_id() == 49) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_wb_connected));
                                tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_welbeing));
                            } else if (status.getProduct_id() == 3 || status.getProduct_id() == 51) {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_connected));
                                tvTshirt.setText(mActivity.getString(R.string.hint_tshirt_fireproof));
                            } else {
                                ivShirt.setImageDrawable(ContextCompat.getDrawable(mActivity, R.mipmap.ic_action_fitness_connected));
                                tvTshirt.setText("");
                            }
                            break;
                    }
                }
            }
        });
    }

    public static void setCloudStatus(final Status status){

        if(mActivity == null)
            return;

        if (ivCloud == null || tvCloud == null)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == null) {
                    ivCloud.setImageResource(R.mipmap.ic_cloud_undefined);
                    tvCloud.setText(mActivity.getResources().getString(R.string.hint_cloud_undefined));
                } else {
                    switch (status.getCloud_connected()) {
                        case -1:
                            ivCloud.setImageResource(R.mipmap.ic_cloud_undefined);
                            tvCloud.setText(mActivity.getResources().getString(R.string.hint_cloud_undefined));
                            break;
                        case 0:
                            ivCloud.setImageResource(R.mipmap.ic_cloud_waiting);
                            tvCloud.setText(mActivity.getResources().getString(R.string.hint_cloud_waiting));
                            break;
                        case 1:
                            ivCloud.setImageResource(R.mipmap.ic_cloud_connected);
                            tvCloud.setText(mActivity.getResources().getString(R.string.hint_cloud_connected));
                            break;
                        case 2:
                            ivCloud.setImageResource(R.mipmap.ic_cloud_disconnected);
                            tvCloud.setText(mActivity.getResources().getString(R.string.hint_cloud_disconnected));
                            break;
                    }
                }
            }
        });
    }

    public static void setStorageStatus(final Status status){

        if(mActivity == null)
            return;

        if (ivStorage == null || tvStorage == null)
            return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == null) {
                    ivStorage.setImageResource(R.mipmap.ic_storage_undefined);
                    if (tvStorage != null) {
                        String placeholder = "-%";
                        tvStorage.setText(placeholder);
                    }
                } else {
                    if (tvStorage != null) {
                        String placeholder = status.getStorage_percentage() + "% available storage";
                        tvStorage.setText(placeholder);
                    }

                    if (status.getStorage_percentage() > 70)
                        ivStorage.setImageResource(R.mipmap.ic_storage_empty);
                    else if (status.getStorage_percentage() > 20)
                        ivStorage.setImageResource(R.mipmap.ic_storage_warning);
                    else if (status.getStorage_percentage() >= 0)
                        ivStorage.setImageResource(R.mipmap.ic_storage_full);
                }

            }
        });
    }
}
