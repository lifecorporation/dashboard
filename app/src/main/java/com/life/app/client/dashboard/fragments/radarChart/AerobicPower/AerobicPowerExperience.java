package com.life.app.client.dashboard.fragments.radarChart.AerobicPower;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AerobicPowerActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.ArrayList;
import java.util.UUID;

/*
 * Created by peppe on 02/08/2016.
 */
public class AerobicPowerExperience extends Fragment implements CountDownAnimation.CountDownListener{

    View mLeak = null;

    public static Activity mInstance = null;

    RelativeLayout back = null;

    static TextView tvPhaseAerobicPower, tvTimeLeftAerobic;

    private static ArrayList<Float> synthesis = null;
    private static ArrayList<ArrayList<Float>> deviations = null;

    static ProgressBar progressBarLeftAerobic, progressBarRightAerobic, progressTimeLeft;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_aerobic_power_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;
        mInstance = getActivity();

        AerobicPowerActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_aerobic_power));

        fragmentManager = getActivity().getSupportFragmentManager();

        tvPhaseAerobicPower       = (TextView) view.findViewById(R.id.tvPhaseAerobicPower);
        tvTimeLeftAerobic         = (TextView) view.findViewById(R.id.tvTimeLeftAerobic);

        progressTimeLeft          = (ProgressBar) view.findViewById(R.id.progressTimeLeftAerobic);
        progressBarLeftAerobic    = (ProgressBar) view.findViewById(R.id.progressBarLeftAerobic);
        progressBarRightAerobic   = (ProgressBar) view.findViewById(R.id.progressBarRightAerobic);

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        startCountDown(view);
    }

    private void startExperience() {
        //Request.startExperience(AerobicPowerActivity.aerobicPowerContext, UUID.fromString(Uuid.EX_AEROBIC_POWER));
        Utils.startExperience(UUID.fromString(Uuid.EX_AEROBIC_POWER));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_AEROBIC_POWER;
    }

    @Override
    public void onResume(){
        super.onResume();
        AerobicPowerActivity.hideTutorial();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private void resetValues(){
        if(synthesis!=null) synthesis.clear();
        if(deviations!=null)    deviations.clear();

        synthesis = null;
        deviations = null;
    }

    public static void onExperienceFinished(Activity activity, String [] data){

        float score = Float.parseFloat(data[2]);
        float vo2Max = Float.parseFloat(data[3]);

        synthesis = new ArrayList<>();
        synthesis.add(score);
        synthesis.add(vo2Max);
        synthesis.add((float)deviations.size());

        saveToSharedPref(activity);
    }

    private static void saveToSharedPref(Activity activity){
        SharedPrefDataHandler.addBest(activity, RadarChartConst.AEROBIC_POWER_KEY , synthesis);
        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.AEROBIC_POWER_KEY  , synthesis);

        for(int i=0;i<deviations.size();i++) {
            SharedPrefDataHandler.addNewTry(activity, RadarChartConst.AEROBIC_POWER_KEY + "_1_TRY_" + (i + 1), deviations.get(i));
        }

        // Save heartBeats
        SharedPrefDataHandler.addNewSet(activity, RadarChartConst.AEROBIC_POWER_KEY + "_1_TRY_1_SET", heartbeats);

        AerobicPowerActivity.getInstance().runOnUiThread(new Runnable(){

            @Override
            public void run() {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        goToResults();
                    }
                }, 500);
            }
        });
    }


    private static FragmentManager fragmentManager;

    private static void goToResults(){
        Bundle b = new Bundle();
        b.putInt("position", 1);
        Fragment f = new AerobicPowerResults();
        f.setArguments(b);
        fragmentManager.beginTransaction().
                replace(R.id.aerobic_power_placeholder, f, f.getClass().getName()).
                addToBackStack(f.getClass().getName()).
                commit();
    }

    private void goBack(){
        AerobicPowerActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), AerobicPowerActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private static float lastDeviation = 0f;
    static float phase = 0;
    public static void updateViews(final String [] data){

        if(deviations == null)
            deviations = new ArrayList<>();

        phase = Float.parseFloat(data[2]);
        final float timeLeft   = Float.parseFloat(data[3]) / 1000;
        final float deviation  = Float.parseFloat(data[4]);

        if(deviation != lastDeviation){
            lastDeviation = deviation;
            ArrayList<Float> newdeviation = new ArrayList<>();
            newdeviation.add(timeLeft);
            newdeviation.add(deviation);
            deviations.add(newdeviation);
        }

        if(AerobicPowerActivity.getInstance()!=null) {
            AerobicPowerActivity.getInstance().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(tvPhaseAerobicPower!=null)   tvPhaseAerobicPower.setText("Phase : " + (int) phase);
                    if(progressTimeLeft!=null)   progressTimeLeft.setProgress(180 - Math.round(timeLeft));
                    if(tvTimeLeftAerobic!=null)  tvTimeLeftAerobic.setText("" + (Math.round(timeLeft)));
                    //update delay progress bars
                    if (deviation > 0) {
                        if(progressBarRightAerobic!=null)  progressBarRightAerobic.setProgress(Math.round(deviation));
                    } else {
                        if(progressBarLeftAerobic!=null)   progressBarLeftAerobic.setProgress(Math.round(deviation) * -1);
                    }
                }
            });
        }
    }

    static ArrayList<Float> heartbeats = null;
    // update heart rate textview
    public static void updateHeartRate (final String [] data) {
        try {

            if((int)phase==1){
                if(heartbeats == null)
                    heartbeats = new ArrayList<>();

                heartbeats.add(Float.parseFloat(data[2]));
            }

        }catch (ExceptionInInitializerError e){
            e.printStackTrace();
        }
    }

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        startExperience();
    }
}