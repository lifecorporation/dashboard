package com.life.app.client.dashboard.utils;

import android.util.Log;

import com.life.app.client.dashboard.protocols.BTProtocolFrame;

import java.util.UUID;

/*
 * Created by chara on 21-Dec-16.
 */
public class Status {

    /*
     *  Basic
     */
    /*
     *  Phone
     */
    private int     phone_sim_ready;        //1 byte
    private int     phone_carrier_len;      //1 byte
    private String  phone_carrier;          //phone_carrier_len bytes
    private int     phone_level;            //1 byte
    private int     phone_connected;        //1 byte
    private int     phone_network_type;     //1 byte
    private int     phone_roaming;          //1 byte

    /*
     *  Wifi
     */
    private int     wifi_status;            //1 byte
    private int     wifi_level;             //1 byte

    /*
     *  Battery
     */
    private int     battery_percentage;     //1 byte

    /*
     *  Storage
     */
    private int     storage_percentage;     //1 byte

    /*
     *  Wearable
     */
    private int     wearable_status;        //1 byte
    private int     wearable_number;        //1 byte
    private byte[]  wearable_product_id;    //wearable_number * 13 bytes
    private int     product_id;             //last

    /*
     * Experience
     */
    private int     experience_status;      //1 byte
    private byte[]  experience_detUUID;     //16 bytes
    private byte[]  experience_expUUID;     //16 bytes
    private int     experience_duration;    //4 bytes

    /*
     * Cloud
     */
    private int     cloud_connected;        //1 byte

    private boolean defined                 =   false;
    private UUID    exp_uuid;
    private UUID    det_uuid;

    public Status(byte[] btStatus) {

        BTProtocolFrame bframe = new BTProtocolFrame(btStatus.length);
        bframe.getFrame().put(btStatus);

        //Log.i("btStatus","btStatus " + Arrays.toString(btStatus));

        bframe.getFrame().position(0);

        this.setPhone_sim_ready(bframe.getFrame().get() & 0xff);
        this.setPhone_carrier_len(bframe.getFrame().get() & 0xff);

        phone_carrier = new String();
        for (int i=0;i<this.getPhone_carrier_len();i++)
            phone_carrier += Character.toString((char) (bframe.getFrame().get() & 0xff));

        this.setPhone_level(bframe.getFrame().get() & 0xff);
        this.setPhone_connected(bframe.getFrame().get() & 0xff);
        this.setPhone_network_type(bframe.getFrame().get() & 0xff);
        this.setPhone_roaming(bframe.getFrame().get() & 0xff);
        this.setWifi_status(bframe.getFrame().get() & 0xff);
        this.setWifi_level(bframe.getFrame().get() & 0xff);
        this.setBattery_percentage(bframe.getFrame().get() & 0xff);
        this.setStorage_percentage(bframe.getFrame().get() & 0xff);
        this.setWearable_status(bframe.getFrame().get() & 0xff);
        this.setWearable_number(bframe.getFrame().get() & 0xff);

        wearable_product_id  = new byte[this.getWearable_number() * 13];
        bframe.getFrame().get(wearable_product_id);
        this.setWearable_product_id(wearable_product_id);

        if(this.getWearable_number()!=0)
            this.setProduct_id(this.getWearable_product_id()[this.getWearable_product_id().length-1]);
        else
            this.setProduct_id(-1);

        this.setExperience_status(bframe.getFrame().get() & 0xff);
        if(this.getExperience_status()!=0){
            experience_detUUID  = new byte[16];
            bframe.getFrame().get(experience_detUUID);
            this.setExperience_detUUID(experience_detUUID);

            experience_expUUID  = new byte[16];
            bframe.getFrame().get(experience_expUUID);
            this.setExperience_expUUID(experience_expUUID);

            byte [] exp_duration = new byte[4];
            bframe.getFrame().get(exp_duration);
            this.setExperience_duration(Utils.byteArrayToInt(exp_duration));
        }else{
            this.setExperience_detUUID(new byte[16]);
            this.setExperience_expUUID(new byte[16]);
            this.setExperience_duration(Utils.byteArrayToInt(new byte[4]));
        }

        this.setCloud_connected(bframe.getFrame().get() & 0xff);
        this.setExp_uuid((Utils.getUUIDFromByte(this.getExperience_expUUID())));
        this.setDet_uuid((Utils.getUUIDFromByte(this.getExperience_detUUID())));
/*
        Log.i("getStatus", "phone_sim_ready: "  + this.getPhone_sim_ready() + "\n" +
                "phone_carrier_len: "           + this.getPhone_carrier_len() + "\n" +
                "phone_carrier: "               + this.getPhone_carrier() + "\n" +
                "phone_level: "                 + this.getPhone_level() + "/4\n" +
                "phone_connected: "             + this.getPhone_connected() + "\n" +
                "phone_network_type: "          + this.getPhone_network_type() + "\n" +
                "phone_roaming: "               + this.getPhone_roaming() + "\n" +
                "wifi_status: "                 + this.getWifi_status() + "\n" +
                "wifi_level: "                  + this.getWifi_level() + "/4" + "\n" +
                "battery_percentage: "          + this.getBattery_percentage() + "%\n" +
                "storage_percentage: "          + this.getStorage_percentage() + "%\n" +
                "wearable_status: "             + this.getWearable_status() + "\n" +
                "wearable_number: "             + this.getWearable_number() + "\n" +
                "wearable_product_id: "         + this.getProduct_id() + "\n" +
                "experience_status: "           + this.getExperience_status() + "\n" +
                "experience_detUUID: "          + this.getDet_uuid() + "\n" +
                "experience_expUUID: "          + this.getExp_uuid() + "\n" +
                "experience_duration: "         + this.getExperience_duration() + "\n" +
                "cloud_connected: "             + this.getCloud_connected() );
*/
        defined=true;
    }

    public void newStatus(byte[] btStatus) {

        BTProtocolFrame bframe = new BTProtocolFrame();
        bframe.getFrame().put(btStatus);

        bframe.getFrame().position(0);

        this.setPhone_sim_ready(bframe.getFrame().get() & 0xff);
        this.setPhone_carrier_len(bframe.getFrame().get() & 0xff);

        phone_carrier = new String();
        for (int i=0;i<this.getPhone_carrier_len();i++)
            phone_carrier += Character.toString((char) (bframe.getFrame().get() & 0xff));

        this.setPhone_level(bframe.getFrame().get() & 0xff);
        this.setPhone_connected(bframe.getFrame().get() & 0xff);
        this.setPhone_network_type(bframe.getFrame().get() & 0xff);
        this.setPhone_roaming(bframe.getFrame().get() & 0xff);
        this.setWifi_status(bframe.getFrame().get() & 0xff);
        this.setWifi_level(bframe.getFrame().get() & 0xff);
        this.setBattery_percentage(bframe.getFrame().get() & 0xff);
        this.setStorage_percentage(bframe.getFrame().get() & 0xff);
        this.setWearable_status(bframe.getFrame().get() & 0xff);
        this.setWearable_number(bframe.getFrame().get() & 0xff);

        wearable_product_id  = new byte[this.getWearable_number() * 13];
        bframe.getFrame().get(wearable_product_id);
        this.setWearable_product_id(wearable_product_id);

        if(this.getWearable_number()!=0)
            this.setProduct_id(this.getWearable_product_id()[this.getWearable_product_id().length-1]);
        else
            this.setProduct_id(-1);

        this.setExperience_status(bframe.getFrame().get() & 0xff);
        if(this.getExperience_status()!=0){
            experience_detUUID  = new byte[16];
            bframe.getFrame().get(experience_detUUID);
            this.setExperience_detUUID(experience_detUUID);

            experience_expUUID  = new byte[16];
            bframe.getFrame().get(experience_expUUID);
            this.setExperience_expUUID(experience_expUUID);

            byte [] exp_duration = new byte[4];
            bframe.getFrame().get(exp_duration);
            this.setExperience_duration(Utils.byteArrayToInt(exp_duration));
        }else{
            this.setExperience_detUUID(new byte[16]);
            this.setExperience_expUUID(new byte[16]);
            this.setExperience_duration(Utils.byteArrayToInt(new byte[4]));
        }

        this.setCloud_connected(bframe.getFrame().get() & 0xff);
        this.setExp_uuid((com.life.services.bind.constants.Utils.getUUIDFromByte(this.getExperience_expUUID())));
        this.setDet_uuid((com.life.services.bind.constants.Utils.getUUIDFromByte(this.getExperience_detUUID())));

        Log.i("getStatus", "phone_sim_ready: "  + this.getPhone_sim_ready() + "\n" +
                "phone_carrier_len: "           + this.getPhone_carrier_len() + "\n" +
                "phone_carrier: "               + this.getPhone_carrier() + "\n" +
                "phone_level: "                 + this.getPhone_level() + "/4\n" +
                "phone_connected: "             + this.getPhone_connected() + "\n" +
                "phone_network_type: "          + this.getPhone_network_type() + "\n" +
                "phone_roaming: "               + this.getPhone_roaming() + "\n" +
                "wifi_status: "                 + this.getWifi_status() + "\n" +
                "wifi_level: "                  + this.getWifi_level() + "/4" + "\n" +
                "battery_percentage: "          + this.getBattery_percentage() + "%\n" +
                "storage_percentage: "          + this.getStorage_percentage() + "%\n" +
                "wearable_status: "             + this.getWearable_status() + "\n" +
                "wearable_number: "             + this.getWearable_number() + "\n" +
                "wearable_product_id: "         + this.getProduct_id() + "\n" +
                "experience_status: "           + this.getExperience_status() + "\n" +
                "experience_detUUID: "          + this.getDet_uuid() + "\n" +
                "experience_expUUID: "          + this.getExp_uuid() + "\n" +
                "experience_duration: "         + this.getExperience_duration() + "\n" +
                "cloud_connected: "             + this.getCloud_connected() );

        defined=true;
    }

    public void setDefined(boolean defined) {
        this.defined = defined;
    }

    public boolean isDefined() {
        return defined;
    }

    public int getPhone_sim_ready() {
        return phone_sim_ready;
    }

    private void setPhone_sim_ready(int phone_sim_ready) {
        this.phone_sim_ready = phone_sim_ready;
    }

    public int getPhone_carrier_len() {
        return phone_carrier_len;
    }

    private void setPhone_carrier_len(int phone_carrier_len) {
        this.phone_carrier_len = phone_carrier_len;
    }

    public String getPhone_carrier() {
        return phone_carrier;
    }

    public void setPhone_carrier(String phone_carrier) {
        this.phone_carrier = phone_carrier;
    }

    public int getPhone_level() {
        return phone_level;
    }

    private void setPhone_level(int phone_level) {
        this.phone_level = phone_level;
    }

    public int getPhone_connected() {
        return phone_connected;
    }

    private void setPhone_connected(int phone_connected) {
        this.phone_connected = phone_connected;
    }

    public int getPhone_network_type() {
        return phone_network_type;
    }

    private void setPhone_network_type(int phone_network_type) {
        this.phone_network_type = phone_network_type;
    }

    public int getPhone_roaming() {
        return phone_roaming;
    }

    private void setPhone_roaming(int phone_roaming) {
        this.phone_roaming = phone_roaming;
    }

    public int getWifi_status() {
        return wifi_status;
    }

    private void setWifi_status(int wifi_status) {
        this.wifi_status = wifi_status;
    }

    public int getWifi_level() {
        return wifi_level;
    }

    private void setWifi_level(int wifi_level) {
        this.wifi_level = wifi_level;
    }

    public int getBattery_percentage() {
        return battery_percentage;
    }

    private void setBattery_percentage(int battery_percentage) {
        this.battery_percentage = battery_percentage;
    }

    public int getStorage_percentage() {
        return storage_percentage;
    }

    private void setStorage_percentage(int storage_percentage) {
        this.storage_percentage = storage_percentage;
    }

    public int getWearable_status() {
        return wearable_status;
    }

    private void setWearable_status(int wearable_status) {
        this.wearable_status = wearable_status;
    }

    public int getWearable_number() {
        return wearable_number;
    }

    private void setWearable_number(int wearable_number) {
        this.wearable_number = wearable_number;
    }

    public byte[] getWearable_product_id() {
        return wearable_product_id;
    }

    public void setWearable_product_id(byte[] wearable_product_id) {
        this.wearable_product_id = wearable_product_id;
    }

    public int getExperience_status() {
        return experience_status;
    }

    private void setExperience_status(int experience_status) {
        this.experience_status = experience_status;
    }

    public byte[] getExperience_detUUID() {
        return experience_detUUID;
    }

    private void setExperience_detUUID(byte[] experience_detUUID) {
        this.experience_detUUID = experience_detUUID;
    }

    public byte[] getExperience_expUUID() {
        return experience_expUUID;
    }

    private void setExperience_expUUID(byte[] experience_expUUID) {
        this.experience_expUUID = experience_expUUID;
    }

    public int getExperience_duration() {
        return experience_duration;
    }

    private void setExperience_duration(int experience_duration) {
        this.experience_duration = experience_duration;
    }

    public int getCloud_connected() {
        return cloud_connected;
    }

    private void setCloud_connected(int cloud_connected) {
        this.cloud_connected = cloud_connected;
    }

    public UUID getExp_uuid() {
        return exp_uuid;
    }

    private void setExp_uuid(UUID exp_uuid) {
        this.exp_uuid = exp_uuid;
    }

    public UUID getDet_uuid() {
        return det_uuid;
    }

    public void setDet_uuid(UUID det_uuid) {
        this.det_uuid = det_uuid;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}