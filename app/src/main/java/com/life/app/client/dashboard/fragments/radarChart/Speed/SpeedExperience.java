package com.life.app.client.dashboard.fragments.radarChart.Speed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Chronometer;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.SpeedActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.constants.Uuid;
import com.life.app.client.dashboard.utils.CountDownAnimation;
import com.life.app.client.dashboard.utils.ExperiencesUtils;
import com.life.app.client.dashboard.utils.HorizontalProgressBar;
import com.life.app.client.dashboard.utils.Percent;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.ArrayList;
import java.util.UUID;

/*
 * Created by peppe on 02/08/2016.
 */
public class SpeedExperience extends Fragment implements CountDownAnimation.CountDownListener{

    RelativeLayout back;

    private static HorizontalProgressBar progressBar;

    View mLeak = null;

    public static Activity mInstance = null;

    Chronometer mChronometer;

    private static FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_speed_experience, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        mLeak = view;
        mInstance = getActivity();

        fragmentManager = getChildFragmentManager();

        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_speed));

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        progressBar  = (HorizontalProgressBar) view.findViewById(R.id.progressBar);

        mChronometer = (Chronometer) view.findViewById(R.id.chronometer1);
        mChronometer.setText(getResources().getString(R.string.prompt_chronometer));

        startCountDown(view);
    }

    private void goBack(){
        SpeedActivity.showTutorial();

        getActivity().finish();

        Intent intent = new Intent(getActivity(), SpeedActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        resetValues();
        ExperiencesUtils.resetBaseExperience(getActivity());
    }

    private void startCountDown(View v){
        CountDownAnimation countDownAnimation = new CountDownAnimation((RelativeLayout) v.findViewById(R.id.rlCountDown),
                (TextView)v.findViewById(R.id.tvCountdown), 3);
        countDownAnimation.setCountDownListener(this);
        (v.findViewById(R.id.rlCountDown)).setOnClickListener(null);
        // Use a set of animations
        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countDownAnimation.setAnimation(animationSet);

        // Customizable start count
        countDownAnimation.setStartCount(3);
        countDownAnimation.start();
    }

    private static int runningprogress = 0;

    @Override
    public void onCountDownEnd(CountDownAnimation animation) {
        //Request.startExperience(getActivity(), UUID.fromString(Uuid.EX_SPEED));
        Utils.startExperience(UUID.fromString(Uuid.EX_SPEED));
        ExperiencesUtils.EXPERIENCE_RUNNING_KEY = Uuid.EX_SPEED;

        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h   = (int)(time /3600000);
                int m = (int)(time - h*3600000)/60000;
                int s= (int)(time - h*3600000- m*60000)/1000 ;
                String hh = h < 10 ? "0"+h: h+"";
                String mm = m < 10 ? "0"+m: m+"";
                String ss = s < 10 ? "0"+s: s+"";
                cArg.setText(mm+":"+ss);
            }
        });
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
    }

    public static void updateProgress(final String [] data){
        if(runningprogress<=100)
            progressBar.setCurrentValue(new Percent(runningprogress));
        runningprogress+=1;
    }

    public static void onExperienceFinished(Activity activity, final String [] data){

        runningprogress= 0;

        saveToSharePref(activity, data);
    }

    private static void saveToSharePref(Activity activity, String [] data){

        ArrayList<Float> synthesis = new ArrayList<>();
        synthesis.add(Float.parseFloat(data[2]));   // score
        synthesis.add(Float.parseFloat(data[3]));   // speed

        SharedPrefDataHandler.addNewSynthesis(activity, RadarChartConst.SPEED_KEY, synthesis);
        SharedPrefDataHandler.addBest(activity, RadarChartConst.SPEED_KEY, synthesis);

        SpeedActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        Bundle b = new Bundle();
                        b.putInt("position", 1);
                        Fragment f = new SpeedResults();
                        f.setArguments(b);
                        fragmentManager.beginTransaction().
                                replace(R.id.speed_placeholder, f, f.getClass().getName()).
                                addToBackStack(f.getClass().getName()).
                                commit();
                    }
                }, 1000);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        SpeedActivity.hideTutorial();
    }

    private void resetValues(){
        if(mChronometer!=null) {
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.stop();
            mChronometer = null;
        }
        runningprogress = 0;
    }
}