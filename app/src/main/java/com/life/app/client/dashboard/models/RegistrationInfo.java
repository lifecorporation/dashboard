package com.life.app.client.dashboard.models;

/**
 * Created by Jonathan Muller on 4/21/16.
 */
public class RegistrationInfo
{

    public String username;
    public String imei;
    public int userId;
    public String mac;
    public String pass;
}
