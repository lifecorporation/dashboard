package com.life.app.client.dashboard.fragments.radarChart.Balance;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.BalanceActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 26/9/2016.
 */
public class BalanceResults extends Fragment {

    View mLeak = null;
    TextView tvBestTime, tvSynthesis;
    RelativeLayout back;

    BarChart mChart;
    ScatterChart scatterChart;

    ArrayList<BarEntry> yVals1 = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_balance_results, container, false);
    }


    ArrayList<ArrayList<Float>> set1 = null;
    ArrayList<ArrayList<Float>> set2 = null;
    ArrayList<ArrayList<Float>> set3 = null;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        Bundle bundle = getArguments();
        int position = bundle.getInt("position");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        BalanceActivity.hideTutorial();

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_balance));

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        mChart       = (BarChart) view.findViewById(R.id.barChart);
        tvBestTime   = (TextView) view.findViewById(R.id.tvBestTime);
        tvSynthesis  = (TextView) view.findViewById(R.id.tvSynthesis);
        scatterChart = (ScatterChart) view.findViewById(R.id.scatterChart);

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.BALANCE_KEY + "_" + position);
        if(synthesis!=null){

            String placeholderBest = String.format(Locale.US, "%.1f", synthesis.get(1));
            String placeholderSynt = String.format(Locale.US, "%.1f", synthesis.get(0));

            tvBestTime.setText(placeholderBest);
            tvSynthesis.setText(placeholderSynt);
        }

        tvSynthesis.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);

        BarChartSetup.setBarChart(mChart, true, "Centroid position", 0f, 25f);
        initiateScatterChart(scatterChart);

        int len1 = 0;
        int len2 = 0;
        int len3 = 0;

        if(synthesis!=null){
            if(synthesis.size()>=3)
                len1 = Math.round(synthesis.get(2));
            if(synthesis.size()>=4)
                len2 = Math.round(synthesis.get(3));
            if(synthesis.size()>=5)
                len3 = Math.round(synthesis.get(4));
        }

        set1 = new ArrayList<>();
        set2 = new ArrayList<>();
        set3 = new ArrayList<>();

        for(int i=0;i<len1;i++)
            set1.add(SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.BALANCE_KEY + "_" + position + "_TRY_1_SET_" + (i+1)));

        for(int i=0;i<len2;i++)
            set2.add(SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.BALANCE_KEY + "_" + position + "_TRY_2_SET_" + (i+1)));

        for(int i=0;i<len3;i++)
            set3.add(SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.BALANCE_KEY + "_" + position + "_TRY_3_SET_" + (i+1)));

        addDataToChart(mChart);
        addDataToScatterChart();
    }

    @Override
    public void onResume(){
        super.onResume();
        BalanceActivity.hideTutorial();
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), BalanceActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private void initiateScatterChart(ScatterChart sChart){
        //sChart.setPinchZoom(true);
        sChart.setDrawGridBackground(false);
        sChart.setScaleEnabled(true);
        sChart.getLegend().setEnabled(false);

        Description descr = new Description();
        descr.setText("");
        descr.setTextColor(GRAPHS_COLOR_WHITE);
        descr.setTextSize(DESCRIPTION_TEXT_SIZE);
        mChart.setDescription(descr);

        XAxis xAxis = sChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(6);
        xAxis.setDrawLabels(true);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis leftAxis = sChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(0f);
        leftAxis.setAxisMinimum(-50f); // this replaces setStartAtZero(true)
        leftAxis.setAxisMaximum(50f);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = sChart.getAxisRight();
        rightAxis.setLabelCount(6, false);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        rightAxis.setSpaceTop(0f);
        rightAxis.setAxisMinimum(-50f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMaximum(50f);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        Legend l = sChart.getLegend();
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART_INSIDE);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(0f);
        l.setTextSize(11f);
        l.setTextColor(GRAPHS_COLOR_WHITE);
        l.setXEntrySpace(1f);
        l.setEnabled(false);

        sChart.getLegend().setEnabled(false);
        sChart.getXAxis().setAxisMinimum(-50f);
        sChart.getXAxis().setAxisMaximum(50f);
    }

    private void addDataToChart(BarChart chart) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(4);

        if (set1 != null && set1.size()>1) yVals1.add(new BarEntry(1, set1.get(set1.size() - 1).get(0)));
        if (set2 != null && set2.size()>1) yVals1.add(new BarEntry(2, set2.get(set2.size() - 1).get(0)));
        if (set3 != null && set3.size()>1) yVals1.add(new BarEntry(3, set3.get(set3.size() - 1).get(0)));


        BarDataSet set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        set1.setHighlightEnabled(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(15f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.7f);

        chart.setData(data);
        chart.invalidate();
    }

    private void addDataToScatterChart() {
        ArrayList<Entry> values = new ArrayList<>();
        for(int i=0;i<set1.size();i++)
            values.add(new Entry(set1.get(i).get(1), set1.get(i).get(2)));
        for(int i=0;i<set2.size();i++)
            values.add(new Entry(set2.get(i).get(1), set2.get(i).get(2)));
        for(int i=0;i<set3.size();i++)
            values.add(new Entry(set3.get(i).get(1), set3.get(i).get(2)));

        Collections.sort(values, new EntryXComparator());

        ScatterDataSet scatterSet1 = new ScatterDataSet(values, "DS 1");
        scatterSet1.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        scatterSet1.setColor(GRAPHS_COLOR_BLUE_LIGHT);
        scatterSet1.setScatterShapeSize(8f);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<>();
        dataSets.add(scatterSet1);

        ScatterData data = new ScatterData(dataSets);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setValueTextSize(10f);

        scatterChart.setData(data);
        BalanceActivity.getInstance().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                scatterChart.invalidate();
            }
        });
    }
}
