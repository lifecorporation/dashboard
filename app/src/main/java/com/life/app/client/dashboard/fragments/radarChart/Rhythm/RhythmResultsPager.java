package com.life.app.client.dashboard.fragments.radarChart.Rhythm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/*
 * Created by chkalog on 28/9/2016.
 */
class RhythmResultsPager extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 2;

    RhythmResultsPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {

        switch (arg0) {
            case 0:
                return new LeftFragment();
            case 1:
                return new RightFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
