package com.life.app.client.dashboard.fragments.radarChart.AnaerobicCapacity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.AnaerobicCapacityActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import java.util.ArrayList;
import java.util.Collections;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 19/10/2016.
 */
public class AnaerobicCapacityResults extends Fragment {

    LineChart mChart;
    RelativeLayout back;

    View mLeak = null;
    int position = 0;

    private ArrayList<Entry> entries = null;

    private static ArrayList<ArrayList<Float>> tries = null;
    private static ArrayList<Float> triesResults = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_anaerobic_capacity_results, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        AnaerobicCapacityActivity.hideTutorial();
    }


    @Override
    public void onResume(){
        super.onResume();

        new Handler().postDelayed(new Runnable(){

            @Override
            public void run() {
                AnaerobicCapacityActivity.hideTutorial();
            }
        },50);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        view.findViewById(R.id.ibInfoExp).setOnClickListener(null);
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_anaerobic_capacity));

        back    = (RelativeLayout) view.findViewById(R.id.back);
        mChart  = (LineChart) view.findViewById(R.id.lineChart);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        Bundle bundle = getArguments();
        position = bundle.getInt("position");
        //Log.i("AnaerobicResults","scelta position " + position);

        loadChartFromprefs(position);
        initiateChart();
        addDataToChart();
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), AnaerobicCapacityActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    private void initiateChart(){
        mChart.clear();
        mChart.animateY(2000);

        Paint paint = mChart.getRenderer().getPaintRender();
        LinearGradient linGrad = new LinearGradient(0, 0, 0, 200,
                GRAPHS_COLOR_BLUE_LIGHT,
                Color.parseColor("#BDBDBD"),
                Shader.TileMode.REPEAT);
        paint.setShader(linGrad);
        mChart.getRenderer().getPaintRender().setShader(linGrad);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(4);
        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(true);
        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(true);
        mChart.getLegend().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(6);
        xAxis.setDrawLabels(true);
        xAxis.setTextColor(GRAPHS_COLOR_WHITE);
        xAxis.setAxisMaxValue(11f);
        xAxis.setAxisMinValue(0);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(0f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        leftAxis.setAxisMaxValue(200f);
        leftAxis.setTextColor(GRAPHS_COLOR_WHITE);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setSpaceTop(0f);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMaxValue(200f);
        rightAxis.setTextColor(GRAPHS_COLOR_WHITE);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.LEFT_OF_CHART_INSIDE);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(0f);
        l.setTextSize(12f);
        l.setTextColor(GRAPHS_COLOR_WHITE);
        l.setXEntrySpace(1f);
        l.setEnabled(false);
    }

    private void addDataToChart() {
        if(entries == null)
            entries = new ArrayList<>();

        for(int i=0;i<tries.size();i++)
            entries.add(new Entry(tries.get(i).get(0), tries.get(i).get(1)));
        Collections.sort(entries, new EntryXComparator());

        LineDataSet dataset = new LineDataSet(entries, "");
        dataset.setCircleHoleRadius(10f);
        dataset.setCircleColor(GRAPHS_COLOR_WHITE);
        dataset.setColor(GRAPHS_COLOR_WHITE);
        dataset.setHighlightEnabled(false);
        dataset.setValueTextColor(GRAPHS_COLOR_WHITE);

        LineData data = new LineData(dataset);
        data.setValueTextSize(12f);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);

        mChart.setData(data);
        AnaerobicCapacityActivity.getInstance().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                mChart.invalidate();
            }
        });
    }

    private void loadChartFromprefs(int position){

        if(tries==null)
            tries = new ArrayList<>();

        if (triesResults == null){
            triesResults = new ArrayList<>();
        }

        for (int i = 0; i < 10; i++){
            // load tries values
            tries.add(SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.ANAEROBIC_CAPACITY_KEY + "_" + position + "_TRY_" + (i+1)));
            //Log.i("AnaerobicResults"," results tries size: " + tries.size() + " tries: " + tries.get(0));
            //float heartRate = tries.get(i).get(0);
            //float speed     = tries.get(i).get(1);
            //Log.i("AnaerobicResults"," results speed: " + speed +"    heartRate: " + heartRate );
        }
    }
}
