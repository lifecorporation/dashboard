package com.life.app.client.dashboard.fragments.radarChart.Explosiveness;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.abilities.ExplosivenessActivity;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.graphs.BarChartSetup;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;
import com.life.app.client.dashboard.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by peppe on 13/10/2016.
 */
public class ExplosivenessResults extends Fragment {

    View mLeak = null;
    RelativeLayout back;

    BarChart mChart;

    ArrayList<BarEntry> yVals1 = new ArrayList<>();

    TextView tvBestHeight, tvSynthesis;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_explosiveness_results, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLeak = view;

        Bundle bundle = getArguments();
        int position = bundle.getInt("position");

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if( i == KeyEvent.KEYCODE_BACK ) {
                    goBack();
                    return true;
                }
                return false;
            }
        });

        tvBestHeight = (TextView) view.findViewById(R.id.tvBestHeight);
        tvSynthesis  = (TextView) view.findViewById(R.id.tvSynthesis);

        ExplosivenessActivity.hideTutorial();
        ((TextView)view.findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_explosivness));

        back = (RelativeLayout) view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        (view.findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        mChart = (BarChart) view.findViewById(R.id.barChart);

        tvSynthesis.setTypeface(Utils.getKnockoutFont(getActivity()), Typeface.BOLD);

        BarChartSetup.setBarChart(mChart, false, "", 0f, 100f);
        updateTextViews(position);

        addDataToChart(mChart, 1, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.EXPLOSIVENESS_KEY + "_" + position + "_TRY_1_SET"));
        addDataToChart(mChart, 2, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.EXPLOSIVENESS_KEY + "_" + position + "_TRY_2_SET"));
        addDataToChart(mChart, 3, SharedPrefDataHandler.getSets(getActivity(), RadarChartConst.EXPLOSIVENESS_KEY + "_" + position + "_TRY_3_SET"));
    }

    private void goBack() {
        getActivity().finish();

        Intent intent = new Intent(getActivity(), ExplosivenessActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        ExplosivenessActivity.hideTutorial();
    }

    private void addDataToChart(BarChart chart, int x, ArrayList<Float> results) {
        float start = 0f;

        chart.getXAxis().setAxisMinimum(start);
        chart.getXAxis().setAxisMaximum(4);

        // aggiungo i valori alla lista degli elementi da visualizzare
        for (int i = results.size()-1; i >=0 ; i--) {
            yVals1.add(new BarEntry(x, results.get(i)));
        }

        BarDataSet set1 = new BarDataSet(yVals1, "");
        set1.setColor(GRAPHS_COLOR_BLUE_LIGHT);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setDrawValues(true);
        data.setHighlightEnabled(false);
        data.setValueTextSize(VALUE_TEXT_SIZE);
        data.setValueTextColor(GRAPHS_COLOR_WHITE);
        data.setBarWidth(0.6f);

        chart.setData(data);
        chart.invalidate();
    }

    private void updateTextViews(int position){
        ArrayList<Float> synthesisArrayList = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.EXPLOSIVENESS_KEY + "_" + position );

        if(synthesisArrayList!=null) {

            float height = synthesisArrayList.get(1);
            int synthesis = Math.round(synthesisArrayList.get(0));

            String placeholder1 = String.format(Locale.US, "%.1f", height) + " cm";
            String placeholder2 = String.valueOf(synthesis);

            tvBestHeight.setText(placeholder1);
            tvSynthesis.setText(placeholder2);
        }
    }
}
