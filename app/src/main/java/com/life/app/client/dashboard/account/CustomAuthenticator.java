package com.life.app.client.dashboard.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.life.app.client.dashboard.activities.BackPressActivity;


/**
 * Created by peppe on 28/06/2016.
 */
public class CustomAuthenticator extends AbstractAccountAuthenticator {

    Context mContext= null;

    public CustomAuthenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse accountAuthenticatorResponse, String s, String s2, String[] strings, Bundle bundle) throws NetworkErrorException {

        Log.i("CustomAuthenticator","chiamato l'add");

        AccountManager accountManager = AccountManager.get(mContext);
        Account[] arrayAccount= accountManager.getAccountsByType("com.life.x10y.account.X10YACCOUNT");
        if (arrayAccount.length > 0){
            //Toast.makeText(mContext,"Puoi avere solo un account",Toast.LENGTH_SHORT).show();
            Log.i("CustomAuthenticator","Puoi avere solo un account");
            Handler mainThreadHandler = new Handler(Looper.getMainLooper());
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext,"Puoi avere solo un account",Toast.LENGTH_SHORT).show();
                }
            };
            mainThreadHandler.post(r);

            // avvio una mia activity che non farà altro che premere il pulsante back
            // altrimenti mi rimane un'activity vuota sopra ogni cosa
            Intent i = new Intent(mContext, BackPressActivity.class);
            mContext.startActivity(i);

        }

        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse, String s) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, Bundle bundle) throws NetworkErrorException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String s, Bundle bundle) throws NetworkErrorException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getAuthTokenLabel(String s) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String s, Bundle bundle) throws NetworkErrorException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String[] strings) throws NetworkErrorException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}