package com.life.app.client.dashboard.utils;

/*
 * Created by chara on 30-Mar-17.
 */

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;

import com.life.app.client.dashboard.constants.Consts;

public class CustomAnimation {

    private static boolean isBodyEnabled = false;

    public static void fadeInViewAnimation(final Context mContext, final View view, long delay){
        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                android.view.animation.Animation mLoadAnimation = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in);
                mLoadAnimation.setDuration(Consts.DURATION_FADE_IN_OUT);
                mLoadAnimation.setFillAfter(true);
                view.startAnimation(mLoadAnimation);
                view.setVisibility(View.VISIBLE);
            }
        };
        mainHandler.postDelayed(myRunnable, delay);
    }

    public static void fadeOutViewAnimation(final Context mContext, final View view, long delay){
        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                android.view.animation.Animation mLoadAnimation = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_out);
                mLoadAnimation.setDuration(Consts.DURATION_FADE_IN_OUT);
                mLoadAnimation.setFillAfter(true);
                view.startAnimation(mLoadAnimation);
                view.setVisibility(View.INVISIBLE);
            }
        };
        mainHandler.postDelayed(myRunnable, delay);
    }

    public static void enableBody(Context mContext, final View view){

        if(isBodyEnabled)
            return;

        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AlphaAnimation animation = new AlphaAnimation(0.2f, 1.0f);
                animation.setDuration(Consts.DURATION_FADE_IN_OUT);
                animation.setFillAfter(true);
                view.startAnimation(animation);

                isBodyEnabled = true;

            }
        };
        mainHandler.postDelayed(myRunnable, 800);

    }

    public static void disableBody(Context mContext, final View view, final long duration, final long delay){
        // Get a handler that can be used to post to the main thread
        Handler mainHandler = new Handler(mContext.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                AlphaAnimation animation = new AlphaAnimation(1.0f, 0.2f);
                animation.setDuration(duration);
                animation.setFillEnabled(true);
                animation.setFillAfter(true);
                view.startAnimation(animation);

                isBodyEnabled = false;

            }
        };
        mainHandler.postDelayed(myRunnable, delay);
    }
}
