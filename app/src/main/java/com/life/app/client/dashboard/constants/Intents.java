package com.life.app.client.dashboard.constants;

/**
 * Created by peppe on 25/08/2016.
 */
public class Intents {

    public static String intentNewCall                     ="com.app.x10y.telephone.NEW_INCOMING_CALL";
    public static String intentHangUp                      ="com.app.x10y.telephone.HANG_UP";
    public static String intentPlaceCall                   ="com.app.x10y.telephone.PLACE_CALL";
    public static String intentCallStatus                  ="com.app.x10y.telephone.CALL_STATUS";
    public static String intentActionDisconnected          ="com.app.x10y.telephone.DISCONNECTED";
    public static String intentStatus                      ="com.app.x10y.telephone.STATUS";
    public static String intentExperienceStatus            ="com.app.x10y.telephone.EXPERIENCE_STATUS";
    public static String intentSnHardReset                 ="com.app.x10y.telephone.SN_HARD_RESET";
    public static String intentShirtSNStatus               ="com.life.services.assets.SHIRT_SENSOR_NETWORK";

}
