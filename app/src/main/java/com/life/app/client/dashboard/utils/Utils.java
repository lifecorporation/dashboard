package com.life.app.client.dashboard.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.design.widget.BottomSheetBehavior;
import android.util.Log;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.protocols.BTProtocolFrame;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.services.bind.constants.SyncConsts;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.UUID;

/*
 * Created by chkalog on 7/9/2016.
 */
public class Utils {

    public static boolean isEmpty(String str){
        return (str != null && !str.trim().isEmpty());
    }

    public static String getMonthName(Context ctx, int month){
        String months[] = ctx.getResources().getStringArray(R.array.months);
        return months[month];
    }

    public static Typeface getIONIconFont(Context _context){
        return Typeface.createFromAsset(_context.getAssets(),  "fonts/ionicons.ttf");
    }

    public static Typeface getKnockoutFont(Context _context){
        return Typeface.createFromAsset(_context.getAssets(),  "fonts/knockout_liteweight_regular.otf");
    }

    private static byte[] getUUIDAsByte(UUID uuid){
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.order(ByteOrder.BIG_ENDIAN);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    /**
     * Converts the array of bytes into a UUID and returns the generated UUID object
     *
     * @param   uuid    an array of bytes
     * @return  the converted UUID object
     */
    public static UUID getUUIDFromByte(byte[] uuid){
        ByteBuffer bb = ByteBuffer.wrap(uuid, 0, 16);
        bb.order(ByteOrder.BIG_ENDIAN);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }

    /**
     * Converts the array of bytes into an int and returns the generated int
     *
     * @param   b    an array of bytes
     * @return  the converted int
     */
    public static int byteArrayToInt(byte[] b){
        return   b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }

    /**
     * Converts an int to an array of bytes and returns the exported array
     *
     * @param   a    an int
     * @return  the converted byte array
     */
    public static byte[] intToByteArray(int a){
        return new byte[] {
                (byte) ((a >> 24) & 0xFF),
                (byte) ((a >> 16) & 0xFF),
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }

    public static void setPeekHeight(Activity activity, BottomSheetBehavior behavior){
        double density = activity.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            //"xxxhdpi"
            behavior.setPeekHeight(160);
        }else if (density >= 3.0 && density < 4.0) {
            //xxhdpi
            behavior.setPeekHeight(110);
        }else if (density >= 2.0) {
            //xhdpi
            behavior.setPeekHeight(80);
        }else if (density >= 1.5 && density < 2.0) {
            //hdpi
            behavior.setPeekHeight(55);
        }else if (density >= 1.0 && density < 1.5) {
            //mdpi
            behavior.setPeekHeight(50);
        }else{
            //ldpi
            behavior.setPeekHeight(40);
        }
    }

    public static int getLinearVertical(Context context){
        double density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            //"xxxhdpi"
            return 800;
        }else if (density >= 3.0 && density < 4.0) {
            //xxhdpi
            return 650;
        }else if (density >= 2.0) {
            //xhdpi
            return 590;
        }else if (density >= 1.5 && density < 2.0) {
            //hdpi
            return 520;
        }else if (density >= 1.0 && density < 1.5) {
            //mdpi
            return 450;
        }else{
            //ldpi
            return 380;
        }
    }


    private static int getLineHorizontal(Context context){
        double density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            //"xxxhdpi"
            return 360;
        }else if (density >= 3.0 && density < 4.0) {
            //xxhdpi
            return 320;
        }else if (density >= 2.0) {
            //xhdpi
            return 280;
        }else if (density >= 1.5 && density < 2.0) {
            //hdpi
            return 240;
        }else if (density >= 1.0 && density < 1.5) {
            //mdpi
            return 200;
        }else{
            //ldpi
            return 180;
        }
    }

    public static void LogDensity(Context context){
        double density = context.getResources().getDisplayMetrics().density;
        if (Log.isLoggable("LogDensity", Log.INFO)) {
            Log.i("LogDensity", density + "");
        }

        Configuration config = context.getResources().getConfiguration();
        if (Log.isLoggable("screenWidth", Log.INFO)) {
            Log.i("screenWidth", config.smallestScreenWidthDp + "");
        }
    }

    /**
     * Constructs the frame object of the message
     *
     * @param   cc byte in hex format (0x21 start, 0x22 stop)
     * @param   payload an array of bytes that contains the experience
     * @return  the frame of the message, converted into an array of bytes
     * @see     UUID
     * @see     ByteBuffer
     */
    private static byte[] prepareRequest(int cc, byte[] payload) {
        ByteBuffer frame;
        if (payload != null) frame = ByteBuffer.allocate(8 + payload.length);
        else frame = ByteBuffer.allocate(8);

        frame.order(ByteOrder.BIG_ENDIAN);
        frame.put((byte) 0x02);
        frame.put((byte) 0x00);
        frame.put((byte) cc);
        if (payload != null) {
            frame.putShort((short) payload.length);
            frame.put(payload);
        } else {
            frame.putShort((byte) 0x00);
        }
        frame.putShort((short) 0);          // crc16
        frame.put((byte) 0x03);             // ETX
        return frame.array();
    }

    public static void scanWifi(){
        byte[] data = prepareRequest(0x30, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    /**
     * Switch wifi on
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void switchWifiON(){
        BTConnectionManagerService.WIFI_SWITCHED = 0x01;
        byte payload[] = new byte[1];
        payload[0] = 1;
        byte [] data = prepareRequest(BTProtocolFrame.CC_SWITCH_WIFI, payload);
        BTConnectionManagerService.send(data);
    }

    /**
     * Switch wifi off
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void switchWifiOFF(){
        BTConnectionManagerService.WIFI_SWITCHED = 0x02;
        byte payload[] = new byte[1];
        payload[0] = 0;
        byte [] data = prepareRequest(BTProtocolFrame.CC_SWITCH_WIFI, payload);
        BTConnectionManagerService.send(data);
    }

    /**
     * Switch wifi off
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void syncStatus(){
        byte [] data = prepareRequest(BTProtocolFrame.CC_SYNC_STATUS, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    /**
     * Start the data upload to cloud
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void startSync(){
        byte [] data = prepareRequest(BTProtocolFrame.CC_START_SYNC, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    /**
     * Start the data upload to cloud
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void stopSync(){
        byte [] data = prepareRequest(BTProtocolFrame.CC_STOP_SYNC, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    /**
     * Configure synchronization
     *
     * @param   automatic set if procedure upload data file automatically
     * @param   connectionType sype of connection active when upload data files
     * @param   powerStatus power supply status to upload data files
     * @param   batteryLowerLimit when in battery mode set the inferior limit
     * @param   timeout seconds to wait before upload
     * @see     SyncConsts
     */
    public static void configureSync(byte automatic, byte connectionType, byte powerStatus, int batteryLowerLimit, int timeout) {
        if(batteryLowerLimit >= 10 && batteryLowerLimit <= 100) {
            ByteBuffer payload = ByteBuffer.allocate(8);
            payload.order(ByteOrder.BIG_ENDIAN);
            payload.put(automatic);
            payload.put(connectionType);
            payload.put(powerStatus);
            payload.put((byte)(batteryLowerLimit & 255));
            payload.putInt(timeout);
            byte[] data = prepareRequest(BTProtocolFrame.CC_GET_SYNC_CONFIGURE, payload.array());
            BTConnectionManagerService.send(data);
        }
    }


    /**
     * Get datetime
     *
     * @see     com.life.services.bind.constants.Consts
     */
    public static void getDatetime(){
        byte [] data = prepareRequest(BTProtocolFrame.CC_GET_DATETIME, null);
        BTConnectionManagerService.send(data);
    }

    /**
     * Set datetime
     *
     * @param   automatic datetime detection
     * @param   datetime ASCII char representing the date and time using ISO 8601, always mandatory
     *                   even if automatic is false
     * @see     com.life.services.bind.constants.Consts
     */
    public static void setDatetime(byte automatic, String datetime) {
        int payloadLen = datetime.getBytes().length + 2;
        byte[] payload = new byte[payloadLen];
        payload[0] = automatic;
        payload[1] = Integer.valueOf(datetime.getBytes().length).byteValue();

        for(int data = 2; data < payloadLen; ++data) {
            payload[data] = datetime.getBytes()[data - 2];
        }

        byte[] var6 = prepareRequest(BTProtocolFrame.CC_SET_DATETIME, payload);
        BTConnectionManagerService.send(var6);
    }

    /**
     * Connect to a wifi network
     *
     * @param   ssid ssid of the Wifi
     * @param   password password of the Wifi
     * @see     com.life.services.bind.constants.Consts
     */
    public static void connectToWifi(String ssid, String password){
        byte[] payload = new byte[0];

        payload = addElement(payload, (byte)ssid.length());
        for(byte byte1 : ssid.getBytes())
            payload = addElement(payload, byte1);

        payload = addElement(payload, (byte)password.length());
        for(byte byte1 : password.getBytes())
            payload = addElement(payload, byte1);

        byte [] data = prepareRequest(BTProtocolFrame.CC_CONNECT_WIFI, payload);
        BTConnectionManagerService.send(data);
    }

    private static byte[] addElement(byte[] org, byte added) {
        byte[] result = Arrays.copyOf(org, org.length +1);
        result[org.length] = added;
        return result;
    }

    public static void disconnectFromWifi(){
        byte[] data = prepareRequest(BTProtocolFrame.CC_DISCONNECT_WIFI, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    public static void requestWifiStatus(){
        byte[] data = prepareRequest(BTProtocolFrame.CC_GET_WIFISTATUS, new byte[0]);
        BTConnectionManagerService.send(data);
    }

    public static void startExperience(UUID uuid){
        byte[] data = prepareRequest(Consts.CC_START_EXPERIENCE , Utils.getUUIDAsByte(uuid));
        BTConnectionManagerService.send(data);
    }

    public static void stopExperience(){
        //Log.d("stopExperience", "chiamato stopExperience");
        byte[] data = prepareRequest(Consts.CC_STOP_EXPERIENCE , null);
        BTConnectionManagerService.send(data);
    }

    public static void getAsset(){
        //Log.i("onStatusReceived", "invio richiesta stato sensori");

        byte[] data = prepareRequest(Consts.CC_GET_ASSETS, null);
        BTConnectionManagerService.send(data);
    }
}
