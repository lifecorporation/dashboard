package com.life.app.client.dashboard.fragments.radarChart.Agility;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.constants.RadarChartConst;
import com.life.app.client.dashboard.utils.SharedPrefDataHandler;

import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.StackedBarModel;

import java.util.ArrayList;

import static com.life.app.client.dashboard.constants.Consts.*;

/*
 * Created by chkalog on 28/9/2016.
 */
public class LeftFragment extends Fragment {

    private static StackedBarChart mChart1, mChart2, mChart3, mChart4;
    private static TextView tvStackedbarchart2, tvStackedbarchart4;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agility_results_left, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mChart1 = (StackedBarChart) view.findViewById(R.id.stackedbarchart1);
        mChart2 = (StackedBarChart) view.findViewById(R.id.stackedbarchart2);
        mChart3 = (StackedBarChart) view.findViewById(R.id.stackedbarchart3);
        mChart4 = (StackedBarChart) view.findViewById(R.id.stackedbarchart4);

        tvStackedbarchart2 = (TextView) view.findViewById(R.id.tvStackedbarchart2);
        tvStackedbarchart4 = (TextView) view.findViewById(R.id.tvStackedbarchart4);

        showHistoryChart();
    }

    public void showHistoryChart() {
        // setup barchart
        if(mChart1!=null && mChart2!=null) {
            displayHistoryValues();
        }
    }

    private void displayHistoryValues() {

        float lefthand = 0f;
        float righthand = 0f;
        float totalvertical = 0f;
        float medio = 0f;
        float post = 0f;

        ArrayList<Float> synthesis = SharedPrefDataHandler.getSynthesis(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position);

        if(synthesis!=null) {

            int totaljumps = Math.round(synthesis.get(0));

            for (int i = 0; i < totaljumps; i++) {

                ArrayList<Float> jumps = SharedPrefDataHandler.getTry(getActivity(), RadarChartConst.AGILITY_KEY + "_" + AgilityResults.position + "_TRY_" + (i + 1));
                //Log.i("jumps", "size : " + jumps.size());
                if (jumps != null && jumps.size() > 7) {
                    StackedBarModel s2 = new StackedBarModel("");
                    s2.addBar(new BarModel(jumps.get(3), GRAPHS_COLOR_BLUE_LIGHT));
                    s2.addBar(new BarModel(jumps.get(4), GRAPHS_COLOR_BLUE_DARK));
                    mChart2.addBar(s2);
                    mChart2.invalidate();

                    StackedBarModel s4 = new StackedBarModel("");
                    s4.addBar(new BarModel(jumps.get(6), GRAPHS_COLOR_GREEN));  // vertical
                    s4.addBar(new BarModel(jumps.get(5), GRAPHS_COLOR_ORANGE));  // medio
                    s4.addBar(new BarModel(jumps.get(7), GRAPHS_COLOR_RED));  // antero
                    mChart4.addBar(s4);

                    lefthand += jumps.get(3);
                    righthand += jumps.get(4);

                    totalvertical += jumps.get(6);
                    medio += jumps.get(5);
                    post += jumps.get(7);

                } else {
                    mChart2.setVisibility(View.GONE);
                    mChart4.setVisibility(View.GONE);
                    tvStackedbarchart2.setVisibility(View.VISIBLE);
                    tvStackedbarchart4.setVisibility(View.VISIBLE);

                    break;
                }
            }

            float leftPercentage = (int)(lefthand / totaljumps);
            float rightPercentage = (int)(righthand / totaljumps);

            float verticalPercentage = (int) (totalvertical / totaljumps);
            float medioPercentage = (int) (medio / totaljumps);
            float postPercentage = (int) (post / totaljumps);

            StackedBarModel s1 = new StackedBarModel("");
            s1.addBar(new BarModel(leftPercentage, GRAPHS_COLOR_BLUE_LIGHT));
            s1.addBar(new BarModel(rightPercentage, GRAPHS_COLOR_BLUE_DARK));
            mChart1.addBar(s1);

            StackedBarModel s3 = new StackedBarModel("");
            s3.addBar(new BarModel(verticalPercentage, GRAPHS_COLOR_GREEN));  // vertical
            s3.addBar(new BarModel(medioPercentage, GRAPHS_COLOR_ORANGE));  // medio
            s3.addBar(new BarModel(postPercentage, GRAPHS_COLOR_RED));  // antero
            mChart3.addBar(s3);

            mChart1.startAnimation();
            mChart2.startAnimation();
            mChart3.startAnimation();
            mChart4.startAnimation();

        }
    }
}
