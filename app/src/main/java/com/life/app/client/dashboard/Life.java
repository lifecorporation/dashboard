package com.life.app.client.dashboard;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import com.google.gson.Gson;
import com.life.app.client.dashboard.objects.User;

/*
 * Created by chara on 30-Jan-17.
 */

public class Life extends Application implements
        Application.ActivityLifecycleCallbacks{

    private static final String SAVED_PREF = "SAVED_PREF";

    public static boolean isAppForeground = false;

    private static Life singleton;

    private com.life.services.bind.status.Status status = null;

    private User user = null;

    public static Life getInstance(){
        return singleton;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }

    public void setStatus(com.life.services.bind.status.Status status){
        this.status = status;
    }

    public com.life.services.bind.status.Status getStatus(){
        return this.status;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        //Log.i("inApplication","onActivityCreated "  + activity.getClass().getName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        //Log.i("inApplication","onActivityStarted " + activity.getClass().getName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        isAppForeground = true;
        //Log.i("inApplication","onActivityResumed " + activity.getClass().getName());
        //Log.i("inApplication","onActivityResumed " + isAppForeground);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isAppForeground = false;
        //Log.i("inApplication","onActivityPaused " + activity.getClass().getName());
        //Log.i("inApplication","onActivityPaused " + isAppForeground);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        //Log.i("inApplication","onActivityStopped "  + activity.getClass().getName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        //Log.i("inApplication","onActivityDestroyed " + activity.getClass().getName());
    }

    public User getUser() {

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(SAVED_PREF, Context.MODE_PRIVATE);
        String jsonInfo = preferences.getString("User", null);

        return new Gson().fromJson(jsonInfo, User.class);

    }

    public void setUser(User user) {
        this.user = user;

        SharedPreferences.Editor prefsEditor = getApplicationContext().getSharedPreferences(SAVED_PREF, Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(this.user); // myObject - instance of MyObject
        prefsEditor.putString("User", json);
        prefsEditor.commit();
    }
}