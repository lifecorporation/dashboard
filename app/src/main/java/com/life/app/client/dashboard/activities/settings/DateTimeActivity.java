package com.life.app.client.dashboard.activities.settings;

/*
 * Created by chara on 23-Feb-17.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.dashboard.R;
import com.life.app.client.dashboard.activities.MyFragmentActivity;
import com.life.app.client.dashboard.constants.Consts;
import com.life.app.client.dashboard.services.BTConnectionManagerService;
import com.life.app.client.dashboard.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

public class DateTimeActivity extends MyFragmentActivity {

    private static int isautomatic = 1;

    static TextInputLayout tvDate, tvTime;

    private static int year    = 0;
    private static int month   = 0;
    private static int day     = 0;
    private static int hours   = 0;
    private static int minutes = 0;
    private static int seconds = 0;

    SwitchCompat switchView;
    TextClock clock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_settings_datatime);

        ((TextView)findViewById(R.id.tvTitle)).setText(getResources().getString(R.string.title_date_and_time));

        (findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.ibBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDate = (TextInputLayout) findViewById(R.id.tvDate);
        tvTime = (TextInputLayout) findViewById(R.id.tvTime);

        switchView = (SwitchCompat) findViewById(R.id.switchView);
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    isautomatic = 1;
                }else{
                    isautomatic = 0;
                }
            }
        });

        clock = (TextClock) findViewById(R.id.clock);
    }

    @Override
    public void onResume(){
        super.onResume();

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, quitFilter);

        Utils.getDatetime();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    public void setDate(View v){
        if(isautomatic==0) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
            if(tvDate.getEditText()!=null) {
                tvDate.getEditText().requestFocusFromTouch();
                tvDate.getEditText().getBackground().setColorFilter(ContextCompat.getColor(DateTimeActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
            }
        }else{
            Toast.makeText(DateTimeActivity.this, getString(R.string.warning_date_auto), Toast.LENGTH_LONG).show();
        }
    }

    public void setTime(View v){
        if(isautomatic==0) {
            DialogFragment newFragment = new TimePickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
            newFragment.setCancelable(false);
            if(tvTime.getEditText()!=null) {
                tvTime.getEditText().requestFocusFromTouch();
                tvTime.getEditText().getBackground().setColorFilter(ContextCompat.getColor(DateTimeActivity.this, R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
            }
        }else{
            Toast.makeText(DateTimeActivity.this, getString(R.string.warning_date_auto), Toast.LENGTH_LONG).show();
        }
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()) {
                        case "4" :
                            Toast.makeText(DateTimeActivity.this, getString(R.string.error_get_datetime), Toast.LENGTH_LONG).show();
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "4" :

                            if(tvDate.getEditText()!=null)
                                tvDate.getEditText().setText(BTConnectionManagerService.getDate().getDate());

                            if(tvTime.getEditText()!=null)
                                tvTime.getEditText().setText(BTConnectionManagerService.getDate().getTime());

                            switch (BTConnectionManagerService.getDate().getAutomatic()){
                                case 0:
                                    switchView.setChecked(false);
                                    break;
                                case 1:
                                    switchView.setChecked(true);
                                    break;
                            }

                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            // If Date and Time retrieved correctly then create Time and Date dialogFragment
            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, year, month, day);

            dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which){

                }
            });

            dpd.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if(updated) {
                        if(tvDate.getEditText()!=null)
                            tvDate.getEditText().setText(day2 + " " + Utils.getMonthName(getActivity(), month) + " " + year);

                        Utils.setDatetime((byte)isautomatic, generateFormat(year, month, day, hours, minutes));
                    }
                }
            });

            return dpd;
        }

        private String day2 = "";
        private boolean updated = false;
        public void onDateSet(DatePicker view, int year1, int month1, int day1) {
            day2 = day1+"";
            if(day1<10)  day2 = "0"+day1;
            day   = day1;
            month = month1;
            year  = year1;
            updated = true;
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if(tvDate.getEditText()!=null)
                tvDate.getEditText().clearFocus();
            updated = false;
        }

        @Override
        public void onStart() {
            super.onStart();
        }
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            //Use the current time as the default values for the time picker
            //final Calendar c = Calendar.getInstance();
            //hours   = c.get(Calendar.HOUR_OF_DAY);
            //minutes = c.get(Calendar.MINUTE);

            //Create and return a new instance of TimePickerDialog
            TimePickerDialog tpd = new TimePickerDialog(getActivity(),this, hours, minutes,
                    android.text.format.DateFormat.is24HourFormat(getActivity()));

            tpd.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which){

                }
            });

            tpd.setButton(DialogInterface.BUTTON_POSITIVE, "Set", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Utils.setDatetime((byte)isautomatic, generateFormat(year, month, day, hours, minutes));
                }
            });

            return tpd;
        }

        //onTimeSet() callback method
        public void onTimeSet(TimePicker view, int hourOfDay, int minute){

            hours   = hourOfDay;
            minutes = minute;

            if(tvTime.getEditText()!=null)
                tvTime.getEditText().setText(hourOfDay + ":" + minute + ":00");
        }

        @Override
        public void onCancel(DialogInterface dialog){

        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if(tvTime.getEditText()!=null)
                tvTime.getEditText().clearFocus();
        }
    }

    private static String generateFormat(int year, int month, int day, int hours, int minutes){

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hours, minutes);
        return dateFormat.format(cal.getTime());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            this.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }
}
